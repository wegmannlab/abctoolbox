# ABCtoolbox 2.0 (Beta) #

We have invested a lot of work into a new version of ABCtoolbox, and  we are happy to release the current beta code. It is fully functional and we are currently not aware of any major bug. 

However, there are some differences in the usage of this compared to the published version. And we are seriously behind in updating the manual (but our [wiki](https://bitbucket.org/phaentu/abctoolbox-public/wiki/Home) starts to take shape!). So here is a short readme on how to get started with this new version.

1. We fused ABCsampler, ABCestimator and transformer into one program (called ABCtoolbox). You can obtain the source of this program as a [zip file](https://bitbucket.org/phaentu/abctoolbox-public/get/master.zip), or, alternatively, you may also clone this repository. Check out the [Downloads page](https://bitbucket.org/phaentu/abctoolbox-public/downloads) (see menu on left), for this.

2. Once you have a hand on the code, compile it from the source by first unzipping the code into an empty directory, entering that directory and typing  
**make**

3. To now tell ABCtoolbox what you want to do, you need to specify a "task", which is either "simulate", "estimate" or "transform". Provide the task just like any other argument: either in the input file or on the command line.

4. The est files no need a new tag at the end of each parameter or complex parameter analogous to fastsimcoal: either "hide" or "output". Only the columns with "output" will be printed to the output file.

5. You can now use estimator with multiple models to do model choice at once and you will get a nice output detailing model choice. Simply specify multiple *simName and params entries:  
simName mode1.txt;model2.txt;model3.txt  
params 1-10;2-6,7;1-8*  

6. There are automatic validation steps available, both for param estimation and model choice. To invoke, use the following tags and specify the number of pseudo-observed sims to be used (e.g. 1000):  
*randomValidation 1000  
retainedValidation 1000  
modelChoiceValidation 1000*

7. ABCtoolbox now checks for correlated stats and params, as very high correlations throw off the matrix calculations. Consider removing highly correlated statistics, for instance by performing a dimension reduction via PLS, or by pruning (add argument "pruneCorrelatedStats" to do this automatically). To modify the threshold for throwing the error, use maxCor followed by a value.

8. There is a greedy search to find stats for model choice. To invoke it, you will need an input file as follows:  
task findStatsModelChoice
simName mode1.txt;model2.txt  
params 2-10;2-8  
numRetained 1000  
maxReadSims 100000  
diracPeakWidth 0.01  
posteriorDensityPoints 100  
standardizeStats 0  
writeRetained 0  
outputPrefix ABC_findStatsModelChoice_
logFile ABC_searchStats.log  
modelChoiceValidation 1000  
maxCorSSFinder 0.95  
maxCor 2.0  
verbose*  
This algorithm is still work in progress, but we have successfully used it already. It will begin with all pairs of stats and evaluate the power to distinguish the models. Then, the best 10 pairs are retained. For each of those, all possible third statistics are added and the power is again computed for all those triplets. Then, again, the best 10 combinations are retained and all next possible stats are added. And so forth until the set of best combinations does not change anymore.  
The algorithm may be slow. To speed up, you can (try in this order) 1) read in less sims (e.g. 50,000 instead of 100,000), 2) use less sims to establish power (e.g. 500 instead of 1000 for modelChoiceValidation) or 3) retain less sims (e.g. 500 instead of 1000). All this changed come at the costs of less power.

9. You can now compute posterior distributions in multiple dimensions. To do so, you need to specify the following:  
*jointPosteriors PARAM1, PARAM2  
jointPosteriorDensityPoints 200*  
This will compute the joint posteriors of the parameters PARAM1 and PARAM2 (names as in est file) on a grid of 200 positions on each side (40,000 positions in 2 dimensions, 8,000,000 in 3 dimensions!!!). For high dimensions, check the MCMC option below.

10. You can now run an MCMC to sample from the posterior distribution. This is also helpful to infer multidimensional posterior distributions with more than 2 dimensions (see problem of too many grid points above)  
*jointSamplesMCMC 100000  
sampleMCMCStart jointmode  
sampleMCMCBurnin 100  
sampleMCMCRangeProp 1  
sampleMCMCSampling 5*  
These are tuning options, but those values should do fine.

11. I also integrated transformer into ABCtoolbox. It is invoked using the task "transform" and used with the arguments input, output, linearComb and boxcox.

12. Finally, the new version is considerably faster!

Have fun with it!