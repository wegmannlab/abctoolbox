/*
 * TApproxWF.cpp
 *
 *  Created on: Feb 12, 2014
 *      Author: wegmannd
 */

#include "TApproxWF.h"

//-------------------------------------------------------
//TTimePointWithSamples
//-------------------------------------------------------
TTimePointWithSamples::TTimePointWithSamples(int NumChromosomes, int NumA){
	numChromosomes=NumChromosomes;
	numA=NumA;
	numB=numChromosomes-numA;
	hasSamples = true;
}

void TTimePointWithSamples::simulateData(double & frequency, TRandomGenerator* RandomGenerator){
	numA = RandomGenerator->getBiomialRand(frequency, numChromosomes);
	numB=numChromosomes-numA;
}

double TTimePointWithSamples::getAlleleFrequency(){
	return (double) numA / (double) numChromosomes;
}

void TTimePointWithSamples::setNumA(int & NumA){
	numA = NumA;
	numB = numChromosomes-numA;
}
//------------------------------------------------------------------------------------------------
//TTimePointVector
//------------------------------------------------------------------------------------------------
TTimePointVector::TTimePointVector(){
	numTimePoints = 0;
	numTimepointsWithSamples = 0;
	interval = -1;
	pointersInitialized = false;
	pointerToTimepoints = NULL;
	pointerToTimepointsWithSamples = NULL;
	hasSomeSamples = false;
	firstTimepointWithSamples = 0;
	hasMutationTime = false;
	//statistics
	fsi = 0.0;
	fsd = 0.0;
	fsi_l = 0.0;
	fsd_l = 0.0;
	t_l = 0.0;
	t_h = 0.0;
}

void TTimePointVector::addTimepoint(int & TimePoint, std::string & Samples){
	if(timePoints.find(TimePoint)!=timePoints.end()) throw "Multiple entries for time point " + toString(TimePoint) + "!";
	//parse samples
	int size, numA;
	int pos = Samples.find_first_of('/');
	if(pos != std::string::npos){
		std::string tmp = Samples.substr(0,pos);
		numA = stringToInt(tmp);
		Samples.erase(0,pos+1);
		size = stringToInt(Samples);
		if(numA > size) throw "More selected alleles than chromosomes in '" + toString(numA) + "/" + toString(size) + "'!";
		if(size>0){
			timePoints.insert(std::pair<int, TTimePoint*>(TimePoint, new TTimePointWithSamples(size, numA)));
			++numTimePoints;
		}
	}
}

void TTimePointVector::addTimepoint(int & TimePoint, int & numA, int & size){
	if(timePoints.find(TimePoint)!=timePoints.end()) throw "Multiple entries for time point " + toString(TimePoint) + "!";
	if(numA > size) throw "More selected alleles than chromosomes in '" + toString(numA) + "/" + toString(size) + "'!";
	if(size>0){
		timePoints.insert(std::pair<int, TTimePoint*>(TimePoint, new TTimePointWithSamples(size, numA)));
		++numTimePoints;
	}
}

double TTimePointVector::estimateInitialFreq(){
	//estimate the frequency at the first time point from the first time point with data
	for(it=timePoints.begin(); it!=timePoints.end(); ++it){
		if(it->second->hasSamples){
			double f = it->second->getAlleleFrequency();
			return f;
		}
	}
	return -1;
}

void TTimePointVector::initDeltaT(){
	if(pointersInitialized){
		delete[] pointerToTimepoints;
		delete[] pointerToTimepointsWithSamples;
	}
	//fill vector of pointers in chronological order and calculate deltaT
	numTimePoints = timePoints.size();
	pointerToTimepoints = new TTimePoint*[numTimePoints];
	pointersInitialized=true;
	int i=0;
	int oldTime = timePoints.begin()->first-1;
	numTimepointsWithSamples = 0;
	for(it=timePoints.begin(); it!=timePoints.end(); ++it, ++i){
		pointerToTimepoints[i] = it->second;
		it->second->deltaT = it->first-oldTime;
		oldTime = it->first;
		if(it->second->hasSamples) ++numTimepointsWithSamples;
	}
	//fill pointer to those with samples
	pointerToTimepointsWithSamples= new TTimePoint*[numTimepointsWithSamples];
	i = 0;
	for(it=timePoints.begin(); it!=timePoints.end(); ++it){
		if(it->second->hasSamples){
			pointerToTimepointsWithSamples[i] = it->second;
			++i;
		}
	}

	//find first time point with samples
	for(it=timePoints.begin(); it!=timePoints.end(); ++it){
		if(it->second->hasSamples){
			firstTimepointWithSamples = it->first;
			hasSomeSamples = true;
			break;
		}
	}
}

void TTimePointVector::currentSimulateData(double frequency, TRandomGenerator* RandomGenerator){
	it->second->simulateData(frequency, RandomGenerator);
}

void TTimePointVector::currentSetData(int numA){
	it->second->setNumA(numA);
}

int TTimePointVector::currentGetNumA(){
	return it->second->getNumA();
}

bool TTimePointVector::calcStats(TStatSettings & settings){
	if(!hasSomeSamples) return false;
	double x_fs, y_fs, z_fs, fsp, nt_fs;
	unsigned int ngen_fs,ngen_fs_l;
	ngen_fs = 0.0;
	ngen_fs_l = 0.0;
	fsi = 0.0;
	fsd = 0.0;
	fsi_l = 0.0;
	fsd_l = 0.0;
	t_l = 0.0;
	t_h = 0.0;

	//find last not fixed / not extinct
	int firstNotfixed = -1;
	int lastNotFixed = 0;
	for(int i = 0; i < numTimepointsWithSamples; ++i){
		x_fs = pointerToTimepointsWithSamples[i]->getAlleleFrequency();
		if(x_fs>0.0 && x_fs<1.0) lastNotFixed = i;
		if(firstNotfixed < 0 && x_fs>0.0 && x_fs<1.0) firstNotfixed = i;
	}

	//go through all pairs of time points
	for(int i = 1; i < numTimepointsWithSamples; ++i){
		x_fs = pointerToTimepointsWithSamples[i-1]->getAlleleFrequency();
		y_fs = pointerToTimepointsWithSamples[i]->getAlleleFrequency();

		if (!(((x_fs < settings.min_freq_fs) & (y_fs < settings.min_freq_fs)) || ((x_fs > settings.max_freq_fs) & (y_fs > settings.max_freq_fs)))) {
			z_fs = (x_fs + y_fs) / 2.0;
			fsp = (x_fs - y_fs) * (x_fs - y_fs) / (z_fs * (1.0 - z_fs));
			nt_fs = 2.0 / (1.0 / (double) (pointerToTimepointsWithSamples[i-1]->numChromosomes) + 1.0 / (double) (pointerToTimepointsWithSamples[i]->numChromosomes));
			fsp = ((fsp*(1.0 - 1.0 / (2.0 * nt_fs)) - 2.0 / nt_fs) / ((1.0 + fsp / 4.0)*(1.0 - 1.0 / ((double) (pointerToTimepointsWithSamples[i]->numChromosomes)))));

			// calculate the total number of generation where we could calculate Fs'
			if ((x_fs < settings.fs_threshold_l && y_fs < settings.fs_threshold_l) || (x_fs > settings.fs_threshold_h && y_fs > settings.fs_threshold_h)){
				ngen_fs_l += pointerToTimepointsWithSamples[i]->deltaT;
				if (x_fs < y_fs)
					fsi_l += fsp;
				else //if (x_fs > y_fs)
					fsd_l += fsp;
			} else {
				ngen_fs += pointerToTimepointsWithSamples[i]->deltaT;
				if (x_fs < y_fs)
					fsi += fsp;
				else //if (x_fs > y_fs)
					fsd += fsp;
			}
		}
		//calculate tl and th
		if(i >= firstNotfixed && i <= lastNotFixed){
			if(y_fs < settings.freq_threshold_l){
				if(x_fs < settings.freq_threshold_l) t_l += pointerToTimepointsWithSamples[i]->deltaT;
				else t_l += pointerToTimepointsWithSamples[i]->deltaT * ((settings.freq_threshold_l - y_fs)/(x_fs - y_fs)); //interpolate
			} else if(y_fs > settings.freq_threshold_h){
				if(x_fs > settings.freq_threshold_h) t_h += pointerToTimepointsWithSamples[i]->deltaT;
				else t_h += pointerToTimepointsWithSamples[i]->deltaT * ((y_fs - settings.freq_threshold_h)/(y_fs - x_fs)); //interpolate
			} else if(x_fs < settings.freq_threshold_l) t_l += pointerToTimepointsWithSamples[i]->deltaT * ((settings.freq_threshold_l - x_fs)/(y_fs - x_fs)); //interpolate
			else if(x_fs > settings.freq_threshold_h) t_h += pointerToTimepointsWithSamples[i]->deltaT * ((x_fs - settings.freq_threshold_h)/(x_fs - y_fs)); //interpolate
		}
	}
	if(ngen_fs == 0 && ngen_fs_l == 0) return false;
	if (ngen_fs > 0) {
		fsi = fsi / ngen_fs;
		fsd = fsd / ngen_fs;
	}
	if (ngen_fs_l > 0) {
		fsi_l = fsi_l / ngen_fs_l;
		fsd_l = fsd_l / ngen_fs_l;
	}
	return true;
}

void TTimePointVector::fillStats(std::vector<double> & statContainer){
	statContainer.clear();
	statContainer.push_back(fsi);
	statContainer.push_back(fsd);
	statContainer.push_back(fsi_l);
	statContainer.push_back(fsd_l);
	statContainer.push_back(t_l);
	statContainer.push_back(t_h);
}
void TTimePointVector::fillStats(double* statContainer){
	statContainer[0] = fsi;
	statContainer[1] = fsd;
	statContainer[2] = fsi_l;
	statContainer[3] = fsd_l;
	statContainer[4] = t_l;
	statContainer[5] = t_h;
}
void TTimePointVector::fillStatHeader(std::vector<std::string> & headerContainer){
	headerContainer.clear();
	headerContainer.push_back("fsi");
	headerContainer.push_back("fsd");
	headerContainer.push_back("fsi_l");
	headerContainer.push_back("fsd_l");
	headerContainer.push_back("t_l");
	headerContainer.push_back("t_h");
}

void TTimePointVector::writeStatsToFile(std::ofstream & out){
	out << "fsi\tfsd\tfsi_l\tfsd_l\tt_l\tt_h" << std::endl;
	out << fsi << "\t" << fsd << "\t" << fsi_l << "\t" << fsd_l << "\t" << t_l << "\t" << t_h << std::endl;
}

int TTimePointVector::numWithDataAboveFreq(double & freq){
	int count = 0;
	for(int i = 0; i < numTimepointsWithSamples; ++i){
		if(pointerToTimepointsWithSamples[i]->getAlleleFrequency() >= freq)
			++count;
	}
	return count;
}
//-------------------------------------------------------
//Tlocus
//-------------------------------------------------------
TLocus::TLocus(std::string Name, TRandomGenerator* RandomGenerator) :
randomGenerator(RandomGenerator),
curLL(0.0),
oldLL(0.0),
transMatInitialized(false),
HMMInitialized(false),
s_cur(0.0),
h_cur(0.5),
s_old(0.0),
h_old(0.5),
tau_cur(0.0),
tau_old(0.0),
twoN(0.0),
priorDens_cur(0.0),
priorDens_old(0.0),
radomForHastings(0.0),
initialFreqEstimate(0.0),
cumulProbInitialFreq(NULL),
nFreqStates(1000),
name(Name),
timePoints(){}

void TLocus::addTimepoint(int & TimePoint, std::string & Samples){
	timePoints.addTimepoint(TimePoint, Samples);
}
void TLocus::addTimepoint(int & TimePoint, int & numA, int & size){
	timePoints.addTimepoint(TimePoint, numA, size);
}
void TLocus::setWFParams(int & TwoN, double & s, double & h){
	twoN = TwoN;
	s_cur = s;
	h_cur = h;
}
void TLocus::initializeTimepoints(){
	timePoints.initDeltaT();
	if(!timePoints.hasSamples()) throw "Locus '" + name + "' has no time point with samples!";
	initialFreqEstimate = timePoints.estimateInitialFreq();
	//if(initialFreqEstimate<0) throw "No time point with any alternative alleles at locus '" + name + "'!";

	//fill vector to sample initial frequencies from

	TTimePoint* tp = timePoints.getFirstTimepointWithSamples();

	cumulProbInitialFreq = new double[nFreqStates];
	double f;
	double numA = tp->getNumA();
	double numB = tp->numChromosomes - tp->getNumA();
	cumulProbInitialFreq[0] = 0.0;
	for(int i=1; i<nFreqStates; ++i){
		f = (double) i / (double) nFreqStates;
		cumulProbInitialFreq[i] = cumulProbInitialFreq[i-1] + pow(f, numA) * pow(1.0-f, numB);
	}
	for(int i=0; i<nFreqStates; ++i) cumulProbInitialFreq[i] /= cumulProbInitialFreq[nFreqStates-1];
}

void TLocus::simulateDataWF(double & initialFreq, int & t_sel, double & epsilon, double & ascertainmentFreq){
	//prepare simulation
	double onePlusS = (1.0 + s_cur);
	double onePlusSH = 1.0 + s_cur * h_cur;
	double oneMinusf, homoNeutral, het, homoSelected, pSucess;
	double twoNDouble = twoN;
	double fWithErrorForSim;
	double f;

	//simulate until ascertainment is passed
	bool passedAscertainment = false;
	while(!passedAscertainment){
		//go through all time points
		timePoints.beginInteration();

		//set starting frequency
		if(initialFreq < 0.0){
			//sample starting frequency from dist
			double tmp = randomGenerator->getRand();
			f = 1.0;
			for(int i=0; i<nFreqStates; ++i){
				if(tmp < cumulProbInitialFreq[i]){
					f = (double) i / (double) nFreqStates;
					break;
				}
			}
		} else f = initialFreq;

		//simulate data at first time point
		timePoints.currentSimulateData(f, randomGenerator);

		//run simulation
		int totalGeneration = timePoints.getStartingTime();
		int nextTimepoint;

		while(timePoints.nextCheck()){
			//iterate generation by generation to next time point
			nextTimepoint = totalGeneration + timePoints.currentDeltaT();
			for(; totalGeneration<nextTimepoint; ++totalGeneration){
				if(totalGeneration < t_sel){
					pSucess = f;
				} else {
					oneMinusf = 1.0 - f;
					homoNeutral = oneMinusf * oneMinusf;
					homoSelected = onePlusS * f * f;
					het = onePlusSH * f * oneMinusf;
					pSucess = (homoSelected + het) / (homoNeutral + 2*het + homoSelected);
				}
				f = randomGenerator->getBiomialRand(pSucess, twoN) / twoNDouble;
				if(f == 0.0 || f == 1.0) break;
			}
			//simulate data
			fWithErrorForSim = f*(1.0 - epsilon) + (1.0 - f)*epsilon;
			if(f == 0.0 || f == 1.0){
				do{
					timePoints.currentSimulateData(fWithErrorForSim, randomGenerator);
				} while(timePoints.nextCheck());
				break;
			} else {
				timePoints.currentSimulateData(fWithErrorForSim, randomGenerator);
			}
		}

		//check acertainment
		if(ascertainmentFreq <= 0.0 || timePoints.numWithDataAboveFreq(ascertainmentFreq) > 1)
			passedAscertainment = true;
	}
}

bool TLocus::calculateStats(TStatSettings & settings){
	return timePoints.calcStats(settings);
}
void TLocus::fillStats(std::vector<double> & statContainer){
	timePoints.fillStats(statContainer);
}
void TLocus::fillStats(double* statContainer){
	timePoints.fillStats(statContainer);
}


void TLocus::fillStatHeader(std::vector<std::string> & headerContainer){
	timePoints.fillStatHeader(headerContainer);
}

void TLocus::writeStatsToFile(std::ofstream & out){
	timePoints.writeStatsToFile(out);
}
//---------------------------------------------------------------------------------------------------
//TApproxWF
//---------------------------------------------------------------------------------------------------
TApproxWF::TApproxWF(std::string* passedParams, int numPassedParams, TRandomGenerator* RandomGenerator){
	if(numPassedParams != 8 && numPassedParams != 9) throw "INTERNALWF: Requires 9 or 10 parameters on the command line: file_with_loci.txt output.txt N s h f t_sel epsilon ascertainment-frequency (default: 0)!";

	randomGenerator = RandomGenerator;

	//first argument is name of file with locus definition
	std::ifstream input(passedParams[0].c_str());
	if(!input) throw "INTERNALWF: File " + passedParams[0] + " could not be opened!";

	std::string line;
	std::vector<std::string> vec;
	int parsedTime;
	int num=0;
	bool first=true;

	while(input.good() && !input.eof()){
		++num;
		std::getline(input, line);
		line=extractBefore(line, "//");
		if(!line.empty()){
			fillVectorFromStringWhiteSpaceSkipEmpty(line, vec);
			if(first){
				//read header and construct loci
				if(vec.size() != 2) throw "INTERNALWF: Wrong number of columns in file '" + passedParams[0] + "'!";
				locus = new TLocus(vec[1], randomGenerator);
				first = false;
			} else {
				//add time points
				if(vec.size() != 2) throw "Wrong number of entries in file " + passedParams[0] + " on line " + toString(num) + " (expecting 2 instead of " + toString(vec.size()) + ")!";
				//parse time
				parsedTime = stringToInt(vec[0]);
				allTimePoints.push_back(parsedTime);
				//add samples
				locus->addTimepoint(parsedTime, vec[1]);
			}
		}
	}
	input.close();
	std::sort(allTimePoints.begin(), allTimePoints.end());
	if(allTimePoints.size()<2) throw "INTERNALWF: Less than 2 time points have been specified";

	locus->initializeTimepoints();

	//second argument is output file name
	outputname = passedParams[1];

}

void TApproxWF::simulate(std::string* passedParams, int & numPassedParams){
	if(numPassedParams != 8 && numPassedParams != 9) throw "INTERNALWF: Requires 9 or 10 parameters on the command line: file_with_loci.txt output.txt N s h f t_sel epsilon ascertainment-frequency (default: 0)!";

	//initialize WF parameters
	int twoN = 2*stringToInt(passedParams[2]);
	if(twoN < 5) throw "Can not run Wright-Fisher simulation with 2N < 5!";
	double s = stringToDouble(passedParams[3]);
	if(s < -1.0 || s > 1.0) throw "Can not run Wright-Fisher simulation with s < -1.0 or s > 1.0!";
	double h = stringToDouble(passedParams[4]);
	if(h < 0.0 || h > 1.0) throw "Can not run Wright-Fisher simulation with h < 0.0 or h > 1.0!";
	double f;
	if(passedParams[5] == "-") f = -1.0;
	else {
		f = stringToDouble(passedParams[5]);
		if(f < 0.0 || f > 1.0) throw "Can not run Wright-Fisher simulation with f < 0.0 or f > 1.0!";
	}
	int t_sel = stringToDouble(passedParams[6]);
	double epsilon = stringToDouble(passedParams[7]);
	if(epsilon < 0.0 || epsilon > 0.5) throw "Can not run Wright-Fisher simulation with epsilon < 0.0 or epsilon > 0.5!";
	locus->setWFParams(twoN, s, h);

	//acertainment frequency
	double ascertainmentFreq = 0.0;
	if(numPassedParams == 9){
		ascertainmentFreq = stringToDouble(passedParams[8]);
		if(ascertainmentFreq < 0.0 || ascertainmentFreq > 1.0)
			throw "Can not run Wright-Fisher simulation with an ascertainment frequency < 0.0 or > 1.0!";
	}

	//run simulation. Note: we replicate an acertainment bias: only simulations where the stats are different from zero are accepted!
	locus->simulateDataWF(f, t_sel, epsilon, ascertainmentFreq);
	locus->calculateStats(statSettings);
}

int TApproxWF::executeAndWrite(std::string* passedParams, int numPassedParams){
	simulate(passedParams, numPassedParams);
	std::ofstream out(outputname.c_str());
	locus->writeStatsToFile(out);
	out.close();
	return 0;
}

int TApproxWF::execute(std::string* passedParams, int numPassedParams){
	simulate(passedParams, numPassedParams);
	return 0;
}

void TApproxWF::fillHeaderNames(std::vector<std::string> & nameContainer){
	locus->fillStatHeader(nameContainer);
}
void TApproxWF::fillValues(std::vector<double> & valueContainer){
	locus->fillStats(valueContainer);
}
void TApproxWF::fillValues(double* valueContainer){
	locus->fillStats(valueContainer);
}









