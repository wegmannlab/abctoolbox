/*
 * TApproxWF.h
 *
 *  Created on: Feb 12, 2014
 *      Author: wegmannd
 */

#ifndef TAPPROXWF_H_
#define TAPPROXWF_H_

#include "TRandomGenerator.h"
#include "stringFunctions.h"
#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include "TExecuteProgram.h"
#include <map>
#include <vector>
#include <algorithm>

class TStatSettings{
public:
	double min_freq_fs;
	double max_freq_fs;
	double fs_threshold_l;
	double fs_threshold_h;
	double freq_threshold_l;
	double freq_threshold_h;

	TStatSettings(){
		min_freq_fs = 0.001;
		max_freq_fs = 1.0 - min_freq_fs;
		fs_threshold_l = 0.001;
		fs_threshold_h = 1.0 - fs_threshold_l;
		freq_threshold_l = 0.01;
		freq_threshold_h = 1.0 - freq_threshold_l;
	};
	void initialize(double & Min_freq_fs, double & Fs_threshold, double & Freq_threshold_l){
		min_freq_fs = Min_freq_fs;
		max_freq_fs = 1.0 - min_freq_fs;
		fs_threshold_l = Fs_threshold;
		fs_threshold_h = 1.0 - fs_threshold_l;
		freq_threshold_l = Freq_threshold_l;
		freq_threshold_h = 1.0 - freq_threshold_l;
	};
};


class TTimePoint{
public:
	bool hasSamples;
	double one;
	int deltaT;
	int numChromosomes;

	TTimePoint(){hasSamples=false;one=1.0;deltaT=-1;numChromosomes=0;};
	virtual ~TTimePoint(){};
	virtual std::string getString(){ return "NA"; }
	virtual bool hasSelectedSaples(){return false;};
	virtual void simulateData(double & frequency, TRandomGenerator* RandomGenerator){};
	virtual double getAlleleFrequency(){ throw "Unable to calculate allele frequency at a time point without samples!";};
	virtual int getNumA(){return 0;};
	virtual void setNumA(int & numA){};
};

class TTimePointWithSamples:public TTimePoint{
public:
	int numA;
	int numB;

	TTimePointWithSamples(int NumChromosomes, int NumA);
	~TTimePointWithSamples(){}

	std::string getString(){  return toString(numA) + "/" + toString(numChromosomes); };
	bool hasSelectedSaples(){ if(numA>0) return true; else return false;};
	void simulateData(double & frequency, TRandomGenerator* RandomGenerator);
	double getAlleleFrequency();
	int getNumA(){return numA;};
	void setNumA(int & NumA);
};


class TTimePointVector{
private:
	std::map<int,TTimePoint*> timePoints;
	TTimePoint** pointerToTimepoints;
	TTimePoint** pointerToTimepointsWithSamples;
	bool pointersInitialized;
	int interval;
	int firstTimepointWithSamples;
	bool hasSomeSamples;
	bool hasMutationTime;
	//iterator
	std::map<int, TTimePoint*>::iterator it;
	std::map<int, TTimePoint*>::reverse_iterator rIt;
	//statistics
	double fsi, fsd, fsi_l, fsd_l, t_h, t_l;

public:
	int numTimePoints;
	int numTimepointsWithSamples;

	TTimePointVector();
	~TTimePointVector(){
		for(it=timePoints.begin(); it!=timePoints.end(); ++it) delete it->second;
		if(pointersInitialized){
			delete[] pointerToTimepoints;
			delete[] pointerToTimepointsWithSamples;
		}
	};
	//void setInterval(int MutTime, int Interval);
	//void update(int MutTime);
	void addTimepoint(int & TimePoint, std::string & Samples);
	void addTimepoint(int & TimePoint, int & numA, int & size);
	double estimateInitialFreq();
	void initDeltaT();
	int getStartingTime(){return timePoints.begin()->first;};
	TTimePoint* getFirstTimepointWithSamples(){return pointerToTimepointsWithSamples[0];};
	bool& hasSamples(){return hasSomeSamples;};
	void beginInteration(){it=timePoints.begin();};
	bool nextCheck(){++it; if(it==timePoints.end()) return false; else return true;};
	int& currentDeltaT(){return it->second->deltaT;};
	bool& currentHasSamples(){return it->second->hasSamples;}
	void currentSimulateData(double frequency, TRandomGenerator* RandomGenerator);
	void currentSetData(int numA);
	int currentGetNumA();
	bool calcStats(TStatSettings & settings);
	void fillStats(std::vector<double> & statContainer);
	void fillStats(double* statContainer);
	void fillStatHeader(std::vector<std::string> & headerContainer);
	void writeStatsToFile(std::ofstream & out);
	int numWithDataAboveFreq(double & freq);
};

class TLocus{
private:
	TRandomGenerator* randomGenerator;
	double curLL, oldLL;
	bool transMatInitialized, HMMInitialized;
	double s_cur, h_cur, s_old, h_old;
	int tau_cur, tau_old;
	int twoN;
	double priorDens_cur, priorDens_old, radomForHastings;
	double initialFreqEstimate;
	double* cumulProbInitialFreq;
	int nFreqStates;

public:
	std::string name;
	TTimePointVector timePoints;

	TLocus(std::string Name, TRandomGenerator* RandomGenerator);
	~TLocus(){};

	void addTimepoint(int & TimePoint, std::string & Samples);
	void addTimepoint(int & TimePoint, int & numA, int & size);
	void setWFParams(int & TwoN, double & s, double & h);
	void initializeTimepoints();
	void simulateDataWF(double & initialFreq, int & t_sel, double & epsilon, double & ascertainmentFreq);
	bool calculateStats(TStatSettings & settings);
	void fillStats(std::vector<double> & statContainer);
	void fillStats(double* statContainer);
	void fillStatHeader(std::vector<std::string> & headerContainer);
	void writeStatsToFile(std::ofstream & out);
};

class TApproxWF:public TExecuteProgram{
public:
	TRandomGenerator* randomGenerator;
	TLocus* locus;
	std::vector<int> allTimePoints;
	TStatSettings statSettings;
	std::string outputname;

	TApproxWF(std::string* passedParams, int numPassedParams, TRandomGenerator* RandomGenerator);
	~TApproxWF(){
		delete locus;
	};
	void simulate(std::string* passedParams, int & numPassedParams);
	int executeAndWrite(std::string* passedParams, int numPassedParams);
	int execute(std::string* passedParams, int numPassedParams);
	void fillHeaderNames(std::vector<std::string> & nameContainer);
	void fillValues(std::vector<double> & valueContainer);
	void fillValues(double* valueContainer);
};



#endif /* TAPPROXWF_H_ */
