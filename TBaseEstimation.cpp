//---------------------------------------------------------------------------


#include "TBaseEstimation.h"
//---------------------------------------------------------------------------
TJointPosteriorSettings::TJointPosteriorSettings(TParameters* parameters, TLog* LogFile){
	//TODO: write summary to logfile
	//compute joint posteriors?
	if(parameters->parameterExists("jointPosteriors")){
		doJointPosterior = true;

		if(parameters->parameterExists("noGridDensity")){
			calcGridDensity = false;
			jointPosteriorDensityFromMCMC = false;
			jointPosteriorDensityPoints = 0;
		} else {
			calcGridDensity = true;

			jointPosteriorDensityPoints = parameters->getParameterDoubleWithDefault("jointPosteriorDensityPoints", 100);
			if(jointPosteriorDensityPoints < 2){
				LogFile->warning("'jointPosteriorDensityPoints' has to be set to 2 or more! Using a value of 2 ...");
				jointPosteriorDensityPoints = 2;
			}

			//how to get the points at which the densities will be computed?
			jointPosteriorDensityPointsFromMarginals=(parameters->getParameterDouble("jointPosteriorDensityPointsFromMarginals", false) != 0);
			jointPosteriorDensityFromMCMC = parameters->parameterExists("jointPosteriorDensityFromMCMC");
		}


		//generate posterior samples?
		numApproxJointSamples = parameters->getParameterIntWithDefault("jointSamplesApprox", 0);
		if(numApproxJointSamples > 0 && calcGridDensity == false){
			throw "Can not produce approximate joint posterior samples without calculating the posterior density on a grid!";
		}

		//sample via MCMC?
		numMCMCJointSamples=parameters->getParameterDouble("jointSamplesMCMC", false);
		if(jointPosteriorDensityFromMCMC && numMCMCJointSamples < 100) throw "At last 100 MCMC samples have to be generated to compute joint posterior densities via MCMC!";
		if(numMCMCJointSamples > 0){
			//how many samples?
			numMCMCJointSamples=parameters->getParameterDouble("jointSamplesMCMC", false);
			//burnin given or default?
			if(parameters->parameterExists("sampleMCMCBurnin")){
			   sampleMCMCBurnin=parameters->getParameterDouble("sampleMCMCBurnin", false);
			} else sampleMCMCBurnin=100;
			//rangeprop given or default?
			if(parameters->parameterExists("sampleMCMCRangeProp")){
			   sampleMCMCRangeProp=parameters->getParameterDouble("sampleMCMCRangeProp", false);
			} else sampleMCMCRangeProp=0.1;
			//sampling given or default?
			if(parameters->parameterExists("sampleMCMCSampling")){
			   sampleMCMCSampling=parameters->getParameterDouble("sampleMCMCSampling", false);
			} else sampleMCMCSampling=10;
			//where to start?
			if(parameters->parameterExists("sampleMCMCStart")){
			   std::string temp=parameters->getParameterString("sampleMCMCStart");
			   if(temp == "random"){
				   sampleMCMCStart = randomStart;
			   }
			   else if(temp == "jointmode"){
				   sampleMCMCStart = jointmodeStart;
			   }
			   else if(temp == "jointposterior"){
				   sampleMCMCStart = jointposteriorStart;
			   }
			   else if(temp == "marginal"){
				   sampleMCMCStart = marginalStart;
			   }
			   else {
				   throw "'" + temp + "' is an unknown tag used for 'sampleMCMCStart'!";
			   }

				if((sampleMCMCStart == jointmodeStart || sampleMCMCStart == jointposteriorStart) && calcGridDensity == false){
					throw "Can not start MCMC at mode or mean of approx joint posterior without calculating the posterior density on a grid!";
				}

		   }
		} else sampleMCMCStart=jointmodeStart;
	} else doJointPosterior = false;
}

//---------------------------------------------------------------------------
//TRootEstimation: an object from which the base estimation is derived, but also TModelaveragingEstimation
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//TBaseEstimation
//---------------------------------------------------------------------------
TBaseEstimation::TBaseEstimation(TSimData* simData, ColumnVector* Theta, double DiracPeakWidth, bool ComputeTruncatedPrior, std::string OutputPrefix, TJointPosteriorSettings* JointPosteriorSettings, TRandomGenerator* RandomGenerator, TLog* LogFile){
	mySimData=simData;
	outputPrefix=OutputPrefix;
	myJointPosteriorSettings=JointPosteriorSettings;
	marDens=0;
	marDensPValue=0;
	tukeyDepth=0;
	tukeyDepthPValue=0;
	logfile=LogFile;
	randomGenerator = RandomGenerator;

	//initialize status
    exponentsInitialized = false;
    exponentsSize = 0;
    trueParamValidationFileOpen=false;
    computeSmoothedTruncatedPrior=ComputeTruncatedPrior;

    //prepare estimation
    posteriorMatrix = new TPosteriorMatrix(Theta, simData->paramRanges);
    diracPeakWidth=DiracPeakWidth;
    prepareDiracPeaks(); //also used for writing the prior!

    //open output files used for all observed data sets
    openPosteriorCharacteristicsFile();
}

//---------------------------------------------------------------------------
void TBaseEstimation::fillExponents_Q_j(){
	//first calculate all Qj in order to shift them -> otherwise maybe too large numbers...
	//the discrepancy is constant and therefore integrated out by normArea
	//loop over all dirac peaks
	if(exponentsInitialized && mySimData->numUsedSims != exponentsSize){
		delete[] exponents;
		exponentsInitialized = false;
	}

	if(!exponentsInitialized){
		exponents = new double[mySimData->numUsedSims];
		exponentsSize = mySimData->numUsedSims;
		exponentsInitialized = true;
	}

	double meanExponent = 0;
	ColumnVector theta_j, v_j;
	for(int d=1; d<=mySimData->numUsedSims;++d){
		//theta_j is a vector with the parameters of one simulation / from the uniform matrix
		theta_j = mySimData->paramMatrix.submatrix(d,d,2,mySimData->numParams+1).as_column();
		v_j = calculate_v_j(theta_j);
		//removed (obsValues-c_zero).t()*SigmaSInv*(obsValues-c_zero) from the exponent since it is constant -> corrected by normArea
		Matrix exponent = theta_j.t()*SigmaThetaInv*theta_j-v_j.t()*T_j*v_j;
		exponents[d-1] = -0.5*exponent(1,1);
		meanExponent += exponents[d-1];
	}

	//shift the Qj
	meanExponent = meanExponent / mySimData->numUsedSims;
	for(int d=0; d<mySimData->numUsedSims;++d) exponents[d]=exponents[d]-meanExponent;
}

//---------------------------------------------------------------------------
bool TBaseEstimation::calculatePosterior(bool verbose){
	if(verbose)  logfile->listFlush("Preparing posterior density calculation ...");
	//prepare posterior Matrix
	posteriorMatrix->initToZero();
	if(!calculate_T_j(T_j)) return false;
	fillExponents_Q_j();
	if(verbose) logfile->write(" done!");

	//now loop over all parameters
	if(verbose) logfile->listFlush("Calculating marginal posterior densities for ", mySimData->numParams, " parameters ...");
	//now calculate the marginal posterior densities
	double c_j;
	double tmpDens;
	ColumnVector theta_j, v_j;
	for(int p=1; p<=mySimData->numParams;++p){
		double tau = T_j(p,p);
		//loop over all dirac peaks
		for(int d=1; d<=mySimData->numUsedSims;++d){
			//theta_j is a vector with the parameters of one simulation / from the uniform matrix
			theta_j = mySimData->paramMatrix.submatrix(d,d,2,mySimData->numParams+1).as_column();
			v_j = calculate_v_j(theta_j);
			c_j = exp(exponents[d-1]);
			ColumnVector t_j = T_j * v_j;
			for(int k=1;k<=posteriorMatrix->posteriorDensityPoints;++k){

				tmpDens = c_j * exp(-pow((*posteriorMatrix->theta)(k)-t_j(p), 2.) / (2.*tau));

				posteriorMatrix->addToPosteriorMatrixDensity(k, p, tmpDens);
			}
		}
	}
	posteriorMatrix->calcMarginalAreas();
	if(verbose) logfile->write(" done!");
	return true;
}
//---------------------------------------------------------------------------
bool TBaseEstimation::calculate_T_j(SymmetricMatrix & m){
	m << CHat.t() * SigmaSInv * CHat + SigmaThetaInv;
	makeInvertable(m);
	double det=m.Determinant();
	if(det==0){
		logfile->warning("Problem inverting Tau_J: determinant = 0!");
		return false;
	}
	try{
		m=m.i();
	} catch(...){
		logfile->warning("Problem inverting Tau_J: determinant = 0!");
		return false;
	}
	return true;
}
//---------------------------------------------------------------------------
ColumnVector TBaseEstimation::calculate_v_j(ColumnVector theta_j){
	return CHat.t()*SigmaSInv*(obsValues-c_zero)+SigmaThetaInv*theta_j;
}
//---------------------------------------------------------------------------
bool TBaseEstimation::performLinearRegression(bool verbose){
	//perform regression
	if(verbose) logfile->listFlush("Performing local linear regression ...");
	C = mySimData->paramMatrix.t() * mySimData->paramMatrix;
	try {
		C = C.i();
	} catch(...){
		logfile->warning("Problems computing inverse of C!");
		std::cout << C << std::endl;
		return false;
	}

	C = mySimData->paramMatrix * C;
	C = mySimData->statMatrix * C;
	c_zero = C.column(1);
	CHat = C.columns(2,C.ncols());
	RHat = mySimData->statMatrix.t()-mySimData->paramMatrix*C.t();
	Matrix SigmaS_temp=RHat.t()*RHat;
	SigmaS_temp=1.0/(mySimData->numUsedSims-mySimData->numParams)*RHat.t()*RHat;
	fillInvertable(SigmaS_temp, SigmaS);

	double det=0;
	try {
		det=SigmaS.Determinant();
	} catch(...){
		logfile->warning("Problems computing determinant of Sigma_S!");
		return false;
	}
	if(det<=0.0){
		logfile->warning("Problems inverting Sigma_S: matrix seems singular!");
		return false;
	}
	try {
		SigmaSInv=SigmaS.i();
	} catch(...){
		logfile->warning("Problems inverting Sigma_S: matrix seems singular!");
		return false;
	}
	if(verbose) logfile->write(" done!");
	return true;
}
//---------------------------------------------------------------------------
void TBaseEstimation::prepareDiracPeaks(){
	//approximation of priors with Dirac peaks
	DiagonalMatrix diracPeakWidthMatrix(mySimData->numParams);
	diracPeakWidthMatrix = diracPeakWidth;
	//SigmaTheta=pow(1./numToRetain, 2./mySimData->numParams)*diracPeakWidthMatrix;
	SigmaTheta = diracPeakWidthMatrix;
	SigmaThetaInv = SigmaTheta.i();
}
//---------------------------------------------------------------------------
void TBaseEstimation::openPosteriorCharacteristicsFile(){
	std::string filename=outputPrefix+"MarginalPosteriorCharacteristics.txt";
	posteriorCharacteristicsFile.open(filename.c_str());
	posteriorMatrix->writePosteriorCharacteristicsHeader(posteriorCharacteristicsFile, computeSmoothedTruncatedPrior);
}

void TBaseEstimation::writePosteriorCharacteristics(const int & obsDataSetNum){
	//write  posterior characteristics, such as mean, mode etc.
	logfile->listFlush("Writing posterior characteristics ...");
	if(computeSmoothedTruncatedPrior) posteriorMatrix->writePosteriorCharacteristics(posteriorCharacteristicsFile, obsDataSetNum, retainedMatrix);
	else posteriorMatrix->writePosteriorCharacteristics(posteriorCharacteristicsFile, obsDataSetNum);
	logfile->write(" done!");
}
//---------------------------------------------------------------------------
Matrix* TBaseEstimation::getPointerToMarginalPosteriorDensityMatrix(){
	return posteriorMatrix->getPointerToMarginalPosteriorDensityMatrix();
}
int TBaseEstimation::getParamColumnFromName(std::string name){
	return mySimData->getParamNumberFromName(name);
}
//---------------------------------------------------------------------------
void TBaseEstimation::writeTruncatedPrior(std::string filenameTag){
	logfile->listFlush("Writing file with marginal densities of the truncated prior ...");
	//calculate normalization area
	ColumnVector areas;
	posteriorMatrix->calcAreasParamScale(retainedMatrix, areas);
	//write to file
	std::ofstream output;
	std::string filename=outputPrefix + "TruncatedPrior";
	filename+= filenameTag;
	filename+=".txt";
	output.open(filename.c_str());
	posteriorMatrix->writeDensities(output, retainedMatrix, areas);
	output.close();
	logfile->write(" done!");
}
//---------------------------------------------------------------------------
void TBaseEstimation::calculateSmoothedRetainedMatrix(){
	//prepare retained Matrix -> same points as for the posterior!
	retainedMatrix=Matrix(posteriorMatrix->posteriorDensityPoints, mySimData->numParams);
	retainedMatrix=0.0;
	//calculate the retained densities
	ColumnVector theta_j;
	for(int p=1; p<=mySimData->numParams;++p){
		//loop over all dirac peaks
		for(int d=1; d<=mySimData->numUsedSims;++d){
			//theta_j is a vector with the parameters of one simulation / from the uniform matrix
			theta_j = mySimData->paramMatrix.submatrix(d,d,2,mySimData->numParams+1).as_column();
			for(int k=1;k<=posteriorMatrix->posteriorDensityPoints;++k){
				retainedMatrix(k,p) = retainedMatrix(k,p) + exp(-pow((*posteriorMatrix->theta)(k)-theta_j(p),2.) / (2.*diracPeakWidth));
			}
		}
	}
}
//---------------------------------------------------------------------------
double TBaseEstimation::marginalDensity(Matrix D, Matrix D_inv, ColumnVector stats){
	ColumnVector theta_j, m_j;
	double f_M = 0.0;
	D = 6.28318530717958623*D;
	double det = D.determinant(); //6.28318530717958623 is 2*Pi
	if(det<=0){
		logfile->warning("Problems calculating the marginal density: determinant is zero!");
		return 0.0;
	}

	//loop over all dirac peaks
	for(int d=1; d<=mySimData->numUsedSims;++d){
		theta_j = mySimData->paramMatrix.submatrix(d,d,2,mySimData->numParams+1).as_column();
		m_j = c_zero+CHat*theta_j;
		Matrix temp = -0.5*(stats-m_j).t()*D_inv*(stats-m_j);
		f_M += exp(temp(1,1));
	}
	f_M = f_M / (mySimData->numUsedSims*sqrt(det));
	f_M = f_M * ((double)mySimData->numUsedSims/(double)mySimData->numReadSims);
	return f_M;
}
//---------------------------------------------------------------------------
void TBaseEstimation::marginalDensitiesOfRetained(int calcObsPValue, Matrix D, Matrix D_inv){
	if(calcObsPValue>mySimData->numUsedSims) calcObsPValue=mySimData->numUsedSims;
	logfile->listFlush("Calculating the marginal density of ", calcObsPValue, " retained simulations ... (0%)");
	fmFromRetained.clear();

	int prog;
	int oldProg=0;
	for(int s=1; s<=calcObsPValue;++s){
		prog=100*(double)s/(double)calcObsPValue;
		if(prog>oldProg){
			oldProg=prog;
 			logfile->listOverFlush("Calculating the marginal density of ", calcObsPValue, " retained simulations ... (", prog, "%)");
		}
	   fmFromRetained.push_back(marginalDensity(D, D_inv, mySimData->statMatrix.column(s)));
	}
	logfile->overList("Calculating the marginal density of ", calcObsPValue, " retained simulations ... done!   ");
}
//---------------------------------------------------------------------------
//JOINT POSTERIOR FUNCTIONS
//---------------------------------------------------------------------------
void TBaseEstimation::performJointPosteriorEstimation(const int & obsDataSetNum){
	if(mySimData->doJointPosteriors){
		logfile->startIndent("Computing joint posterior distribution(s):");

		//preapre filename tag
		std::ostringstream tos; tos << obsDataSetNum;
		std::string filenameTag="_Obs"+ tos.str();

		//calculate grid density
		if(myJointPosteriorSettings->calcGridDensity){
			calculateJointPosterior();
			writeJointPosteriorFiles(filenameTag);
		}

		//generate samples
		generateSamplesFromJointPosteriors(filenameTag);

		logfile->endIndent();
	}
}
//---------------------------------------------------------------------------
void TBaseEstimation::calculateJointPosterior(){
	//compute joint posterior densities
	//calculatePosterior has to be called first!!
	ColumnVector theta_j, v_j;
	ColumnVector* t_j_sub;
	ColumnVector* theta_sub;
	SymmetricMatrix* T_j_sub;
	ColumnVector t_j;
	int z = 0;
	int prog = 0;
	int oldProg = 0;
	//TODO: output is confusing as the parameter numbers are different form the col numbers.
	//        -> also print col numbers and maybe param names??
	for(TJointPosteriorStorage* posteriorStorage : mySimData->jointPosteriorStorage){
		++z;
		//for(std::vector<TJointPosteriorStorage*>::iterator posteriorStorage = mySimData->jointPosteriorStorage.begin(); posteriorStorage != mySimData->jointPosteriorStorage.end(); ++posteriorStorage, ++z){
		logfile->startIndent("Computing joint posterior densities for parameters " + posteriorStorage->getParameterStringOfNames(", ", " and ") + ":");
		prog = 0;
		oldProg = 0;

		//prepare object
		if(myJointPosteriorSettings->jointPosteriorDensityPointsFromMarginals) posteriorStorage->prepareSamplingVectors(myJointPosteriorSettings->jointPosteriorDensityPoints, posteriorMatrix->posteriorMatrix);
		else posteriorStorage->prepareSamplingVectors(myJointPosteriorSettings->jointPosteriorDensityPoints);

		//compute densities. Use iteration offered by the object
		if(myJointPosteriorSettings->jointPosteriorDensityFromMCMC){
			logfile->list("density will be computed from MCMC run generating joint posterior samples.");
		} else {
			logfile->listFlush("Computing density at ",posteriorStorage->getNumSamplingVecs(), " positions ... (0%)");
			posteriorStorage->reset();
			T_j_sub = posteriorStorage->get_T_j_sub(T_j);
			for(int d=1; d<=mySimData->numUsedSims;++d){
				posteriorStorage->restart();

				theta_j = mySimData->paramMatrix.submatrix(d,d,2,mySimData->numParams+1).as_column();
				v_j = calculate_v_j(theta_j);
				double c_j = exp(exponents[d-1]);
				for(int i=0; i<posteriorStorage->getNumSamplingVecs(); ++i){
					theta_sub = posteriorStorage->next();
					t_j_sub = posteriorStorage->get_t_j_sub(T_j*v_j);
					posteriorStorage->addDensity(c_j*exp(-0.5 * (((*theta_sub)-(*t_j_sub)).t() * (*T_j_sub).i() * ((*theta_sub)-(*t_j_sub))).AsScalar()));
				}
				//show progress
				prog = 100 * (double) d / (double)mySimData->numUsedSims;
				if(prog>oldProg){
					oldProg = prog;
					logfile->listOverFlush("Computing density at ", posteriorStorage->getNumSamplingVecs(), " positions ... (", prog, "%)");
				}
			}
			logfile->overList("Computing density at ", posteriorStorage->getNumSamplingVecs(), " positions ... done!        ");
		}
		logfile->endIndent();
	}
}
//---------------------------------------------------------------------------
void TBaseEstimation::writeJointPosteriorFiles(std::string & filenameTag){
	logfile->listFlush("Writing file(s) with joint posterior estimates ...");
	std::string filename;
	for(TJointPosteriorStorage* a : mySimData->jointPosteriorStorage){
		filename = outputPrefix + "jointPosterior_";
		filename += a->getParameterString('_');
		filename += filenameTag;
		filename += ".txt";

		a->computeHDI();
		a->writeToFile(filename, posteriorMatrix->paramRanges);
	}
	logfile->write(" done!");
}
//---------------------------------------------------------------------------
void TBaseEstimation::generateSamplesFromJointPosteriors(std::string & filenameTag){
	if(myJointPosteriorSettings->numApproxJointSamples>0) generateApproxmiateSamplesFromJointPosteriors(filenameTag);
	if(myJointPosteriorSettings->numMCMCJointSamples>0) generateMCMCSamplesFromJointPosteriors(filenameTag);
}

void TBaseEstimation::generateApproxmiateSamplesFromJointPosteriors(std::string & filenameTag){
	std::string filename;
   	for(TJointPosteriorStorage* a : mySimData->jointPosteriorStorage){
 	   filename=outputPrefix + "jointPosteriorSamples_";
   	   filename += a->getParameterString('_');
   	   filename += filenameTag;
   	   filename += ".txt";
   	   //open file and write header...
   	   std::ofstream out(filename.c_str());
	   out << "number";
	   for(std::vector<int>::iterator p=a->params.begin(); p!=a->params.end(); ++p){
		   out << "\t" << mySimData->paramNames[(*p)-1];
	   }
   	   out << std::endl;
	   //get approximate samples
   	   a->writeApproximatelyRandomSamples(myJointPosteriorSettings->numApproxJointSamples, &out, posteriorMatrix->paramRanges, randomGenerator);
   	   out.close();
   	}
}

void TBaseEstimation::generateMCMCSamplesFromJointPosteriors(std::string & filenameTag){
	std::string filename;
	for(TJointPosteriorStorage* a : mySimData->jointPosteriorStorage){
 	   filename=outputPrefix + "jointPosteriorSamples_";
   	   filename += a->getParameterString('_');
   	   filename += filenameTag;
   	   filename += ".txt";
   	   //open file and write header...
   	   std::ofstream out(filename.c_str());
	   out << "number";
	   for(std::vector<int>::iterator p = a->params.begin(); p != a->params.end(); ++p){
		   out << "\t" << mySimData->paramNames[(*p)-1];
	   }
   	   out << std::endl;
   	   //run MCMC
	   runJointSampleMCMC(&out, a);
   	   out.close();
   	}
}
//---------------------------------------------------------------------------
ColumnVector TBaseEstimation::getSampleMCMCStart(TJointPosteriorStorage* a){
	ColumnVector oldPos;
	if(myJointPosteriorSettings->sampleMCMCStart == jointmodeStart){
		logfile->list("Starting chain at joint posterior mode");
		oldPos = a->getJointMode(); //get mode from
	} else {
		if(myJointPosteriorSettings->sampleMCMCStart == jointposteriorStart){
			logfile->list("Starting chain at random location from the approximate joint posterior.");
			oldPos = a->getJointapproxRandomSample(randomGenerator);
		} else {
			oldPos.ReSize(a->params.size());
			if(myJointPosteriorSettings->sampleMCMCStart == marginalStart){
				logfile->list("Starting chain at random location from to the marginal posteriors.");
				//we assume uniform distribution of posterior density points!
				oldPos.ReSize(a->params.size());
				for(unsigned int n=0; n<a->params.size(); ++n){
					oldPos.element(n) = posteriorMatrix->getRandomParameterAccordingToMarginal(a->params[n], randomGenerator->getRand());
				}
			} else {
				logfile->list("Starting chain at random location within the parameter range.");
				for(unsigned int n=0; n<a->params.size(); ++n){
					oldPos.element(n) = randomGenerator->getRand();
				}
			}
		}
	}

	return oldPos;
}
//---------------------------------------------------------------------------
void TBaseEstimation::runJointSampleMCMC(std::ofstream* out, TJointPosteriorStorage* a){
	logfile->startIndent("Generating ", myJointPosteriorSettings->numMCMCJointSamples, " random samples from the joint posterior for parameters " + a->getParameterStringOfNames(", ", " and ") + ":");
	logfile->list("Starting an MCMC run with range proportion ", myJointPosteriorSettings->sampleMCMCRangeProp, ".");

	ColumnVector theta_j, v_j;
	ColumnVector t_j;

	SymmetricMatrix* T_j_sub;
	ColumnVector oldPos, newPos;

	//get starting position
	oldPos = getSampleMCMCStart(a);

	//get ranges
	double* ranges;
	ranges = new double[a->params.size()];
	for(unsigned int n=0; n<a->params.size(); ++n){
		ranges[n] = posteriorMatrix->getPosteriorScaledSD(n)*myJointPosteriorSettings->sampleMCMCRangeProp;
	}

	//a->fillMCMCranges(ranges, sampleMCMCRangeProp);
	//for(unsigned int n=0; n<a->params.size(); ++n){ cout << "RANGE " << n << ": " << ranges[n] << endl;}

	//prepare run
	T_j_sub=a->get_T_j_sub(T_j);
	double r;
	double oldDens, newDens;
	oldDens = getJointPosteriorDensity(T_j_sub, &oldPos, a);

	//run burnin
	logfile->listFlush("Starting burnin of ", myJointPosteriorSettings->sampleMCMCBurnin, " ...");
	for(int i=0; i<myJointPosteriorSettings->sampleMCMCBurnin; ++i){
	   	//propose new position
		newPos=oldPos;
		for(unsigned int n=0; n<a->params.size(); ++n){
			r=randomGenerator->getRand(0.0, ranges[n]);
			newPos.element(n)+=r-ranges[n]/2.0;
		}
		//get new posterior density
		newDens=getJointPosteriorDensity(T_j_sub, &newPos, a);
		//accept?
		if(randomGenerator->getRand() < (newDens/oldDens)){
			oldPos=newPos;
			oldDens=newDens;
		}
	}
	logfile->write(" done!");

	//run chain...
	clock_t starttime=clock();
	int prog, oldProg;
	oldProg=0;
	int accepted=0;
	int length=myJointPosteriorSettings->sampleMCMCSampling*(myJointPosteriorSettings->numMCMCJointSamples-1)+1;
	logfile->listFlush("Starting chain of ", length, " and sampling every ", myJointPosteriorSettings->sampleMCMCSampling, "th step ...");
	for(int i=0; i<length; ++i){
		//propose new position
		newPos=oldPos;
		for(unsigned int n=0; n<a->params.size(); ++n){
			r=randomGenerator->getRand(0.0, ranges[n]);
			newPos.element(n)+=r-ranges[n]/2.0;
		}
		//get new posterior density
		newDens=getJointPosteriorDensity(T_j_sub, &newPos, a);
		//accept?
		if(randomGenerator->getRand() < (newDens/oldDens)){
			oldPos=newPos;
			oldDens=newDens;
			++accepted;
		}

		if(i % myJointPosteriorSettings->sampleMCMCSampling == 0){
			*out << i;
			for(unsigned int n=0; n<a->params.size(); ++n){
				*out << "\t" << posteriorMatrix->toParameterScale(a->params.at(n), oldPos.element(n));
			}
			if(myJointPosteriorSettings->jointPosteriorDensityFromMCMC){
				//save to bin
				a->addSampleToDensity(1.0, &oldPos);
			}

			*out << std::endl;
			//report progress
			prog=(100*(double)i/(double)length);
			if(prog>oldProg){
				oldProg=prog;
				double acc=(double)accepted/(double)i;
				logfile->listOverFlush("Starting chain of ", length,  " and sampling every " + toString(myJointPosteriorSettings->sampleMCMCSampling) + "th step ... (" + toString(prog) + "%, acceptance " + toString(acc) + ")         ");
			}
		}
	}

	//delete
	delete[] ranges;

	double runtime=(double) (clock()-starttime)/CLOCKS_PER_SEC/60;
	logfile->overList("Starting chain of ", length, " and sampling every " + toString(myJointPosteriorSettings->sampleMCMCSampling) + "th step ... done in ", runtime , " min!                         ");
	logfile->conclude("Acceptance rate was ", ((double)accepted / (double) length ));
	logfile->endIndent();
}
//---------------------------------------------------------------------------
double TBaseEstimation::getJointPosteriorDensity(SymmetricMatrix* T_j_sub, ColumnVector* theta_sub, TJointPosteriorStorage* a){
	double dens = 0.0;
	ColumnVector theta_j, v_j;
	ColumnVector* t_j_sub;
	if(theta_sub->Minimum()<0 || theta_sub->Maximum()>1) return 0;
	for(int d=1; d <=mySimData->numUsedSims; ++d){
		theta_j = mySimData->paramMatrix.submatrix(d,d,2,mySimData->numParams+1).as_column();
		v_j = calculate_v_j(theta_j);
		double c_j = exp(exponents[d-1]);
		t_j_sub = a->get_t_j_sub(T_j*v_j);
		dens+=c_j*exp(-0.5 * (((*theta_sub)-(*t_j_sub)).t() * (*T_j_sub).i() * ((*theta_sub)-(*t_j_sub))).AsScalar());
	}
	return dens;
}


