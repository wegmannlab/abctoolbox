//---------------------------------------------------------------------------

#ifndef TBaseEstimationH
#define TBaseEstimationH
#include "TObsData.h"
#include "TSimData.h"
#include "TParameters.h"
#include "TRandomGenerator.h"
#include <stdlib.h>
#include <time.h>
#include "TLog.h"
#include <algorithm>
#include "stringFunctions.h"
#include "matrixFunctions.h"
#include "TPosteriorMatrix.h"

//---------------------------------------------------------------------------
enum MCMCStartType { randomStart, jointmodeStart, jointposteriorStart, marginalStart };

class TJointPosteriorSettings{
public:
	bool doJointPosterior;
	bool calcGridDensity;
	int jointPosteriorDensityPoints;
	bool jointPosteriorDensityPointsFromMarginals;
	bool jointPosteriorDensityFromMCMC;
	vector<std::string> jointParameters;

	//joint posterior samples
	long numApproxJointSamples;
	long numMCMCJointSamples;
	MCMCStartType sampleMCMCStart;
	double sampleMCMCRangeProp;
	int sampleMCMCBurnin;
	int sampleMCMCSampling;

	TJointPosteriorSettings(){
		doJointPosterior = false;
		calcGridDensity = false;
		jointPosteriorDensityPoints = 0;
		jointPosteriorDensityPointsFromMarginals = false;
		jointPosteriorDensityFromMCMC = false;
		numApproxJointSamples = 0;
		numMCMCJointSamples = 0;
		sampleMCMCStart = randomStart;
		sampleMCMCRangeProp = 0.0;
		sampleMCMCBurnin = 0;
		sampleMCMCSampling = 0;
	};
	TJointPosteriorSettings(TParameters* parameters, TLog* LogFile);
};

//---------------------------------------------------------------------------
class TBaseEstimation {
   public:
	  //pointers
	  TRandomGenerator* randomGenerator;
	  TSimData* mySimData;
	  TJointPosteriorSettings* myJointPosteriorSettings;
	  TPosteriorMatrix* posteriorMatrix;
	  TLog* logfile;

	  //estimation
	  double diracPeakWidth;
	  double marDens;
	  double marDensPValue;
	  double tukeyDepth;
	  double tukeyDepthPValue;
	  double posteriorProbability;

	  //storage
	  ColumnVector obsValues;
	  Matrix C;
	  ColumnVector c_zero;
	  Matrix CHat;
	  Matrix RHat;
	  SymmetricMatrix SigmaS;
	  Matrix SigmaSInv;
	  Matrix SigmaTheta;
	  Matrix SigmaThetaInv;
	  Matrix retainedMatrix;
	  SymmetricMatrix T_j;
	  double* exponents;
	  bool exponentsInitialized;
	  int exponentsSize;
	  vector<double> fmFromRetained;

	  //output
	  ofstream trueParamCharacteristicsFile;
	  ofstream propCommonSurfacePriorPosterior;
	  ofstream trueParamValidationFile;
	  ofstream posteriorCharacteristicsFile;
	  std::string outputPrefix;

	  bool trueParamValidationFileOpen;
	  bool computeSmoothedTruncatedPrior;

	  TBaseEstimation(TSimData* simData, ColumnVector* Theta, double DiracPeakWidth, bool ComputeTruncatedPrior, std::string OutputPrefix, TJointPosteriorSettings* JointPosteriorSettings, TRandomGenerator* RandomGenerator, TLog* LogFile);
	  virtual ~TBaseEstimation(){
		  if(exponentsInitialized) delete[] exponents;
		  if(trueParamValidationFileOpen) trueParamValidationFile.close();
		  delete posteriorMatrix;
	  };
	  double* getPointerToStatValues(long & simNum){return mySimData->getPointerToStatValues(simNum);};
	  void preparePrior(bool verbose=false);
	  virtual bool calculatePosterior(bool verbose=false);
	  virtual bool calculate_T_j(SymmetricMatrix & m);
	  virtual ColumnVector calculate_v_j(ColumnVector theta_j);
	  bool performLinearRegression(bool verbose=false);
	  void prepareDiracPeaks();
	  void writeTruncatedPrior(std::string filenameTag="");
	  Matrix* getPointerToMarginalPosteriorDensityMatrix();
	  int getParamColumnFromName(std::string name);
	  void calculateSmoothedRetainedMatrix();
	  double marginalDensity(Matrix D, Matrix D_inv, ColumnVector stats);
	  void marginalDensitiesOfRetained(int calcObsPValue, Matrix D, Matrix D_inv);
	  void openPosteriorCharacteristicsFile();
	  void writePosteriorCharacteristics(const int & obsDataSetNum);
	  //joint posterior / samples from joint posteriors
	  void performJointPosteriorEstimation(const int & obsDataSetNum);
	  void fillExponents_Q_j();
	  void calculateJointPosterior();
	  void writeJointPosteriorFiles(std::string & filenameTag);
	  void generateSamplesFromJointPosteriors(std::string & filenameTag);
	  void generateApproxmiateSamplesFromJointPosteriors(std::string & filenameTag);
	  void generateMCMCSamplesFromJointPosteriors(std::string & filenameTag);
	  ColumnVector getSampleMCMCStart(TJointPosteriorStorage* a);
	  void runJointSampleMCMC(ofstream* out, TJointPosteriorStorage* a);
	  double getJointPosteriorDensity(SymmetricMatrix* T_j_sub, ColumnVector* theta_sub, TJointPosteriorStorage* a);

};


#endif
