//---------------------------------------------------------------------------

#include "TDataVector.h"

//------------------------------------------------------------------------------
//TData
//------------------------------------------------------------------------------
TData::TData(std::vector<std::string> & statNames, double* statValues, TLog* gotlogfile, bool DoBoosting){
	sumStatFile="";
	logFile=gotlogfile;
	doBoosting = DoBoosting;
	storageInitialized=false;
	simDataInitialized=false;
	numSimData=0;
	tagsToInitializeProgramsPrepared = false;

	//define number of stats
    numObsDataOrig = (int)statNames.size();
    numObsData = numObsDataOrig;
	//Boosting: currently just products of all stats
    if(doBoosting){
    	numObsDataBoosting = (numObsDataOrig*numObsDataOrig + numObsDataOrig)/2;
    	numObsData += numObsDataBoosting;
    } else numObsDataBoosting = 0;

    //prepare storage
    obsDataName = new std::string[numObsData];
    obsData = new double[numObsData];
	oldData = new double[numObsData];
	obsDataVariance=new double[numObsData];
	for(int i=0; i<numObsData; ++i) obsDataVariance[i]=1;
	simDataPointers = new double*[numObsData];
	storageInitialized=true;
	getSimDataFromProgram=false;
	simulationProgram = NULL;
	simDataInput_Array = NULL;
	simDataInputArrayInitialized = false;

	//save names and values
	int i = 0;
	for(std::vector<std::string>::iterator itNames = statNames.begin(); itNames!=statNames.end(); ++itNames, ++i){
		obsDataName[i] = *itNames;
		obsData[i] = statValues[i];
	}
	//Boosting: add all products
	if(doBoosting){
		std::vector<std::string>::iterator itNamesSecond;
		int first = 0;
		int second;
		i = numObsDataOrig;
		for(std::vector<std::string>::iterator itNames = statNames.begin(); itNames!=statNames.end(); ++itNames, ++first){
			second = first;
			for(itNamesSecond = itNames; itNamesSecond!=statNames.end(); ++itNamesSecond, ++i, ++second){
				obsDataName[i] = (*itNames) + "_X_" + (*itNamesSecond);
				obsData[i] = statValues[first] * statValues[second];
			}
		}
	}
}

void TData::setSumStatFile(std::string & name){
	logFile->list("Expecting simulated summary statistics in file '" + name + "'.");
	sumStatFile = name;
}

void TData::setProgramToGetSimDataFrom(TProgram* prog){
	getSimDataFromProgram = true;
	simulationProgram = prog;
	logFile->list("Obtaining simulated statistics directly from the internal program '" + simulationProgram->name + "'.");
}

void TData::initializeWithoutSumStatProgram(){
	logFile->list("No program to calculate summary statistics defined for this data object.");
	if(!getSimDataFromProgram){
		logFile->conclude("Assuming file '" + sumStatFile + "' to be written in the simulation program step.");
		std::string foundThere=locateFileRecursively(sumStatFile, ".");
		if(foundThere=="") throw "The file '" + sumStatFile + "' has not been found!";
		sumStatFile = foundThere;
	}
}

//------------------------------------------------------------------------------
std::string TData::locateFileRecursively(std::string file, std::string dir){
	DIR *pdir;
	struct dirent *pent;
	std::string name;
	std::string ret;
	pdir=opendir(dir.c_str());
	if(pdir==NULL){
		closedir(pdir);
		return "";
	}
	while ((pent=readdir(pdir))){
		name=(std::string) pent->d_name;
		//if file found
		if(name==file){
			closedir(pdir);
			return file;
		}
		//TODO: using a . to exclude files is dangerous, as directories might have . too!
		if(name.find_first_of('.')==std::string::npos){
			ret=locateFileRecursively(file, dir+"/"+name);
			if(ret!=""){
				closedir(pdir);
				return name+"/"+ret;
			}
		}
	}
	closedir(pdir);
	return "";
}
//------------------------------------------------------------------------------
void TData::fillTagsToInitializePrograms(){
	if(!tagsToInitializeProgramsPrepared){
		initializationTags.insert(std::pair<std::string, std::string>("SIMNUM", "SIMNUM"));
		initializationTags.insert(std::pair<std::string, std::string>("INDEX", "INDEX"));
		initializationTags.insert(std::pair<std::string, std::string>("SSFILENAME", sumStatFile));

		if(simDataFileName.size()>0){
			int i=1;
			for(std::vector<std::string>::iterator it=simDataFileName.begin(); it!=simDataFileName.end(); ++it, ++i){
				initializationTags.insert(std::pair<std::string, std::string>("SIMDATANAME" + toString(i), *it));
			}
			if(simDataFileName.size()==1) initializationTags.insert(std::pair<std::string, std::string>("SIMDATANAME", simDataFileName.front()));
		}

		//prepare tags to be parsed when executed
		//Note: order is hard-coded in alphabetical order (due to map sorting...)
		executingTags.insert(std::pair<std::string, std::string>("INDEX", ""));
		executingTags.insert(std::pair<std::string, std::string>("SIMNUM", ""));

		tagsToInitializeProgramsPrepared = true;
	}
}
void TData::updateExecutingTags(int & simnum, int & index){
	//first index
	std::map<std::string, std::string>::iterator tagIt=executingTags.begin();
	tagIt->second = toString(index);
	//then simnum
	++tagIt;
	tagIt->second = toString(simnum);
}
//------------------------------------------------------------------------------
void TData::initializeSumstatProgram(std::string & gotSumstatprogram, std::vector<std::string> & gotParameters, std::vector<std::string> & SimDataNames, TPriorVector* priors){
	//first parse and find files with simulated data
	if(!SimDataNames.empty()){
		if(SimDataNames.size()>1) logFile->startNumbering("Expecting to find the simulate data in:");
		else logFile->list("Expecting to find the simulate data in '" + SimDataNames.front() + "'");
		for(std::vector<std::string>::iterator it=SimDataNames.begin(); it!=SimDataNames.end(); ++it){
			simDataFileName.push_back(*it);
			if(SimDataNames.size()>1) logFile->number("'" + *it+ "'");
			//first check if the path provided is a valid file name
			std::ifstream test(it->c_str());
			if(test.good()){
				logFile->conclude("located in the working directory");
				test.close();
			} else {
				//else search through subdirectories
				std::string foundThere=locateFileRecursively(*it, ".");
				if(foundThere == "") throw "The file '" + *it + "' has not been found!";
				if(foundThere == *it)
					logFile->conclude("located in the working directory");
				else {
				    logFile->conclude("located in '" + foundThere + "'");
				    simDataFileName.back() = foundThere;
				}
			}
		}
		if(SimDataNames.size()>1) logFile->endNumbering();
	}
	logFile->startIndent("Using program '" + gotSumstatprogram + "' to calculate summary statistics");
	fillTagsToInitializePrograms();
	sumStatProgram.initialize(gotSumstatprogram, gotParameters, initializationTags, priors, logFile);
	logFile->list("Command line to be used: " + sumStatProgram.getCommandLine());
	logFile->endIndent();

}
void TData::initializeScriptBeforeSSCalc(std::string gotScript, std::vector<std::string> gotParameters){
	logFile->startIndent("Launching script '" + gotScript + "' before calculating summary statistics");
	fillTagsToInitializePrograms();
	scriptBeforeSSCalc.initialize(gotScript, gotParameters, initializationTags, logFile);
	logFile->list("Command line to be used: " + scriptBeforeSSCalc.getCommandLine());
	logFile->endIndent();
}
void TData::initializeScriptAfterSSCalc(std::string gotScript, std::vector<std::string> gotParameters){
	logFile->startIndent("Launching script '" + gotScript + "' after calculating summary statistics");
	fillTagsToInitializePrograms();
	scriptAfterSSCalc.initialize(gotScript, gotParameters, initializationTags, logFile);
	logFile->list("Command line to be used: " + scriptAfterSSCalc.getCommandLine());
	logFile->endIndent();
}
//------------------------------------------------------------------------------
void TData::setOutputFile(int OutputFileDescriptor){
	if(sumStatProgram.initialized) sumStatProgram.setOutputFile(OutputFileDescriptor);
	if(scriptBeforeSSCalc.initialized) scriptBeforeSSCalc.setOutputFile(OutputFileDescriptor);
	if(scriptAfterSSCalc.initialized) scriptAfterSSCalc.setOutputFile(OutputFileDescriptor);
}
void TData::setExternalErrorhandling(std::string & externalErrorHandling){
	if(sumStatProgram.initialized) sumStatProgram.setExternalErrorhandling(externalErrorHandling);
	if(scriptBeforeSSCalc.initialized) scriptBeforeSSCalc.setExternalErrorhandling(externalErrorHandling);
	if(scriptAfterSSCalc.initialized) scriptAfterSSCalc.setExternalErrorhandling(externalErrorHandling);
}
//------------------------------------------------------------------------------
void TData::calculateSumStats(int & simnum, int & index){
	if(sumStatProgram.initialized){
		updateExecutingTags(simnum, index);
		if(scriptBeforeSSCalc.initialized) scriptBeforeSSCalc.execute(executingTags);
		sumStatProgram.execute(executingTags);
		if(scriptAfterSSCalc.initialized) scriptAfterSSCalc.execute(executingTags);
	}
}
//------------------------------------------------------------------------------
void TData::readInitialSumStats(){
	std::string conclude;
	if(getSimDataFromProgram){
		//obtain names and values from internal program
		logFile->listFlush("Getting simulated summary statistics from internal program ...");
		simDataFileName = simulationProgram->getOutputNames();
		simDataInput = simulationProgram->getOutputValues();
	} else {
		logFile->listFlush("Reading simulated summary statistics from '" + sumStatFile + "' ...");
		std::string foundThere=locateFileRecursively(sumStatFile, ".");
			if(foundThere=="") throw "The file '" + sumStatFile + "' has not been found!";
			if(foundThere == sumStatFile)
				conclude = "This file is located in the working directory.";
			else
				conclude = "This file is located in '" + foundThere + "'";
			sumStatFile = foundThere;
		readHeaderAndValuesUnique(sumStatFile, simDataFileName, simDataInput);
	}
	logFile->write(" done");
	if(!conclude.empty()) logFile->conclude(conclude);

	//create array for future storage and copy values
	numSimData=(int) simDataFileName.size();
	simDataInput_Array = new double[numSimData + numObsDataBoosting];
	simDataInputArrayInitialized = true;
	int j=0;
	for(std::vector<double>::iterator itVal=simDataInput.begin(); itVal!=simDataInput.end(); ++itVal, ++j)
		simDataInput_Array[j] = *itVal;

	//fill simDataPointers with pointers to simDataInput where the value is required for distance calculations (is present in obsData)
	std::vector<std::string>::iterator it;
	std::vector<double>::iterator itVal;
	for(int i=0; i<numObsDataOrig;++i){ //only those before boosting
		itVal = simDataInput.begin();
		j = 0;
		for(it=simDataFileName.begin();; ++it, ++itVal, ++j){
			if(it==simDataFileName.end()) throw "Statistics '" + obsDataName[i] + "' is missing!";
			if(obsDataName[i]==*it){
				//simDataPointers[i]=&(*itVal);
				simDataPointers[i] = &(simDataInput_Array[j]);
				break;
			}
		}
	}
	logFile->conclude("Read ", numSimData, " Data");

	//Boosting: currently just product between stats in obs files
	if(doBoosting){
		//fill in products and save pointers
		int indexSimData = numSimData;
		int indexPointers = numObsDataOrig;
		for(int first=0; first < numObsDataOrig; ++first){
			for(int second=first; second < numObsDataOrig; ++second){
				simDataInput_Array[indexSimData] =  (*simDataPointers[first]) * (*simDataPointers[second]);
				simDataPointers[indexPointers] = &(simDataInput_Array[indexSimData]);
				++indexSimData;
				++indexPointers;
			}
		}
	}
}
//------------------------------------------------------------------------------
void TData::readSumStats(){
	if(getSimDataFromProgram){
		//simDataName = simulationProgram->getOutputNames(); -> we dump names: assume to be always the same order!
		simulationProgram->getOutputValues(simDataInput_Array);
	} else {
      // open file
	  std::ifstream is (sumStatFile.c_str()); // opening the file for reading
	  if(!is){
		  throw "The file containing the computed summary statistics '" + sumStatFile + "' could not be opened!";
	  }
	  std::string line;
	  getline(is, line); //read header -> dump!
	  getline(is, line); //read values
	  is.close();

	  //put values in array
	  simDataInput.clear();
	  trimString(line);
	  if(!fillVectorFromStringWhiteSpaceSkipEmptyArray(line, simDataInput_Array, numSimData)){
		  logFile->warning("Wrong number of values (not " + toString(simDataFileName.size()) +") in the file containing summary statistics '" + sumStatFile + "'!");
		  throw "Wrong number of values (not " + toString(simDataFileName.size()) +") in the file containing summary statistics '" + sumStatFile + "'!";
	  }
	}

	//Boosting: currently just product between stats in obs files
	if(doBoosting){
		//fill in products and save pointers
		int indexSimData = numSimData;
		for(int first=0; first < numObsDataOrig; ++first){
			for(int second=first; second < numObsDataOrig; ++second){
				simDataInput_Array[indexSimData] =  (*simDataPointers[first]) * (*simDataPointers[second]);
				++indexSimData;
			}
		}
	}
}
//------------------------------------------------------------------------------
void
TData::writeData(std::ofstream& ofs){
   for(int i=0; i<numObsData;++i){
	  ofs << "\t" << *simDataPointers[i];
   }
}
//------------------------------------------------------------------------------
void
TData::writeObsData(std::ofstream& ofs){
   for(int i=0; i<numObsData;++i) ofs << "\t" << obsData[i];
}

//------------------------------------------------------------------------------
void
TData::writeHeader(std::ofstream& ofs){
	for(int i=0; i<numObsData;++i) ofs << "\t" << obsDataName[i];
}
//------------------------------------------------------------------------------
void
TData::saveOldValues(){
	for(int i=0; i<numObsData;++i) oldData[i]=*simDataPointers[i];
}
//------------------------------------------------------------------------------
void
TData::resetOldValues(){
	for(int i=0; i<numObsData;++i) *simDataPointers[i]=oldData[i];
}

//------------------------------------------------------------------------------
int TData::getObsDataIndexFromName(std::string name){
	for(int i=0; i<numObsData;++i) if(name==simDataFileName[i]) return i;
	return false;
}

//------------------------------------------------------------------------------
//TDataVector
//------------------------------------------------------------------------------
TDataVector::TDataVector(TParameters* gotParameters, TInputFileVector* gotInputFiles, TLog* gotlogfile, std::string & externalErrorHandling, int & childOutputfileDescriptor,  std::string Suffix){
	logFile=gotlogfile;
	inputFiles=gotInputFiles;
	tempSuffix=Suffix;

	logFile->startIndent("Initializing summary statistics from simulation output:");

	//------------------------------------------------
	//Create data objects from obs files
	//------------------------------------------------
	bool doBoosting = gotParameters->parameterExists("doBoosting");
	if(doBoosting) logFile->list("Summary statistics will be boosted (using all pairwise-products)");

	logFile->startIndent("Reading obs files:");
	std::vector<std::string> tmp, obsNames;
	gotParameters->fillParameterIntoVector("obsName",tmp,';');
	//some could be vectors -> expand those!
	repeatAndExpandIndexes(tmp, obsNames);
	if(obsNames.empty()) throw "No obs files specified!";

	//create TData objects
	TObsData obsData(logFile);
	for(std::vector<std::string>::iterator it=obsNames.begin(); it!=obsNames.end(); ++it){
		//read obs file using TObsData object
		obsData.update(*it);
		//create a TData object for each entry
		for(int s=0; s<obsData.numObsDataSets; ++s){
			vecDataObjects.push_back(new TData(obsData.obsNameVector, obsData.getObservedValues(s), logFile, doBoosting));
		}
	}
	logFile->endIndent();
	logFile->conclude("Created a total of " + toString(vecDataObjects.size()) + " data objects");

	//------------------------------------------------
	//sum stat names
	//------------------------------------------------
	//read where to get summary statistics from
	std::vector<std::string> sumStatNames;
	if(gotParameters->parameterExists("sumStatName")){
		gotParameters->fillParameterIntoVector("sumStatName",tmp,';');
		//replace TEMPSUFFIX tag
		std::size_t pos;
		for(std::vector<std::string>::iterator it=tmp.begin(); it!=tmp.end(); ++it){
			pos=it->find("TEMPSUFFIX");
			if(pos!=std::string::npos)
				*it = it->replace(pos, 10, tempSuffix);
		}
		//some could be vectors -> expand those!
		repeatAndExpandIndexes(tmp, sumStatNames);
		if(sumStatNames.size() != vecDataObjects.size()) throw "Unequal number of observed data entries and files to read summary statistics from!";
	} else {
		for(unsigned int i=0; i<vecDataObjects.size(); ++i)
			sumStatNames.push_back("summary_stats"+tempSuffix+".txt");
	}

	logFile->startNumbering("Initializing data objects:");

	//------------------------------------------------
	//summary statistics program
	//------------------------------------------------
	std::vector<std::string> sumStatProg;
	std::vector< std::vector<std::string> > sumStatParamList;
	std::vector< std::vector<std::string> > vecSimDataName;
	if(gotParameters->parameterExists("sumStatProgram")){
		useSumstatProgram=true;
		gotParameters->fillParameterIntoVector("sumStatProgram",tmp,';');
		//some could be vectors -> expand those!
		repeatAndExpandIndexes(tmp, sumStatProg);
		if(sumStatProg.size() != vecDataObjects.size()) throw "Unequal number of observed data entries and programs to calculate summary statistics!";

		//read arguments for sum stat program
		if(gotParameters->parameterExists("sumStatArgs")){
			gotParameters->fillParameterIntoVector("sumStatArgs",tmp,';');
			//some could be vectors -> expand those!
			repeatAndExpandIndexesOfSubs(tmp, sumStatParamList, " \t\f\v\n\r");
			if(sumStatParamList.size() != vecDataObjects.size()) throw "Number of argument lists does not match number of summary statistics programs!";
		} else {
			for(unsigned int i=0; i<vecDataObjects.size(); ++i)
				sumStatParamList.push_back(std::vector<std::string>());
		}

		//read names of files with simulated data -> note, this is optional!
		if(gotParameters->parameterExists("simDataName")){
			gotParameters->fillParameterIntoVector("simDataName", tmp, ';');
			//some could be vectors -> expand those!
			repeatAndExpandIndexesOfSubs(tmp, vecSimDataName, ",");
			if(vecSimDataName.size() !=  vecDataObjects.size()) throw "Number of file lists for simulated data does not match number of data objects!";
			//temp suffix
			std::size_t pos;
			for(std::vector< std::vector<std::string> >::iterator it=vecSimDataName.begin(); it!=vecSimDataName.end(); ++it){
				for(std::vector<std::string>::iterator sit = it->begin(); sit!=it->end(); ++sit){
					pos=sit->find("TEMPSUFFIX");
					if(pos!=std::string::npos)
						*sit = sit->replace(pos, 10, tempSuffix);
				}
			}
		} else {
			for(unsigned int i = 0; i<vecDataObjects.size(); ++i)
				vecSimDataName.push_back(std::vector<std::string>());
		}
	} else useSumstatProgram=false;


	//------------------------------------------------
	//Script BEFORE sumstat program
	//------------------------------------------------
	std::vector<std::string> scriptBefore;
	std::vector< std::vector<std::string> > scriptParamListBefore;
	launchScriptBeforeSS=false;
	if(useSumstatProgram){
		gotParameters->fillParameterIntoVector("scriptBeforeSS", tmp, ';', false);
		if(tmp.size()>0){
			//some could be vectors -> expand those!
			repeatAndExpandIndexes(tmp, scriptBefore);
			if(scriptBefore.size()!=vecDataObjects.size()) throw "Unequal number of scripts to launch before summary statistics calculation and programs to calculate summary statistics!";
			//read parameters
			if(gotParameters->parameterExists("scriptBeforeSSArgs")){
				gotParameters->fillParameterIntoVector("scriptBeforeSSArgs", tmp, ';');
				//some could be vectors -> expand those!
				repeatAndExpandIndexesOfSubs(tmp, scriptParamListBefore, " ");
				if(scriptParamListBefore.size() !=  scriptBefore.size()) throw "Number of argument lists does not match number of scripts to launch before summary statistics calculation!";
			} else {
				for(unsigned int i = 0; i<scriptBefore.size(); ++i)
					scriptParamListBefore.push_back(std::vector<std::string>());
			}
			launchScriptBeforeSS = true;
		}
	}

	//------------------------------------------------
	//Script AFTER sumstat program
	//------------------------------------------------
	std::vector<std::string> scriptAfter;
	std::vector< std::vector<std::string> > scriptParamListAfter;
	launchScriptAfterSS=false;
	if(useSumstatProgram){
		gotParameters->fillParameterIntoVector("scriptAfterSS", tmp, ';', false);
		if(tmp.size()>0){
			//some could be vectors -> expand those!
			repeatAndExpandIndexes(tmp, scriptAfter);
			if(scriptAfter.size()!=vecDataObjects.size()) throw "Unequal number of scripts to launch after summary statistics calculation and programs to calculate summary statistics!";
			//read parameters
			if(gotParameters->parameterExists("scriptAfterSSArgs")){
				gotParameters->fillParameterIntoVector("scriptAfterSSArgs", tmp, ';');
				//some could be vectors -> expand those!
				repeatAndExpandIndexesOfSubs(tmp, scriptParamListAfter, " ");
				if(scriptParamListAfter.size() !=  scriptAfter.size()) throw "Number of argument lists does not match number of scripts to launch after summary statistics calculation!";
			} else {
				for(unsigned int i = 0; i<scriptAfter.size(); ++i)
					scriptParamListAfter.push_back(std::vector<std::string>());
			}
			launchScriptAfterSS = true;
		}
	}

	//------------------------------------------------
	//Initialize programs
	//------------------------------------------------
	int i=0;
	int simnum = -1;
	for(curDataObject = vecDataObjects.begin(); curDataObject != vecDataObjects.end(); ++curDataObject, ++i){
		logFile->number("Data object " + toString(i+1) + ":");
		logFile->addIndent();
		//summary statistics file
		if(sumStatNames[i] == "FROMINTERNAL") (*curDataObject)->setProgramToGetSimDataFrom(gotInputFiles->getLinkToSimulationProgramForDataObject());
		else (*curDataObject)->setSumStatFile(sumStatNames[i]);
		//program to calculate sum stats
		if(useSumstatProgram && sumStatProg[i]!="-" && !sumStatProg[i].empty()){
			(*curDataObject)->initializeSumstatProgram(sumStatProg[i], sumStatParamList[i], vecSimDataName[i], inputFiles->priors);
			if(launchScriptBeforeSS){
				if(scriptBefore[i]=="-" || scriptBefore[i]=="") logFile->list("No script is launched before summary statistics calculation.");
				else (*curDataObject)->initializeScriptBeforeSSCalc(scriptBefore[i], scriptParamListBefore[i]);
			}
			if(launchScriptAfterSS){
				if(scriptAfter[i]=="-" || scriptAfter[i]=="") logFile->list("No script is launched before summary statistics calculation.");
				else (*curDataObject)->initializeScriptAfterSSCalc(scriptAfter[i], scriptParamListAfter[i]);
			}
			(*curDataObject)->setExternalErrorhandling(externalErrorHandling);
			if(childOutputfileDescriptor>0) (*curDataObject)->setOutputFile(childOutputfileDescriptor);
			//run on test simulation and parse initial data
			logFile->listFlush("Execute on data from test simulation ...");
			(*curDataObject)->calculateSumStats(simnum, i);
			logFile->write(" done!");
		} else  (*curDataObject)->initializeWithoutSumStatProgram();
		(*curDataObject)->readInitialSumStats();
		(*curDataObject)->saveOldValues();
		//end
		logFile->endIndent();
	}
	logFile->endNumbering();

	summarizeDataObjects();

	//standardization of linear combinations?
	stdLinearCombForDist=gotParameters->parameterExists("stdLinearCombForDist");
	if(stdLinearCombForDist) logFile->list("Linear combinations will be standardized for distance calculation");

	logFile->endIndent();

}
//------------------------------------------------------------------------------
void TDataVector::setOutputFile(int OutputFileDescriptor){
	 for(curDataObject=vecDataObjects.begin();curDataObject!=vecDataObjects.end(); ++curDataObject){
		 (*curDataObject)->setOutputFile(OutputFileDescriptor);
	 }
}
//------------------------------------------------------------------------------
void TDataVector::summarizeDataObjects(){
	//summarize some features of the TData objects
	  //numObsData
	  numObsData=0;
	  curDataObject=vecDataObjects.begin();
	  for(;curDataObject!=vecDataObjects.end(); ++curDataObject){
		 numObsData+=(*curDataObject)->numObsData;
	  }

	  //create an array with pointers to all obs Data, obs data variance and sim data entries
	  pointersToObsData=new double*[numObsData];
	  pointersToVariances=new double*[numObsData];
	  pointersToSimData=new double*[numObsData];
	  int i=0;
	  for(curDataObject=vecDataObjects.begin();curDataObject!=vecDataObjects.end(); ++curDataObject){
		 for(int j=0; j<(*curDataObject)->numObsData; ++j){
			pointersToObsData[i]=&(*curDataObject)->obsData[j];
			pointersToVariances[i]=&(*curDataObject)->obsDataVariance[j];
			pointersToSimData[i]=(*curDataObject)->simDataPointers[j];
			++i;
		 }
	  }

	  //create an Array with all ObsDataNames (with Prefix added if more than one object)
	  obsDataNamesWithPrefix=new std::string[numObsData];
	  curDataObject=vecDataObjects.begin();
	  i=0;
	  int numObs=1;
	  if(vecDataObjects.size()>1){
		  for(;curDataObject!=vecDataObjects.end(); ++curDataObject, ++numObs){
			  for(int j=0; j<(*curDataObject)->numObsData; ++j){
				  obsDataNamesWithPrefix[i]="Obs" + toString(numObs) + "_" + (*curDataObject)->obsDataName[j];
				  ++i;
			  }
		  }
	  } else {
		  for(int j=0; j<(*curDataObject)->numObsData; ++j){
			  obsDataNamesWithPrefix[i]=(*curDataObject)->obsDataName[j];
			  ++i;
		  }
	  }
}
//------------------------------------------------------------------------------
void TDataVector::calculateSumStats(int simnum){
   curDataObject=vecDataObjects.begin();
   int index = 1;
   for(;curDataObject!=vecDataObjects.end(); ++curDataObject, ++index){
	   (*curDataObject)->calculateSumStats(simnum, index);
	   (*curDataObject)->readSumStats();
   }
}
//------------------------------------------------------------------------------
//Functions to calculate the distances
//!!!!if one is changed, change all others as well!!!!!!
double TDataVector::calculateDistance(){
   double dis=0;
   for(int i=0; i<numObsData; ++i){
      dis+=(*pointersToObsData[i]-*pointersToSimData[i])*(*pointersToObsData[i]-*pointersToSimData[i]) / *pointersToVariances[i];
   }
   return sqrt(dis);
}
double TDataVector::calculateDistance(TLinearComb* pcaObject){
	return pcaObject->calculateDistance(pointersToSimData, stdLinearCombForDist);
}
double TDataVector::calculateDistance(double* data){
   double dis=0;
   for(int i=0; i<numObsData; ++i){
	  dis += (*pointersToObsData[i]-data[i])*(*pointersToObsData[i]-data[i]) / *pointersToVariances[i];
   }
   return sqrt(dis);
}
double TDataVector::calculateDistance(double* data, TLinearComb* pcaObject){
	return pcaObject->calculateDistance(data, stdLinearCombForDist);
}
//------------------------------------------------------------------------------
void TDataVector::saveOldValues(){
   curDataObject=vecDataObjects.begin();
   for(;curDataObject!=vecDataObjects.end(); ++curDataObject){
	   (*curDataObject)->saveOldValues();
   }
}
//------------------------------------------------------------------------------
void TDataVector::resetOldValues(){
   curDataObject=vecDataObjects.begin();
   for(;curDataObject!=vecDataObjects.end(); ++curDataObject){
	   (*curDataObject)->resetOldValues();
   }

}
//------------------------------------------------------------------------------
void TDataVector::writeHeader(std::ofstream& ofs){
	for(int i=0; i<numObsData; ++i){
		ofs << "\t" << obsDataNamesWithPrefix[i];
	}
}
void TDataVector::writeHeaderOnlyOneDataObject(int thisObs, std::ofstream& ofs){
   vecDataObjects[thisObs]->writeHeader(ofs);
}
//------------------------------------------------------------------------------
void TDataVector::writeData(std::ofstream& ofs){
   for(int i=0; i<numObsData; ++i){
	  ofs << "\t" << *pointersToSimData[i];
   }
}
void TDataVector::writeDataOnlyOneDataObject(int thisObs, std::ofstream& ofs){
	vecDataObjects[thisObs]->writeData(ofs);
}
//------------------------------------------------------------------------------
bool TDataVector::matchColumns(std::vector<std::string> & colNames, int numColNames, int** pointerToArrayToFill){
   curDataObject=vecDataObjects.begin();
   int* arrayToFill=*pointerToArrayToFill;
   for(int k=0; k<numObsData; ++k){
	  for(int j=0; j<(numColNames+1); ++j){
		 if(j==numColNames)
			throw "The data column '" + (*curDataObject)->obsDataName[k] + "' is missing in the calibration File!!!";
		 if(obsDataNamesWithPrefix[k]==colNames[j]){
			arrayToFill[k]=j;
			break;
		 }
	  }
   }
   return true;
}
//------------------------------------------------------------------------------
std::vector<std::string> TDataVector::getNamesVector(){
	std::vector<std::string> statNames;
   for(int i=0; i < numObsData; ++i)
	   statNames.push_back(obsDataNamesWithPrefix[i]);
   return statNames;
}
//------------------------------------------------------------------------------
std::vector<double> TDataVector::getObsDataVector(){
	std::vector<double> obsData;
   for(int i=0; i < numObsData; ++i)
	   obsData.push_back(*pointersToObsData[i]);
   return obsData;
}
//------------------------------------------------------------------------------
void TDataVector::fillObsDataArray(double* & obsData){
   obsData=new double[numObsData];
   for(int i=0; i < numObsData; ++i)
	   obsData[i]=*pointersToObsData[i];
}
//------------------------------------------------------------------------------
