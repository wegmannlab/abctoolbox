/*
 * TEstimator.cpp
 *
 *  Created on: Sep 9, 2011
 *      Author: wegmannd
 */

#include "TEstimator.h"
//----------------------------------------------------------------------------------
//TSumStatSet
//----------------------------------------------------------------------------------
bool TSumStatSet::operator==(TSumStatSet & other){
	if(stats.size() != other.size()) return false;
	for(std::vector<int>::iterator it=stats.begin(); it!=stats.end(); ++it){
		if(!other.contains(*it)) return false;
	}
	return true;
}
bool TSumStatSet::operator!=(TSumStatSet & other){
	return !(*this == other);
}

bool TSumStatSet::contains(int & a) {
	for(std::vector<int>::iterator it=stats.begin(); it!=stats.end(); ++it){
		if(a==(*it)) return true;
	}
	return false;
}
bool TSumStatSet::addStat(int & a){
	for(it=stats.begin(); it!=stats.end(); ++it)
		if(a==(*it)) return false;
	double temp;
	for(it=stats.begin(); it!=stats.end(); ++it){
		temp=mySimData->getLargestCorrelation(*it, a);
		if(temp>largestCorrelation) largestCorrelation=temp;
	}
	stats.push_back(a);
	//update hash
	hash+=pow(2.0,a);
	return true;
}
void TSumStatSet::truncateObsData(){
	myObsData->useOnlyTheseStats(stats);
}
void TSumStatSet::printHeader(std::ofstream & outfile){
	outfile << "Hash\tPower\tLargestPairwiseCorrelation\tStatistics(Numbers)\tStatistics(Names)" << std::endl;
}
void TSumStatSet::print(std::ofstream & outfile){
	//sort statistics by number
	sort(stats.begin(), stats.end());
	it=stats.begin();
	outfile << hash << "\t" << power << "\t" << largestCorrelation << "\t" << (*it);
	++it;
	for(; it!=stats.end(); ++it) outfile << "," << (*it);
	it=stats.begin();
	outfile << "\t" << myObsData->getStatNameFromColNumber(*it);
	++it;
	for(; it!=stats.end(); ++it) outfile << "," << myObsData->getStatNameFromColNumber(*it);
	outfile << std::endl;
}

struct compareTSumStatSets{
  bool operator ()(TSumStatSet* i, TSumStatSet* j){
		if(i->power > j->power) return true;
		else {
			if(i->hash > j->hash) return false;
			else {
				if(i->power == j->power && i->hash < j->hash ) return true;
				else return false;
			}
		}
  };
};


/*
bool sortingFunctionTSumStatSetPointers(TSumStatSet* i, TSumStatSet* j){
	cout << "SORTING FUNC: " << flush;
	cout << i->power << " vs " << j->power << endl;
	if(i->power > j->power) return true;
	else {
		cout << "HASH: " << flush;
		cout << i->hash << " vs " << j->hash << endl;
		if(i->hash < j->hash) return true;
	}
	return false;
};
*/
//----------------------------------------------------------------------------------
//TEstimator
//----------------------------------------------------------------------------------
TEstimator::TEstimator(TParameters* parameters, TRandomGenerator* RandomGenerator, TLog* LogFile){
	//TODO: write summary to logfile
	randomGenerator=RandomGenerator;
	logfile=LogFile;
	myParameters=parameters;
	myJointPosteriorSettings = NULL;

   //read some parameters
   outputPrefix=parameters->getParameterString("outputPrefix", false);
   if(outputPrefix=="") outputPrefix="ABC_GLM_";

   standardizeStats=parameters->parameterExists("standardizeStats");
   diracPeakWidth=parameters->getParameterDoubleWithDefault("diracPeakWidth", 0.001);
   modelAveraging=parameters->parameterExists("modelAveraging");
   posteriorDensityPoints = parameters->getParameterDouble("posteriorDensityPoints", false);

   if(posteriorDensityPoints<1) posteriorDensityPoints=100;
   preparePosteriorDensityPoints();
   writeTruncatedPrior=false;
   writeRetainedSimulations=false;

   //params regarding validation
   marDensPValue=0;
   numReplicatesPValTukeyDepth=0;
   numSimsRetainedValidation=0;
   numSimsRandomValidation=0;

   //read observed data and simulation sets
   myObsData = new TObsData(parameters->getParameterString("obsName"), logfile);
   mySimData = new TSimDataVector(parameters, myObsData, logfile);
   paramRangesName=parameters->getParameterString("paramRangesName", false);
   if(paramRangesName.size()>0) mySimData->useParamRangesFromFile(paramRangesName);
   if(modelAveraging) mySimData->matchCommonParams();
}
//--------------------------------------------------------------
void TEstimator::performEstimations(TParameters* parameters){
	//read some parameters
	writeRetainedSimulations=parameters->parameterExists("writeRetained");
	writeTruncatedPrior=parameters->parameterExists("writeTruncatedPrior");

	//do some validation tasks?
	marDensPValue=parameters->getParameterInt("marDensPValue", false);
	if(marDensPValue>0 && marDensPValue<10){
	   logfile->warning("Ignoring 'marDensPValue' values <10!");
	   marDensPValue=0;
	}

	numReplicatesPValTukeyDepth=parameters->getParameterInt("tukeyPValue", false);
	if(numReplicatesPValTukeyDepth>0 && numReplicatesPValTukeyDepth<10){
	   logfile->warning("Ignoring 'tukeyPValue' values <10!");
	   numReplicatesPValTukeyDepth = 0;
	}

	numSimsRetainedValidation=parameters->getParameterDouble("retainedValidation", false);
	numSimsRandomValidation=parameters->getParameterDouble("randomValidation", false);
	numSimsModelChoiceValidation=parameters->getParameterDouble("modelChoiceValidation", false);

	//joint posterior settings
	myJointPosteriorSettings=new TJointPosteriorSettings(myParameters, logfile);
	if(myJointPosteriorSettings->doJointPosterior) mySimData->initializeJointPosteriorCalculation();
	//estimation type
	estimationType=myParameters->getParameterStringWithDefault("estimationType", "standard");

	if(estimationType == "standard"){
		logfile->startIndent("Performing an ABC-GLM standard estimations:");
	} else if(estimationType == "independentReplicates")
		logfile->startIndent("Performing an estimation based on independent replicates.");
	else if(estimationType == "independentReplicatesMCMC")
		logfile->startIndent("Performing an MCMC estimation based on independent replicates.");
	else throw "The estimation type '"+estimationType+"' does not exist!";

	//prepare estimations
	standardize();
	openModelFitFile();

	//run estimation
	if(estimationType == "standard") runStandardEstimation();
	else if(estimationType == "independentReplicates") runIndependentReplicatesEstimation();
	else if(estimationType == "independentReplicatesMCMC") runIndependentReplicatesMCMCEstimation(parameters);
	logfile->endIndent();
}
//--------------------------------------------------------------
void TEstimator::openModelFitFile(){
	std::string filename=outputPrefix+"modelFit.txt";
	modelFitFile.open(filename.c_str());
	//write header
	modelFitFile << "dataSet";
	writeModelFitFileHeader(modelFitFile);
}
//--------------------------------------------------------------
void TEstimator::writeModelFitFileHeader(std::ofstream & out, bool forValidation){
	if(forValidation) out << "trueModel\tobsNum";
	else {
		if(marDensPValue>0){
			for(unsigned int i=1;i<=mySimData->simDataVec.size(); ++i)
				out << "\tmodel" << i << "_marginalDensityPValue";
		}
	}
	for(unsigned int i=1;i<=mySimData->simDataVec.size(); ++i)
		out << "\tmodel" << i << "_marginalDensity";

	if(!forValidation){
		if(numReplicatesPValTukeyDepth > 0){
			for(unsigned int i=1;i<=mySimData->simDataVec.size(); ++i)
				out << "\tmodel" << i << "_TukeydepthPValue";
		}

		for(unsigned int i=1;i<=mySimData->simDataVec.size(); ++i)
			out << "\tmodel" << i << "_TukeyDepth";
	}

	if(mySimData->simDataVec.size()>1){
		for(unsigned int i=1;i<=mySimData->simDataVec.size(); ++i)
			out << "\tmodel" << i << "_BayesFactor";

		for(unsigned int i=1;i<=mySimData->simDataVec.size(); ++i)
			out << "\tmodel" << i << "_posteriorProbability";

		out << "\tchosenModel";
	}
	out << std::endl;
}
//--------------------------------------------------------------
void TEstimator::standardize(){
	logfile->listFlush("Standardizing Parameters ...");
	mySimData->standardizeParameters();
	logfile->write(" done!");
	if(standardizeStats){
		logfile->listFlush("Standardizing statistics (distance calculation only)...");
		mySimData->standardizeStatistics();
		logfile->write(" done!");
	}
}
//--------------------------------------------------------------
void TEstimator::preparePosteriorDensityPoints(){
	//prepare vector for points at which the density (also prior!!!) is estimated
	theta=ColumnVector(posteriorDensityPoints);
	//since the parameters are standardized between 0 and 1, we can easy calculate the posterior between 0 and 1
	double step=1.0/(posteriorDensityPoints-1.0);
	for(int j=0; j<posteriorDensityPoints; ++j) theta(j+1)=(j)*step;
}

//--------------------------------------------------------------
void TEstimator::runStandardEstimation(){
	//prepare estimation objects
	std::vector<TStandardEstimation*> estimators;
	int m = 0;
	std::string filename;
	for(std::vector<TSimData*>::iterator it=mySimData->simDataVec.begin(); it!=mySimData->simDataVec.end(); ++it, ++m){
		filename=outputPrefix+"model"+toString(m)+"_";
		estimators.push_back(new TStandardEstimation((*it), &theta, diracPeakWidth, writeTruncatedPrior, filename, myJointPosteriorSettings, randomGenerator, logfile));
	}

	//run for each data set sequentially
	bool estimationOk;
	for(int i=0;i<myObsData->numObsDataSets;++i){
		logfile->startIndent("Performing estimations for observed data set ", i, ":");
		estimationOk=true;
		int simDataNum=0;
		//perform individual estimations on each model
		for(TStandardEstimation* it : estimators){
			++simDataNum;
			if(estimators.size()>1) logfile->startIndent("Performing estimations for model ", simDataNum, ":");
			//regression and marginal posteriors
			if(!it->performPosteriorEstimation(i, writeRetainedSimulations, marDensPValue, numReplicatesPValTukeyDepth)){
				estimationOk=false;
				if(estimators.size()>1) logfile->endIndent();
				break;
			}
			//joint posterior
			it->performJointPosteriorEstimation(i);
			//retained validation
			if(numSimsRetainedValidation>0) it->performRetainedValidation(numSimsRetainedValidation, "_Obs"+toString(i));
			if(estimators.size()>1) logfile->endIndent();
		}
		//perform model choice, model averaging, model fit, ...
		if(estimationOk){
			logfile->listFlush("Writing model fit ...");

			//compute / write model fit
			modelFitFile << i;
			//marginal density
			if(marDensPValue>0){
				for(TStandardEstimation* it : estimators)
					modelFitFile << "\t" << it->marDensPValue;
			}
			double sumMarDens = 0.0;
			for(TStandardEstimation* it : estimators){
				modelFitFile << "\t" << it->marDens;
				sumMarDens += it->marDens;
			}

			//Tukey depth
			if(numReplicatesPValTukeyDepth>0){
				for(TStandardEstimation* it : estimators)
					modelFitFile << "\t" << it->tukeyDepthPValue;
			}
			for(TStandardEstimation* it : estimators)
				modelFitFile << "\t" << it->tukeyDepth;

			logfile->write(" done!");
			//model choice
			if(mySimData->simDataVec.size()>1){
				logfile->listFlush("Performing model choice ...");
				int bestModel = 0;
				double bestMarDens = 0.0;
				double bestBF = 0.0;
				int i=0;
				for(TStandardEstimation* it : estimators){
					++i;

					//calculate denominator separately (not from sumMarDens) to avoid issues with small numbers
					double denominator = 0.0;
					for(TStandardEstimation* it2 : estimators){
						if(it2 != it) denominator += it2->marDens;
					}
					double BF = it->marDens / denominator;
					modelFitFile << "\t" << BF;
					if(it->marDens > bestMarDens){
						bestModel = i;
						bestMarDens = it->marDens;
						bestBF = BF;
					}

				}
				//calculate and write posterior probabilities
				double postProb;
				i=0;
				for(TStandardEstimation* it : estimators){
					++i;
					postProb = it->marDens / (sumMarDens);
					modelFitFile << "\t" << postProb;
					it->posteriorProbability=postProb;
				}

				modelFitFile << "\t" << bestModel;
				logfile->write(" done!");
				logfile->conclude("Best fitting model is model ", bestModel, " with BayesFactor ", bestBF);
			}
			modelFitFile << std::endl;
			//model averaging
			if(modelAveraging){
				//computed posterior densities weighted by posterior probability for common params
				logfile->startIndent("Performing model averaging:");
				TModelAvergingEstimation modavg(mySimData->commonParams, &estimators, &theta, outputPrefix, logfile);
				modavg.performPosteriorEstimation(i, true);
				logfile->endIndent();
			}
		}
	  logfile->endIndent("Estimations for data set ", i, " finished successfully!");

   }
   //perform validation using random simulations
   if(numSimsRandomValidation>0){
	   for(std::vector<TStandardEstimation*>::iterator it=estimators.begin(); it!=estimators.end(); ++it){
		   (*it)->performRandomValidation(numSimsRandomValidation);
	   }
   }
	//perform validation of model choice
	//TODO: move upfront in order to be used for each obs data set to do the correction
	if(numSimsModelChoiceValidation>0) validateModelChoice(&estimators, numSimsModelChoiceValidation, true, true);

	//delete estimators
	for(std::vector<TStandardEstimation*>::iterator it=estimators.begin(); it!=estimators.end(); ++it){
		delete *it;
	}
}

//--------------------------------------------------------------
void TEstimator::runIndependentReplicatesEstimation(){
	//prepare estimation objects
	std::vector<TIndependentMeasuresEstimation*> estimators;
	int m=0;
	std::string filename;
	for(std::vector<TSimData*>::iterator it=mySimData->simDataVec.begin(); it!=mySimData->simDataVec.end(); ++it, ++m){
		filename=outputPrefix+"model"+toString(m)+"_";
		estimators.push_back(new TIndependentMeasuresEstimation((*it), &theta, diracPeakWidth, writeTruncatedPrior, filename, myJointPosteriorSettings, randomGenerator, logfile));
	}

	bool estimationOk = true;
	int simDataNum=0;

	logfile->startIndent("Performing estimations:");
	for(TIndependentMeasuresEstimation* it : estimators){
		++simDataNum;

		if(estimators.size()>1) logfile->startIndent("Performing estimations for model ", simDataNum+1, ":");
		//regression and marginal posteriors
		if(!it->performPosteriorEstimation(writeRetainedSimulations, marDensPValue)){
			estimationOk = false;
			break;
		}
		//joint posterior
		it->performJointPosteriorEstimation(0);

		//retained validation
		//if(numSimsRetainedValidation>0) (*it)->performRetainedValidation(numSimsRetainedValidation, "_Obs"+toString(i));
		if(estimators.size()>1) logfile->endIndent();
	}

	if(estimationOk){
		logfile->listFlush("Writing model fit ...");
		//compute / write model fit
		modelFitFile << "0";
		if(marDensPValue>0){
			for(TIndependentMeasuresEstimation* it : estimators)
				modelFitFile << "\t" << it->marDensPValue;
		}
		double sumMarDens=0;
		for(TIndependentMeasuresEstimation* it : estimators){
			modelFitFile << "\t" << it->marDens;
			sumMarDens += it->marDens;
		}
		logfile->write(" done!");
		if(mySimData->simDataVec.size()>1){
			logfile->listFlush("Performing model choice ...");
			int bestModel=0;
			double bestMarDens=0;
			int i=0;
			for(TIndependentMeasuresEstimation* it : estimators){
				++i;
				modelFitFile << "\t" << it->marDens / (sumMarDens - it->marDens);
				if(it->marDens > bestMarDens){
					bestModel = i+1;
					bestMarDens = it->marDens;
				}
			}
			for(TIndependentMeasuresEstimation* it : estimators)
				modelFitFile << "\t" << it->marDens / (sumMarDens);
			double BF=bestMarDens / (sumMarDens - bestMarDens);
			modelFitFile << "\t" << bestModel;
			logfile->write(" done!");
			logfile->conclude("Best fitting model is model ", bestModel, " with BayesFactor ", BF);
		}
		modelFitFile << std::endl;
   }
	logfile->endIndent("Estimations  finished successfully!");

	//perfom validation using random simulations

	/*
	if(numSimsRandomValidation>0){
	   for(std::vector<TIndependentMeasuresEstimation*>::iterator it=estimators.begin(); it!=estimators.end(); ++it){
		   (*it)->performRandomValidation(numSimsRandomValidation);
	   }
   }
   */
	//perfom validation of model choice
	//TODO: move upfront in order to be used for each obs data set to do the correction
	//if(numSimsModelChoiceValidation>0) validateModelChoice(&estimators, numSimsModelChoiceValidation);

	//delete estimators
	for(std::vector<TIndependentMeasuresEstimation*>::iterator it=estimators.begin(); it!=estimators.end(); ++it){
		delete *it;
	}
}

//--------------------------------------------------------------
void TEstimator::runIndependentReplicatesMCMCEstimation(TParameters* parameters){
	//prepare estimation objects
	std::vector<TIndependentMeasuresMCMCEstimation*> estimators;
	int m=0;
	std::string filename;
	for(std::vector<TSimData*>::iterator it=mySimData->simDataVec.begin(); it!=mySimData->simDataVec.end(); ++it, ++m){
		filename=outputPrefix+"model"+toString(m)+"_";
		estimators.push_back(new TIndependentMeasuresMCMCEstimation(*parameters, (*it), &theta, diracPeakWidth, writeTruncatedPrior, filename, myJointPosteriorSettings, randomGenerator, logfile));
	}

	bool estimationOk = true;
	int simDataNum = 0;

	logfile->startIndent("Performing estimations:");
	for(TIndependentMeasuresMCMCEstimation* it : estimators){
		++simDataNum;
		if(estimators.size()>1) logfile->startIndent("Performing estimations for model ", simDataNum+1, ":");
		//regression and marginal posteriors
		if(!it->performPosteriorEstimation(writeRetainedSimulations, marDensPValue, numReplicatesPValTukeyDepth)){
			estimationOk = false;
			break;
		}
		//joint posterior
		//(*it)->performJointPosteriorEstimation(0);

		//retained validation
		//if(numSimsRetainedValidation>0) (*it)->performRetainedValidation(numSimsRetainedValidation, "_Obs"+toString(i));
		if(estimators.size()>1) logfile->endIndent();
	}

	/*
	if(estimationOk){
		logfile->listFlush("Writing model fit ...");
		//compute / write model fit
		modelFitFile << "0";
		if(marDensPValue>0){
			for(std::vector<TIndependentMeasuresEstimation*>::iterator it=estimators.begin(); it!=estimators.end(); ++it)
				modelFitFile << "\t" << (*it)->marDensPValue;
		}
		double sumMarDens=0;
		for(std::vector<TIndependentMeasuresEstimation*>::iterator it=estimators.begin(); it!=estimators.end(); ++it){
			modelFitFile << "\t" << (*it)->marDens;
			sumMarDens+=(*it)->marDens;
		}
		logfile->write(" done!");
		if(mySimData->simDataVec.size()>1){
			logfile->listFlush("Performing model choice ...");
			int bestModel=0;
			double bestMarDens=0;
			int i=0;
			for(std::vector<TIndependentMeasuresEstimation*>::iterator it=estimators.begin(); it!=estimators.end(); ++it, ++i){
				modelFitFile << "\t" << (*it)->marDens / (sumMarDens - (*it)->marDens);
				if((*it)->marDens > bestMarDens){
					bestModel=i+1;
					bestMarDens=(*it)->marDens;
				}
			}
			for(std::vector<TIndependentMeasuresEstimation*>::iterator it=estimators.begin(); it!=estimators.end(); ++it)
				modelFitFile << "\t" << (*it)->marDens / (sumMarDens);
			double BF=bestMarDens / (sumMarDens - bestMarDens);
			modelFitFile << "\t" << bestModel;
			logfile->write(" done!");
			logfile->conclude("Best fitting model is model ", bestModel, " with BayesFactor ", BF);
		}
		modelFitFile << std::endl;
   }
   */
	logfile->endIndent("Estimations  finished successfully!");

	//perfom validation using random simulations

	/*
	if(numSimsRandomValidation>0){
	   for(std::vector<TIndependentMeasuresEstimation*>::iterator it=estimators.begin(); it!=estimators.end(); ++it){
		   (*it)->performRandomValidation(numSimsRandomValidation);
	   }
   }
   */
	//perfom validation of model choice
	//TODO: move upfront in order to be used for each obs data set to do the correction
	//if(numSimsModelChoiceValidation>0) validateModelChoice(&estimators, numSimsModelChoiceValidation);

	//delete estimators
	for(std::vector<TIndependentMeasuresMCMCEstimation*>::iterator it=estimators.begin(); it!=estimators.end(); ++it){
		delete *it;
	}
}


//--------------------------------------------------------------
double TEstimator::validateModelChoice(std::vector<TStandardEstimation*>* estimators, int numSimsForValidation, bool writeOutput, bool verbose){
	if(verbose) logfile->listFlush("Performing validation of model choice ...");
	//open file
	std::ofstream modelChoiceValFile;
	std::string filename;
	if(writeOutput){
		filename=outputPrefix+"modelChoiceValidation.txt";
		modelChoiceValFile.open(filename.c_str());
		writeModelFitFileHeader(modelChoiceValFile, true);
	}

	//choose pseudo observed data sets
	long numSimsModelChoiceValidationPerModel=(double) numSimsForValidation/(double) estimators->size();
	numSimsForValidation=estimators->size()*numSimsModelChoiceValidationPerModel; //adjust to usable number
	std::vector<TModChoiceValiContainer*> pseudoObsSims;
	int modelNum=0;
	for(std::vector<TStandardEstimation*>::iterator it=estimators->begin(); it!=estimators->end(); ++it, ++modelNum){
		pseudoObsSims.push_back(new TModChoiceValiContainer(*it, modelNum, numSimsModelChoiceValidationPerModel));
	}

	//loop over simulations
	std::vector<TModChoiceValiContainer*>::iterator trueModelSimNumber;
	double sumMarDens, bestMarDens;
	std::vector<TModChoiceValiContainer*>::size_type bestModel;
	double power=0;
	clock_t starttime=clock();
	double runtime;

	//storage: [estimator used][estimator of true sim][# pseudo obs]
	double*** mdStorage=new double**[pseudoObsSims.size()];
	for(std::vector<TModChoiceValiContainer*>::size_type i=0; i<pseudoObsSims.size(); ++i){
		mdStorage[i]=new double*[pseudoObsSims.size()];
		for(std::vector<TModChoiceValiContainer*>::size_type j=0; j<pseudoObsSims.size(); ++j)
			mdStorage[i][j]=new double[numSimsModelChoiceValidationPerModel];
	}

	long i=0;
	long maxI=numSimsForValidation*estimators->size();
	int prog=0;
	int oldProg=0;

	#ifdef USE_OMP
	#pragma omp parallel for num_threads(pseudoObsSims.size())
	#endif
	for(std::vector<TModChoiceValiContainer*>::size_type est=0; est<pseudoObsSims.size(); ++est){
		for(std::vector<TModChoiceValiContainer*>::size_type trueEst=0; trueEst<pseudoObsSims.size(); ++trueEst){
			for(std::vector<long>::size_type n=0; n < pseudoObsSims[trueEst]->pseudoObsSimNumbers.size();++n){
				//calculate md
				++i;
				if(verbose){
					prog=100.0 * ((double) i / (double) maxI);
					if(prog>oldProg){
						oldProg=prog;
						logfile->listOverFlush("Performing validation of model choice ... (", prog, "%)");
					}
				}
				mdStorage[est][trueEst][n] = pseudoObsSims[est]->calcMdReturn(pseudoObsSims[trueEst]->pointerToStatValues(n));
			}
		}
	}

	//prepare confusion matrix
	int** confusion;
	int numModels = estimators->size();
	if(writeOutput){
		confusion = new int*[numModels];
		for(int i=0; i<numModels; ++i){
			confusion[i] = new int[estimators->size()];
			for(int j=0; j<numModels; ++j) confusion[i][j] = 0;
		}
	}
	int numInConfusion;

	//write output anf calc confusion matrix
	for(std::vector<long>::size_type n=0; n < pseudoObsSims[0]->pseudoObsSimNumbers.size();++n){
		for(std::vector<TModChoiceValiContainer*>::size_type trueEst=0; trueEst<pseudoObsSims.size(); ++trueEst){

			//Compute posterior probabilities, Bayes factors and choose best model
			bestModel=0;
			bestMarDens=0;
			sumMarDens=0;
			for(std::vector<TModChoiceValiContainer*>::size_type est=0; est<pseudoObsSims.size(); ++est){
				sumMarDens+=mdStorage[est][trueEst][n];
				if(mdStorage[est][trueEst][n] > bestMarDens){
					bestModel=est;
					bestMarDens=mdStorage[est][trueEst][n];
				}
			}

			if(bestMarDens > 0){
				//write marginal density, BF and posterior probabilities
				if(writeOutput){
					modelChoiceValFile << trueEst << "\t" << pseudoObsSims[trueEst]->pseudoObsSimNumbers[n];
					for(std::vector<TModChoiceValiContainer*>::size_type est=0; est<pseudoObsSims.size(); ++est)
						modelChoiceValFile << "\t" << mdStorage[est][trueEst][n];
					for(std::vector<TModChoiceValiContainer*>::size_type est=0; est<pseudoObsSims.size(); ++est)
						modelChoiceValFile << "\t" << mdStorage[est][trueEst][n] / (sumMarDens - mdStorage[est][trueEst][n]);
					for(std::vector<TModChoiceValiContainer*>::size_type est=0; est<pseudoObsSims.size(); ++est)
						modelChoiceValFile << "\t" << mdStorage[est][trueEst][n] / sumMarDens;
					modelChoiceValFile << "\t" << bestModel << std::endl;
				}

				//confusion
				if(writeOutput){
					confusion[trueEst][bestModel] += 1;
					++numInConfusion;
				}

				if(bestModel==trueEst) power=power+1;
			}
		}
	}
	if(writeOutput) modelChoiceValFile.close();

	//compute power
	power = power / (double) numSimsForValidation;

	//compute and print confusion matrix
	std::ofstream confusionFile;
	std::string confusionFilename;
	if(writeOutput){
		//open file and write header
		confusionFilename = outputPrefix + "confusionMatrix.txt";
		confusionFile.open(confusionFilename.c_str());

		confusionFile << "trueModel";
		for(int i=0; i<numModels; ++i) confusionFile << "\tmodel" << i+1 << "_raw";
		for(int i=0; i<numModels; ++i) confusionFile << "\tmodel" << i+1 << "_prop";
		confusionFile << "\tfalsePositives\tfalseNegatives" << std::endl;

		//calculate false positives
		double* falsePositives = new double[estimators->size()];
		int sum;
		for(int i=0; i<numModels; ++i){
			falsePositives[i]  = 0.0;
			sum = 0;
			for(int j=0; j<numModels; ++j){
				if(j!=i){
					falsePositives[i] += confusion[j][i];
					//get how often we tried
					for(int k=0; k<numModels; ++k){
						sum += confusion[j][k];
					}
				}
			}
			falsePositives[i]  = falsePositives[i] / (double) sum;
		}

		for(int i=0; i<numModels; ++i){
			confusionFile << i;
			sum = 0;
			for(int j=0; j<numModels; ++j){
				confusionFile << "\t" << confusion[i][j];
				sum += confusion[i][j];
			}
			for(int j=0; j<numModels; ++j){
				confusionFile << "\t" << (double) confusion[i][j] / (double) sum;
			}
			//now false positives and false negatives
			confusionFile << "\t" << falsePositives[i] << "\t" << (double) (sum - confusion[i][i]) / (double) sum << std::endl;
		}

		//clean up
		confusionFile.close();
		delete[] falsePositives;
	}

	//delete storage
	for(std::vector<TModChoiceValiContainer*>::size_type i=0; i<pseudoObsSims.size(); ++i){
		for(std::vector<TModChoiceValiContainer*>::size_type j=0; j<pseudoObsSims.size(); ++j)
			delete[] mdStorage[i][j];
		delete[] mdStorage[i];
	}
	delete[] mdStorage;
	for(std::vector<TModChoiceValiContainer*>::iterator it=pseudoObsSims.begin(); it!=pseudoObsSims.end(); ++it)
		delete *it;
	if(writeOutput){
		for(int i=0; i<numModels; ++i)
			delete[] confusion[i];
		delete[] confusion;
	}

	//exit
	if(verbose){
		runtime=(double) (clock()-starttime)/CLOCKS_PER_SEC/60;
		logfile->overList("Performing validation of model choice ... done (in ", runtime, " min)");
		logfile->conclude("Estimated power to choose the correct model: " + toString(power));
		if(writeOutput) logfile->conclude("Validation results written to '" + filename +"'");
		if(writeOutput) logfile->conclude("Confusion matrix written to '" + confusionFilename + "'");
	}
	//return power
	return power;
}


void TEstimator::findStatsModelChoice(TParameters* parameters){
	logfile->list("Performing a greedy search for optimal summary statistics for model choice:");
	logfile->suppressWarings();

	//read / check parameters
	myJointPosteriorSettings=new TJointPosteriorSettings();
	numSimsModelChoiceValidation=parameters->getParameterDouble("modelChoiceValidation", false);
	if(numSimsModelChoiceValidation<10) throw "Number of simulation to be used for model choice validation is < 10!";
	double maxCor;
	if(myParameters->parameterExists("maxCorSSFinder")){
		maxCor=myParameters->getParameterDouble("maxCorSSFinder");
	} else maxCor = 0.95;
	int numBestSetsToKeep;
	if(myParameters->parameterExists("numBestSetsToKeep")){
		numBestSetsToKeep=myParameters->getParameterDouble("numBestSetsToKeep");
	} else numBestSetsToKeep=12;

	//standardize and open output
	standardize();
	openModelFitFile();

	//prepare estimation objects
	std::vector<TStandardEstimation*> estimators;
	for(std::vector<TSimData*>::iterator it=mySimData->simDataVec.begin(); it!=mySimData->simDataVec.end(); ++it){
		estimators.push_back(new TStandardEstimation((*it), &theta, diracPeakWidth, writeTruncatedPrior, "", myJointPosteriorSettings, randomGenerator, logfile));
	}

	//generate a stat set objects of for each stat
	logfile->startIndent("Computing power for each statistics used alone:");
	std::map<double, TSumStatSet*> statSets;
	std::vector<TSumStatSet*> nextExtendibleStatSets;
	std::vector<TSumStatSet*> previousExtendibleStatSets;

	//open file to write results such that the whole thing can be aborted any time
	std::ofstream outfile_unsorted;
	std::string filename=outputPrefix+"greedySearchForBestStatisticsForModelChoice_unsorted.txt";
	outfile_unsorted.open(filename.c_str());
	statSets.begin()->second->printHeader(outfile_unsorted);

	//get power for each individual stat
	TSumStatSet* tempSet;
	logfile->listFlush("Performing power analysis ... ");
	for(int i=0;i<myObsData->numStats;++i){
		logfile->listOverFlush("Performing power analysis for statistic ", (i+1), " of ", myObsData->numStats," ... ");
		tempSet=new TSumStatSet(mySimData, myObsData, i);
		tempSet->truncateObsData();
		tempSet->setPower(validateModelChoice(&estimators, numSimsModelChoiceValidation, false, false));
		tempSet->print(outfile_unsorted);
		statSets.insert(std::pair<double, TSumStatSet*>(pow(2.0, i), tempSet));
		previousExtendibleStatSets.push_back(tempSet);
	}
	logfile->overList("Performing power analysis for ", myObsData->numStats," statistics ... done!                         ");

	/*
	* loop as follows:
	* 1) add each possible stat to each stat object
	* 2) filter by correlation
	* 3) run power analysis
	* 4) keep addition if power is better than original
	*/


	double tempHash;
	std::vector<double> powerSorted;
	std::vector<double>::reverse_iterator psIt;
	double powerThreshold;
	struct timeval start, end;
	for(int s=1;s<myObsData->numStats && previousExtendibleStatSets.size()>0;++s){
		//add at max all stats!
		logfile->startIndent("Adding statistics to sets, iteration ", s, ":");
		gettimeofday(&start, NULL);
		int setNum=1;
		nextExtendibleStatSets.clear();
		toString((unsigned int) previousExtendibleStatSets.size());
		for(std::vector<TSumStatSet*>::iterator it=previousExtendibleStatSets.begin(); it!=previousExtendibleStatSets.end(); ++it, ++setNum){
			std::string progressString="Extending set " + toString(setNum) + " of " + toString(previousExtendibleStatSets.size()) + " sets";
			logfile->listOverFlush(progressString + " ...            ");
			//add new sets by adding all possible statistics to each set
			//if set has not yet been extended
			if((*it)->extendible){
				for(int j=0;j<myObsData->numStats;++j){
					logfile->listOverFlush(progressString + " with statistics ", (j+1), " of " + toString(myObsData->numStats) + " ...");
					tempHash=(*it)->getHashWith(j);
					if(statSets.find(tempHash)==statSets.end()){
						//hash does not exist -> create new set
						tempSet=new TSumStatSet(*(*it), j);
						if(tempSet->getLargestCorrelation() < maxCor){
							//compute power
							tempSet->truncateObsData();
							tempSet->setPower(validateModelChoice(&estimators, numSimsModelChoiceValidation, false, false));
							//only extend further if power was larger than power of parent
							if(tempSet->power > (*it)->power){
								nextExtendibleStatSets.push_back(tempSet);
							}
							else tempSet->extendible=false;
						} else {
							tempSet->extendible=false; //do not extend if correlation was problematic
						}
						//save set
						statSets.insert(std::pair<double, TSumStatSet*>(tempHash, tempSet));
						tempSet->print(outfile_unsorted);
					}
				}
				(*it)->extendible=false; //prevent from further extending this set
			}
		}
		gettimeofday(&end, NULL);
		logfile->overList("Extending ", previousExtendibleStatSets.size(), " sets ... done (in ", ((end.tv_sec  - start.tv_sec)/60.0), " min)!                              ");
		logfile->conclude(nextExtendibleStatSets.size(), " extendible sets added.");
		if(nextExtendibleStatSets.size()<1){
			logfile->conclude("Ending search!");
			break;
		}

		//only extend best sets -> save those in previousExtendableStatSets
		logfile->startIndent("Retaining the most powerful ", numBestSetsToKeep, " sets for further consideration:");
		previousExtendibleStatSets.clear();
		if(statSets.size() > (std::map<double, TSumStatSet*>::size_type) numBestSetsToKeep){
			logfile->listFlush("Sorting sets by power ...");
			powerSorted.clear();
			for(std::map<double, TSumStatSet*>::iterator it=statSets.begin(); it!=statSets.end(); ++it){
				powerSorted.push_back(it->second->power);
			}
			sort(powerSorted.begin(), powerSorted.end());

			psIt=powerSorted.rbegin();
			psIt+=(numBestSetsToKeep-1);
			logfile->write(" done!");
			if(psIt==powerSorted.rend()){
				logfile->listFlush("Keeping all extendible sets ...");
				//keep all extendible because the additional entries have the same power
				for(std::vector<TSumStatSet*>::iterator it=nextExtendibleStatSets.begin(); it!=nextExtendibleStatSets.end(); ++it){
					previousExtendibleStatSets.push_back(*it);
				}
				logfile->write(" done!");
			} else {
				powerThreshold=(*psIt);
				logfile->listFlush("Keeping extendible sets with power >= ", powerThreshold, " ...");
				for(std::vector<TSumStatSet*>::iterator it=nextExtendibleStatSets.begin(); it!=nextExtendibleStatSets.end(); ++it){
					if((*it)->power < powerThreshold) (*it)->extendible=false;
					else previousExtendibleStatSets.push_back(*it);
				}
				logfile->write(" done!");
			}
		} else {
			//keep all extendibles
			logfile->listFlush("Keeping all extendible sets ...");
			for(std::vector<TSumStatSet*>::iterator it=nextExtendibleStatSets.begin(); it!=nextExtendibleStatSets.end(); ++it){
				previousExtendibleStatSets.push_back(*it);
			}
			logfile->write(" done!");
		}
		logfile->conclude(previousExtendibleStatSets.size(), " extendible sets retained for further extension.");
		if(previousExtendibleStatSets.size()==0) logfile->conclude("ending search!");
		logfile->endIndent();

		logfile->endIndent();
	}
	myObsData->useAllStats();
	outfile_unsorted.close();


	//write all sets in a sorted way
	std::ofstream outfile;
	filename=outputPrefix+"greedySearchForBestStatisticsForModelChoice.txt";
	outfile.open(filename.c_str());
	statSets.begin()->second->printHeader(outfile);
	//sort sets by power
	std::vector<TSumStatSet*> statSetsSorted;
	for(std::map<double, TSumStatSet*>::iterator it=statSets.begin(); it!=statSets.end(); ++it){
		statSetsSorted.push_back(it->second);
	}
	sort(statSetsSorted.begin(), statSetsSorted.end(), compareTSumStatSets());

	//write output
	for(std::vector<TSumStatSet*>::iterator it=statSetsSorted.begin(); it!=statSetsSorted.end(); ++it){
		(*it)->print(outfile);
	}
	outfile.close();

	//delete all sets
	for(std::map<double, TSumStatSet*>::iterator it=statSets.begin(); it!=statSets.end(); ++it){
		delete it->second;
	}

	//delete estimators
	for(std::vector<TStandardEstimation*>::iterator it=estimators.begin(); it!=estimators.end(); ++it){
		delete *it;
	}
	logfile->showWarings();
}
