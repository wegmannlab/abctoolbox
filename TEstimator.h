/*
 * TEstimator.h
 *
 *  Created on: Sep 9, 2011
 *      Author: wegmannd
 */

#ifndef TESTIMATOR_H_
#define TESTIMATOR_H_

#include "TStandardEstimation.h"
#include "TModelAveragingEstimation.h"
#include <math.h>

#include "TIndependentMeasuresEstimation.h"
#include "TIndependentMeasuresMCMCEstimation.h"
#include "TRandomGenerator.h"
#ifdef USE_OMP
#include <omp.h>
#endif
#include <sys/time.h>
#include "stringFunctions.h"

class TSumStatSet{
public:
	vector<int> stats;
	vector<int>::iterator it;
	TSimDataVector* mySimData;
	TObsData* myObsData;
	double largestCorrelation;
	double hash;
	double power;
	bool extendible;

	TSumStatSet(TSimDataVector* SimData, TObsData* ObsData){ init(SimData, ObsData);};
	TSumStatSet(TSimDataVector* SimData, TObsData* ObsData, int & a){init(SimData, ObsData); addStat(a);};
	TSumStatSet(TSimDataVector* SimData, TObsData* ObsData, int & a, int & b){
		init(SimData, ObsData);
		addStat(a);
		addStat(b);
	};
	TSumStatSet(const TSumStatSet & other){
		init(other);
	};
	TSumStatSet(const TSumStatSet & other, int & a){
		init(other);
		addStat(a);
	};
	void init(TSimDataVector* SimData, TObsData* ObsData){
		mySimData=SimData;
		myObsData=ObsData;
		largestCorrelation=0;
		hash=0;
		power=-1;
		extendible=true;
	};
	void init(const TSumStatSet & other){
		mySimData=other.mySimData;
		myObsData=other.myObsData;
		largestCorrelation=other.largestCorrelation;
		hash=other.hash;
		power=-1;
		extendible=true;
		for(unsigned int i=0; i<other.stats.size(); ++i) //iterators are not const!
			stats.push_back(other.stats[i]);
	};
	unsigned int size() const { return stats.size(); };
	bool operator==(TSumStatSet & other);
	bool operator!=(TSumStatSet &other);
	bool contains(int & a);
	bool addStat(int & a);
	double getHash(){return hash;}
	double getHashWith(int a){if(!contains(a)) return hash+pow(2.0,a); else return hash;};
	double getLargestCorrelation(){return largestCorrelation;};
	void truncateObsData();
	void setPower(double Power){power=Power;};
	void printHeader(ofstream & outfile);
	void print(ofstream & outfile);
};

class TModChoiceValiContainer{
public:
	TStandardEstimation* estimator;
	vector<long> pseudoObsSimNumbers;
	vector<long>::iterator it;
	double md;
	int modelNum;

	TModChoiceValiContainer(TStandardEstimation* Estimator, int & ModelNum, long & numSims){
		estimator=Estimator;
		estimator->fillVectorWithNumbersOfRandomSims(pseudoObsSimNumbers, numSims);
		it=pseudoObsSimNumbers.begin();
		modelNum=ModelNum;
		md=-1;
	};
	void advance(){++it;};
	bool end(){return (it==pseudoObsSimNumbers.end());};
	double size(){ return pseudoObsSimNumbers.size(); };
	long curSimNumber(){return *it;};
	void calcMd(){ md=estimator->runValidationEstimation( *it ); };
	void calcMd(double* obsValues){ md=estimator->runValidationEstimation(obsValues); };
	double calcMdReturn(double* obsValues){ return estimator->runValidationEstimation(obsValues); };
	double* pointerToStatValues(){ return estimator->getPointerToStatValues(*it); };
	double* pointerToStatValues(long n){ return estimator->getPointerToStatValues(pseudoObsSimNumbers[n]); };
};

class TEstimator{
private:
	TRandomGenerator* randomGenerator;
	TLog* logfile;
	TParameters* myParameters;
	//TBaseEstimation* myEstimation;
	std::string estimationType;

	//data sets
	TObsData* myObsData;
	TSimDataVector* mySimData;
	bool paramRangesFromFile;
	std::string paramRangesName;

	//estimation
	bool standardizeStats;
	int posteriorDensityPoints;
	double diracPeakWidth;
	bool modelAveraging;

	//output
	std::string outputPrefix;
	bool writeTruncatedPrior;
	bool writeRetainedSimulations;
	ofstream modelFitFile;

	//joint posteriorspseudoObsSimNumbers
	TJointPosteriorSettings* myJointPosteriorSettings;

	//validation
	int marDensPValue;
	int numTukeyDirections;
	int numReplicatesPValTukeyDepth;
	long numSimsRetainedValidation;
	long numSimsRandomValidation;
	long numSimsModelChoiceValidation;

	//for posterior estimation
	ColumnVector theta;


public:
	TEstimator(TParameters* parameters, TRandomGenerator* RandomGenerator, TLog* LogFile);
	~TEstimator(){
		modelFitFile.close();
		delete myJointPosteriorSettings;
		delete myObsData;
		delete mySimData;
	};
	void performEstimations(TParameters* parameters);
	void openModelFitFile();
	void writeModelFitFileHeader(ofstream & out, bool forValidation=false);
	void standardize();
	void preparePosteriorDensityPoints();
	void runStandardEstimation();
	void runIndependentReplicatesEstimation();
	void runIndependentReplicatesMCMCEstimation(TParameters* parameters);
	double validateModelChoice(vector<TStandardEstimation*>* estimators, int numSimsForValidation, bool writeOutput=true, bool verbose=false);
	void findStatsModelChoice(TParameters* parameters);
};

#endif /* TESTIMATOR_H_ */
