//---------------------------------------------------------------------------
#include "TExecuteProgram.h"
//---------------------------------------------------------------------------
TExecuteProgram::TExecuteProgram(std::string exe, std::string* parameter, int numParameter, int passedParamIdentifier){
   myExe=exe;
   myParameter=new char*[numParameter+2];
   myParameter[0]=(char*)myExe.c_str();
   for(int i=0; i<numParameter; ++i){
	   myParameter[i+1]=(char*)parameter[i].c_str();
   }
   myParameter[numParameter+1]=NULL;
   paramsInitialized=true;
   myPassedParamIdentifier=passedParamIdentifier+1;
   outputfileOpen=false;
   redirectOutput=false;
   outputFileDescriptor=-1;
   redirect=NULL;
   redirectDescriptor=-1;
}
TExecuteProgram::TExecuteProgram(std::string exe){
   myExe=exe;
   myParameter=new char*[2];
   myParameter[0]=(char*)myExe.c_str();
   myParameter[1]=NULL;
   paramsInitialized=true;
   myPassedParamIdentifier=-1;
   outputfileOpen=false;
   redirectOutput=false;
   outputFileDescriptor=-1;
   redirect=NULL;
   redirectDescriptor=-1;
}

void TExecuteProgram::setOutputFile(int OutputFileDescriptor){
	outputFileDescriptor=OutputFileDescriptor;
	outputfileOpen=true;
}
void TExecuteProgram::setRedirectionFile(std::string & filename){
	redirectOutput=true;
	redirectName=filename;
}
//---------------------------------------------------------------------------
//TODO: test vfork vs fork
int TExecuteProgram::execute(){
	try{
		#ifdef _WINDOWS_
			returnValue=spawnv(P_WAIT, myParameter[0], myParameter);
			//if(returnValue==-1)
			//throw "YYYY Error when executing '" + myExe +"'!" + myExe + "'!";
			return returnValue;
		#else
			int pid, status;
			pid = vfork();
			if (pid == -1)
				throw "Calling program '" + myExe + "': not able to create fork!";
			if (pid == 0){
				//CHILD
				if(outputfileOpen){
					dup2(outputFileDescriptor,1);dup2(outputFileDescriptor,2);
				}
				if(redirectOutput){
					//overwrite std if outputfile is set. err will still go to outputfile
					redirectDescriptor=open(redirectName.c_str(), O_WRONLY | O_TRUNC | O_CREAT, S_IRUSR | S_IRGRP | S_IWGRP | S_IWUSR);
					if(redirectDescriptor<0) throw "Failed to open redirection file '"+redirectName+"'!";
					dup2(redirectDescriptor,1);// only stdout
				}
				int err=execv(myParameter[0], myParameter);
				if(redirectOutput) close(redirectDescriptor);
				//exit
				if(err!=0) exit (err);
				else exit(0);
			}

			waitpid(pid, &status, 0);

			//cout << "WEXITSTATUS: '" << WEXITSTATUS(status) << "'" << endl;

			if(!WIFEXITED(status)) return -1;
			return WEXITSTATUS(status);
			//throw "Error when executing '" + myExe +"'!";

		#endif
	  }
	  catch(std::string & error){
		 throw error;
	  }
	  catch (...){
		 throw "Error when executing '" + myExe +"'!";
	  }
	return -1;

} // executeProgram
//---------------------------------------------------------------------------
int TExecuteProgram::execute(std::string passedParam){
	// parameters to pass have to be char*!!!!!!
	if(myPassedParamIdentifier>0 && passedParam!="") myParameter[myPassedParamIdentifier]=(char*)passedParam.c_str();
	return execute();
}
//---------------------------------------------------------------------------
int TExecuteProgram::execute(std::string* passedParams, int numPassedParams){
	if(paramsInitialized){
		delete[] myParameter;
		paramsInitialized=false;
	}
	myParameter=new char*[numPassedParams+2];
	myParameter[0]=(char*)myExe.c_str();
	for(int i=1; i<=numPassedParams; ++i){
		myParameter[i]=(char*)passedParams[i-1].c_str();
	}
	myParameter[numPassedParams+1]=NULL;
	paramsInitialized=true;
	return execute();
}
//---------------------------------------------------------------------------
//TGLM
//---------------------------------------------------------------------------
TGLM::TGLM(std::string* passedParams, int numPassedParams, TRandomGenerator* RandomGenerator){
	randomGenerator=RandomGenerator;
	if(numPassedParams<3) throw "The INTERNALGLM program requires at least three parameters (not " + toString(numPassedParams) + ")!";
	myExe="INTERNALGLM";
	//the first argument is the name of the matrix file
	std::string matricesFilename=passedParams[0];
	//the second argument is the output filename
	outputname=passedParams[1];

	//read file with matrix definitions
	std::ifstream is;
	is.open(matricesFilename.c_str());
	if(!is) throw "The file with the definitions of the matrices '"+matricesFilename+"' could not be opend!";

	//read first line -> should be a "C"!
	std::string line;
	getline(is, line);
	trimString(line);
	if(line!="C") throw "The file with the definitions of the matrices '"+matricesFilename+"' does not start with matrix C (tag missing?)!";

	//read matrix C as a vector of arrays
	std::vector<double> lineVec;
	std::vector<double*> Ctemp;

	//read first line
	fillVectorFromLineWhiteSpace(is, lineVec);

	numParams=lineVec.size();
	Ctemp.push_back(new double[numParams]);
	for(int i=0;i<numParams;++i) Ctemp[0][i]=lineVec[i];

	//read all the other lines
	getline(is, line);
	while(!stringContains(line, "c0")){
		trimString(line);
		if(!line.empty()){
			Ctemp.push_back(new double[numParams]);
			fillVectorFromStringWhiteSpace(line, lineVec);
			if(lineVec.size() != (std::vector<double>::size_type) numParams) throw "The file with the definitions of the matrices '"+matricesFilename+"' contains unequal number of values on the different lines specifying the matrix C!";
			for(int i=0; i<numParams; ++i)
				Ctemp[Ctemp.size()-1][i]=lineVec[i];
		}
		getline(is, line);
	}
	numStats=Ctemp.size();

	C.ReSize(numStats, numParams);
	for(int i=0; i<numStats; ++i){
		for(int j=0; j<numParams; ++j){
			C.element(i,j)=Ctemp[i][j];
		}
	}

	//clean up
	for(std::vector<double*>::iterator it=Ctemp.begin(); it!=Ctemp.end(); ++it){
		delete[] (*it);
	}

	//read the c0 vector
	c0.ReSize(numStats);

	fillVectorFromLineWhiteSpace(is, lineVec);
	if(lineVec.size() > (std::vector<double>::size_type) numStats) throw "The file with the definitions of the matrices '"+matricesFilename+"' contains too many values for c0!";
	if(lineVec.size() < (std::vector<double>::size_type) numStats) throw "The file with the definitions of the matrices '"+matricesFilename+"' contains too few values for c0!";
	for(int i=0; i<numStats; ++i) c0.element(i)=lineVec[i];

	//read variances
	line.clear();
	while(line.empty()){
		getline(is, line);
		trimString(line);
	}
	if(line!="Sigma") throw "The file with the definitions of the matrices '"+matricesFilename+"' does not contain the matrix Sigma (tag missing?)!";
	Sigma.ReSize(numStats);

	for(int l=0; l<numStats;++l){
		lineVec.clear();
		while(lineVec.empty()){
			fillVectorFromLineWhiteSpace(is, lineVec);
		}
		if(lineVec.empty()) throw "The file with the definitions of the matrices '"+matricesFilename+"' contains too few rows for Sigma!";
		if(lineVec.size() > (std::vector<double>::size_type) numStats) throw "The file with the definitions of the matrices '" + matricesFilename + "' contains too many values for Sigma on line " + toString(l) + "!";
		if(lineVec.size() < (std::vector<double>::size_type) numStats) throw "The file with the definitions of the matrices '" + matricesFilename + "' contains too few values for Sigma on line " + toString(l) + "!";
		for(int i=0; i<numStats; ++i) Sigma.element(l, i)=lineVec[i];
	}
	getline(is, line);
	trimString(line);
	if(!line.empty()) throw "The file with the definitions of the matrices '"+matricesFilename+"' contains too few rows for Sigma!!";
	is.close();
	//DONE reading matrix file....

	//check the number of passed parameters
	if(numPassedParams<(2+numParams)) throw "Too few parameters passed to the INTERNALGLM program!";
	if(numPassedParams>(2+numParams)) throw "Too many parameters passed to the INTERNALGLM program!";

	//prepare some matrices
	try{
		A=Cholesky(Sigma);
	} catch (...){
		throw "INTERNALGLM program: problem solving the Cholesky decomposition of Sigma!";
	}
	e.ReSize(numStats);
	P.ReSize(numParams);
	s.ReSize(numStats);
}
//---------------------------------------------------------------------------
void TGLM::simulate(std::string* passedParams, int & numPassedParams){
	//read params from input --> first two are filenames
	for(int i=0;i<numParams;++i)
		P.element(i)=atof(passedParams[i+2].c_str());

	for(int i=0;i<numStats;++i)
		e.element(i)=randomGenerator->getNormalRandom(0,1);
	e=A*e;

	//compute stats
	s=C*P+c0+e;
}

int TGLM::execute(std::string* passedParams, int numPassedParams){
	simulate(passedParams, numPassedParams);
	if(writeOutput) writeStatesToFile();
	return 0;
}

void TGLM::writeStatesToFile(){
	std::ofstream out;
	out.open(outputname.c_str());
	if(!out) throw "INTERNALGLM program: the output file '"+outputname+"' could not be opened!";
	out << "Stat_1";
	for(int i=1;i<numStats;++i){
		out << "\t" << "Stat_" << i+1;
	}
	out << std::endl;

	//write stats
	out << s.element(0);
	for(int i=1;i<numStats;++i){
		out << "\t" << s.element(i);
	}
	out << std::endl;
	out.close();
}

void TGLM::fillHeaderNames(std::vector<std::string> & nameContainer){
	nameContainer.clear();
	for(int i=0;i<numStats;++i){
		nameContainer.push_back("Stat_" + toString(i+1));
	}
}
void TGLM::fillValues(std::vector<double> & valueContainer){
	valueContainer.clear();
	for(int i=0;i<numStats;++i){
		valueContainer.push_back(s.element(i));
	}
}
void TGLM::fillValues(double* valueContainer){
	for(int i=0;i<numStats;++i){
		valueContainer[i] = s.element(i);
	}
}

//---------------------------------------------------------------------------
//TNormdist
//---------------------------------------------------------------------------
TNormdist::TNormdist(std::string* passedParams, int numPassedParams, TRandomGenerator* RandomGenerator){
	randomGenerator=RandomGenerator;
	if(numPassedParams!=4) throw "The INTERNALNORMDIST program requires 4 command line arguments (not " + toString(numPassedParams) + "): output.txt numSamples mean sd!";
	myExe="INTERNALNORMDIST";
	//stats
	sampleMean = 0;
	sampleSD = 0;

	//second argument is output file name
	outputname = passedParams[0];
}


void TNormdist::simulate(std::string* passedParams, int & numPassedParams){
	//read params from input. Order is: n, mean, sd
	int numSamples = stringToInt(passedParams[1]);
	double mean = stringToDouble(passedParams[2]);
	double sd = stringToDouble(passedParams[3]);

	//generate samples and calculate sample mean and sample sd
	sampleMean = 0;
	double* samples = new double[numSamples];
	for(int i=0; i<numSamples; ++i){
		samples[i]=randomGenerator->getNormalRandom(mean, sd);
		sampleMean += samples[i];
	}
	sampleMean /= (double) numSamples;
	sampleSD = 0;
	for(int i=0; i<numSamples; ++i){
		sampleSD += (samples[i]-sampleMean)*(samples[i]-sampleMean);
	}
	sampleSD = sqrt(sampleSD / (double) numSamples);
	delete[] samples;
}

int TNormdist::execute(std::string* passedParams, int numPassedParams){
	simulate(passedParams, numPassedParams);
	if(writeOutput) writeStatsToFile();
	return 0;
}

void TNormdist::writeStatsToFile(){
	std::ofstream out;
	out.open(outputname.c_str());
	if(!out) throw "INTERNALNORMDIST program: the output file '"+outputname+"' could not be opened!";
	out << "Mean\tSD" << std::endl;
	out << sampleMean << "\t" << sampleSD << std::endl;
	out.close();
}

void TNormdist::fillHeaderNames(std::vector<std::string> & nameContainer){
	nameContainer.clear();
	nameContainer.push_back("Mean");
	nameContainer.push_back("SD");
}
void TNormdist::fillValues(std::vector<double> & valueContainer){
	valueContainer.clear();
	valueContainer.push_back(sampleMean);
	valueContainer.push_back(sampleSD);
}
void TNormdist::fillValues(double* valueContainer){
	valueContainer[0] = sampleMean;
	valueContainer[1] = sampleSD;
}

