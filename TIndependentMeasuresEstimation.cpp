//---------------------------------------------------------------------------


#include "TIndependentMeasuresEstimation.h"
//---------------------------------------------------------------------------

TIndependentMeasuresEstimation::TIndependentMeasuresEstimation(TSimData* simData, ColumnVector* Theta, double DiracPeakWidth, bool ComputeTruncatedPrior, std::string OutputPrefix, TJointPosteriorSettings* JointPosteriorSettings, TRandomGenerator* RandomGenerator, TLog* logFile)
:TBaseEstimation(simData, Theta, DiracPeakWidth, ComputeTruncatedPrior, OutputPrefix, JointPosteriorSettings, RandomGenerator, logFile){
	v_jPrecomputed=false;
	marDensVecFilled=false;
	marDensPValueVecFilled=false;
	marDensPValueVec=NULL;
	marDensVec=NULL;
}
//---------------------------------------------------------------------------
bool TIndependentMeasuresEstimation::performPosteriorEstimation(bool writeRetainedSimulations, int numSimsForObsPValues){
	//fill matrices
	makeRejection(writeRetainedSimulations);

	if(computeSmoothedTruncatedPrior){
		calculateSmoothedRetainedMatrix();
		writeTruncatedPrior();
	}
	//check if a statistic is monomorphic
	if(mySimData->someStatsAreMonomorphic()){
	  logfile->warning("Some statistics are monomorphic among the retained simulations -> Skipping this dataset!");
	  return false;
	}

	//perform ABC-GLM regression
	if(!performLinearRegression(logfile->verbose())){
		logfile->warning("Regression failed -> Skipping this dataset!");
		return false;
	}
	calculatePosterior(logfile->verbose());

	//compute model fit
	calculateMarginalDensity(numSimsForObsPValues);

	//write estimation results
	std::string filename=outputPrefix+"MarginalPosteriorDensities.txt";
	ofstream marDensFile;
	marDensFile.open(filename.c_str());
	posteriorMatrix->writePosteriorFiles(marDensFile);
	writePosteriorCharacteristics(0);
	return true;
}
//---------------------------------------------------------------------------
void TIndependentMeasuresEstimation::makeRejection(bool writeRetainedSimulations){
	//prepare distance array
	double* distances=new double[mySimData->numReadSims];
	//TODO: allow for multiple sets with multiple observed values
	mySimData->calculateLargestDistances(0, mySimData->pointerToObsData->numObsDataSets, distances, logfile->verbose());
	//calculate distance and make rejection
	mySimData->fillStatAndParamMatrices(distances, logfile->verbose());
	//write retained, if requested
	if(writeRetainedSimulations) mySimData->writeRetained(outputPrefix, distances);
	delete[] distances;
}
//---------------------------------------------------------------------------
bool TIndependentMeasuresEstimation::calculate_T_j(SymmetricMatrix & m){
	m << mySimData->pointerToObsData->numObsDataSets*CHat.t()*SigmaSInv*CHat+SigmaThetaInv;
	double det=m.determinant();
	if(det==0){
		logfile->warning("Problem inverting Tau_J: determinant = 0!");
		return false;
	}
	try{
		m=m.i();
	} catch(...){
		logfile->warning("Problem inverting Tau_J: determinant = 0!");
		return false;
	}
	return true;
}
//---------------------------------------------------------------------------
//TODO: allow for multiple sets of observed data
ColumnVector TIndependentMeasuresEstimation::calculate_v_j(ColumnVector theta_j){
	if(!v_jPrecomputed){
		int i=0;
		mySimData->pointerToObsData->getObsValuesIntoColumnVector(i, obsValues);
		ColumnVector diff=obsValues-c_zero;
		for(i=1;i<mySimData->pointerToObsData->numObsDataSets;++i){
			mySimData->pointerToObsData->getObsValuesIntoColumnVector(i, obsValues);
			diff += obsValues - c_zero;
		}
		v_jPreComputedPart = CHat.t() * SigmaSInv * diff;
		v_jPrecomputed = true;
	}
	return v_jPreComputedPart + SigmaThetaInv * theta_j;
}
//---------------------------------------------------------------------------
void TIndependentMeasuresEstimation::calculateMarginalDensity(int numSimsForObsPValue){
   Matrix D = SigmaS+CHat*SigmaTheta*CHat.t();
   Matrix D_inv=D.i();

   marDensVec=new double[mySimData->pointerToObsData->numObsDataSets];
   marDensVecFilled=true;

   //compute p-value for each data set
   //global p-value will be the smallest fo all p-values
   if(numSimsForObsPValue>0){
		marDensPValueVec=new double[mySimData->pointerToObsData->numObsDataSets];
		marginalDensitiesOfRetained(numSimsForObsPValue, D, D_inv);
		marDensPValueVecFilled=true;
		marDensPValue=1;
   }

   ColumnVector oneObs;
   for(int z=0;z<mySimData->pointerToObsData->numObsDataSets;++z){
	   mySimData->pointerToObsData->getObsValuesIntoColumnVector(z, oneObs);
	   marDensVec[z]=marginalDensity(D, D_inv, oneObs);
   		if(numSimsForObsPValue>0){
   			marDensPValueVec[z]=0;
   			for(int i=0;i<numSimsForObsPValue;++i) if(fmFromRetained[i]<=marDensVec[z]) ++marDensPValueVec[z];
   			marDensPValueVec[z]=marDensPValueVec[z]/(double)numSimsForObsPValue;
   			if(marDensPValueVec[z]<marDensPValue) marDensPValue=marDensPValueVec[z];
   		}
   }

   //Calculate the marginal density for the combined data set
   //get all data sets into column vectors;
   ColumnVector* allObservedValues=new ColumnVector[mySimData->pointerToObsData->numObsDataSets];
   for(int z=0;z<mySimData->pointerToObsData->numObsDataSets;++z){
	   mySimData->pointerToObsData->getObsValuesIntoColumnVector(z, allObservedValues[z]);
   }
   //loop over all dirac peaks
   ColumnVector theta_j, m_j;
   marDens=0.0;
   for(int d=1; d<=mySimData->numUsedSims;++d){
	   theta_j=mySimData->paramMatrix.submatrix(d,d,2,mySimData->numParams+1).as_column();
	   m_j=c_zero+CHat*theta_j;
	   double f_M_temp=1;
	   for(int z=0;z<mySimData->pointerToObsData->numObsDataSets;++z){
		   Matrix temp=-0.5*(allObservedValues[z]-m_j).t()*D_inv*(allObservedValues[z]-m_j);
		   f_M_temp*=exp(temp(1,1));
	   }
	   marDens+=f_M_temp;
   }
   delete[] allObservedValues;
   marDens=marDens/(mySimData->numUsedSims*pow((double)sqrt((2*3.14159265358979323846*D).determinant()),(int)mySimData->pointerToObsData->numObsDataSets));
   marDens=marDens*((double)mySimData->numUsedSims/(double)mySimData->numReadSims);
}
//---------------------------------------------------------------------------
