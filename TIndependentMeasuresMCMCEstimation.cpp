//---------------------------------------------------------------------------


#include "TIndependentMeasuresMCMCEstimation.h"

TIMDataSet::TIMDataSet(int NumRetainedSims, double* c_j_exponents, Matrix & CHat, ColumnVector & c_zero, Matrix & SigmaSInv, Matrix SigmaThetaInv, ColumnVector obsValues, SymmetricMatrix T_j, Matrix paramMatrix){
	numRetainedSims = NumRetainedSims;

	//store matrices
	_CHat = CHat;
	_c_zero = c_zero;
	_SigmaSInv = SigmaSInv;
	_SigmaThetaInv = SigmaThetaInv;
	_obsValues = obsValues;
	_T_j = T_j;
	_T_j_inv = _T_j.i();
	_paramMatrix = paramMatrix;
	_numParamCols = _paramMatrix.ncols();

	//store exponents
	_c_j_exponents = new double[numRetainedSims];
	for(int d = 0; d < numRetainedSims; ++d)
		_c_j_exponents[d] = c_j_exponents[d];
}


double	TIMDataSet::calcScaledLogPosteriorDensity(const ColumnVector & theta){
	double dens = 0.0;

	//loop over all dirac peaks
	for(int d = 1; d <= numRetainedSims; ++d){
		//theta_j is a vector with the parameters of one simulation / from the uniform matrix
		ColumnVector theta_j = _paramMatrix.submatrix(d,d,2, _numParamCols).as_column();
		ColumnVector v_j = _CHat.t() * _SigmaSInv * (_obsValues - _c_zero) + _SigmaThetaInv * theta_j;
		ColumnVector t_j = theta - _T_j * v_j;
		dens += exp((_c_j_exponents[d-1] - 0.5 * t_j.t() * _T_j_inv * t_j).AsScalar());
	}
	return log(dens);
}

void TIMDataSet::fillWithFirstSimulation(ColumnVector & theta){
	theta = _paramMatrix.submatrix(1,1,2, _numParamCols).as_column();
};


//---------------------------------------------------------------------------
TIndependentMeasuresMCMCEstimation::TIndependentMeasuresMCMCEstimation(TParameters & params, TSimData* simData, ColumnVector* Theta, double DiracPeakWidth, bool ComputeTruncatedPrior, std::string OutputPrefix, TJointPosteriorSettings* JointPosteriorSettings, TRandomGenerator* RandomGenerator, TLog* logFile)
:TBaseEstimation(simData, Theta, DiracPeakWidth, ComputeTruncatedPrior, OutputPrefix, JointPosteriorSettings, RandomGenerator, logFile){

	//read MCMC params
	 MCMC_length = params.getParameterLongWithDefault("mcmcLength", 10000);
	 thinning = params.getParameterIntWithDefault("thinning", 10);
	 burnin = params.getParameterIntWithDefault("burnin", 500);
	 numBurnins = params.getParameterIntWithDefault("numBurnin", 3);


	 //proposal range per param (will be updated after burnin)
	 double proposalRange = params.getParameterDoubleWithDefault("propRange", 0.1);
	 proposalRanges = new double[mySimData->numParams];
	 acceptanceRates = new double[mySimData->numParams];
	 for(int p=0; p<mySimData->numParams; ++p){
		 proposalRanges[p] = proposalRange;
		 acceptanceRates[p] = 0.0;
	 }


	/*
	 * TODO: read prior from est file. See also below.
	//read .est file
	my_string estName=parameters->getParameter("estName");
	myPriors=new TPriorVector(estName, logFile, randomGenerator);
	//check if all parameters, for which stimates are desired, are present in the est file
	for(int i=0; i<mySimData->numParams;++i){
		if(!myPriors->isPrior(mySimData->paramNames[i])) throw "No prior distribution defined for parameter '" + mySimData->paramNames[i] + "')!";
	}
	*/
}

//---------------------------------------------------------------------------
void TIndependentMeasuresMCMCEstimation::createModelsForEachDataSet(bool writeRetainedSimulations, int numSimsForObsPValue, int numReplicatesPValTukeyDepth){
	//TODO: add obs p-value!

	//prepare distance array
	myDataSets.clear();
	double* tempDistances = new double[mySimData->numReadSims];

	//open Tukey file
	std::string filename = outputPrefix + "TukeyDepth.txt";
	std::ofstream tukeyOut(filename.c_str());
	tukeyOut << "ObsNum\tTukey";
	if(numReplicatesPValTukeyDepth > 0)
		tukeyOut << "\tp-val";
	tukeyOut << "\n";

	//reject for each data set and perform linear regression
	logfile->startIndent("Fitting GLM regression model for " + toString(mySimData->pointerToObsData->numObsDataSets) + " data set:");
	for(int i=0; i<mySimData->pointerToObsData->numObsDataSets; ++i){
			logfile->startIndent("Fit model for observed data set " + toString(i) + ":");
			mySimData->getObsStatValuesIntoColumnVector(i, obsValues);

			//calculate distance and make rejection
			mySimData->calculateDistances(i, tempDistances, logfile->verbose());
			mySimData->fillStatAndParamMatrices(tempDistances, logfile->verbose());

			//write retained, if requested
			if(writeRetainedSimulations) mySimData->writeRetained(outputPrefix, tempDistances, i);
			//mySimData->writeRetainedScaled(outputPrefix, tempDistances, thisObsDataSet);

			//Calculate Tukey depth
			logfile->startIndent("Calculating Tukey depth of observed data:");
			tukeyDepth = mySimData->computeTukeyDepthOfObservedAmongRetained(i);
			logfile->list("Tukey depth = ", tukeyDepth);
			tukeyOut << i+1 << "\t" << tukeyDepth;
			if(numReplicatesPValTukeyDepth > 0){
				tukeyDepthPValue = mySimData->computePValueOfTukeyDepthAmongRetained(tukeyDepth, numReplicatesPValTukeyDepth);
				tukeyOut << "\t" << tukeyDepthPValue;
			}
			tukeyOut << "\n";
			logfile->endIndent();

			//perform linear regression
			performLinearRegression(true);

			//claculate T_j
			if(!calculate_T_j(T_j)) throw "Aborting estimation!";

			//fill exponents for Q_j
			fillExponents_Q_j();

			//save model for each data set
			myDataSets.emplace_back(mySimData->numUsedSims, exponents, CHat, c_zero, SigmaSInv, SigmaThetaInv, obsValues, T_j, mySimData->paramMatrix);

			logfile->endIndent();
	}
	logfile->endIndent();

	//close tukey file
	tukeyOut.close();
}

//---------------------------------------------------------------------------
bool TIndependentMeasuresMCMCEstimation::performPosteriorEstimation(bool writeRetainedSimulations, int numSimsForObsPValue, int numReplicatesPValTukeyDepth){
	//first generate all models
	createModelsForEachDataSet(writeRetainedSimulations, numSimsForObsPValue, numReplicatesPValTukeyDepth);

/*
 * TODO: let users choose different priors as read from est file
	//store pointers to priors
   TSimplePrior** pointersToSimplePriors;
   pointersToSimplePriors = new TSimplePrior*[mySimData->paramsStandardized];
   for(int i=0; i<mySimData->numParams;++i){
	   pointersToSimplePriors[i]=myPriors->getSimplePriorFromName(mySimData->paramNames[i]);

   }
   //and scale them as is done for the simData
   if(mySimData->paramsStandardized){
	   for(int i=0; i<mySimData->numParams;++i){
		   myPriors->getSimplePriorFromName(mySimData->paramNames[i])->scale(mySimData->paramMaxima[i], mySimData->paramMinima[i]);
	   }
   }
   logfile->write(" done!");
   */

	//open output file for MCMC samples
	std::string filename = outputPrefix + "MCMC.txt";
	std::ofstream mcmcOut(filename.c_str());
	for(int p=0; p<mySimData->numParams; ++p){
		if(p>0) mcmcOut << "\t";
		mcmcOut << mySimData->paramNames[p];
	}
	mcmcOut << "\n";

	//choose a position from the retained sims of data set one....
	ColumnVector theta;
	myDataSets[0].fillWithFirstSimulation(theta);

	//Run burnin
	if(burnin > 0 && numBurnins > 0){
		logfile->startIndent("Performing " + toString(numBurnins) + " burnin(s) of " + toString(burnin) + " iterations each:");

		for(int b=0; b<numBurnins; ++b){
			runMCMC(theta, burnin, mcmcOut, true);

			//update proposal ranges
			logfile->startIndent("Acceptance rates were:");
			for(int p=0; p<mySimData->numParams; ++p){
				proposalRanges[p] *= 3.0 * acceptanceRates[p];
				logfile->list(mySimData->paramNames[p] + " = " + toString(acceptanceRates[p]));
			}
			logfile->endIndent();
		}
		logfile->endIndent();
	}


	//Run MCMC
	runMCMC(theta, MCMC_length, mcmcOut, true); //true only for debug!

	//print acceptance rates
	for(int p=0; p<mySimData->numParams; ++p)
		logfile->conclude("Acceptance rate " + mySimData->paramNames[p] + " = " + toString(acceptanceRates[p]));


	/*
   //calculate posterior
   float bandwidthSquare=0.01;
   logfile->flush((my_string)"   - calculating posterior densities for " + mySimData->numParams + " parameters ...");
   preparePosteriorDensityPoints();
   posteriorMatrix=Matrix(posteriorDensityPoints, mySimData->numParams);
   posteriorMatrix=0.0;
   for(int p=1; p<=mySimData->numParams;++p){
	   for(int i=1; i<=floor(MCMCLength/thinning);++i){
		   for(int k=1; k<=posteriorDensityPoints; ++k){
			   posteriorMatrix(k,p)+=exp(-0.5*(savedMCMCSteps(i,p)-theta(k))*(savedMCMCSteps(i,p)-theta(k))/bandwidthSquare);
		   }
	   }
    }
   logfile->write(" done!");

   //write posterior
   writePosteriorFiles();
   writePosteriorCharacteristics();
   //calculate marginal density
   //printMarginalDensity(0);
*/

   return true;
};


void TIndependentMeasuresMCMCEstimation::runMCMC(ColumnVector & curTheta, long steps, std::ofstream & mcmcOut, bool write){
	int prog, oldProg;
	oldProg = 0;
	clock_t starttime=clock();
	logfile->listFlush("Running an MCMC with " + toString(steps) + " steps ... (0%)");

	for(int p=0; p<mySimData->numParams; ++p)
		acceptanceRates[p] = 0.0;

	//old theta
	ColumnVector oldTheta = curTheta;

	//hasting terms
	double oldLogHastingTerm = getLogHastingTerm(oldTheta);
	double curLogHastingTerm;

	//run MCMC
	for(int i=0; i<steps;++i){
	   //update each parameter in turn
	   for(int p=0; p<mySimData->numParams; ++p){
		   //propose new value
		   curTheta(p+1) += randomGenerator->getRand() * proposalRanges[p] - 0.5 * proposalRanges[p];

		   //calculate hastings
		   curLogHastingTerm = getLogHastingTerm(curTheta);

		   //accept or reject
	   	   if(log(randomGenerator->getRand()) < (curLogHastingTerm - oldLogHastingTerm)){
	   		   oldLogHastingTerm = curLogHastingTerm;
	   		   oldTheta = curTheta;
	   		   ++acceptanceRates[p];
	   	   } else {
	   		   curTheta = oldTheta;
	   	   }
	   }

	   if(write && i % thinning == 0){
		   for(int p=0; p<mySimData->numParams; ++p){
			   if(p>0) mcmcOut << "\t";
			   mcmcOut << posteriorMatrix->paramRanges->toParamScale(p+1, oldTheta(p+1));
		   }
		   mcmcOut << std::endl;
	   }

	   //report progress
	   prog = 100 * (float) i / (float) steps;
	   if(prog > oldProg){
		   oldProg = prog;
		   logfile->listOverFlush("Running an MCMC with " + toString(steps) + " steps ... (" + toString(prog) +"%)");
	   }
	}

	//update acceptance rates
	for(int p=0; p<mySimData->numParams; ++p)
		acceptanceRates[p] /= (double) steps;

	float runtime = (float) (clock()-starttime)/CLOCKS_PER_SEC/60;
	logfile->overList("Running an MCMC with " + toString(steps) + " steps ... done in " + toString(runtime) + " min!    ");
}

//---------------------------------------------------------------------------
double TIndependentMeasuresMCMCEstimation::getLogHastingTerm(const ColumnVector & theta){

	/*
	 * TDOD: prios....
	if(mySimData->paramsStandardized){
		for(int i=0; i<mySimData->numParams;++i){
			curTheta(i+1)=(myPriors->getValue(mySimData->paramNames[i])-mySimData->paramMinima[i])/(mySimData->paramMaxima[i]-mySimData->paramMinima[i]);
		}
	} else{
		for(int i=0; i<mySimData->numParams;++i)
			curTheta(i+1)=myPriors->getValue(mySimData->paramNames[i]);
	}
	*/

	double density = 1.0;

	//get posterior densities of each data set
	for(std::vector<TIMDataSet>::iterator it=myDataSets.begin(); it!=myDataSets.end(); ++it)
		density += it->calcScaledLogPosteriorDensity(theta);

	return density;
}

//---------------------------------------------------------------------------
/*
void TIndependentMeasuresMCMCEstimation::printMarginalDensity(int thisObsDataSet){
   double f_M=0.0;
   Matrix D = SigmaS+CHat*SigmaTheta*CHat.t();
   Matrix D_inv=D.i();
   ColumnVector theta_j, m_j;
   //get mean of s_obs
   ColumnVector meanObs;

   if(calcObsPValue) calculateMarginalDensitiesOfRetained(D, D_inv);

   logfile->write("   - marginal density: ");
   for(int z=0;z<myObsData->numObsDataSets;++z){
	   myObsData->getObsValuesIntoColumnVector(z, meanObs);
        //loop over all dirac peaks
   		f_M=0.0;
   		for(int d=1; d<=mySimData->numUsedSims;++d){
   			theta_j=mySimData->paramMatrix.submatrix(d,d,2,mySimData->numParams+1).as_column();
   			m_j=c_zero+CHat*theta_j;
   			Matrix temp=-0.5*(meanObs-m_j).t()*D_inv*(meanObs-m_j);
   			f_M+=exp(temp(1,1));
   		}
   		f_M=f_M/(mySimData->numUsedSims*sqrt((2*3.14159265358979323846*D).determinant()));
   		f_M=f_M*((double)mySimData->numUsedSims/(double)mySimData->numReadSims);
   		if(calcObsPValue){
   			float p=0;
   			for(int i=0;i<calcObsPValue;++i) if(fmFromRetained[i]<=f_M) ++p;
   			logfile->write((my_string)"        - Obs_" + z + "\t" + (my_string) f_M + "\t(p-value " + (my_string) (p/(float)calcObsPValue) + ")");
   			mdFile << thisObsDataSet << "\t" << f_M << "\t" << p/(float)calcObsPValue << endl;
   		} else {
   			logfile->write((my_string)"        - Obs_" + z + "\t" + (my_string) f_M);
   			mdFile << thisObsDataSet << "\t" << f_M << endl;
   		}
   }
   mdFile.close();
}
*/
//---------------------------------------------------------------------------

