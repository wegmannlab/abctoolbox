//---------------------------------------------------------------------------

#ifndef TIndependentMeasuresMCMCEstimationH
#define TIndependentMeasuresMCMCEstimationH
#include "TBaseEstimation.h"
//#include "TPriorVector.h"
#include <sstream>


//---------------------------------------------------------------------------
class TIMDataSet{
private:
	int numRetainedSims;
	double* _c_j_exponents;
	ColumnVector _c_zero;
	Matrix _CHat;
	Matrix _SigmaSInv;
	Matrix _SigmaThetaInv;
	Matrix _paramMatrix;
	ColumnVector _obsValues;
	Matrix _T_j, _T_j_inv;
	int _numParamCols;

public:
	TIMDataSet(int NumRetainedSims, double* c_j_exponents, Matrix & CHat, ColumnVector & c_zero, Matrix & SigmaSInv, Matrix SigmaThetaInv, ColumnVector obsValues, SymmetricMatrix T_j, Matrix paramMatrix);

	double calcScaledLogPosteriorDensity(const ColumnVector & theta);
	void fillWithFirstSimulation(ColumnVector & theta);
};

//---------------------------------------------------------------------------
class TIndependentMeasuresMCMCEstimation:public TBaseEstimation {
private:
	std::vector<TIMDataSet> myDataSets;

	long MCMC_length;
	long burnin;
	int numBurnins;
	int thinning;
	double* proposalRanges;
	double* acceptanceRates;

	void createModelsForEachDataSet(bool writeRetainedSimulations, int numSimsForObsPValue, int numReplicatesPValTukeyDepth);
	void runMCMC(ColumnVector & curTheta, long steps, std::ofstream & mcmcOut, bool write);
	double getLogHastingTerm(const ColumnVector & theta);

public:

	TIndependentMeasuresMCMCEstimation(TParameters & params, TSimData* simData, ColumnVector* Theta, double DiracPeakWidth, bool ComputeTruncatedPrior, std::string OutputPrefix, TJointPosteriorSettings* JointPosteriorSettings, TRandomGenerator* RandomGenerator, TLog* logFile);
	  ~TIndependentMeasuresMCMCEstimation(){
		delete[] proposalRanges;
		delete[] acceptanceRates;
	  };
	  bool performPosteriorEstimation(bool writeRetainedSimulations, int numSimsForObsPValue, int numReplicatesPValTukeyDepth);
};


#endif

