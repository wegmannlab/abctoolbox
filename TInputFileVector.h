//---------------------------------------------------------------------------

#ifndef TInputFileVectorH
#define TInputFileVectorH
#include "TLog.h"
#include <vector>
#include <math.h>
#include "TPrior.h"
#include "stringFunctions.h"
#include "TParameters.h"
#include "TExecuteProgram.h"
#include "TProgram.h"

//---------------------------------------------------------------------------
//a single input file. One TInputfile may actually be a collection of files
class TSimulationProgramInputFileLine{
public:
	std::vector<std::string> fragments;
	std::vector<bool> isParameterTag;
	std::vector<bool>::iterator paramTagIt;
	std::vector<TPrior*> parameterPointer;
	std::vector<TPrior*>::iterator paramPointerIt;
	TSimulationProgramInputFileLine(std::string line, TPriorVector* gotPriors);
	void writeLine(std::ofstream& out);
	void addRequiredParameters(std::vector<TPrior*> & RequiredParams);
};

class TSimulationProgramInputFile{
public:
	std::string simulationProgramInputFilename;
	std::string newSimulationProgramInputFilename;
	std::vector<TSimulationProgramInputFileLine> lines;

	TSimulationProgramInputFile(std::string Name, TPriorVector* gotPriors, std::string & suffix);
	void createNewSimulationProgramInputFile();
	void addRequiredParameters(std::vector<TPrior*> & RequiredParams);
};
//---------------------------------------------------------------------------
class TInputFile{
	private:
		std::vector<TSimulationProgramInputFile> simulationProgramInputFiles;
		bool tagsToInitializeProgramsPrepared;
		std::map<std::string, std::string> initializationTags, executingTags;
		TPriorVector* priors;
		std::vector<TPrior*> requiredParameters;
		std::vector<TPrior*>::iterator requiredParametersIt;
		TProgram simulationProgram;
		TProgram scriptAfterSimulations;
		TProgram scriptBeforeSimulations;
		TLog* logfile;
		TRandomGenerator* randomGenerator;

	public:
	  bool hasChanged;

	  TInputFile(std::vector<std::string> Siminputnames, TPriorVector* gotPriors, TLog* Logfile, std::string suffix, TRandomGenerator* RandomGenerator);
	  ~TInputFile(){};

	  void writeNamesToLogfile();
	  void setOutputFile(int OutputFileDescriptor);
	  void setSimulationProgramRedirection(std::string & filename);
	  void setExternalErrorhandling(std::string & externalErrorHandling);
	  void createNewInputFiles();
	  void writeNewInputFiles();
	  std::string getFirstSimulationProgramInputFile();
	  void fillTagsToInitializePrograms();
	  void updateExecutingTags(int simnum, int & index);
	  void initializeProgram(TProgram & progPointer, std::string & simulationprogram, std::vector<std::string> & gotParameter);
	  void initializeSimulationProgramm(std::string & simulationprogram, std::vector<std::string> & gotParameter);
	  void initializeScriptAfterSimulation(std::string & gotScript, std::vector<std::string> & gotParameter);
	  void initializeScriptBeforeSimulation(std::string & gotScript, std::vector<std::string> & gotParameter);
	  void writeCommandLines(int & simnum, int & index);
	  void performSimulation(int simnum, int & index);
	  void splitParamString(std::vector<std::string> & vec,  std::string & gotParameter);
	  std::string getSimProgram();
	  TProgram* getPointerToSimulationProgram(){return &simulationProgram;};
};
//---------------------------------------------------------------------------
class TInputFileVector{
	public:
	  std::vector<TInputFile*> vecInputFileObjects;
	  std::vector<TInputFile*>::iterator curInputFileObject;
	  TLog* logFile;
	  TPriorVector* priors;
	  std::string estFile;
	  bool launchScriptAfterSimulation;
	  bool launchScriptBeforeSimulation;
	  bool alsoLaunchedWhenNoParamChanged;
	  TRandomGenerator* randomGenerator;
	  std::string tempSuffix;

	  TInputFileVector(TParameters* gotParameters, TLog* gotLogFile, TRandomGenerator* RandomGenerator, std::string & externalErrorHandling, int & childOutputfileDescriptor, std::string Suffix="-temp");
	  ~TInputFileVector(){
		  delete priors;
		  for(curInputFileObject=vecInputFileObjects.begin(); curInputFileObject!=vecInputFileObjects.end(); ++curInputFileObject)
			  delete *curInputFileObject;
	  }
	  void initializeSimulationPrograms(TParameters* gotParameters);
	  int numInputFiles(){return vecInputFileObjects.size(); }
	  void setOutputFile(int OutputFileDescriptor);
	  void writeNewInputFiles(int simnum);
	  void createNewInputFiles(int simnum);
	  void createNewInputFilesMcmc(int simnum);
	  void createNewInputFilesMcmcUpdateOnePriorOnly(int simnum);
	  bool creatNewInputFilesPMC(double* newParams, int simnum);
	  void getNewValueMcmc(const int& priorNumber);
	  void performSimulations(int simnum=-1);
	  void writeUsedCommandLines(int simnum=-1);
	  std::vector<std::string> getVectorOfInputFileNames();
	  std::string getSimplePriorName(int num);
	  double getLogPriorDensity();
	  double getLogPriorDensity(double* values);
	  int getNumberOfSimplePriorFromName(std::string name);
	  void setPriorValue(int priorNumber, double value);
      void updateCombinedParameters();
      TProgram* getLinkToSimulationProgramForDataObject();
};





#endif
