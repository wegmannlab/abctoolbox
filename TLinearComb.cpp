//---------------------------------------------------------------------------
#include "TLinearComb.h"
//---------------------------------------------------------------------------
TLinearComb::TLinearComb(std::string gotLinearCombFileName, std::vector<std::string> NamesInInputFiles, int maxLinearCombToUse){
		LinearCombFileName=gotLinearCombFileName;
		namesInInputFiles=NamesInInputFiles;
		linearCombFileRead=false;
		obsLinearCombComputed=false;
		readLinearCombFile();
		maxNumCombToUse=maxLinearCombToUse;
		if(maxNumCombToUse<0) maxNumCombToUse=numLinearComb;
		maxNumCombToUse=numLinearComb;
		simPCA=new double[maxNumCombToUse];
		oldSimPCA=new double[maxNumCombToUse];
}
//------------------------------------------------------------------------------
void TLinearComb::readLinearCombFile(){
		numLinearComb=0;
		std::string line;
		std::vector<std::string> vecLine;
		std::vector<double> tempPCAVector;
		std::vector<double*> pcaVector;
		std::vector<double*>::iterator curPCA, endPCA;
		std::vector<double> statMeanVector;
		std::vector<double> statSdVector;

		//open LinearCombFile
		std::ifstream linearCombFile (LinearCombFileName.c_str()); // opening the file for reading
		if(!linearCombFile)
		   throw "The Linear-Combination-File '" + LinearCombFileName + "' can not be read!";

		//read PCA File
		double temp;
		int lineNum=0;
		while(linearCombFile.good() && !linearCombFile.eof()){
			++lineNum;
			getline(linearCombFile, line);
			line=extractBeforeDoubleSlash(line);
			trimString(line);
			if(!line.empty()){
				//split by white space
				fillVectorFromStringWhiteSpace(line, vecLine);
				if(vecLine.size() < 4) throw "Less than four entries on line " + toString(lineNum) + "!";
				//read name
				statNamesVector.push_back(vecLine[0]);

				//read the mean
				statMeanVector.push_back(stringToDouble(vecLine[1]));

				//read the sd
				temp=stringToDouble(vecLine[2]);
				if(temp<=0.0) throw "SD of zero in the Linear-Combination-File '" + LinearCombFileName + "' on line " + toString(lineNum) + "! Has a Box-Cox transformation been used?";
				statSdVector.push_back(temp);

				//read linear combination into temp vector
				tempPCAVector.clear();
				std::vector<std::string>::iterator it=vecLine.begin();

				for(std::vector<std::string>::size_type i=3; i<vecLine.size(); ++i){
					if(it->empty()) break;
					tempPCAVector.push_back(stringToDouble(vecLine[i]));
				}

				//create linear combination array
				if((int)tempPCAVector.size()==numLinearComb || numLinearComb==0){
				   numLinearComb=tempPCAVector.size();
				   //put into array for fast calculations
				   double* tempArray=new double[numLinearComb];
				   for(int i=0; i<numLinearComb; ++i) tempArray[i]=tempPCAVector[i];
				   pcaVector.push_back(tempArray);
				} else
				   throw "Unequal number of columns among the lines in the Linear-Combination-File '" + LinearCombFileName + "'!";
			}
		}
		linearCombFile.close();

		//check stats
		std::string missingStat=getFirstMissingStat();
		if(!missingStat.empty())
		   throw "The summary statistic '"+ missingStat +"' is missing in the simulated File, but required by the Linear-Combination-File '"+ LinearCombFileName + "'!";


		//write pca into an array.
		//The order of the array is similar to the order in the inputFile
		//all stats have an array which contains zeros if the stat ist not specified in the PCA_file
		linearCombFileRead=true;
		endPCA=pcaVector.end();
		pcaVectorSize=pcaVector.size();
		pca=new double*[pcaVectorSize];
		mean=new double[pcaVectorSize];
		sd=new double[pcaVectorSize];
		numStats=namesInInputFiles.size();
		statIsUsed=new int[numStats];
		int k=0;
		int j=0;
		bool* usedPCAVectors=new bool[pcaVectorSize];
		for(int i=0; i<pcaVectorSize; ++i) usedPCAVectors[i]=false;
		for(curNameInInputFiles=namesInInputFiles.begin();curNameInInputFiles!=namesInInputFiles.end(); ++curNameInInputFiles, ++j){
			curPCA=pcaVector.begin();
			statIsUsed[j]=-1;
			for(int i=0;curPCA!=endPCA; ++curPCA, ++i){
				if(statNamesVector[i]==*curNameInInputFiles){
					pca[k]=*curPCA;
					usedPCAVectors[i]=true;
					mean[k]=statMeanVector[i];
					sd[k]=statSdVector[i];
					statIsUsed[j]=k;
					++k;
				}
			}
		}
		for(int i=0; i<pcaVectorSize; ++i){
			if(!usedPCAVectors[i]) delete[] pcaVector[i];
		}
		delete[] usedPCAVectors;
		linearCombVariances = new double[numLinearComb];
}
//------------------------------------------------------------------------------
void TLinearComb::calcObsLinearComb(std::vector<double> obsData){
	//order has to be the same as the names passed to the constructor!!!
	if(!obsLinearCombComputed){
		obs=new double[numStats];
		obsPCA=new double[maxNumCombToUse];
		obsLinearCombComputed=true;
	}
	int i=0;
	for(std::vector<double>::iterator it = obsData.begin(); it!=obsData.end(); ++it, ++i)
		obs[i] = *it;

	//calculate pca for obsData
	for(int i=0; i<maxNumCombToUse; ++i){
		obsPCA[i] = getPCA(i, obs);
	}
}
//------------------------------------------------------------------------------
int TLinearComb::checkName(std::string name){
	for(curStatName=statNamesVector.begin(); curStatName!=statNamesVector.end(); ++curStatName){
		if(*curStatName==name) return true;
	}
	return false;
}
//------------------------------------------------------------------------------
int TLinearComb::getStatColNumberFromName(std::string name){
	int i=0;
	for(curStatName=statNamesVector.begin(); curStatName!=statNamesVector.end(); ++curStatName, ++i){
		if(*curStatName==name) return i;
	}
	return -1;
}
//------------------------------------------------------------------------------
int TLinearComb::getNumberOfFirstUsedStat(){
	for(int i=0; i<numStats; ++i){
		if(statIsUsed[i]>=0) return i;
	}
	return numStats+1;
}
//------------------------------------------------------------------------------
std::string TLinearComb::getFirstMissingStat(){
   int check;
   for(curStatName=statNamesVector.begin();curStatName!=statNamesVector.end(); ++curStatName){
	  curNameInInputFiles=namesInInputFiles.begin();
	  check=false;
	  for(statNamesVector.begin();curNameInInputFiles!=namesInInputFiles.end(); ++curNameInInputFiles){
		if(*curNameInInputFiles==*curStatName){
		   check=true;
		   break;
		}
	  }
	  if(!check) return *curStatName;
   }
   return "";
}
//------------------------------------------------------------------------------
void TLinearComb::calcSimDataPCA(double** simData){
   for(int i=0; i<maxNumCombToUse; ++i) simPCA[i]=getPCA(i, simData);
}
void TLinearComb::calcSimDataPCA(double* simData){
   for(int i=0; i<maxNumCombToUse; ++i) simPCA[i]=getPCA(i, simData);
}
void TLinearComb::calcSimDataPCA(std::vector<double> & simData){
   for(int i=0; i<maxNumCombToUse; ++i) simPCA[i]=getPCA(i, simData);
}
//------------------------------------------------------------------------------
double TLinearComb::calculateDistance(bool & standardize){
	double dis=0;
	if(standardize){
	   for(int i=0; i<maxNumCombToUse; ++i){
		  dis+=(obsPCA[i]-simPCA[i])*(obsPCA[i]-simPCA[i])/linearCombVariances[i];
	   }
	} else {
		for(int i=0; i<maxNumCombToUse; ++i){
		   dis+=(obsPCA[i]-simPCA[i])*(obsPCA[i]-simPCA[i]);
	   }
	}
	return sqrt(dis);
}

double TLinearComb::calculateDistance(double** simData, bool & standardize){
  //calculate transformation
  calcSimDataPCA(simData);
  //calculate distance
  return calculateDistance(standardize);
}
double TLinearComb::calculateDistance(double* simData, bool & standardize){
  //calculate transformation
  calcSimDataPCA(simData);
  //calculate distance
  return calculateDistance(standardize);
}
//------------------------------------------------------------------------------
void TLinearComb::writeHeader(std::ofstream& ofs){
   for(int i=0; i<maxNumCombToUse; ++i){
	  ofs << "\t" << "LinearCombination_" << i;
   }
}
//------------------------------------------------------------------------------
void TLinearComb::writeSimPCA(std::ofstream& ofs){
   for(int i=0; i<maxNumCombToUse; ++i){
	  ofs << "\t" << simPCA[i];
   }
}
//------------------------------------------------------------------------------
//Functions to calculate the linear combinations
//!!!!change both or none!!!
double TLinearComb::getPCA(int & num, double* stats){
	 double pcaCalc=0;
	 int stat=0;
	 for(int i=0; i<numStats; ++i){
		if(statIsUsed[i]>=0){
			stat = statIsUsed[i];
			pcaCalc += pca[stat][num]*(stats[i]-mean[stat])/sd[stat];
		}
	 }
	 return pcaCalc;
}
double TLinearComb::getPCA(int & num, double** stats){
	double pcaCalc=0;
	int stat=0;
	for(int i=0; i<numStats; ++i){
		if(statIsUsed[i]>=0){
			stat = statIsUsed[i];
			pcaCalc+= pca[stat][num]*(*stats[i]-mean[stat])/sd[stat];
		}
	}
	return pcaCalc;
}
double TLinearComb::getPCA(int & num, std::vector<double> & stats){
	double pcaCalc=0;
	int stat=0;
	for(int i=0; i<numStats; ++i){
		if(statIsUsed[i]>=0){
			stat = statIsUsed[i];
			pcaCalc+= pca[stat][num]*(stats[i]-mean[stat])/sd[stat];
		}
	}
	return pcaCalc;
}
//------------------------------------------------------------------------------
void TLinearComb::saveOldValues(){
   for(int i=0; i<maxNumCombToUse; ++i){
	  oldSimPCA[i]=simPCA[i];
   }
}
void TLinearComb::resetOldValues(){
   for(int i=0; i<maxNumCombToUse; ++i){
	  simPCA[i]=oldSimPCA[i];
   }
}



