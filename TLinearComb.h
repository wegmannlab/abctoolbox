//---------------------------------------------------------------------------

#ifndef TLinearCombH
#define TLinearCombH
//---------------------------------------------------------------------------
#include <fstream>
#include <sstream>
#include <vector>
#include <math.h>
#include "stringFunctions.h"

class TLinearComb{
	public:
		std::vector<std::string> statNamesVector;
		std::vector<std::string>::iterator curStatName;
	   double* mean;
	   double* sd;
	   double* obs;
	   std::vector<double>::iterator curDouble, endDouble;
	   std::vector<std::string> namesInInputFiles;
	   std::vector<std::string>::iterator curNameInInputFiles;
	   std::string LinearCombFileName;
	   double** pca;
	   int* statIsUsed; // if>0 it points to the row in the linear comb file
	   int numLinearComb;
	   int maxNumCombToUse;
	   int numParams;
	   int numStats;
	   double* linearCombVariances;
	   double* obsPCA;
	   double* simPCA;
	   double* oldSimPCA;
	   bool linearCombFileRead;
	   int pcaVectorSize;
	   bool obsLinearCombComputed;

	   TLinearComb(){
		   mean=NULL;
		   sd=NULL;
		   obs=NULL;
		   pca=NULL;
		   statIsUsed=NULL;
		   numLinearComb=0;
		   maxNumCombToUse=0;
		   numParams=0;
		   numStats=0;
		   linearCombVariances=NULL;
		   obsPCA=NULL;
		   simPCA=NULL;
		   oldSimPCA=NULL;
		   linearCombFileRead=false;
		   pcaVectorSize=0;
		   obsLinearCombComputed=false;
	   };
	   TLinearComb(std::string gotLinearCombFileName, std::vector<std::string> NamesInInputFiles, int maxLinearCombToUse=-1);
	   virtual ~TLinearComb(){
		   if(linearCombFileRead){
			   for(int i=0; i<pcaVectorSize; ++i){
				   delete[] pca[i];
			   }
			   delete[] pca;
			   delete[] mean;
			   delete[] sd;
			   delete[] statIsUsed;
			   delete[] linearCombVariances;
		   }
		   delete[] simPCA;
		   delete[] oldSimPCA;
		   if(obsLinearCombComputed){
			  delete[] obs;
			  delete[] obsPCA;
		   }
	   };
	   int checkName(std::string name);
	   int getStatColNumberFromName(std::string name);
	   std::string getFirstMissingStat();
	   int getNumberOfFirstUsedStat();
	   void writePCA(std::ofstream* output, std::vector<double> tempInput);
	   virtual void readLinearCombFile();
	   virtual void calcObsLinearComb(std::vector<double> obsData);
	   double getPCA(int & num, double* stats);
	   double getPCA(int & num, double** stats);
	   double getPCA(int & num, std::vector<double> & stats);
	   virtual void calcSimDataPCA(double** simData);
	   virtual void calcSimDataPCA(double* simData);
	   virtual void calcSimDataPCA(std::vector<double> & stats);
	   double calculateDistance(bool & standardize);
	   double calculateDistance(double** simData, bool & standardize);
	   double calculateDistance(double* simData, bool & standardize);

	   void writeHeader(std::ofstream& ofs);
	   void writeSimPCA(std::ofstream& ofs);
	   void saveOldValues();
	   void resetOldValues();
};
#endif
