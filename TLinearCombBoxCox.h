//---------------------------------------------------------------------------

#ifndef TLinearCombBoxCoxH
#define TLinearCombBoxCoxH
//---------------------------------------------------------------------------
#include <sstream>
#include <vector>
#include "TLinearComb.h"
#include <math.h>

class TLinearCombBoxCox:public TLinearComb{
	public:
	   double* lambda;
	   double* gm;
	   double* min;
	   double* max;
	   //tmp variable
	   double* transformedData;

	   TLinearCombBoxCox(std::string gotLinearCombFileName, std::vector<std::string> NamesInInputFiles, int maxLinearCombToUse=-1);
	   virtual ~TLinearCombBoxCox(){
		   if(linearCombFileRead){
			   delete[] max;
			   delete[] min;
			   delete[] lambda;
			   delete[] gm;
			   delete[] transformedData;
		   }
	   };
	   virtual void readLinearCombFile();
	   virtual void calcObsLinearComb(std::vector<double> obsData);
	   virtual void calcSimDataPCA(double** simData);
	   virtual void calcSimDataPCA(double* simData);
	   virtual void calcSimDataPCA(std::vector<double> & simData);
	   double getBoxCox(double & simData, int & stat);
};
#endif
