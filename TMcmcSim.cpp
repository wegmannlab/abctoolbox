//---------------------------------------------------------------------------
#include "TMcmcSim.h"
//---------------------------------------------------------------------------
TMcmcSim::TMcmcSim(TParameters* GotParameters, std::string gotexedir, TLog* gotLogFile, TRandomGenerator* RandomGenerator):TSim(GotParameters, gotexedir, gotLogFile, RandomGenerator){
	nbSims=gotParameters->getParameterDouble("numSims");
	//calibration
	calibrationFile=gotParameters->getParameterString("calName", false);
	caliSimsFilled=false;
	myCaliData=NULL;
	startingPointFixed=false;
	//initialize variables
	startupLength=-1;
	burninAttemps=0;
	startupAttempts=0;
	threshold=-1;
	distance=-1;
	oldDistance=-1;
	logPriorDensity = -1;
	logOldPriorDensity = -1;

	nbSimulationsPerformed=0;
	nbSimulationsAccepted=0;
	nbSimulationsLargerThreshold=0;
	stopIfBurninFailed=false;
	countBurninTowardsNumSims=false;
	caliSimsFilled=false;

	//output
	std::string mcmcSampling=gotParameters->getParameterString("mcmcSampling", false);
	if(mcmcSampling=="") mcmcSampling="1";
	myOutputFiles=new TOutputVector(mcmcSampling, outName, myData, inputFiles, writeStatsToOutputFile, addDistanceToOutputfile, writeSeparateOutputFiles, forceOverWrite);
	logFile->endIndent();
};
//---------------------------------------------------------------------------
void TMcmcSim::performCalibratingSimulations(){
	//This function performs a given number of simulations and calculates for
	//all summary statistics mean and variance (used to normalize the distances).
	myCaliData->empty(gotParameters->getParameterInt("numCaliSims"));
	//Run simulations to fill the arrays
	int nbSimulationsPerformed=0;
	clock_t starttime=clock();
	int prog, oldProg;
	oldProg=0;
	logFile->listFlush("Performing ", myCaliData->num_sims, " calibration simulations ... (0%)");
	for(int i=0; i< myCaliData->num_sims ; ++i){
		inputFiles->createNewInputFiles(-1);
		inputFiles->performSimulations(-1);
		myData->calculateSumStats(-1);
		myCaliData->addSimToDB(myData, inputFiles);
		++nbSimulationsPerformed;
		//report progress
		prog=floor((100*(float)i/(float)myCaliData->num_sims));
		if(prog>oldProg){
			oldProg=prog;
			logFile->listOverFlush("Performing ", myCaliData->num_sims, " calibration simulations ... (", prog, "%)");
		}
	}
	float runtime=(float) (clock()-starttime)/CLOCKS_PER_SEC/60;
	logFile->overList("Performing ", myCaliData->num_sims, " calibration simulations ... done in ", runtime, "min!");
}
//---------------------------------------------------------------------------
void TMcmcSim::initialCalibration(){
	logFile->startIndent("Calibration:");
	myCaliData=new TSimDatabase(0, myData, inputFiles, logFile);
	//set threshold
	if(gotParameters->parameterExists("threshold")){
		threshold=gotParameters->getParameterDouble("threshold");
		myCaliData->setFixedThreshold(threshold);
	} else {
		float thresholdProportion=gotParameters->getParameterDouble("thresholdProp");
		fillCalibrationSims();
		threshold=myCaliData->getThreshold(thresholdProportion);
	}
	logFile->list("Threshold (distance of acceptance) is set to ", threshold);
	//set range
	if(gotParameters->parameterExists("range")){
		double mcmc_range=gotParameters->getParameterDouble("range");
		myCaliData->setFixedMcmcRanges(mcmc_range);
		logFile->list("Proposal range set to ", mcmc_range);
	} else {
		double mcmc_range_proportion=gotParameters->getParameterDouble("rangeProp");
		fillCalibrationSims();
		myCaliData->setMcmcRanges(mcmc_range_proportion);
		logFile->list("Proposal range set to ", mcmc_range_proportion, " times the standard deviation among simulations below threshold.");
	}

	//starting point
	std::string startingPoint=gotParameters->getParameterStringWithDefault("startingPoint", "random");
	if(startingPoint.find_first_of('#')==std::string::npos){
		fillCalibrationSims();
		if(startingPoint=="best") myCaliData->setPriorStartingConditionsFromBestSimulation();
		else if(startingPoint=="random") myCaliData->setPriorStartingConditionsAtRandom(randomGenerator->getRand());
		else throw "Starting point tag '" + startingPoint + "' unknown!";
	} else {
		myCaliData->setPriorStartingConditions(startingPoint);
		startingPointFixed=true;
	}
	logOldPriorDensity = inputFiles->getLogPriorDensity();
	logFile->endIndent();
}

void TMcmcSim::fillCalibrationSims(){
	if(!caliSimsFilled){
		  if(!calibrationFile.empty()){
			  myCaliData->addSimsFromFile(calibrationFile, gotParameters->getParameterInt("numCaliSims"));
		  } else {
			  performCalibratingSimulations();
			  myCaliData->writeToFile(outName + "_calibration_file.txt");
		  }
		  logFile->startIndent("Calculating means and variances:");
		  myCaliData->calculateMeanVariances();
		  if(doLinearComb) myCaliData->calculateDistances(myLinearComb);
		  else myCaliData->calculateDistances();
		  logFile->endIndent();
		  caliSimsFilled=true;
	}
}
//---------------------------------------------------------------------------
void TMcmcSim::resetCounters(){
	//counters
	nbSimulationsAccepted=0;
	nbSimulationsLargerThreshold=0;
}
//---------------------------------------------------------------------------
void TMcmcSim::performSimulation(int s){
	  inputFiles->performSimulations(s);
	  myData->calculateSumStats(s);
}
void TMcmcSim::rejectProposal(){
	  inputFiles->priors->resetOldValues();
	  myData->resetOldValues();
	  if(doLinearComb) myLinearComb->resetOldValues();
	  distance = oldDistance;
}
void TMcmcSim::performMcmcSimulation(int s){
	inputFiles->createNewInputFilesMcmc(s);
	performSimulation(s);
	distance = calcDistance();
	if(repeatsPerParameterVector>1){
		for(int i=1; i<repeatsPerParameterVector; ++i){
		   performSimulation(s);
		   distance+=calcDistance();
		}
		distance = distance/repeatsPerParameterVector;
	}
	//check acceptance
	if(distance <= threshold){
	  if(calculcateH()){
		 ++nbSimulationsAccepted;
		 inputFiles->priors->saveOldValues();
		 myData->saveOldValues();
		 oldDistance = distance;
		 logOldPriorDensity = logPriorDensity;
		 if(doLinearComb) myLinearComb->saveOldValues();
	  } else rejectProposal();
	} else {
		rejectProposal();
		++nbSimulationsLargerThreshold;
	}
}
//---------------------------------------------------------------------------
void TMcmcSim::writeSimulations(int & s){
	if(addDistanceToOutputfile){
		if(doLinearComb)
			myOutputFiles->writeSimulations(s, myLinearComb, distance);
		else
			myOutputFiles->writeSimulations(s, distance);
	} else {
		if(doLinearComb)
			myOutputFiles->writeSimulations(s, myLinearComb);
		else
			myOutputFiles->writeSimulations(s);
	}
}
//---------------------------------------------------------------------------
void TMcmcSim::performStartup(){
   clock_t starttime=clock();
   logFile->startIndent("Performing start-up (burn-in):");

   //how to do burnin?
   startupAttempts=gotParameters->getParameterDoubleWithDefault("startupAttempts",1);
   if(startingPointFixed && startupAttempts>1){
	   startupAttempts=1;
	   logFile->warning("Only one start-up attempt used when starting point is fixed!");
   }
   logFile->list("Max start up attempts is set to ",  startupAttempts);

   if(startupAttempts<1) logFile->conclude("Skipping start-up");
   else {
	   startupLength=gotParameters->getParameterDouble("startupLength",false);
	   if(!startupLength) startupLength=100;
	   logFile->list("Start up length is set to ",  startupLength);
	   stopIfBurninFailed=gotParameters->getParameterDouble("stopIfStartupFailed",false);

	   //run burnin!
	   int prog, oldProg;
	   oldProg=0;
	   logFile->listFlush("Performing start up of ", startupLength, " simulations ... (0%)");
	   nbSimulationsAccepted=0;
	   burninAttemps=1;
	   for(int s=1; s<=startupLength; ++s, ++nbSimulationsPerformed){
		  performMcmcSimulation(s);
		  //report progress
		  prog=floor(100*(float)s/(float)startupLength);
		  if(prog>oldProg){
			  oldProg=prog;
			  logFile->listOverFlush("Performing start up of ", startupLength, " simulations ... (", prog, "%)");
		  }
		  if(s==startupLength){
			 if(startupPassed()){
				float runtime=(float) (clock()-starttime)/CLOCKS_PER_SEC/60;
				logFile->overList("Performing start up of ", startupLength, " simulations ... done in ", runtime, "min!");
				resetCounters();
			 } else {
				float runtime=(float) (clock()-starttime)/CLOCKS_PER_SEC/60;
				logFile->overList("Performing start up of ", startupLength, " simulations ... failed! (", runtime, "min)");
				if(burninAttemps>=startupAttempts || nbSimulationsPerformed>= (nbSims-startupLength+1)){
				   if(!stopIfBurninFailed){
					   logFile->list("Will continue despite failed start-up");
				   } else throw "Start up failed!";
				} else {
				   ++burninAttemps;
				   resetStartingPoints();
				   logFile->listFlush("Performing start up of ", startupLength, " simulations ... (0%)");
				   s=0;
				   oldProg=0;
				}
			 }
		  }
	   }
   }
   logFile->endIndent();
}

bool TMcmcSim::startupPassed(){
	if(nbSimulationsAccepted>1) return true;
	return false;
}

void TMcmcSim::resetStartingPoints(){
	myCaliData->setPriorStartingConditionsAtRandom(randomGenerator->getRand());
	logOldPriorDensity = inputFiles->getLogPriorDensity();
}

void TMcmcSim::runMcmcChain(){
	 //reset Counters
	 resetCounters();
	 nbSimulationsPerformed=0;

	 //Do calibrating simulations
	 initialCalibration();

	 //write output  header
	 if(!gotParameters->parameterExists("noHeader")){
		 if(doLinearComb) myOutputFiles->writeHeader(myLinearComb);
		 else myOutputFiles->writeHeader();
	 }

	 //Loop of Chain, start only if number of sims > 0
	 if(nbSims>0){
		performStartup();
		//start Loop
		if(gotParameters->parameterExists("countStartupTowardsNumSims")){
			logFile->list("Simulations from the burnin will count towards total number of simulations conducted");
			nbSims=nbSims-nbSimulationsPerformed;
			logFile->conclude("Launching a chain of " + toString(nbSims) + " simulations");
		}
		resetCounters();
		clock_t starttime=clock();
		int prog, oldProg;
		oldProg=0;
		std::vector<int>::iterator nextDens = densityLengths.begin();
		logFile->listFlush("Performing ", nbSims, " simulations ... (0%)");
		for (int s=1; s<=nbSims; ++s, ++nbSimulationsPerformed){
			performMcmcSimulation(s);
			writeSimulations(s);

		   //report progress
		   prog=floor(100*(float)s/(float)nbSims);
		   if(prog>oldProg){
			   oldProg=prog;
			   logFile->listOverFlush("Performing ", nbSims, " simulations ... (", prog, "%)");
		   	}
		   //calc / write densities
		   if(calcDensBins){
			   addCurrentParamsToDensityBins();
			   if(nextDens!=densityLengths.end() && s == *nextDens){
				   writeDensityBins(toString(*nextDens));
				   ++nextDens;
			   }
		   }
		}
		float runtime=(float) (clock()-starttime)/CLOCKS_PER_SEC/60;
		logFile->overList("Performing ", nbSims, " simulations ... done in ", runtime, "min!");

		//write counters to logFile
		logFile->conclude("number of start up attempts needed: ", (burninAttemps));
		logFile->conclude("number of performed jumps: ", nbSimulationsPerformed);
		logFile->conclude("number of simulations in chain: ", nbSims);
		logFile->conclude("number of accepted simulations: ", nbSimulationsAccepted);
		logFile->conclude("acceptance rate: ", nbSimulationsAccepted/(float) nbSims);
		logFile->conclude("number of simulations where distance larger than threshold: ", nbSimulationsLargerThreshold);


	} //end if nbsims >0
	 delete myOutputFiles;
}

//---------------------------------------------------------------------------
void TMcmcSim::runSimulations(){
	logFile->startIndent("Performing an MCMC chain:");
	logFile->list("Performing a total of ", nbSims, " simulations");
	runMcmcChain();
	logFile->endIndent();
	writeDensityBins("end");
};

//------------------------------------------------------------------------------
//calculate the acceptance probability of new priors and check, if we accept them
bool TMcmcSim::calculcateH(){
    //as we use a symmetric transition kernel, this part is reduced to the quotient
	//of Pi(new)/Pi(old)
	logPriorDensity = inputFiles->getLogPriorDensity();

	double h = exp(logPriorDensity - logOldPriorDensity);

	if(h>=1.0) return true;
	if(randomGenerator->getRand() < h)	return true;
	else return false;
}
//------------------------------------------------------------------------------

