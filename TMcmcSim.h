#ifndef TMcmcSimH
#define TMcmcSimH

#include "TSim.h"
#include "TSimDatabase.h"
#include <vector>
#include "TLog.h"
#include "stringFunctions.h"
#include <iostream>
//---------------------------------------------------------------------------

class TMcmcSim:public TSim{
	public:
		int nbSims;
		std::string calibrationFile;
		TSimDatabase* myCaliData;
		double threshold;
		double distance, oldDistance;
		double logPriorDensity, logOldPriorDensity;
		int nbSimulationsPerformed;
		float nbSimulationsAccepted;
		float nbSimulationsLargerThreshold;
		int startupLength;
		int startupAttempts;
		int burninAttemps;
		bool stopIfBurninFailed;
		bool countBurninTowardsNumSims;
		bool caliSimsFilled;
		bool startingPointFixed;

		TMcmcSim(TParameters* gotParameters, std::string gotexedir, TLog* gotLogFile, TRandomGenerator* RandomGenerator);
		TMcmcSim();
		virtual ~TMcmcSim(){
			delete myCaliData;
		};

		void runSimulations();
		bool calculcateH();
		double calculcatePi(TPriorVector Prior);
		double calculatePriorProbability(TPrior Prior);
        // double calculateDistance(TDataVector* observed, TDataVector* simulated);
		double chiSquare(double obs, double exp);
		void performCalibratingSimulations();
		virtual void initialCalibration();
		void fillCalibrationSims();
		void resetCounters();
		void runMcmcChain();
		void performSimulation(int s=-1);
		void rejectProposal();
		virtual void performMcmcSimulation(int s=-1);
		void performSimulationSeveralRepeats(int numRepeatsPerParameterVector, int s=-1);
		virtual void writeSimulations(int & s);
		void performStartup();
		virtual bool startupPassed();
		virtual void resetStartingPoints();
};
#endif
