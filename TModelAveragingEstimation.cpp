/*
 * TModelAveragingEstimation.cpp
 *
 *  Created on: Nov 13, 2013
 *      Author: wegmannd
 */

#include "TModelAveragingEstimation.h"

//---------------------------------------------------------------------------
//TModelAvergingEstimation
//---------------------------------------------------------------------------
TModelAvergingEstimation::TModelAvergingEstimation(TSimDataCommonParams* CommonParams, vector<TStandardEstimation*>* Estimations, ColumnVector* Theta, std::string OutputPrefix, TLog* LogFile){
	commonParams=CommonParams;
	estimations=Estimations;
	logfile=LogFile;
	outputPrefix=OutputPrefix;
	//prepare posterior Matrix
	posteriorMatrix=new TPosteriorMatrix(Theta, commonParams->paramRanges);

	//open posterior characteristics file
	std::string filename=outputPrefix+"ModelAveraging_MarginalPosteriorCharacteristics.txt";
	posteriorCharacteristicsFile.open(filename.c_str());
	posteriorMatrix->writePosteriorCharacteristicsHeader(posteriorCharacteristicsFile);
}

//---------------------------------------------------------------------------
bool TModelAvergingEstimation::performPosteriorEstimation(int ObsDataSetNum, bool verbose){
	//loop over all parameters
	int paramCol;
	Matrix* postMat;
	float weight;
	posteriorMatrix->initToZero();
	if(verbose)  logfile->listFlush("Calculating marginal posterior distributions by averaging across models ...");
	for(estIt=estimations->begin(); estIt!=estimations->end(); ++estIt){
		postMat=(*estIt)->getPointerToMarginalPosteriorDensityMatrix();
		for(int p=1; p<=commonParams->numParams; ++p){
			paramCol=(*estIt)->getParamColumnFromName(commonParams->getParamNameFromNumber(p));
			weight=(*estIt)->posteriorProbability/(*estIt)->posteriorMatrix->getMarginalArea(paramCol);
			for(int k=1;k<=posteriorMatrix->posteriorDensityPoints;++k){
				posteriorMatrix->posteriorMatrix(k,p)+=(*postMat)(k,paramCol)*weight;
			}
		}
	}
	//calculate marginal areas
	posteriorMatrix->calcMarginalAreas();
	if(verbose) logfile->write(" done!");

	//write marginal densities
	if(verbose) logfile->listFlush("Writing marginal posterior densities ...");
	std::string filename=outputPrefix+"ModelAveraging_MarginalPosteriorDensities_Obs" + toString(ObsDataSetNum) + ".txt";
	ofstream marDensFile;
	marDensFile.open(filename.c_str());
	posteriorMatrix->writePosteriorFiles(marDensFile);
	posteriorMatrix->writePosteriorCharacteristics(posteriorCharacteristicsFile, ObsDataSetNum);
	if(verbose) logfile->write(" done!");
	//done!

	return true;
}



