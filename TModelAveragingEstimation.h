/*
 * TModelAveragingEstimation.h
 *
 *  Created on: Nov 13, 2013
 *      Author: wegmannd
 */

#ifndef TMODELAVERAGINGESTIMATION_H_
#define TMODELAVERAGINGESTIMATION_H_

#include "TBaseEstimation.h"
#include "TStandardEstimation.h"

//---------------------------------------------------------------------------
class TModelAvergingEstimation{
public:
	TSimDataCommonParams* commonParams;
	TPosteriorMatrix* posteriorMatrix;
	vector<TStandardEstimation*>* estimations;
	vector<TStandardEstimation*>::iterator estIt;
	TLog* logfile;
	std::string outputPrefix;
	ofstream posteriorCharacteristicsFile;

	TModelAvergingEstimation(TSimDataCommonParams* CommonParams, vector<TStandardEstimation*>* Estimations, ColumnVector* Theta, std::string OutputPrefix, TLog* LogFile);
	~TModelAvergingEstimation(){
		posteriorCharacteristicsFile.close();
		delete posteriorMatrix;
	};

	bool performPosteriorEstimation(int ObsDataSetNum, bool verbose);



};



#endif /* TMODELAVERAGINGESTIMATION_H_ */
