#ifndef TPmcSimH
#define TPmcSimH

#include "TSim.h"
#include "TSimDatabase.h"
#include <vector>
#include "TLog.h"
#include "matrixFunctions.h"
//#include "stringFunctions.h"
//---------------------------------------------------------------------------

class TPmcSim:public TSim{
	public:
		std::string calibrationFile;
		TSimDatabase** mySimData;
		int numInterations;
		unsigned int particleSize;
		int lastParticleSize;
		int repeatsPerParameterVector;
		int nextDb, currentDb, oldDb;
		double pmc_range_proportion;
		double tolerance;
		unsigned int numRetained;
		double** weights;
		double* paramMin;
		double* paramMax;
		SymmetricMatrix Sigma, Sigma_inv;
		Matrix A;
		unsigned int numCaliSims;

		TPmcSim(TParameters* gotParameters, std::string gotexedir, TLog* gotLogFile, TRandomGenerator* RandomGenerator);
		TPmcSim();
		~TPmcSim(){};

		virtual void runSimulations();
		bool calculcateH();
		double calculcatePi(TPriorVector Prior);
		double calculatePriorProbability(TPrior Prior);
        // double calculateDistance(TDataVector* observed, TDataVector* simulated);
		double chiSquare(double obs, double exp);
		void performCalibratingSimulations();
		void initialCalibration();
		void resetCounters();
		void performSimulation(int s=-1);
		void performSimulationSeveralRepeats(int numRepeatsPerParameterVector, int s=-1);
		void checkAcceptance();
		void performBurnin();
};
#endif
