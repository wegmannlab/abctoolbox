/*
 * TPosteriorMatrix.cpp
 *
 *  Created on: Jan 20, 2018
 *      Author: phaentu
 */

#include "TPosteriorMatrix.h"

TPosteriorMatrix::TPosteriorMatrix(ColumnVector* Theta, TParamRanges* ParamRanges){
	theta=Theta;
	paramRanges=ParamRanges;
	posteriorDensityPoints=theta->nrows();
	step=1.0/(posteriorDensityPoints-1.0);
	posteriorMatrix=Matrix(posteriorDensityPoints, paramRanges->numParams);
	initToZero();
}
//---------------------------------------------------------------------------
void TPosteriorMatrix::calcMarginalAreas(){
	marginalAreas.ReSize(paramRanges->numParams);
	marginalAreasParamScale.ReSize(paramRanges->numParams);
	for(int param=1; param<=paramRanges->numParams; ++param){
		marginalAreas(param) = calcArea(posteriorMatrix.column(param));
		marginalAreasParamScale(param) = marginalAreas(param) * paramRanges->getMinMaxDiff(param);
	}
}

void TPosteriorMatrix::calcAreasParamScale(const Matrix & densities, ColumnVector & areas){
	areas.ReSize(paramRanges->numParams);
	for(int param=1; param<=paramRanges->numParams; ++param){
		areas(param) = calcAreaParamScale(densities, param);
	}
}
double TPosteriorMatrix::calcAreaParamScale(const Matrix & densities, const int & param){
	return calcArea(densities.column(param)) * paramRanges->getMinMaxDiff(param);
}

double TPosteriorMatrix::calcArea(const ColumnVector densities){
	return calcAreaByReference(densities);
}
double TPosteriorMatrix::calcAreaByReference(const ColumnVector & densities){
	//area i s calculated by assuming the density is always given in the middle of a bar from (pos - step/2 , pos + step/2)
	//Hence: all densities have a weight of 1, expect the first and the last
	double area = 0;
	for(int i=2; i<densities.size();++i) area += densities(i);
	area += 0.5 * (densities(1) + densities(densities.size()));
    return step * area;
}

//---------------------------------------------------------------------------
void TPosteriorMatrix::writePosteriorFiles(std::ofstream & output){
   //write values for each parameter: norm area!
   writeDensities(output, posteriorMatrix, marginalAreasParamScale);
}
void TPosteriorMatrix::writeDensities(std::ofstream & output, Matrix & densities, ColumnVector & areas){
	//write Header
	output << "number";
	for(int p=1; p<=paramRanges->numParams;++p){
		output << "\t" << paramRanges->getParamName(p) << "\t" << paramRanges->getParamName(p) << ".density";
	}
	output << std::endl;
	//write values for each parameter: norm area!
	for(int k=1;k<=posteriorDensityPoints;++k){
		  output << k;
		  for(int p=1; p<=paramRanges->numParams;++p){
			 output << "\t" << paramRanges->toParamScale(p, (*theta)(k)) << "\t" << densities(k,p)/areas(p);
		  }
		  output << std::endl;
	}
}
double TPosteriorMatrix::toParameterScale(const int & param, const double & pos){
	return paramRanges->toParamScale(param, pos);
}
double TPosteriorMatrix::toInternalScale(const int & param, const double & val){
	return paramRanges->toInternalScale(param, val);
}
//---------------------------------------------------------------------------
double TPosteriorMatrix::thetaParameterScale(const int & param, const int & k){
	return paramRanges->toParamScale(param, (*theta)(k));
}
//double TPosteriorMatrix::theatParameterScale(const int & param, const double & pos){
//	return paramRanges->toParamScale(param, pos);
//}
//---------------------------------------------------------------------------
double TPosteriorMatrix::getPositionOfMode(const int & param){
	double max=0;
	int pos=1;
	for(int k=1;k<=posteriorDensityPoints;++k){
		if(posteriorMatrix(k,param)>max){
			max = posteriorMatrix(k,param);
			pos = k;
		}
	}
	return thetaParameterScale(param, pos);
}
//---------------------------------------------------------------------------
double TPosteriorMatrix::getPosteriorMean(const int & param){
	double mean=0;
	double w=0;
	for(int k=1;k<=posteriorDensityPoints;++k){
			mean+=posteriorMatrix(k,param)*thetaParameterScale(param, k);
			w+=posteriorMatrix(k,param);
	}
	return mean/w;
}
//---------------------------------------------------------------------------
double TPosteriorMatrix::getPosteriorquantile(const int & param, double q){
	double neededArea = q * marginalAreas(param);

	double a = 0.0;
	double pos = 0.0;
	double area = 0.0;

	//is first bar enough?
	pos = step * 0.5;
	area = posteriorMatrix(1,param) * step * 0.5;
	if(area >= neededArea){
		pos = neededArea / posteriorMatrix(1,param);
		return paramRanges->toParamScale(param, pos);
	}

	//else: sum bars
	for(int k=2;k<=posteriorDensityPoints;++k){
		if(k==1 || k == posteriorDensityPoints) a = step * posteriorMatrix(k,param) * 0.5;
		else a = step * posteriorMatrix(k,param);
		if((area+a) >= neededArea){
			/*
			double add = step*(-0.5+(neededArea-area)/a);
			add = add*paramRanges->getMinMaxDiff(param)/(posteriorDensityPoints-1);
			return thetaParameterScale(param, k)+add;
			*/

			pos += (neededArea - area)/posteriorMatrix(k,param);
			return paramRanges->toParamScale(param, pos);
		}
		else {
			area += a;

			pos += step;
		}
	}

	return 0;
}
void TPosteriorMatrix::writeQuantile(const int & param, double quantile, std::ofstream & output){
	quantile = (1.0 - quantile) / 2.0;
	output << "\t" << getPosteriorquantile(param, quantile) << "\t" << getPosteriorquantile(param, 1.0 - quantile);
}
//---------------------------------------------------------------------------
double TPosteriorMatrix::getPosInCumulative(const int & param, double & val){
	//Note: assumes uniform posterior density points from 1 to posteriorDensityPoints
	if(val < 0.0) return 0;
	if(val > 1.0) return 1;
	//else compute position among posterior density points
	int upper = floor(val / step) + 1;
	double area = 0.0;
	//add all posterior densities below pos
	for(int k=1;k<upper;++k) area += posteriorMatrix(k, param);
	double tot = area;
	for(int k=upper;k<=posteriorDensityPoints;++k) tot += posteriorMatrix(k, param);
	//add fraction of last
	area += posteriorMatrix(upper, param) * (val - (upper - 1)*step);
	return area / tot;
}
//---------------------------------------------------------------------------
void TPosteriorMatrix::getPosteriorHDIScaled(const int & param, const double & q, double & lower, double & upper){
	//get shortest interval of fraction q
	//since two values have to be returned, the results are written to a passed variable
	//go through each posterior point and find HDI. return smallest interval.
	//Note: equal spacing between points implied here!
	lower = 0;
	upper = 1;
	double neededArea = q * marginalAreas(param);
	double pos=0, max=0.0;

	//find density at mode
	for(int k=1;k<=posteriorDensityPoints;++k){
		if(posteriorMatrix(k,param) > max){
			max = posteriorMatrix(k,param);
			pos = k;
		}
	}

	//check if bar with mode is enough -> take care of boundaries!
	if(pos==1 && step * max / 2.0 >= neededArea){
		//take left of bar = 1 + required width
		double w = (neededArea/(step*max/2.0));
		lower = 0;
		upper = w * step;
	} else if(pos == posteriorDensityPoints && step * max / 2.0 >= neededArea){
		//take right of bar = posteriorDensityPoints - required width
		double w = (neededArea/(step*max/2.0));
		lower = (posteriorDensityPoints - w - 1.0) * step;
		upper = (posteriorDensityPoints - 1.0) * step;
	} else if(pos>1 && pos < posteriorDensityPoints && step * max >= neededArea){
		//take center of bar +- half of the width
		double w = (neededArea/(step*max));
		lower = (pos - (w/2.0) - 1.0) * step;
		upper = (pos + (w/2.0) - 1.0) * step;
	} else {
		//find it as combination of bars...
		double smallestHdi=1;
		int kl = 1;
		int ku = 1;
		double templ = 0.0;
		double tempu = 0.0;
		double tempArea = 0.0;
		double tempd = 0.0;
		double upperBar, lowerBar;
		while(kl < posteriorDensityPoints){
			//find interval for this lower bound
			upperBar = step * posteriorMatrix(ku,param);
			if(ku == 1 || ku == posteriorDensityPoints) upperBar = upperBar / 2.0; //only step/2 at border...
			if(tempArea + upperBar > neededArea){
				lowerBar = step * posteriorMatrix(kl,param);
				if(kl == 1) lowerBar = lowerBar / 2.0;
				//which density is higher? -> keep higher total and add from smaller...
				if(posteriorMatrix(ku,param) > posteriorMatrix(kl,param)){
					//ku density is higher -> add partial kl density
					//if Area minus lower bar too big -> there is a better solution for sure later on
					if(tempArea + upperBar - lowerBar < neededArea){
						//there is a solution with a partial lower bar
						if(ku == posteriorDensityPoints) tempu = (ku - 1.0) * step;
						else tempu = (ku - 0.5)*step; //complete upper bar, we are 1 of with enumerating (+0.5 -1 = -0.5)
						if(kl == 1) templ = 0.0;
						else templ = (kl-0.5 - (neededArea-(tempArea+upperBar-lowerBar))/lowerBar)*step;
						tempd = tempu-templ;
						if(smallestHdi >= tempd){
							smallestHdi = tempd;
							lower = templ;
							upper = tempu;
						}
					}
					//move one forward, but do not add upper bar, may be not needed in full
				} else {
					//kl density is higher-> add partial ku density
					if(kl == 1) templ = 0.0;
					else templ = (kl - 1.5)*step;
					if(ku == posteriorDensityPoints) tempu = (ku - 1.5 + (neededArea - tempArea)/2.0/upperBar )*step;
					else tempu = (ku - 1.5 + (neededArea - tempArea)/upperBar )*step;
					tempd = tempu-templ;
					if(smallestHdi >= tempd){
						smallestHdi = tempd;
						lower = templ;
						upper = tempu;
					}
					//move one forward and accept whole new bar
					tempArea += upperBar; //add upper bar total, since we remove lower bar which is larger
					++ku;
				}
				//move
				tempArea -= lowerBar; //remove lower bar
				++kl;
			} else {
				tempArea += upperBar;
				++ku;
			}
			if(ku > posteriorDensityPoints) break; //we are through!
		}
	}
}

void TPosteriorMatrix::getPosteriorHDI(const int & param, const double & q, double & lower, double & upper){
	getPosteriorHDIScaled(param, q, lower, upper);
	upper = paramRanges->toParamScale(param, upper);
	lower = paramRanges->toParamScale(param, lower);
}

void TPosteriorMatrix::writeHDI(const int & param, const double & HDI, std::ofstream & output){
	double lower, upper;
	getPosteriorHDI(param, HDI, lower, upper);
	output << "\t" << lower << "\t" << upper;
}
double TPosteriorMatrix::getPosInPosteriorHDI(int param, double trueValue, int steps){
	//this function return the HPDI of the true parameter value
	//the procedure is kind of crude (and could be improved):
	//I implemented a binary search and for comparison always call the HDI function, which seems a bit slow.
	//But for now....
	double hdi = 0.5;
	double lower, upper;
	double binaryStep=0.25;
	for(int i=0; i<steps; ++i){
		getPosteriorHDIScaled(param, hdi, lower, upper);
		if(trueValue==lower || trueValue==upper) return hdi;
		if(trueValue>lower && trueValue<upper){
			//hdi is too large
			hdi -= binaryStep;
		} else {
			//hdi is too small
			hdi += binaryStep;
		}
		binaryStep /= 2.0;
	}
	return hdi;
}
//---------------------------------------------------------------------------
void TPosteriorMatrix::writePosteriorCharacteristicsHeader(std::ofstream & posteriorCharacteristicsFile, bool addL1PriorPosterior){
	posteriorCharacteristicsFile << "dataSet";
    for(int p=1; p<=paramRanges->numParams;++p){
    	std::string pName = paramRanges->getParamName(p);
    	posteriorCharacteristicsFile << "\t" << pName << "_mode";
    	posteriorCharacteristicsFile << "\t" << pName << "_mean";
    	posteriorCharacteristicsFile << "\t" << pName << "_median";

    	posteriorCharacteristicsFile << "\t" << pName << "_q50_lower";
    	posteriorCharacteristicsFile << "\t" << pName << "_q50_upper";
    	posteriorCharacteristicsFile << "\t" << pName << "_q90_lower";
    	posteriorCharacteristicsFile << "\t" << pName << "_q90_upper";
    	posteriorCharacteristicsFile << "\t" << pName << "_q95_lower";
    	posteriorCharacteristicsFile << "\t" << pName << "_q95_upper";
    	posteriorCharacteristicsFile << "\t" << pName << "_q99_lower";
    	posteriorCharacteristicsFile << "\t" << pName << "_q99_upper";

    	posteriorCharacteristicsFile << "\t" << pName << "_HDI50_lower";
    	posteriorCharacteristicsFile << "\t" << pName << "_HDI50_upper";
    	posteriorCharacteristicsFile << "\t" << pName << "_HDI90_lower";
    	posteriorCharacteristicsFile << "\t" << pName << "_HDI90_upper";
    	posteriorCharacteristicsFile << "\t" << pName << "_HDI95_lower";
    	posteriorCharacteristicsFile << "\t" << pName << "_HDI95_upper";
    	posteriorCharacteristicsFile << "\t" << pName << "_HDI99_lower";
    	posteriorCharacteristicsFile << "\t" << pName << "_HDI99_upper";

    	if(addL1PriorPosterior) posteriorCharacteristicsFile << "\t" << pName << "_L1";
   }
    posteriorCharacteristicsFile << std::endl;
}
void TPosteriorMatrix::writePosteriorCharacteristics(std::ofstream & posteriorCharacteristicsFile, const int & obsDataSetNum, Matrix & priorDensities){
	posteriorCharacteristicsFile << obsDataSetNum;
	for(int p=1; p<=paramRanges->numParams;++p){
		writePosteriorCharacteristicsOneParam(posteriorCharacteristicsFile, p);
		//calc and write L1
		posteriorCharacteristicsFile << "\t" << getLOnedistwithPosterior(p, priorDensities);
	}
	posteriorCharacteristicsFile << std::endl;
}
void TPosteriorMatrix::writePosteriorCharacteristics(std::ofstream & posteriorCharacteristicsFile, const int & obsDataSetNum){
	posteriorCharacteristicsFile << obsDataSetNum;
	for(int p=1; p<=paramRanges->numParams;++p){
		writePosteriorCharacteristicsOneParam(posteriorCharacteristicsFile, p);
	}
	posteriorCharacteristicsFile << std::endl;
}
void TPosteriorMatrix::writePosteriorCharacteristicsOneParam(std::ofstream & posteriorCharacteristicsFile, const int & param){
	posteriorCharacteristicsFile << "\t" << getPositionOfMode(param);
	posteriorCharacteristicsFile << "\t" << getPosteriorMean(param);
	posteriorCharacteristicsFile << "\t" << getPosteriorquantile(param, 0.5);

	writeQuantile(param, 0.50, posteriorCharacteristicsFile);
	writeQuantile(param, 0.90, posteriorCharacteristicsFile);
	writeQuantile(param, 0.95, posteriorCharacteristicsFile);
	writeQuantile(param, 0.99, posteriorCharacteristicsFile);
	//TODO: Check HDI!
	writeHDI(param, 0.50, posteriorCharacteristicsFile);
	writeHDI(param, 0.90, posteriorCharacteristicsFile);
	writeHDI(param, 0.95, posteriorCharacteristicsFile);
	writeHDI(param, 0.99, posteriorCharacteristicsFile);
}
void TPosteriorMatrix::writeValidationHeader(std::ofstream & output, std::string & paramName){
	output << "\t" << paramName << "\t" << paramName << "_mode\t" << paramName << "_mean\t" << paramName << "_quantile\t" << paramName << "_HDI";
}
void TPosteriorMatrix::writeValidationStats(std::ofstream & output, const int & param, double val){
	output << "\t" << toParameterScale(param, val);
	output << "\t" << getPositionOfMode(param);
	output << "\t" << getPosteriorMean(param);
	output << "\t" << getPosInCumulative(param, val);
	output << "\t" << getPosInPosteriorHDI(param, val, 20);
}
//---------------------------------------------------------------------------
double TPosteriorMatrix::getLOnedistwithPosterior(const int & param, Matrix & densities){
	//calculate common surface: norm area
	double LOneDist=0;
	double area = calcAreaParamScale(densities, param);

	for(int k=1;k<=posteriorDensityPoints;++k){
		LOneDist+=abs(densities(k,param)/area - posteriorMatrix(k,param)/marginalAreasParamScale(param));
	}
	return LOneDist;
}
//---------------------------------------------------------------------------
double TPosteriorMatrix::getRandomParameterAccordingToMarginal(const int & param, double randomVal){
	//we assume uniform distribution of posterior density points!
	double sum = 0.0;
	double r = randomVal * posteriorMatrix.Column(param).Sum();
	int k=0;

	while(sum<r && k<posteriorDensityPoints){
		++k;
		sum+=posteriorMatrix(k,param);
	}
	double diff=abs(posteriorMatrix(k,param)-posteriorMatrix(k-1,param));

	return (double) k-(sum-r)/diff / (double) (posteriorDensityPoints-1);
}
double TPosteriorMatrix::getPosteriorScaledSD(const int & param){
	//get mean
	double m = 0;
	double w = 0;
	for(int k = 0; k < posteriorDensityPoints; ++k){
		m += posteriorMatrix.element(k,param) * k;
		w += posteriorMatrix.element(k,param);
	}
	m = m * step / w;
	//get std
	double sd = 0;
	for(int k=0; k<posteriorDensityPoints; ++k)
		sd += posteriorMatrix.element(k,param)*(k*step-m)*(k*step-m);
	sd = sd / w;
	return sqrt(sd);
}
void TPosteriorMatrix::setParameterRange(const std::string & param, std::pair<double,double> minmax){
	paramRanges->setMinMax(param, minmax);
}

