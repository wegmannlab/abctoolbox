/*
 * TPosteriorMatrix.h
 *
 *  Created on: Jan 20, 2018
 *      Author: phaentu
 */

#ifndef TPOSTERIORMATRIX_H_
#define TPOSTERIORMATRIX_H_

#include <algorithm>
#include "TSimData.h"
#include "matrixFunctions.h"

//---------------------------------------------------------------------------
//TPosteriorMatrix
//---------------------------------------------------------------------------
class TPosteriorMatrix{
public:
	//pointers
	TParamRanges* paramRanges;

	//estimation
	double step; //step between posterior density points
	int posteriorDensityPoints;
	int obsDataSetNum;

	//storage
	ColumnVector marginalAreasParamScale, marginalAreas;
	Matrix posteriorMatrix;
	ColumnVector* theta;

	TPosteriorMatrix(ColumnVector* Theta, TParamRanges* ParamRanges);
	~TPosteriorMatrix(){};
	void initToZero(){posteriorMatrix=0;};
	void setPosteriorMatrixDensity(const int & pos, const int & param, const double & value){posteriorMatrix(pos,param)=value;};
	void addToPosteriorMatrixDensity(const int & pos, const int & param, const double & value){posteriorMatrix(pos,param)=posteriorMatrix(pos,param)+value;};
	void calcMarginalAreas();
	double getMarginalArea(const int & param){return marginalAreasParamScale(param);};
	void calcAreasParamScale(const Matrix & densities, ColumnVector & areas);
	double calcAreaParamScale(const Matrix & densities, const int & param);
	double calcArea(const ColumnVector densities);
	double calcAreaByReference(const ColumnVector & densities);
	void writePosteriorFiles(ofstream & output);
	void writeDensities(ofstream & output, Matrix & densities, ColumnVector & areas);
	double toParameterScale(const int & param, const double & pos);
	double toInternalScale(const int & param, const double & val);
	double thetaParameterScale(const int & param, const int & k);
	//double theatParameterScale(const int & param, const double & pos);
	double getPositionOfMode(const int & param);
	double getPosteriorMean(const int & param);
	double getPosteriorquantile(const int & param, double q);
	double getPosInCumulative(const int & param, double & val);
	void writeQuantile(const int & param, double quantile, ofstream & output);
	void getPosteriorHDIScaled(const int & param, const double & q, double & lower, double & upper);
	void getPosteriorHDI(const int & param, const double & q, double & lower, double & upper);
	void writeHDI(const int & param, const double & HDI, ofstream & output);
	double getPosInPosteriorHDI(int param, double trueValue, int steps);
	void writePosteriorCharacteristicsHeader(ofstream & posteriorCharacteristicsFile, bool addL1PriorPosterior=false);
	void writePosteriorCharacteristics(ofstream & posteriorCharacteristicsFile, const int & obsDataSetNum);
	void writePosteriorCharacteristics(ofstream & posteriorCharacteristicsFile, const int & obsDataSetNum, Matrix & priorDensities);
	void writePosteriorCharacteristicsOneParam(ofstream & posteriorCharacteristicsFile, const int & param);
	void writeValidationHeader(std::ofstream & output, std::string & paramName);
	void writeValidationStats(ofstream & output, const int & param, double val);
	double getLOnedistwithPosterior(const int & param, Matrix & densities);
	double getRandomParameterAccordingToMarginal(const int & param, double randomVal);
	double getPosteriorScaledSD(const int & param);
	void setParameterRange(const std::string & param, pair<double,double> minmax);
	Matrix* getPointerToMarginalPosteriorDensityMatrix(){return &posteriorMatrix;};
};


#endif /* TPOSTERIORMATRIX_H_ */
