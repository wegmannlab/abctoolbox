//---------------------------------------------------------------------------
#ifndef TPriorH
#define TPriorH
#include "TRandomGenerator.h"
#include "stringFunctions.h"
#include "TLog.h"
#include <vector>
#include <map>
const double _nan=-999999;
const double log_zero_density = -99999999.9;

//TODO: replace hyper prior tags such that only true hyper priors are used
//------------------------------------------------------------------------------
//TPrior
//------------------------------------------------------------------------------
class TPrior{
   public:
	  std::string name;
	  bool isInt;
	  bool output;
	  double curValue, oldValue;
	  TRandomGenerator* randomGenerator;
	  bool changed;
	  bool isConstant;

	  TPrior(TRandomGenerator* RandomGenerator, std::string Name);
	  ~TPrior(){}
	  void initialize(std::vector<std::string> & line);
	  void makeCurValueInt();
	  void writeValue(const double& value, std::ofstream& file);
	  void writeCurValue(std::ofstream& file);
	  /*
	  void writeHyperPriorGamma(const double& arg, std::ofstream& file);
	  void writeHyperPriorBeta(std::ofstream& file);
	  void writeHyperPriorNormal(const double& stdev, std::ofstream& file);
	  void writeHyperPriorNormalPositive(const double& stdev, std::ofstream& file);
	  void writeHyperPriorLognormal(const double& stdev, std::ofstream& file);
	  void writeHyperPriorLognormalParametersInLog10Scale(const double& stdev, std::ofstream& file); // base 10!!!
	  */
	  void saveOldValue();
	  void resetOldValue();
	  void setCurValue(const double & newValue);
};

//------------------------------------------------------------------------------
//TSimplePrior
//------------------------------------------------------------------------------
class THierarchicalPrior;
class TSimplePrior:public TPrior{
   public:
	  double mcmcStep;
	  double upperLimit, lowerLimit;
	  bool isHyperPrior;
	  std::vector<THierarchicalPrior*> hypoPriors;

	  TSimplePrior(TRandomGenerator* RandomGenerator, std::string & Name, std::vector<std::string> & line);
	  TSimplePrior(TRandomGenerator* RandomGenerator, std::string & Name);
	  virtual ~TSimplePrior(){};
	  void setLimits(double min, double max);
	  void registerHypoPrior(THierarchicalPrior* Hypo){
		  hypoPriors.push_back(Hypo);
		  isHyperPrior = true;
	  };
	  void setMCMCStepRelativeToRange(double McmcStep);
	  void setMCMCStep(double McmcStep);
	  virtual void chooseStartingValueFromPrior(){
		  changeCurrentValue();
	  };
	  void changeCurrentValue();
	  virtual void _changeCurrentValue(){};
	  virtual double getLogPriorDensity();
	  virtual double getLogPriorDensityFromValue(const double& value);
	  virtual void getNewValuesMcmc();
	 // virtual void scale(const double& max, const double& min);
};
//------------------------------------------------------------------------------
class TUniformPrior:public TSimplePrior{
   public:
	  double logDensity;

	  TUniformPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line);
	  void _changeCurrentValue();
	  double getLogPriorDensityFromValue(const double& value);
};
class TFixedPrior:public TSimplePrior{
   public:
		TFixedPrior(TRandomGenerator* RandomGenerator, std::string & Name, std::vector<std::string> & line);
		TFixedPrior(TRandomGenerator* RandomGenerator, std::string Name, double val);
		void _changeCurrentValue(){changed=false;};
		double getLogPriorDensity();
		double getLogPriorDensityFromValue(const double& value);
		void getNewValuesMcmc(){changed=false;};
};
class TLogUniformPrior:public TSimplePrior{
   public:
      double logLimitDiff;
      double logLowerLimit, logUpperLimit;

	  TLogUniformPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line);
	  void _changeCurrentValue();
	  double getLogPriorDensityFromValue(const double& value);
};
class TNormalPrior:public TSimplePrior{
   public:
   double mean, sigma, logAreaAfterTruncation, logSigmaSqrtTwoPi, twoSigmaSquared;
	  TNormalPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line);
	  void _changeCurrentValue();
	  double getLogPriorDensityFromValue(const double& value);
	  //void scale(const double& min, const double& max);
};
class TLogNormalPrior:public TSimplePrior{
   public:
	  double mean, sigma;
	  double meanInLog, sigmaInLog;
	  double logSqrtTwoPiSigma, twoSigmaSquared, logAreaAfterTruncation;
	  TLogNormalPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line);
	  void _changeCurrentValue();
	  double complementaryErrorFunction(double x);
	  double complementaryErrorCheb(double x);
	  double getLogPriorDensityFromValue(const double& value);
	  //void scale(const double& min, const double& max);
};
class TGammaPrior:public TSimplePrior{
   public:
		double alpha, beta;
		double alphaLogBetaMinusLogGamma, alphaMinusOne, logAreaAfterTruncation;

		TGammaPrior(TRandomGenerator* RandomGenerator, std::string Name);
		TGammaPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line);
	  void initializeDensityCalculation();
	  void _changeCurrentValue();
	  double getLogPriorDensityFromValue(const double& value);
};
class TGammaMeanPrior:public TGammaPrior{
   public:
		double mean;

	  TGammaMeanPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line);
};
class TExponentialPrior:public TSimplePrior{
   public:
	  double lambda;
	  double logLambda;
	  double minInCumul, maxInCumul;
	  double logAreaAfterTruncation;

	  TExponentialPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line);
	  void initializeTruncation();
	  void _changeCurrentValue();
	  double getLogPriorDensityFromValue(const double& value);
};
class TGeneralizedParetoPrior:public TSimplePrior{
   public:
	  double locationMu;
	  double scaleSigma;
	  double shapeXi;
	  double logSigma;
	  double logAreaAfterTruncation;

	  TGeneralizedParetoPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line);
	  void initializeTruncation();
	  void _changeCurrentValue();
	  double getPriorDensity(const double& value);
	  double getLogPriorDensityFromValue(const double& value);
};
class TMirroredGeneralizedParetoPrior:public TSimplePrior{
   public:
	  double locationMu;
	  double scaleSigma;
	  double shapeXiLow, shapeXiHigh;
	  double zero;
	  double probAboveMu;
	  double logAreaAfterTruncationAboveMu, logAreaAfterTruncationBelowMu;
	  double logSigma;

	  TMirroredGeneralizedParetoPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line);
	  void initializeTruncation();
	  void _changeCurrentValue();
	  double getPriorDensity(const double& value);
	  double getLogPriorDensityFromValue(const double& value);
};
//---------------------------------------------------------------------------
//THierarchicalPrior
//---------------------------------------------------------------------------
class THierarchicalPrior:public TSimplePrior{
	public:
	std::vector<TSimplePrior*> fixedPriors;
	double logAreaAfterTruncation;
	bool updateTruncatedArea;

	THierarchicalPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line);
	~THierarchicalPrior(){
		for(std::vector<TSimplePrior*>::iterator it=fixedPriors.begin(); it!=fixedPriors.end(); ++it)
			delete *it;
	};
	void hyperPriorChanged(){
		updateTruncatedArea = true;
	};
	virtual void calculateTruncatedArea(){};
};

class THierarchicalNormalPrior:public THierarchicalPrior{
	public:
		TSimplePrior* mean;
		TSimplePrior* sigma;
		double logSqrtTwoPi;

		THierarchicalNormalPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line, std::map<std::string,TSimplePrior*> & mapSimplePrior);
		void _changeCurrentValue();
		void calculateTruncatedArea();
		double getLogPriorDensityFromValue(const double& value);
		//void scale(const double& max, const double& min);
};

class THierarchicalGammaPrior:public THierarchicalPrior{
	public:
		TSimplePrior* alpha;
		TSimplePrior* beta;

		THierarchicalGammaPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line, std::map<std::string,TSimplePrior*> & mapSimplePrior);
		void _changeCurrentValue();
		void calculateTruncatedArea();
		double getLogPriorDensityFromValue(const double& value);
		//void scale(const double& max, const double& min);
};

class THierarchicalGammaMeanPrior:public THierarchicalPrior{
	public:
		TSimplePrior* alpha;
		TSimplePrior* mean;

		THierarchicalGammaMeanPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line, std::map<std::string,TSimplePrior*> & mapSimplePrior);
		void _changeCurrentValue();
		void calculateTruncatedArea();
		double getLogPriorDensityFromValue(const double& value);
		//void scale(const double& max, const double& min);
};

class THierarchicalGeneralizedParetoPrior:public THierarchicalPrior{
	public:
		TSimplePrior* locationMu;
		TSimplePrior* scaleSigma;
		TSimplePrior* shapeXi;
		double zero, logAreaAfterTruncation, probPositive;

		THierarchicalGeneralizedParetoPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line, std::map<std::string,TSimplePrior*> & mapSimplePrior);
		void calcTruncatedArea();
		void _changeCurrentValue();
		void calculateTruncatedArea();
		double getLogPriorDensityFromValue(const double& value);
};

class THierarchicalMirroredGeneralizedParetoPrior:public THierarchicalPrior{
	public:
		TSimplePrior* locationMu;
		TSimplePrior* scaleSigma;
		TSimplePrior* shapeXiLow;
		TSimplePrior* shapeXiHigh;
		TSimplePrior* probAboveMu;
		double logAreaAfterTruncationAboveMu, logAreaAfterTruncationBelowMu;
		double zero, probAboveMuInternal;

		THierarchicalMirroredGeneralizedParetoPrior(TRandomGenerator* RandomGenerator, std::string Name, std::vector<std::string> & line, std::map<std::string,TSimplePrior*> & mapSimplePrior);
		void calculateTruncatedArea();
		void _changeCurrentValue();
		double getLogPriorDensityFromValue(const double& value);
};
//---------------------------------------------------------------------------
//TEquation
//---------------------------------------------------------------------------
class TPriorVector;
class TEquation_base{
private:
	std::string originalEuqation;
public:
	TEquation_base* equat_object;
	std::string* original;
	std::string myString;
	bool isConstant;
	TEquation_base(){
		equat_object=NULL;
		original=NULL;
		isConstant=false;
	};
	TEquation_base(std::string & s, TPriorVector* priorVec);
	TEquation_base(std::string s, TPriorVector* priorVec, std::string* Original);
	virtual ~TEquation_base(){if(equat_object!=NULL){delete equat_object;}};
	void init(std::string & equat, TPriorVector* priorVec, std::string* Original);
	bool generateAppropriateObject(std::string s, TEquation_base** equatObj, TPriorVector* priorVec);
	virtual double getValue(){return equat_object->getValue();};
	virtual void what(){std::cout << "TEquation_base from '" << myString << "'"; if(isConstant) std::cout << "(constant) "; std::cout << " with:" << std::endl; equat_object->what();};
	bool replaceOperatorsOfValuesWithValue();
	virtual bool hasChanged(){return equat_object->hasChanged();};
};

class TEquation_value: public TEquation_base{
public:
	double val;
	TEquation_value(std::string s, double Val, std::string* Original);
	~TEquation_value(){};
	double getValue(){return val;};
	virtual void what(){std::cout << "TEquation_value from '" << myString << "'"; if(isConstant) std::cout << "(constant)"; std::cout << std::endl; };
	virtual bool hasChanged(){return false;}
};

class TEquation_prior: public TEquation_base{
public:
	TPrior* prior;
	TEquation_prior(std::string s, TPrior* Prior, std::string* Original);
	~TEquation_prior(){};
	 double getValue(){return prior->curValue;};
	 virtual void what(){std::cout << "TEquation_prior from '" << myString << "'"; if(isConstant) std::cout << "(constant)"; std::cout << std::endl; };
	 virtual bool hasChanged(){return prior->changed;};
};

class TEquation_function: public TEquation_base{
public:
	double (TEquation_function::*pt2Function)(double);

	TEquation_function(std::string function, std::string Equat, TPriorVector* priorVec, std::string* Original);
	~TEquation_function(){};
	//implemented functions (allows for better error handling)
	double func_exp(double val);
	double func_pow10(double val);
	double func_pow2(double val);
	double func_log(double val);
	double func_log10(double val);
	double func_log2(double val);
	double func_sqrt(double val);
	double func_round(double val);
	double func_abs(double val);
	double func_ceil(double val);
	double func_floor(double val);

	double getValue(){
		return (*this.*pt2Function) (equat_object->getValue());
	};
	virtual void what(){std::cout << "TEquation_function from '" << myString << "' "; if(isConstant) std::cout << "(constant) "; std::cout << " with:" << std::endl;
		equat_object->what();
	};
	virtual bool hasChanged(){return equat_object->hasChanged();};
};

class TEquation_operator: public TEquation_base{
public:
	TEquation_base* first;
	TEquation_base* second;
	char sign;

	TEquation_operator(std::string firstString, std::string secondString, char Sign, TPriorVector* priorVec, std::string* Original);
	~TEquation_operator(){
		if(first!=NULL) delete first;
		if(second!=NULL) delete second;
	};
	double getValue();
	virtual void what(){
		std::cout << "TEquation_operator from '" << myString << "' "; if(isConstant) std::cout << "(constant) "; std::cout << "with:" << std::endl;
		first->what();
		second->what();
	};
	virtual bool hasChanged(){return (first->hasChanged() || second->hasChanged());};
};

//------------------------------------------------------------------------------
//TCombinedPrior
//------------------------------------------------------------------------------
class TCombinedPrior: public TPrior{
   public:
		TEquation_base* equation;
		TCombinedPrior(TRandomGenerator* RandomGenerator, std::string Name, std::string Equation, std::vector<std::string> & line, TPriorVector* priorVec);
		~TCombinedPrior(){
			delete equation;
		}
		void update(){
			if(equation->hasChanged()){
				curValue=equation->getValue();
				changed = true;
			}
		};
};


//---------------------------------------------------------------------------
//TRule
//---------------------------------------------------------------------------
class TRule{
   public:
	  TEquation_base* first;
	  TEquation_base* second;
	  char sign;

	  TRule(std::string firstString, char Sign, std::string secondString, TPriorVector* priorVec);
	  ~TRule(){
		  if(first) delete first;
		  if(second) delete second;
	  }
	  bool passed();
};
//---------------------------------------------------------------------------
//TPriorVector
//---------------------------------------------------------------------------
//TODO: make sub class using TEstimator to initialize and use it for posterior predictive simulations
class TPriorVector{
	public:
	    TRandomGenerator* randomGenerator;
	    std::vector<TSimplePrior*> simplePriors;
	    std::vector<TSimplePrior*>::iterator vecSimplePriorIt;
	    std::vector<TSimplePrior*>::iterator nextMcmcUpdatePrior;
		std::map<std::string,TSimplePrior*> mapSimplePrior;
		std::map<std::string,TSimplePrior*>::iterator curSimplePrior;
		std::vector<TCombinedPrior*> combinedPriors;
		std::vector<TCombinedPrior*>::iterator vecCombinedPriorIt;
		std::map<std::string,TCombinedPrior*> mapCombinedPrior;

		std::map<std::string,TCombinedPrior*>::iterator curCombinedPrior;
		std::vector<TRule*> rules;
		unsigned int numPrior;
		unsigned int numSimplePrior;
		unsigned int numCombinedPrior;
		TPrior* curPrior;
		TLog* logFile;
		std::string simplePriorNamesConcatenated;

		TPriorVector(std::string fileName, TLog* gotLogFile, TRandomGenerator* randomGenerator);
		~TPriorVector(){
			for(std::vector<TRule*>::iterator it=rules.begin(); it!=rules.end(); ++it)
				delete (*it);
		   for(curSimplePrior=mapSimplePrior.begin(); curSimplePrior!=mapSimplePrior.end(); ++curSimplePrior)
			   delete curSimplePrior->second;
		   for(curCombinedPrior=mapCombinedPrior.begin(); curCombinedPrior!=mapCombinedPrior.end(); ++curCombinedPrior)
			   delete curCombinedPrior->second;
		}
		std::string getSimplePriorName(const int & num);
		std::string& getListOfSimplePriorNames(){return simplePriorNamesConcatenated;};
		void getNewValues();
		void resetOldValues();
		void saveOldValues();
		void readPriorsAndRules(std::string fileName);
		void updateCombinedParameters();
		void writeHeader(std::ofstream& ofs);
		void writeHeaderSimplePriors(std::ofstream& ofs);
		void writeHeaderCombinedPriors(std::ofstream& ofs);
		void writeParameters(std::ofstream& ofs);
		void writeParametersSimplePriors(std::ofstream& ofs);
		void writeParametersCombinedPriors(std::ofstream& ofs);
		void getNewValuesMcmcUpdateOnePriorOnly();
		void getNewValuesMcmc(const int& priorNumber);
		//void getNewValuesMcmc(const int& priorNumber, double & mcmcStep);
		void getNewValuesMcmc();
		void getNewValuesMcmc(TSimplePrior* thisSimplePrior);
		//void getNewValuesMcmc(TSimplePrior* thisSimplePrior);
		bool getNewValuesPMC(double* newParams);
		double getLogPriorDensity();
		double getLogPriorDensity(double* values);
		double getValue(const std::string& Name);
		bool isPrior(const std::string& name);

		double calcEquation(const std::string & equation);
		double calcEquation(std::string equat, const std::string & original);
		bool isPriorTag(const std::string& name);
		bool isHyperPriorWithTag(const std::string& name);
		bool writeCurValueToFileFromName(const std::string& name, std::ofstream& file);
	    bool writeCurValueWithHyperprior(const std::string& name, std::ofstream& file);
		TPrior* getPriorFromName(const std::string& name);
		bool simplePriorExists(const std::string& name);
		bool combinedPriorExists(const std::string& name);
		bool priorExists(const std::string& name);
		bool simplePriorIsConstant(const int& priorNumber);
		bool priorIsConstant(const std::string& name);
		bool isHyperPrior(const int& priorNumber);
		TSimplePrior* getSimplePriorFromName(const std::string& name);
		TCombinedPrior* getCombinedPriorFromName(const std::string& name);
		int getNumberOfSimplePriorFromName(const std::string& name);
		void setSimplePriorValue(const int& priorNumber, const double& value);
		void setSimplePriorValue(const std::string& name, const double& val);
		void setSimplePriorMCMCStep(const int& priorNumber, const double& prop);
		void chooseStartingValueFromPrior(const int& priorNumber);
		void chooseStartingValueHyperPrior(const int& priorNumber);
};



#endif
