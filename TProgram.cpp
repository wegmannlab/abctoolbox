/*
 * TProgram.cpp
 *
 *  Created on: Jul 31, 2014
 *      Author: wegmannd
 */


#include "TProgram.h"


//---------------------------------------------------------------------------
//TProgram
//---------------------------------------------------------------------------
TProgram::TProgram(){
	priors = NULL;
	logfile = NULL;
	numValues = 0;
	valuesToPassRaw = NULL;
	valuesToPass = NULL;
	program = NULL;
	checkParams = false;
	paramsInitialized = false;
	initialized = false;
	getOutputIntoVectors = false;
	programIsInternal = false;
	externalErroHandling = 2;
}
TProgram::TProgram(std::string & prog, std::vector<std::string> & param, std::map<std::string, std::string> & tags, TPriorVector* Priors, TLog* Logfile, TRandomGenerator* RandomGenerator){
	initialize(prog, param, tags, Priors, Logfile, RandomGenerator);
}
//------------------------------------------------------------------------------
void TProgram::initialize(std::string & prog, std::vector<std::string> & param, std::map<std::string, std::string> & tags, TPriorVector* Priors, TLog* Logfile, TRandomGenerator* RandomGenerator){
	logfile = Logfile;
	initializeParameters(param, tags);
	parseParametersForPriors(Priors);
	initializeProgram(prog, RandomGenerator);
}

void TProgram::initialize(std::string & prog, std::vector<std::string> & param, std::map<std::string, std::string> & tags, TPriorVector* Priors, TLog* Logfile){
	logfile = Logfile;
	initializeParameters(param, tags);
	parseParametersForPriors(Priors);
	initializeProgram(prog);
}

void TProgram::initialize(std::string & prog, std::vector<std::string> & param, std::map<std::string, std::string> & tags, TLog* Logfile){
	logfile = Logfile;
	initializeParameters(param, tags);
	initializeProgram(prog);
}
//------------------------------------------------------------------------------
void TProgram::initializeParameters(std::vector<std::string> & param, std::map<std::string, std::string> & tags){
	numValues=param.size();
	if(numValues > 0){
		valuesToPassRaw = new std::string[numValues];
		valuesToPass = new std::string[numValues];
		paramsInitialized = true;

		//save values and check if they are priors or tags
		for(int i=0; i<numValues; ++i){
			valuesToPassRaw[i] = param[i];
			valuesToPass[i] = param[i];
			//check for presence of tag
			for(std::map<std::string, std::string>::iterator tagIt=tags.begin(); tagIt!=tags.end(); ++tagIt){
				if(valuesToPassRaw[i] == tagIt->first){
					valuesToPass[i] = tagIt->second;
					paramsToCheck.push_back(i);
					break;
				}
			}
		}
		if(paramsToCheck.size() > 0) checkParams = true;
	}
}

void TProgram::parseParametersForPriors(TPriorVector* Priors){
	priors = Priors;
	if(paramsInitialized){
		for(int i=0; i<numValues; ++i){
			if(priors->priorExists(valuesToPassRaw[i])){
				paramsThatArePriors.insert(std::pair<int, TPrior*>(i, priors->getPriorFromName(valuesToPassRaw[i])));
			}
		}
		if(paramsThatArePriors.size() > 0) checkParams = true;
	}
}
//------------------------------------------------------------------------------
void TProgram::checkIfItIsAValidProgram(std::string & prog){
	if(prog.empty()) throw "Can not initialize an empty string as a program!";
	//check if program exists
	struct stat st;
	if (stat(prog.c_str(), &st) < 0) throw "Program / script '" + prog +"' does not exist!";
	//check if it is executable
	if((st.st_mode & S_IEXEC) == 0) throw "Program / script '" + prog +"' is not executable!";
}

void TProgram::initializeProgram(std::string & prog, TRandomGenerator* RandomGenerator){
	name = prog;
	if(prog=="INTERNALGLM"){
		if(paramsThatArePriors.size() < 1) throw "The INTERNALGLM program requires model parameters as arguments!";
		program=new TGLM(valuesToPass, numValues, RandomGenerator);
		programIsInternal = true;
	} else if(prog=="INTERNALNORMDIST"){
		if(paramsThatArePriors.size() < 1) throw "The INTERNALNORMDIST program requires model parameters as arguments!";
		program=new TNormdist(valuesToPass, numValues, RandomGenerator);
		programIsInternal = true;
	} else if(prog=="INTERNALWF"){
		if(paramsThatArePriors.size() < 1) throw "The INTERNALWF program requires model parameters as arguments!";
		program=new TApproxWF(valuesToPass, numValues, RandomGenerator);
		programIsInternal = true;
	} else {
		checkIfItIsAValidProgram(prog);
		if(checkParams || !paramsInitialized) program = new TExecuteProgram(prog.c_str());
		else program = new TExecuteProgram(prog.c_str(), valuesToPass, numValues);
	}
	initialized = true;
}
void TProgram::initializeProgram(std::string & prog){
	name = prog;
	checkIfItIsAValidProgram(prog);
	if(checkParams  || !paramsInitialized) program = new TExecuteProgram(prog.c_str());
	else program = new TExecuteProgram(prog.c_str(), valuesToPass, numValues);
	initialized = true;
}

//------------------------------------------------------------------------------
void TProgram::writefoundPriorNames(TLog* logfile){
	if(paramsThatArePriors.size()>0){
		std::string out =  "Parameter name(s) among arguments: ";
		bool first=true;
		for(std::map<int, TPrior*>::iterator it = paramsThatArePriors.begin(); it != paramsThatArePriors.end(); ++it){
			if(first) first = false;
			else out += ", ";
			out += it->second->name;
		}
		logfile->conclude(out);
	} else logfile->conclude("No parameters among arguments");
}
//---------------------------------------------------------------------------
std::string TProgram::getCommandLine(std::map<std::string, std::string> & tags){
	std::string tmp=program->getMyExe();
	if(checkParams)	parseValuesToPass(tags);
	for(int i=0;i<numValues;++i)
		tmp+=" " + valuesToPass[i];
	return tmp;
}
std::string TProgram::getCommandLine(){
	std::string tmp=program->getMyExe();
	for(int i=0;i<numValues;++i)
		tmp+=" " + valuesToPass[i];
	return tmp;
}
//---------------------------------------------------------------------------
void TProgram::parseValuesToPass(std::map<std::string, std::string> & tags){
	//replace tags
	std::map<std::string, std::string>::iterator tagIt;
	for(std::vector<int>::iterator it=paramsToCheck.begin(); it!=paramsToCheck.end(); ++it){
		for(tagIt=tags.begin(); tagIt!=tags.end(); ++tagIt){
			 if(valuesToPassRaw[*it]==tagIt->first){
				 valuesToPass[*it]=tagIt->second;
			 }
		}
	}
	//replace prior tags with current values
	for(std::map<int, TPrior*>::iterator it = paramsThatArePriors.begin(); it != paramsThatArePriors.end(); ++it){
		valuesToPass[it->first] = toString(it->second->curValue);
	}
}
//---------------------------------------------------------------------------
void TProgram::execute(std::map<std::string, std::string> & tags){
	//adjust parameters if priors are among them
	int returnVal;
	//run simulation
	if(checkParams){
		parseValuesToPass(tags);
		returnVal = program->execute(valuesToPass, numValues);
	} else returnVal = program->execute();
	//check return value
	if(returnVal != 0 && externalErroHandling>0){
		std::string errorMessage = "The program / script '" + name + "'  returned an error (exit status " + toString(returnVal) + ")!";
		errorMessage += "\n" + getCommandLine(tags);
		if(externalErroHandling ==1) logfile->warning(errorMessage);
		else throw errorMessage;
	}
}
//---------------------------------------------------------------------------
void TProgram::setOutputFile(int & OutputFileDescriptor){
	program->setOutputFile(OutputFileDescriptor);
}
void TProgram::setRedirection(std::string & filename){
	program->setRedirectionFile(filename);
}
void TProgram::setExternalErrorhandling(std::string & externalErrorHandling){
	if(externalErrorHandling=="suppress") externalErroHandling = 0;
	else if(externalErrorHandling=="warn") externalErroHandling = 1;
	else if(externalErrorHandling=="abort") externalErroHandling = 2;
	else externalErroHandling = 2; //default
}
//---------------------------------------------------------------------------
void TProgram::addRequiredParameters(std::vector<TPrior*> & RequiredParams){
	std::vector<TPrior*>::iterator it;
	bool found;
	for(std::map<int, TPrior*>::iterator priorIt = paramsThatArePriors.begin(); priorIt != paramsThatArePriors.end(); ++priorIt){
		//check if parameter has already been added
		found = false;
		for(it = RequiredParams.begin(); it != RequiredParams.end(); ++it){
			if(*it == priorIt->second){
				found = true;
				break;
			}
		}
		if(!found) RequiredParams.push_back(priorIt->second);
	}
}
//---------------------------------------------------------------------------
bool TProgram::canBeLinkedToDataObject(){
	return !getOutputIntoVectors*programIsInternal;
};
void TProgram::linkToDataObject(){
	if(!canBeLinkedToDataObject()) throw "Program '" + name + "' can not be linked to a data object!";
	program->writeOutput = false;
	program->execute(valuesToPass, numValues);
	program->fillHeaderNames(vecOfNames);
	program->fillValues(vecOfValues);
	getOutputIntoVectors = true;
};

std::vector<std::string>& TProgram::getOutputNames(){
	return vecOfNames;
};
std::vector<double>& TProgram::getOutputValues(){
	program->fillValues(vecOfValues);
	if(vecOfNames.size() != vecOfValues.size()) program->fillHeaderNames(vecOfNames);
	return vecOfValues;
};
void TProgram::getOutputValues(double* array){
	program->fillValues(array);
};


