/*
 * TProgram.h
 *
 *  Created on: Jul 31, 2014
 *      Author: wegmannd
 */

#ifndef TPROGRAM_H_
#define TPROGRAM_H_

#include "TExecuteProgram.h"
#include "TApproxWF.h"
#include "TPrior.h"
#include <map>
#include <sys/stat.h>

//---------------------------------------------------------------------------
//TProgram
//---------------------------------------------------------------------------
//Todo: change std:string array to char* in order to pass directly to TExecuteProgram
class TProgram{
private:
	TLog* logfile;
	bool paramsInitialized;
	TExecuteProgram* program;
	bool programIsInternal;
	std::string* valuesToPassRaw;
	std::string* valuesToPass;
	bool checkParams;
	TPriorVector* priors;
	std::vector<int> paramsToCheck;
	std::map<int, TPrior*> paramsThatArePriors;
	int numValues;
	bool getOutputIntoVectors;
	std::vector<std::string> vecOfNames;
	std::vector<double> vecOfValues;
	int externalErroHandling; //0 = suppress, 1 = warn, 2 = abort

public:
	std::string name;
	bool initialized;

	TProgram();
	TProgram(std::string & prog, std::vector<std::string> & param, std::map<std::string, std::string> & tags, TPriorVector* Priors, TLog* Logfile, TRandomGenerator* RandomGenerator);
	~TProgram(){
		if(initialized){
			delete program;
		}
		if(paramsInitialized){
			delete[] valuesToPassRaw;
			delete[] valuesToPass;
		}
	};
	void initialize(std::string & prog, std::vector<std::string> & param, std::map<std::string, std::string> & tags, TPriorVector* Priors, TLog* Logfile, TRandomGenerator* RandomGenerator);
	void initialize(std::string & prog, std::vector<std::string> & param, std::map<std::string, std::string> & tags, TPriorVector* Priors, TLog* Logfile);
	void initialize(std::string & prog, std::vector<std::string> & param, std::map<std::string, std::string> & tags, TLog* Logfile);
	void initializeParameters(std::vector<std::string> & param, std::map<std::string, std::string> & tags);
	void parseParametersForPriors(TPriorVector* Priors);
	void checkIfItIsAValidProgram(std::string & prog);
	void initializeProgram(std::string & prog, TRandomGenerator* RandomGenerator);
	void initializeProgram(std::string & prog);
	void splitParamString(std::vector<std::string> & vec,  std::string & gotParameter);
	void writefoundPriorNames(TLog* logfile);
	std::string getCommandLine(std::map<std::string, std::string> & tags);
	std::string getCommandLine();
	void parseValuesToPass(std::map<std::string, std::string> & tags);
	void execute(std::map<std::string, std::string> & tags);
	void setOutputFile(int & OutputFileDescriptor);
	void setRedirection(std::string & filename);
	void setExternalErrorhandling(std::string & externalErrorHandling);
	void addRequiredParameters(std::vector<TPrior*> & RequiredParams);
	void storeOutputIntoVectorsForDataObject(){getOutputIntoVectors = true;};
	bool canBeLinkedToDataObject();
	void linkToDataObject();
	std::vector<std::string>& getOutputNames();
	std::vector<double>& getOutputValues();
	void getOutputValues(double* array);
};


#endif /* TPROGRAM_H_ */
