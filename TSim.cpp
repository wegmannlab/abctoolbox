//---------------------------------------------------------------------------


#include "TSim.h"
//---------------------------------------------------------------------------
TSim::TSim(TParameters* GotParameters, std::string gotexedir, TLog* gotLogFile, TRandomGenerator* RandomGenerator){
  logFile=gotLogFile;
  randomGenerator=RandomGenerator;
  gotParameters=GotParameters;
  exeDir=gotexedir;

  //read parameters from inputfile and write logfile
  logFile->startIndent("General settings:");
  tempSuffix=gotParameters->getParameterString("tempSuffix",false);
  if(tempSuffix=="") tempSuffix="-temp";
  logFile->list("Suffix for temporary output files: " +  tempSuffix);
  outName=gotParameters->getParameterStringWithDefault("outName", "ABCrun");
  logFile->list("Prefix for output files: " +  outName);
  if(gotParameters->getParameterDouble("separateOutputFiles", 0)){
		writeSeparateOutputFiles=true;
		logFile->list("A separate Outputfile is written for every obs file");
  } else {
		writeSeparateOutputFiles=false;
		logFile->list("All Statistics are written in one single Outputfile");
  }
  writeStatsToOutputFile = !gotParameters->parameterExists("hideStats");
  if(!writeStatsToOutputFile) logFile->list("Statistics will not be written to output files.");
  addDistanceToOutputfile = gotParameters->parameterExists("addDistanceToOutputfile");
  if(addDistanceToOutputfile) logFile->list("The Distance is added to the output files");
  forceOverWrite = gotParameters->parameterExists("overWrite");

  //read other stuff from log file
  repeatsPerParameterVector=gotParameters->getParameterInt("runsPerParameterVector",false);
  if(!repeatsPerParameterVector) repeatsPerParameterVector=1;
  logFile->list("Always performing " + toString(repeatsPerParameterVector) + " run(s) per parameter vector");

  //open output for all child programs / scripts
   if(gotParameters->parameterExists("childOutputName")){
  	  childOutputfileDescriptor=open(gotParameters->getParameterString("childOutputName").c_str(), O_WRONLY | O_TRUNC | O_CREAT, S_IRUSR | S_IRGRP | S_IWGRP | S_IWUSR);
  	  if(childOutputfileDescriptor<0) throw "Failed to open output file '"+gotParameters->getParameterString("childOutputName")+"'";
  	  logFile->list("Output of child processes is written to '"+gotParameters->getParameterString("childOutputName")+"'");
    } else childOutputfileDescriptor=-1;

   //read output triggers: one of either abort, warn, suppress
   errorHandling = gotParameters->getParameterStringWithDefault("executionErrors", "abort");
 	if(errorHandling == "abort") logFile->list("Will abort sampling when external programs return errors.");
 	else if(errorHandling == "warn")	logFile->list("Will print warnings when external programs return errors.");
 	else if(errorHandling == "suppress")	logFile->list("Will suppress errors returned by external programs.");
 	else throw "Unknown instruction to handle errors of external programs: '" + errorHandling + "' is neither 'abort', 'warn' nor 'suppress''!";

   //construct bins for density histograms
	if(gotParameters->parameterExists("densityBins")){
		logFile->startIndent("Estimating marginal parameter densities during sampling:");
		calcDensBins=true;
		numBins=gotParameters->getParameterInt("densityBins");
		if(numBins<10){
			numBins=10;
			logFile->warning("Number of density bins (" + toString(numBins) + ") too small (<10). Using 10 bins insetad.");
		}
		logFile->list("Using " + toString(numBins) + " bins");
		fillVectorFromString(gotParameters->getParameterString("densityLength", false), densityLengths, ';');
		std::string s; concatenateString(densityLengths, s, ", ");
		logFile->list("Writing densities after " + s + " simulations and at the end of the run.");
	} else calcDensBins=false;

  logFile->endIndent();

  //Initialize input files and data objects
  inputFiles=initializeSimulations(tempSuffix);

  myData=performFirstSimAndCreateDataObjects(inputFiles, tempSuffix);

  //create density bins
  if(calcDensBins){
	delta=new float[inputFiles->priors->numSimplePrior];
	densBins=new int*[inputFiles->priors->numSimplePrior];
	for(unsigned int i=0; i<inputFiles->priors->numSimplePrior; ++i){
		densBins[i]=new int[numBins];
		for(int j=0; j<numBins;++j) densBins[i][j]=0;
		delta[i]=(inputFiles->priors->simplePriors[i]->upperLimit - inputFiles->priors->simplePriors[i]->lowerLimit) /numBins;
	}
  }
  //linear transformation to statistics applied?
  linearCombFileName=gotParameters->getParameterString("linearCombName",0);
  if(!linearCombFileName.empty()){
	  if(writeSeparateOutputFiles) throw "Linear transformation can not be used if several output files are written!";
	  doLinearComb=true;
	  logFile->startIndent("Linear combinations:");
	  logFile->list("as defined in file '" +  linearCombFileName +"'");
	  doBoxCox=gotParameters->parameterExists("doBoxCox");
	  if(doBoxCox) logFile->list("all statistics are Box-Cox transformed prior to linear combination computation");
	  //initialize pcaObject
	  createLinearCombObject();
	  logFile->endIndent();
  } else doLinearComb=false;
};

//---------------------------------------------------------------------------
TInputFileVector* TSim::initializeSimulations(std::string & TempSuffix){
	//create input Files
	inputFilesVec.push_back(new TInputFileVector(gotParameters, logFile, randomGenerator, errorHandling, childOutputfileDescriptor, TempSuffix));
	return (*inputFilesVec.rbegin());
}

//---------------------------------------------------------------------------
TDataVector* TSim::performFirstSimAndCreateDataObjects(TInputFileVector* & theseInputFiles, std::string & TempSuffix){
	//create Data Object
	myDataVec.push_back(new TDataVector(gotParameters, inputFiles, logFile, errorHandling, childOutputfileDescriptor, TempSuffix));
	return myDataVec.back();
}

//---------------------------------------------------------------------------
void TSim::createLinearCombObject(){
	//create vector with the names from the input file, therefore the names of the simulated stats in the right order
	std::vector<std::string> statNames=myData->getNamesVector();
	std::vector<double> obsData=myData->getObsDataVector();
	//double* obsData;
	//myData->fillObsDataArray(obsData);
	if(doBoxCox){
		myLinearComb=new TLinearCombBoxCox(linearCombFileName, statNames);
	}
	else myLinearComb=new TLinearComb(linearCombFileName, statNames);
	myLinearComb->calcObsLinearComb(obsData);
}
//---------------------------------------------------------------------------
double TSim::calcDistance(){
   if(doLinearComb) return myData->calculateDistance(myLinearComb);
   else return myData->calculateDistance();
}
double TSim::calcDistance(TDataVector* Data){
   if(doLinearComb) return Data->calculateDistance(myLinearComb);
   else return Data->calculateDistance();
}
//---------------------------------------------------------------------------
void TSim::runSimulations(){
   throw "runSimulations() not implemented for TSim!!";
};
//---------------------------------------------------------------------------
void TSim::addCurrentParamsToDensityBins(){
	for(unsigned int i=0; i<inputFiles->priors->numSimplePrior; ++i){
		++densBins[i][(int)floor((inputFiles->priors->simplePriors[i]->curValue - inputFiles->priors->simplePriors[i]->lowerLimit)/delta[i])];
	}
}

void TSim::writeDensityBins(std::string length){
	//write bins
	if(calcDensBins){
		std::string str = outName + "_density_" + length + ".txt";
		std::ofstream out;
		 out.open(str.c_str());
		 for(int j=0; j<numBins;++j){
			 out << j;
			 for(unsigned int i=0; i<inputFiles->priors->numSimplePrior; ++i){
				 out << "\t" << inputFiles->priors->simplePriors[i]->lowerLimit + (j+0.5)*delta[i] << "\t" << densBins[i][j];
			 }
			 out << std::endl;
		 }
		 out.close();
	}
}

