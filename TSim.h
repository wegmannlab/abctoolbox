#ifndef TsimH
#define TsimH
#include "TInputFileVector.h"
#include "TDataVector.h"
#include "TParameters.h"
#include "TExecuteProgram.h"
#include "TLinearComb.h"
#include "TLinearCombBoxCox.h"
#include "TOutput.h"
#include "TLog.h"
#include <vector>

#ifndef _WINDOWS_
   #include <stdio.h>
   #include <fcntl.h>
   #include <unistd.h>
#endif

class TSim{
	public:
	std::string outName;
	  bool append;
	  bool writeSeparateOutputFiles;
	  bool addDistanceToOutputfile;
	  bool writeStatsToOutputFile;
	  bool forceOverWrite;
	  std::vector<TDataVector*> myDataVec;
	  TDataVector* myData;
	  std::vector<TInputFileVector*> inputFilesVec;
	  TInputFileVector* inputFiles;
	  TOutputVector* myOutputFiles;
	  std::ofstream est;
	  std::string exeDir;
	  std::string arpFile;
	  std::string arpDirectory;
	  std::string sumStatsTempFileName;
	  std::string tempSuffix;
	  TLog* logFile;
	  TRandomGenerator* randomGenerator;
	  TParameters* gotParameters;
	  bool doLinearComb, doBoxCox;
	  std::string linearCombFileName;
	  TLinearComb* myLinearComb;
	  int childOutputfileDescriptor;
	  int repeatsPerParameterVector;
	  std::string errorHandling;
	  //density bins
	  bool calcDensBins;
	  int** densBins;
	  float* delta;
	  int numBins;
	  std::vector<int> densityLengths;


      TSim(){
    	  myData=NULL;
    	  randomGenerator=NULL;
    	  logFile=NULL;
    	  repeatsPerParameterVector=1;
    	  myLinearComb=NULL;
    	  childOutputfileDescriptor=0;
    	  append=false;
    	  writeStatsToOutputFile = false;
    	  forceOverWrite = false;
    	  writeSeparateOutputFiles=false;
    	  addDistanceToOutputfile=false;
    	  doLinearComb=false;
    	  doBoxCox=false;
    	  inputFiles=NULL;
    	  gotParameters=NULL;
    	  calcDensBins=false;
    	  densBins=NULL;
    	  delta=NULL;
    	  numBins=0;
    	  myOutputFiles=NULL;
      };
      TSim(const TSim & sim);
	  TSim(TParameters* gotParameters, std::string gotexedir, TLog* gotLogFile, TRandomGenerator* RandomGenerator);
      virtual ~TSim(){
       	  if(calcDensBins){
			delete[] delta;
			for(unsigned int i=0; i<inputFiles->priors->numSimplePrior; ++i) delete[] densBins[i];
			delete[] densBins;
		  }
    	  for(std::vector<TDataVector*>::iterator it=myDataVec.begin(); it!=myDataVec.end(); ++it)
    	      delete (*it);
    	  for(std::vector<TInputFileVector*>::iterator it=inputFilesVec.begin(); it!=inputFilesVec.end(); ++it)
    		  delete (*it);
    	  if(childOutputfileDescriptor>0) close(childOutputfileDescriptor);
      };
	  virtual void runSimulations();
	  void createLinearCombObject();
	  double calcDistance();
	  double calcDistance(TDataVector* Data);
	  TInputFileVector* initializeSimulations(std::string & TempSuffix);
	  TDataVector* performFirstSimAndCreateDataObjects(TInputFileVector* & theseInputFiles, std::string & TempSuffix);
	  void addCurrentParamsToDensityBins();
	  void writeDensityBins(std::string length);
};
#endif
