//---------------------------------------------------------------------------
#include "TSimData.h"

//---------------------------------------------------------------------------
//TJointPosteriorStorage
//---------------------------------------------------------------------------
TJointPosteriorStorage::TJointPosteriorStorage(TLog* logFile){
	logfile = logFile;
	//initialize other variables
	t_j_sub = NULL;
	T_j_sub = NULL;
	vectorsAndMatricesAllocated = false;
	numSamplingVecs = -1;
	at = -1; //so next will give the first element after reset;
	numPosteriorDensityPoints = -1;
	areaComputed = false;
	hdiComputed = false;
	area = 0.0;
	cumulDist = NULL;
	cumulDistAllocated = false;
	cumulDistComputed = false;
};


void TJointPosteriorStorage::addParam(int param, std::string paramName){
	if(!contains(param)){
		params.push_back(param);
		paramNames.push_back(paramName);
	}
};

void TJointPosteriorStorage::restart(){
	at = -1;
};

void TJointPosteriorStorage::printPos(){
	std::cout << "POSITION = " << samplingVecs[0]->getPosition(0);
	for(int i=1; i<numSamplingVecs; ++i){
		std::cout << ", " << samplingVecs[i]->getPosition(0);
	}
	std::cout << std::endl;
};

void TJointPosteriorStorage::reset(){
	//clear densities
	for(std::vector<TJointPosteriorUnit*>::iterator it = samplingVecs.begin(); it != samplingVecs.end(); ++it){
		(*it)->clear();
	}

	//reset cumulative
	at = -1;
	areaComputed = false;
	hdiComputed = false;
	area = 0.0;
	cumulDistComputed = false;
};

bool TJointPosteriorStorage::contains(int param){
	for(std::vector<int>::iterator a=params.begin(); a!=params.end(); ++a){
		if((*a)==param) return true;
	}
	return false;
}

void TJointPosteriorStorage::check(){
	//sort params..
	sort(params.begin(), params.end());
	//only one parameter?
	if(params.size()==1) throw "Only one parameter chosen to compute joint posterior estimate!";
}

bool TJointPosteriorStorage::allExpectedParamsInArray(int* array, int arrayLength){
	bool found;
	for(std::vector<int>::iterator it=params.begin();it!=params.end(); ++it){
		found=false;
		for(int i=0;i<arrayLength;++i){
			if(array[i]==(*it)){
				found=true;
				break;
			}
		}
		if(!found) return false;
	}
	return true;
};

bool TJointPosteriorStorage::allExpectedParamsMinusOneInArray(int* array, int arrayLength){
	//the numbers in the array start by 0, rather than one!
	bool found;
	for(std::vector<int>::iterator it=params.begin();it!=params.end(); ++it){
		found=false;
		for(int i=0;i<arrayLength;++i){
			if(array[i]==(*it)-1){
				found=true;
				break;
			}
		}
		if(!found) return false;
	}
	return true;
};

void TJointPosteriorStorage::prepareVectorsAndMatrices(int jointPosteriorDensityPoints){
	if(vectorsAndMatricesAllocated && jointPosteriorDensityPoints != numPosteriorDensityPoints){
		clear();
		vectorsAndMatricesAllocated = false;
	}

	if(!vectorsAndMatricesAllocated){
		numPosteriorDensityPoints = jointPosteriorDensityPoints;
		numSamplingVecs = pow((double) numPosteriorDensityPoints, (int) params.size());
		t_j_sub = new ColumnVector(params.size());
		T_j_sub = new SymmetricMatrix(params.size());
		for(int i=0; i<numSamplingVecs; ++i){
			samplingVecs.push_back(new TJointPosteriorUnit(params.size(), i));
		}

		vectorsAndMatricesAllocated = true;
	}
};

void TJointPosteriorStorage::prepareSamplingVectors(int jointPosteriorDensityPoints){
	//equally spaced positions
	logfile->listFlush("Preparing grid of ", jointPosteriorDensityPoints, " equally spaced positions per parameter ... ");

	//Prepare ColumnVectors and Matrices needed
	prepareVectorsAndMatrices(jointPosteriorDensityPoints);

	//create ColumnVectors and compute weights
	int numPointsMinusOne = numPosteriorDensityPoints - 1;
	double step = 1.0/(double) numPointsMinusOne;
	int interval;
	int numSteps;
	for(unsigned int n=0; n<params.size(); ++n){
		interval = pow((double) numPosteriorDensityPoints, (int) n);
		for(int i=0; i<numSamplingVecs; ++i){
			numSteps = (int) floor(i/interval) % numPosteriorDensityPoints;
			samplingVecs[i]->setPosition(n, numSteps * step);
			if(numSteps==0 || numSteps==numPointsMinusOne) samplingVecs[i]->scaleWeight(0.5);
		}
	}

	//make sure the weights sum to 1
	standardizeWeights();
	logfile->write(" done!");
}

void TJointPosteriorStorage::prepareSamplingVectors(int jointPosteriorDensityPoints, Matrix posteriorMatrix){
	logfile->listFlush("Preparing grid of ", jointPosteriorDensityPoints, " positions per parameter according to marginal densities ... ");

	//Prepare ColumnVectors and Matrices needed
	prepareVectorsAndMatrices(jointPosteriorDensityPoints);

	//use 10% of points to fill undersampled location (especially with low densities)
	int numSteps = numPosteriorDensityPoints;
	int numStepsMinusOne = numSteps-1;

	//scan posteriorMatrix to get positions
	double** positions=new double*[params.size()];
	double step=1.0/(double) (numSteps-7);
	double marginalStep=(double) 1.0/(double) (posteriorMatrix.Nrows()-1);
	double area, oldArea=0, totalArea;
	double next;
	int param;
	int cur, old;
	for(unsigned int n=0; n<params.size(); ++n){
		param=params.at(n);
		positions[n]=new double[numPosteriorDensityPoints];
		totalArea=0;
		//get total area to standardize
		for(int i=1; i<posteriorMatrix.Nrows();++i){
			totalArea+=(posteriorMatrix(i+1,param)+posteriorMatrix(i,param))/(double)2.0;
		}
		totalArea=totalArea*marginalStep;
		//now run through marginal
		area=0; cur=1; old=1;
		//set first = 0
		positions[n][0]=0;
		for(int i=1; i<numStepsMinusOne; ++i){
			if(i==1) next=totalArea*step/100.0;
			else {
				if(i==2) next=totalArea*step/10.0;
				else {
					if(i==3) next=totalArea*step/2.0;
					else {
						if(i==(numSteps-4)) next=totalArea*(1.0-step/2.0);
						else {
							if(i==(numSteps-3)) next=totalArea*(1.0-step/10.0);
							else {
								if(i==(numSteps-2)) next=totalArea*(1.0-step/100.0);
								else next=step*(i-3)*totalArea;
							}
						}
					}
				}
			}

			while(area<next){
				old=cur;
				++cur;
				oldArea=area;
				area+=marginalStep*(posteriorMatrix(cur,param)+posteriorMatrix(old,param))/(double) 2.0;
			}
			positions[n][i]=marginalStep*(old + (next - oldArea)/(area - oldArea)-1);
		}
		positions[n][numStepsMinusOne]=1;
	}

	//now fill sampling vectors and compute weights
	int interval;
	for(unsigned int n=0; n<params.size(); ++n){
		interval=pow((double) numPosteriorDensityPoints, (int) n);
		for(int i=0; i<numSamplingVecs; ++i){
			numSteps=(int) floor(i/interval) % numPosteriorDensityPoints;
			samplingVecs[i]->setPosition(n, positions[n][numSteps]);
			if(numSteps==0){
				samplingVecs[i]->scaleWeight((positions[n][numSteps+1]-positions[n][numSteps])/2);
			} else {
				if(numSteps==(numPosteriorDensityPoints-1)){
					samplingVecs[i]->scaleWeight((positions[n][numSteps]-positions[n][numSteps-1])/2);
				} else {
					samplingVecs[i]->scaleWeight((positions[n][numSteps+1]-positions[n][numSteps-1])/2);
				}
			}
		}
	}
	//make sure the weights sum to 1
	standardizeWeights();

	//clean
	for(unsigned int n=0; n<params.size(); ++n){
		delete[] positions[n];
	}
	delete[] positions;

	logfile->write(" done!");
}

void TJointPosteriorStorage::standardizeWeights(){
	double sum=0;
	for(int i=0; i<numSamplingVecs; ++i) sum+=samplingVecs[i]->getWeight();
	for(int i=0; i<numSamplingVecs; ++i) samplingVecs[i]->scaleWeight(1/sum);
}

SymmetricMatrix* TJointPosteriorStorage::get_T_j_sub(SymmetricMatrix & T_j){
	for(unsigned int m=0; m<params.size(); ++m){
		for(unsigned int n=0; n<params.size(); ++n){
			T_j_sub->element(m,n)=T_j(params.at(m), params.at(n));
		}
	}
	return T_j_sub;
}

ColumnVector* TJointPosteriorStorage::next(){
	++at;
	return samplingVecs[at]->getPointerToPosition();
}

ColumnVector* TJointPosteriorStorage::get_t_j_sub(ColumnVector t_j){
	for(unsigned int n = 0; n<params.size(); ++n){
		t_j_sub->element(n) = t_j(params.at(n));
	}
	return t_j_sub;
};

long TJointPosteriorStorage::getIndexAt(ColumnVector* position){
	// find index of bin, into which the position falls using Gaussian Search
	for(std::vector<int>::size_type n=0; n<params.size(); ++n){
		if(position->element(n)<0.0 || position->element(n)>1.0) return -1;
	}

	long bottom, top, here;
	double lower, upper;
	long maxPos=numPosteriorDensityPoints-1;
	long index=0;
	for(std::vector<int>::size_type n=0; n<params.size(); ++n){
		long interval=pow((double) numPosteriorDensityPoints, (int) n);
		//start Gaussian search in middle
		bottom=-1; top=numPosteriorDensityPoints;
		while(true){
			here=(double) (bottom+top)/2.0;
			//get lower bound
			if(here==0) lower=samplingVecs[here*interval]->getPosition(n);
			else lower=(samplingVecs[(here-1)*interval]->getPosition(n)+samplingVecs[here*interval]->getPosition(n))/2.0;
			//is position lower than lower bound?
			if(position->element(n)<lower){
				//move top here
				top=here;
			} else {
				//get upper bound
				if(here==maxPos) upper=samplingVecs[here*interval]->getPosition(n);
				else upper=(samplingVecs[(here+1)*interval]->getPosition(n)+samplingVecs[here*interval]->getPosition(n))/2.0;
				//is position larger?
				if(position->element(n)>upper){
					//move bottom here
					bottom=here;
				} else {
					//we found it!
					index+=here*interval;
					break;
				}
			}
		}
	}
	return index;
}
void TJointPosteriorStorage::addSampleToDensity(double value, ColumnVector* position){
	//add a sample to the density
	//this function is used when samples are generated using MCMC
	long index=getIndexAt(position);
	if(index>=0) samplingVecs[index]->addToDensity(1.0);
}

void TJointPosteriorStorage::clear(){
	//clear all data storage variables
	if(vectorsAndMatricesAllocated){
		for(std::vector<TJointPosteriorUnit*>::iterator it=samplingVecs.begin(); it!=samplingVecs.end();++it){
			delete *it;
		}
		samplingVecs.clear();
		delete t_j_sub;
		delete T_j_sub;

		vectorsAndMatricesAllocated = false;
	}

	if(cumulDistAllocated){
		delete[] cumulDist;
		cumulDistAllocated = false;
	}
};

double TJointPosteriorStorage::getArea(){
	//get 2D, 3D, nD 'area' to standardize surface to one
	if(!areaComputed){
		area=0;
		for(int i=0; i<numSamplingVecs; ++i){
			area+=samplingVecs[i]->getArea();
		}
		areaComputed=true;
	}
	return area;
}

std::string TJointPosteriorStorage::getParameterString(char delim){
	std::ostringstream tos;
	tos << params[0];
	for(unsigned int n=1; n<params.size(); ++n){
		tos << delim << params[n];
	}
	return tos.str();
}

std::string TJointPosteriorStorage::getParameterStringOfNames(std::string delim, std::string lastDelim){
	std::string paramString="";
	if(lastDelim.empty()) lastDelim=delim;
	for(std::vector<int>::size_type i=0; i<params.size(); ++i){
		if(i==(params.size()-1)) paramString+=lastDelim;
		else if(i>0) paramString+=", ";
		paramString+="'"+paramNames[i]+"'";
	}
	return paramString;
}

void TJointPosteriorStorage::calculateCumulativeDistribution(){
	//allocate storage
	if(!cumulDistAllocated){
		cumulDist = new double[numSamplingVecs];
		cumulDistAllocated = true;
	}

	//calculate distribution
	double area = getArea();
	cumulDist[0] = samplingVecs[0]->getArea();
	for(long i=1; i<numSamplingVecs; ++i){
		cumulDist[i]=cumulDist[i-1]+samplingVecs[i]->getArea()/area;
	}
	cumulDistComputed = true;
};

ColumnVector TJointPosteriorStorage::getJointapproxRandomSample(TRandomGenerator* randomGenerator){
	//This is done with the approximation that within one unit area the posterior is flat
	//Therefore, we can first pick a unit area according to the cumulative distribution
	//Then we pick uniform random values for the parameters within the unit area

	//generate cumulative distribution;
	if(!cumulDistComputed){
		calculateCumulativeDistribution();
	}

	//generate a random sample
	ColumnVector randomSample(params.size());
	double r;
	long a;
	long interval;
	int marginalRank;
	double lower, upper;
	//pick a unit square
	r = randomGenerator->getRand();
	a = 0;
	if(r>0){
		while(cumulDist[a]<r) ++a;
	}

	//pick random parameter values within this unit area
	for(unsigned int n=0; n<params.size(); ++n){
		//find range
		interval=pow((double) numPosteriorDensityPoints, (int) n);
		//at border?
		marginalRank=(int) floor(a/interval) % numPosteriorDensityPoints;
		if(marginalRank==0){
			lower=samplingVecs[a]->getPosition(n);
			upper=(samplingVecs[a]->getPosition(n)+samplingVecs[a+interval]->getPosition(n))/2.0;
		} else {
			lower=(samplingVecs[a-interval]->getPosition(n)+samplingVecs[a]->getPosition(n))/2.0;
			if(marginalRank==(numPosteriorDensityPoints-1)){
				upper=samplingVecs[a]->getPosition(n);
			} else upper=(samplingVecs[a]->getPosition(n)+samplingVecs[a+interval]->getPosition(n))/2.0;
		}
		//pick random location within range
		randomSample.element(n)=randomGenerator->getRand(lower, upper);
	}
	return randomSample;
}

void TJointPosteriorStorage::writeApproximatelyRandomSamples(long numSamples, std::ofstream* out, TParamRanges* paramRanges,TRandomGenerator* randomGenerator){
	//this function generates samples from the joint posterior distribution
	std::string myParamString=getParameterStringOfNames(", ", " and ");
	logfile->listFlush("Generating ", numSamples, " random samples from the joint posterior for parameters " + myParamString + " ...");
	ColumnVector randomSample;
	long printstep=numSamples/(long) 100;
	int prog=0;
	if(printstep<1)	printstep=1;

	for(long i=0; i<numSamples; ++i){
		*out << i;
		//get random sample
		randomSample=getJointapproxRandomSample(randomGenerator);
		for(unsigned int n=0; n<params.size(); ++n){
			*out << "\t" << paramRanges->toParamScale(params.at(n), randomSample.element(n));
		}
		*out << std::endl;
		//report progress
		if(i % printstep == 0){
			++prog;
			logfile->listOverFlush("Generating ", numSamples, " random samples from the joint posterior for parameters " + myParamString + " ... (", prog, "%)");
		}
	}
	logfile->overWrite("Generating ", numSamples, " random samples from the joint posterior for parameters " + myParamString + " ... done!    ");
}

ColumnVector TJointPosteriorStorage::getJointMode(){
	ColumnVector jointMode(params.size());
	int maxPos=0;
	double max=0;
	for(int i=0; i<numSamplingVecs; ++i){
		if(samplingVecs[i]->getDensity()>max){
			max=samplingVecs[i]->getDensity();
			maxPos=i;
		}
	}
	for(unsigned int n=0; n<params.size(); ++n){
		jointMode.element(n)=samplingVecs[maxPos]->getPosition(n);
	}
	return jointMode;
}

bool compareDensity(TJointPosteriorUnit* a, TJointPosteriorUnit* b){
	return (a->getDensity() > b->getDensity());
};

bool compareNum(TJointPosteriorUnit* a, TJointPosteriorUnit* b){
	return (a->getNum() < b->getNum());
};

void TJointPosteriorStorage::computeHDI(){
	//Approximation: only WHOLE rectangles are used and we allow the surface to be disjunct
	if(!hdiComputed){
		//sort by density
		sort(samplingVecs.begin(), samplingVecs.end(), compareDensity);

		//compute HDI for each unit
		double hdi=0.0;
		for(TJointPosteriorUnit* i : samplingVecs){
			hdi += i->getArea();
			i->setHDI(hdi);
		}
		hdi = 1.0/hdi;

		for(TJointPosteriorUnit* i : samplingVecs){
			i->scaleHDI(hdi);
		}

		//sort again by standard order
		sort(samplingVecs.begin(), samplingVecs.end(), compareNum);
		hdiComputed = true;
	}
};

double TJointPosteriorStorage::getHDIAt(ColumnVector* position){
	computeHDI();
	long index=getIndexAt(position);
	if(index<0) return 1.0;
	return samplingVecs[index]->getHDI();
}

void TJointPosteriorStorage::writeToFile(std::string & filename, TParamRanges* paramRanges){
	std::ofstream out(filename.c_str());
	//write header...
	out << "number";
	for(std::vector<int>::iterator a=params.begin(); a!=params.end(); ++a){
		out << "\t" << paramRanges->getParamName((*a));
	}
	out << "\tdensity\tHDI" << std::endl;

	//write data
	computeHDI();
	double area=getArea();
	double jointBaseAreaParameterScale=1;
	for(unsigned int n=0; n<params.size(); ++n){
		jointBaseAreaParameterScale*=paramRanges->getMinMaxDiff(params.at(n));
	}

	for(int i=0; i<numSamplingVecs; ++i){
		out << i;
		for(unsigned int n=0; n<params.size(); ++n){
			out << "\t" << paramRanges->toParamScale(params.at(n), samplingVecs[i]->getPosition(n));
			//out << "\t" << samplingVecs[i]->getPosition(n);
		}
		out << "\t" << jointBaseAreaParameterScale*samplingVecs[i]->getDensity()/area;
		out << "\t" << samplingVecs[i]->getHDI() << std::endl;
	}
	out.close();
}
//---------------------------------------------------------------------------
//TParamRanges
//---------------------------------------------------------------------------
TParamRanges::TParamRanges(std::vector<std::string> * ParamNames){
	numParams=ParamNames->size();
	paramNames=ParamNames;
	paramMinMax=new std::pair<double,double>[numParams];
	paramMinMaxDiff=new double[numParams];
	minMaxFound=false;
}

void TParamRanges::setMinimum(const int & param, double min){
	int p=param-1;
	paramMinMax[p].first=min;
	calcMinMaxDiff(p);
}
void TParamRanges::setMaximum(const int & param, double max){
	paramMinMax[param-1].second=max;
}
void TParamRanges::setMinMax(const int & param, std::pair<double,double> & minmax){
	int p=param-1;
	paramMinMax[param-1]=minmax;
	calcMinMaxDiff(p);
}
void TParamRanges::setMinMax(const std::string & param, std::pair<double,double> & minmax){
	int p=getParamNumFromName(param);
	setMinMax(p, minmax);
}
void TParamRanges::calcMinMaxDiff(const int & paramMinusOne){
	paramMinMaxDiff[paramMinusOne]=paramMinMax[paramMinusOne].second-paramMinMax[paramMinusOne].first;
}
//---------------------------------------------------------------------------
void TParamRanges::findMinMax(std::vector<double*> & simParams){
	for(int j=0; j<numParams; ++j){
		paramMinMax[j].first=simParams[0][j];
		paramMinMax[j].second=simParams[0][j];
	}
	//get max value
	for(std::vector<double*>::iterator it=simParams.begin(); it!=simParams.end(); ++it){
		for(int j=0; j<numParams; ++j){
			if((*it)[j]<paramMinMax[j].first) paramMinMax[j].first=(*it)[j];
			if((*it)[j]>paramMinMax[j].second) paramMinMax[j].second=(*it)[j];
		}
	}
	for(int p=0; p<numParams; ++p) calcMinMaxDiff(p);

	minMaxFound=true;
}
//---------------------------------------------------------------------------
std::string TParamRanges::getParamName(const int & param){
	return (*paramNames)[param-1];
}
int TParamRanges::getParamNumFromName(const std::string & param){
	for(int i=0; i<numParams;++i){
		if((*paramNames)[i]==param) return i+1;
	}
	return -1;
}
//---------------------------------------------------------------------------
double TParamRanges::getMin(const int & param){
	return paramMinMax[param-1].first;
}
double TParamRanges::getMin(const std::string & param){
	int p=getParamNumFromName(param);
	if(p<0) throw "Parameter '"+param+"' does not exist in TParamRanges!";
	return getMin(p);
}
double TParamRanges::getMax(const int & param){
	return paramMinMax[param-1].second;
}
double TParamRanges::getMax(const std::string & param){
	int p=getParamNumFromName(param);
	if(p<0) throw "Parameter '"+param+"' does not exist in TParamRanges!";
	return getMax(p);
}
//---------------------------------------------------------------------------
void TParamRanges::standardizeParams(std::vector<double*> & simParams){
	//squeeze between 0 and 1
	if(!minMaxFound) findMinMax(simParams);
	for(std::vector<double*>::iterator it=simParams.begin(); it!=simParams.end(); ++it){
	  for(int j=0; j<numParams; ++j){
		  (*it)[j] = ((*it)[j]-paramMinMax[j].first)/(paramMinMaxDiff[j]);
	  }
	}
}
double TParamRanges::toParamScale(const int & param, const double & val){
	int p=param-1;
	return paramMinMax[p].first + val * paramMinMaxDiff[p];
}
double TParamRanges::toInternalScale(const int & param, const double & val){
	int p=param-1;
	return (val-paramMinMax[p].first)/paramMinMaxDiff[p];
}
//---------------------------------------------------------------------------
//TBaseSimData
//---------------------------------------------------------------------------
TBaseSimData::TBaseSimData(TObsData* obsData, TLog* LogFile){
	init(obsData, LogFile);
}
void TBaseSimData::init(TObsData* obsData, TLog* LogFile){
	logFile=LogFile;
	pointerToObsData=obsData;
	paramMaximaMinimaSet=false;
	numUsedSims=0;
}
//---------------------------------------------------------------------------
int TBaseSimData::getParamNumberFromName(std::string name){
	for(int i=0; i<numParams;++i){
		if(paramNames[i]==name) return i+1;
	}
	return -1;
}
bool TBaseSimData::isParam(std::string name){
	for(int i=0; i<numParams;++i){
		if(paramNames[i]==name) return true;
	}
	return false;
}
//---------------------------------------------------------------------------
std::string TBaseSimData::getParamNameFromNumber(int num){
	return paramNames[num-1];
}
//---------------------------------------------------------------------------
void TBaseSimData::findParamMaximaMinima(){
	throw "findParamMaximaMinima is not defined for the base class!";
}
double TBaseSimData::getParamMinimumFromName(std::string name){
	return paramRanges->getMin(name);
}
double TBaseSimData::getParamMaximumFromName(std::string name){
	return paramRanges->getMax(name);
}
void TBaseSimData::setParamMinMax(std::string name, std::pair<double,double> minmax){
	paramRanges->setMinMax(name, minmax);
}
//---------------------------------------------------------------------------
void TBaseSimData::addJointPosteriors(std::string & parameterString){
	//which sets of parameters? find all cases delimited by '#'
	if(!parameterString.empty()){
		int internalNumber;
		std::vector<std::string> sets;
		std::vector<std::string> paramNames;
		fillVectorFromString(parameterString, sets, '#');
		for(std::vector<std::string>::iterator setIt=sets.begin(); setIt!=sets.end(); ++setIt){
			jointPosteriorStorage.push_back(new TJointPosteriorStorage(logFile));
			fillVectorFromString(*setIt, paramNames, ',');
			for(std::vector<std::string>::iterator it=paramNames.begin(); it!=paramNames.end(); ++it){
				//check if it is a parameter
				if(!isParam(*it)) throw "Column " + *it + " is requested for joint posterior estimation, but is not a parameter!";
				internalNumber=getParamNumberFromName(*it);
				(*jointPosteriorStorage.rbegin())->addParam(internalNumber, *it);
			}
		}
		//check...
		for(std::vector<TJointPosteriorStorage*>::iterator a=jointPosteriorStorage.begin(); a!=jointPosteriorStorage.end(); ++a) (*a)->check();
		doJointPosteriors = true;
	}
}
//---------------------------------------------------------------------------
//TSimData
//---------------------------------------------------------------------------
TSimData::TSimData(std::string & simfilename, std::string params, long & MaxSimsToRead, TObsData* obsData, TLog* LogFile){
	init(simfilename, params, MaxSimsToRead, obsData, LogFile);
	readSimFile(simStats);
}
//---------------------------------------------------------------------------
void TSimData::init(std::string & simfilename, std::string & params, long & MaxSimsToRead, TObsData* obsData, TLog* LogFile){
	//this reads the file!
	TBaseSimData::init(obsData, LogFile);
	logFile=LogFile;
	simFileName=simfilename;
	paramString=params;
	numStats=pointerToObsData->numStats;
	statMeansCalculated=false;
	statSDsCalculated=false;
	standardizeStats=false;
	statMinMaxCalculated=false;
	paramsStandardized=false;
	retainedDefined=false;
	thresholdDefined=false;
	doJointPosteriors=false;
	trueParamsAvailable=false;
	//read the file
	maxSimsToRead=MaxSimsToRead;
	correlationsParamsComputed=false;
	correlationsStatsComputed=false;
}
//---------------------------------------------------------------------------
void TSimData::readParameterString(int numColsInFile){
	numParams=0;
	simColIsParam=new bool[numColsInFile];
	for(int j=0; j<numColsInFile; ++j) simColIsParam[j]=false;
	//read parameter string... -> get which params to estimate!
	std::vector<int> p;
	if(!fillSequenceFromString(paramString, p, ',')) throw "Problem reading the parameter string!";;
	for(std::vector<int>::iterator it=p.begin(); it!=p.end(); ++it){
	   if(*it < 1) throw "Problem reading the parameter string: parameter requested is < 1!";
	   if(*it > numColsInFile) throw "Requested parameter is missing in file '" + simFileName +"'!";
	   simColIsParam[*it - 1]=true;
	   ++numParams;
	}
	if(numParams==0) throw "Parameter string is empty!";
}
//---------------------------------------------------------------------------
void TSimData::readSimFile(std::vector<double*> & statVector){
   //open file and read first line
   logFile->listFlush("Reading up to ", maxSimsToRead, " simulations from file '" + simFileName + "' ...");
   std::ifstream is (simFileName.c_str()); // opening the file for reading
   if(!is) throw "File with simulations '" + simFileName + "' could not be opened!";

   //read header line with names
   std::vector<std::string> simNameVector;
   fillVectorFromLineWhiteSpaceSkipEmpty(is, simNameVector);

   std::string s=getFirstNonUniqueString(simNameVector);
      if(!s.empty()) throw "Entry '"+s+"' is listed multiple times in header of file '"+ simFileName +"'!";


   //go through the names and find corresponding columns for the stats
   int* matchSimToObsStats;
   bool* simColIsStat;
   simColIsStat=new bool[simNameVector.size()];
   matchSimToObsStats=new int[simNameVector.size()];
   for(unsigned int j=0; j<simNameVector.size(); ++j) simColIsStat[j]=false;
   int colnum;
   unsigned int i;
   int numStatsMatched=0;
   for(i=0; i<simNameVector.size();++i){
	  colnum=pointerToObsData->getStatColNumberFromName(simNameVector[i]);
	  if(colnum>=0){
		 simColIsStat[i]=true;
		 matchSimToObsStats[i]=colnum;
		 ++numStatsMatched;
	  }
   }
   if(numStatsMatched!=pointerToObsData->numStats) throw "Some statistic(s) is/are missing in the simulation file!";;

   //now match parameters
   readParameterString(simNameVector.size());

   //check if some params are also stats....
   for(unsigned int j=0; j<simNameVector.size(); ++j){
	   if(simColIsStat[j] && simColIsParam[j]) throw "Column '"+simNameVector[j]+"' is requested as parameter and statistic!";
   }

   //store parameter names
   for(unsigned int j=0; j<simNameVector.size(); ++j){
	   if(simColIsParam[j]){
		   paramNames.push_back(simNameVector[j]);
	   }
   }

   //now read params and stats into big arrays
   long l=0;
   std::string line;
   std::vector<double> vecLine;
   numReadSims=0;
   int prog=0;
   int oldProg=0;

   while(is.good() && !is.eof() && numReadSims<maxSimsToRead){
	   getline(is, line);
	   //remove white spaces
	   trimString(line);
	   ++l;
	   if(!line.empty()){
		  //allocate memory
		  try{
			  simParams.push_back(new double[numParams]);
			  statVector.push_back(new double[pointerToObsData->numStats]);
		  } catch (...){
		     throw "Not able to allocate enough memory! Are you really going to read in " + toString(maxSimsToRead) + "simulations?";
		  }

		  //read and store
		  fillVectorFromStringWhiteSpaceSkipEmptyCheck(line, vecLine);
		  if(vecLine.size() > simNameVector.size())
			  throw "Too many columns on line " + toString(l) + " of the simulation file '" + simFileName + "'!";
		  else if(vecLine.size()<simNameVector.size()) throw "Too few columns on line " + toString(l) + " of the simulation file '" + simFileName + "'!";
		  int p=0;
		  for(unsigned int i=0;i<simNameVector.size(); ++i){
			  if(simColIsParam[i]){
				  simParams[numReadSims][p]=vecLine[i];
				  ++p;
			  } else {
				  if(simColIsStat[i]){
					  statVector[numReadSims][matchSimToObsStats[i]]=vecLine[i];
				  }
			  }
		  }
		  ++numReadSims;
		  prog=100.0*((double) numReadSims / (double) maxSimsToRead);
		  if(prog>oldProg){
			  oldProg=prog;
			  logFile->listOverFlush("Reading up to ", maxSimsToRead, " simulations from file '" + simFileName + "' ... (", prog, "%)");
		  }
	  }
   }
   numReadSims=statVector.size();

   //done
   is.close();
   logFile->overList("Reading up to ", maxSimsToRead, " simulations from file '" + simFileName + "' ... done!   ");
   logFile->conclude(numReadSims, " simulations with ", numParams, " parameters and ", toString(pointerToObsData->numStats) + " statistics each.");

   //check if parameters are variable
   if(someParamsAreMonomorphic())
	   throw "Monomorphic parameters found! Please remove these from the list of parameters.";

   //Print warnings if statistics are
   calculateMinMaxofStats();
   if(someStatsAreMonomorphic()){
	   logFile->startIndent("The following statistics are monomorphic:");
	   for(int i=0; i<pointerToObsData->numStats;++i){
	   		if(statIsMonomorphic[i]){
	   			logFile->list(pointerToObsData->obsNameVector[i]);
	   		}
	   }
	   logFile->endIndent();
   }

   //delete variables
   delete[] matchSimToObsStats;
   delete[] simColIsStat;
}
//---------------------------------------------------------------------------
void TSimData::addTueParams(std::string filename){
	//todo: use standard string to read!
	if(!filename.empty()){
		trueParamName=filename;
		trueParamsAvailable=true;
		logFile->listFlush("Reading file with true parameters'" + trueParamName + "' ...");
		std::ifstream is (trueParamName.c_str()); // opening the file for reading
		if(!is) throw "File with true parameters '" + trueParamName + "' could not be opened!";

		//read header with names
		fillVectorFromLineWhiteSpaceSkipEmpty(is,trueNameVector);
		numTrueParams=trueNameVector.size();

		//are the parameters with true values estimated?
		trueParamNumInSimData=new int[numTrueParams];
		for(int i=0; i<numTrueParams; ++i){
			trueParamNumInSimData[i]=getParamNumberFromName(trueNameVector[i]);
			if(trueParamNumInSimData[i]<0) throw "The parameter '"+trueNameVector[i]+"' is not estimated, but requested from the file with true parameter values!";
		}

		//every line is a new dataset and corresponds to the observed data sets...
		std::vector<double*>::reverse_iterator it;
		std::vector<double> lineAsDouble;
		int lineNum=1;
		while(is.good() && !is.eof()){
			++lineNum;
			fillVectorFromLineWhiteSpaceSkipEmpty(is, lineAsDouble);
			if(lineAsDouble.size()>0){
				if((int) lineAsDouble.size()!=numTrueParams) throw "Number of items on line " + toString(lineNum) + " does not match the number of names in header line in true parameter file '"+trueParamName+"'!";
				trueParams.push_back(new double[numTrueParams]);
				it=trueParams.rbegin();
				for(int i=0; i<numTrueParams; ++i)
					(*it)[i]=lineAsDouble[i];
			}
		}
		is.close();
		logFile->write(" done!");
		logFile->conclude(trueParams.size(), " sets with ", numTrueParams, " true parameters each.");

	} else trueParamsAvailable=false;
}
//---------------------------------------------------------------------------
int TSimData::getParamNumFromColNum(int colNum){
	if(simColIsParam[colNum-1]){
		int n=0;
		for(int i=0; i<colNum; ++i){
			n+=simColIsParam[i];
		}
		return n;
	}
	return 0;
}
//---------------------------------------------------------------------------
void TSimData::calculateStatisticsMeans(){
	//prepare Mean array
	statMeans=new double[pointerToObsData->numStats];
	for(int j=0; j<pointerToObsData->numStats; ++j){
		statMeans[j]=0;
	}
   //calculate means
   for(std::vector<double*>::iterator it=simStats.begin(); it!=simStats.end(); ++it){
	  for(int j=0; j<pointerToObsData->numStats; ++j){
	     statMeans[j]+=(*it)[j];
	  }
   }
   for(int j=0; j<pointerToObsData->numStats; ++j){
	  statMeans[j]=statMeans[j]/numReadSims;
	  //statMeans[j]=0;
   }
   statMeansCalculated=true;
}
//---------------------------------------------------------------------------
void TSimData::calculateStatisticsSDs(){
	if(!statMeansCalculated) calculateStatisticsMeans();
	//prepare SD array
	statSDs=new double[pointerToObsData->numStats];
	for(int j=0; j<pointerToObsData->numStats; ++j){
		statSDs[j]=0;
	}
	//calculate SDs
	long i=0;
	for(std::vector<double*>::iterator it=simStats.begin(); it!=simStats.end(); ++it, ++i){
	  for(int j=0; j<pointerToObsData->numStats; ++j){
		 statSDs[j]+=((*it)[j]-statMeans[j])*((*it)[j]-statMeans[j]);

	  }
	}
	for(int j=0; j<pointerToObsData->numStats; ++j){
	  statSDs[j]=sqrt(statSDs[j]/(numReadSims-1));
	  //test if its is nan or inf
	  if(statSDs[j] != statSDs[j] || statSDs[j] == std::numeric_limits<double>::infinity()){
		  throw "Problem calculating the standard deviation of the simulated statistic '"+pointerToObsData->obsNameVector[j]+"'!";
	  }

	}
	statSDsCalculated=true;
}
//---------------------------------------------------------------------------
void TSimData::standardizeStatistics(){
	if(!statSDsCalculated) calculateStatisticsSDs();
	standardizeStats=true;
}
//---------------------------------------------------------------------------
void TSimData::findParamMaximaMinima(){
	if(!paramMaximaMinimaSet){
		paramRanges=new TParamRanges(&paramNames);
		paramMaximaMinimaSet=true;
		paramRanges->findMinMax(simParams);
	}
}
//---------------------------------------------------------------------------
void TSimData::standardizeParameters(){
	//standardize the params by substracting the minimum and dividing them by the maximum
	findParamMaximaMinima();
	paramRanges->standardizeParams(simParams);
	paramsStandardized=true;
}
//---------------------------------------------------------------------------
bool TSimData::someStatsAreMonomorphic(){
	calculateMinMaxofStats();
	bool ret=false;
	for(int i=0; i<pointerToObsData->numStats;++i){
		if(statIsMonomorphic[i]){
			logFile->warning("The statistic '"+pointerToObsData->obsNameVector[i]+"' is monomorphic!");
			ret=true;
		}
	}
	return ret;
}

//---------------------------------------------------------------------------
bool TSimData::someParamsAreMonomorphic(){
	if(!paramMaximaMinimaSet) findParamMaximaMinima();
	bool ret = false;
	for(int i=0; i<numParams;++i){
		if(paramRanges->getMinMaxDiff(i+1) == 0.0){
			logFile->warning("The parameter '" + paramNames[i] + "' is monomorphic!");
			ret=true;
		}
	}
	return ret;
}
//---------------------------------------------------------------------------
void TSimData::calculateMinMaxofStats(){
	if(!statMinMaxCalculated){
		//prepare storage
		statMaxima=new double[pointerToObsData->numStats];
		statMinima=new double[pointerToObsData->numStats];
		statIsMonomorphic = new bool[pointerToObsData->numStats];

		//compute min and max
		for(int j=0; j<pointerToObsData->numStats; ++j){
			statMinima[j]=simStats[0][j];
			statMaxima[j]=simStats[0][j];
		}
		for(std::vector<double*>::iterator it=simStats.begin(); it!=simStats.end(); ++it){
			for(int j=0; j<pointerToObsData->numStats; ++j){
				if((*it)[j]<statMinima[j]) statMinima[j]=(*it)[j];
				if((*it)[j]>statMaxima[j]) statMaxima[j]=(*it)[j];
			}
		}

		//store if stats are monomorphic
		for(int j=0; j<pointerToObsData->numStats; ++j){
			if(statMinima[j] == statMaxima[j]){
				statIsMonomorphic[j] = true;
				hasMonomorphicStats = true;
			}
			else statIsMonomorphic[j] = false;
		}

		statMinMaxCalculated=true;
	}
}
//---------------------------------------------------------------------------
void TSimData::checkIfstatsAreWithinRange(double* obsValueArray){
	if(!statMinMaxCalculated) calculateMinMaxofStats();
	for(int i=0; i<pointerToObsData->numStats;++i){
		if(obsValueArray[i]>statMaxima[i] || obsValueArray[i]<statMinima[i])
			logFile->warning("The observed value of the statistic '"+pointerToObsData->obsNameVector[i]+"' is outside the simulated range of this statistic!");
	}
}
//---------------------------------------------------------------------------
void TSimData::checkCorrelations(double & threshold){
	if(threshold < 1.0 || numToRetain < numReadSims){
		checkCorrelationsStatistics(threshold);
		checkCorrelationsParameters(threshold);
	}
}

void TSimData::checkCorrelationsAndRemoveCorrelatedStats(double & threshold){
	if(threshold < 1.0 || numToRetain < numReadSims){
		pruneCorrelatedStats(threshold);
		checkCorrelationsParameters(threshold);
	}
}

void TSimData::checkCorrelationsStatistics(double & threshold){
	logFile->startIndent("Checking for highly correlated statsitics:");
	computePairwiseCorrelationsStatistics();
	bool problematicPairsFound=false;
	for(int i=0; i<(pointerToObsData->numStats -1); ++i){
		for(int j=(i+1); j<pointerToObsData->numStats; ++j){
			if(correlationsStats[i][j] > threshold + 0.000000001){ //addition needed because otherwise maxCor = 1 is not sufficient to avoid pruning
				if(!problematicPairsFound){
					problematicPairsFound=true;
					logFile->startIndent("Found correlations above chosen threshold (", threshold,"): ");
				}
				logFile->list("Cor(" + pointerToObsData->obsNameVector[i] + ", " + pointerToObsData->obsNameVector[j] + ") = " + toString(correlationsStats[i][j]));
				problematicPairsFound=true;
			}
		}
	}
	if(problematicPairsFound) throw "Highly correlated statistics found!";
	else logFile->endIndent("No correlations exceeded the chosen threshold of ", threshold, ".");
}

void TSimData::pruneCorrelatedStats(double & threshold){
	logFile->startIndent("Pruning highly correlated statsitics:");
	//compute correlations
	computePairwiseCorrelationsStatistics();
	//Find the stats that have to be pruned
	int numStatsRemoved=0;
	bool* keepTheseStats=new bool[pointerToObsData->numStats];
	for(int i=0; i<pointerToObsData->numStats; ++i) keepTheseStats[i]=true;
	std::vector<int> theseStats;
	for(int i=0; i<(pointerToObsData->numStats -1); ++i){
		for(int j=(i+1); j<pointerToObsData->numStats; ++j){
			if(correlationsStats[i][j] > threshold + 0.000000001){ //addition needed because otherwise maxCor = 1 is not sufficient to avoid pruning
				if(keepTheseStats[i]==true && keepTheseStats[j]==true && pointerToObsData->statIsUsed(i) && pointerToObsData->statIsUsed(j)){
					if(numStatsRemoved==0){
						logFile->startIndent("Found correlations above chosen threshold (", threshold,"): ");
					}
					logFile->list("Removing statistics '" + pointerToObsData->obsNameVector[i] + "' (correlation with '" + pointerToObsData->obsNameVector[j] + "' is " + toString(correlationsStats[i][j]) + ")");
					theseStats.push_back(i);
					keepTheseStats[i]=false;
					++numStatsRemoved;
				}
			}
		}
	}
	//clean up
	delete[] keepTheseStats;
	//prune them
	pointerToObsData->excludeTheseStats(theseStats);
	//conclude
	if(numStatsRemoved>0) logFile->endIndent();
	logFile->conclude(numStatsRemoved, " statistics have been removed!");
	logFile->endIndent();
}

void TSimData::checkCorrelationsParameters(double & threshold){
	logFile->startIndent("Checking for highly correlated parameters:");
	computePairwiseCorrelationsParams();
	bool problematicPairsFound=false;
	for(int i=0; i<(numParams -1); ++i){
		for(int j=(i+1); j<numParams; ++j){
			if(correlationsParams[i][j]>threshold){
				if(!problematicPairsFound){
					problematicPairsFound=true;
					logFile->startIndent("Found correlations above chosen threshold (", threshold,"): ");
				}
				logFile->list("Cor(" + paramNames[i] + ", " + paramNames[j] + ") = " + toString(correlationsParams[i][j]));
			}
		}
	}
	if(problematicPairsFound) throw "Highly correlated parameters found!";
	else logFile->endIndent("No correlations exceeded the chosen threshold of ", threshold, ".");
}

double TSimData::computePearsonCorrelation(std::vector<double*> &vec1, int col1, double & vecSum1, double & sumOfSquares1, std::vector<double*> & vec2, int col2,double & vecSum2, double & sumOfSquares2){
	//compute Pearson correlation
	double pearson=0;
	std::vector<double*>::iterator it1=vec1.begin();
	for(std::vector<double*>::iterator it2=vec2.begin();it2!=vec2.end(); ++it1, ++it2) pearson+=(*it1)[col1] * (*it2)[col2];
	pearson=vec1.size() * pearson - vecSum1*vecSum2;
	pearson=pearson/(
			sqrt(vec1.size()*sumOfSquares1 - vecSum1*vecSum1)
		  * sqrt(vec2.size()*sumOfSquares2 - vecSum2*vecSum2)
	);
	return pearson;
};

void TSimData::computePairwiseCorrelations(std::vector<double*> & vec, int & length, double*** storage, bool verbose){
	if(verbose) logFile->listFlush("Calculate pairwise correlations ...");
	//compute all sums and sums of squares
	double* sum;
	double* sumOfSquares;
	sum=new double[length];
	sumOfSquares=new double[length];
	for(int i=0; i<length; ++i){
		sum[i]=0;
		sumOfSquares[i]=0;
	}
	for(std::vector<double*>::iterator it=vec.begin();it!=vec.end(); ++it){
		for(int i=0; i<length; ++i){
			sum[i]+=(*it)[i];
			sumOfSquares[i]+=(*it)[i]*(*it)[i];
		}
	}
	//prepare storage
	(*storage)=new double*[length];
	for(int i=0; i<length; ++i) (*storage)[i]=new double[length];
	//compute all correlations
	for(int i=0; i<(length-1); ++i){
		(*storage)[i][i] = 1;
#ifdef USE_OMP
#pragma omp parallel for
#endif
		for(int j=i+1; j<length; ++j){
			(*storage)[i][j] = computePearsonCorrelation(vec, i, sum[i], sumOfSquares[i], vec, j, sum[j], sumOfSquares[j]);
			(*storage)[j][i] = (*storage)[i][j]; //store symmetric
		}
	}
	(*storage)[length-1][length-1]=1;

	//clean up
	delete[] sum;
	delete[] sumOfSquares;
	if(verbose) logFile->write("done!");
}

void TSimData::computePairwiseCorrelationsStatistics(bool verbose){
	//compute all sums and sums of squares
	if(!correlationsStatsComputed){
		computePairwiseCorrelations(simStats, numStats, &correlationsStats, verbose);
		correlationsStatsComputed=true;
	}
}
void TSimData::computePairwiseCorrelationsParams(){
	//compute all sums and sums of squares
	if(!correlationsParamsComputed){
		computePairwiseCorrelations(simParams, numParams, &correlationsParams);
		correlationsParamsComputed=true;
	}
}
double TSimData::getCorrelationBetweenStats(const int & stat1, const int & stat2){
	computePairwiseCorrelationsStatistics(false);
	return correlationsStats[stat1][stat2];
}
//---------------------------------------------------------------------------
void TSimData::setThreshold(const double & ThresholdToUse){
	thresholdToUse=ThresholdToUse;
	thresholdDefined=true;
}
void TSimData::setNumToRetain(const int & NumToRetain){
	numToRetain=NumToRetain;
	retainedDefined=true;
}
//---------------------------------------------------------------------------
void TSimData::calculateDistances(int obsDataSetNum, double* distances, bool verbose){
	calculateDistances(pointerToObsData->getObservedValuesAllStats(obsDataSetNum), distances, verbose);
}
//---------------------------------------------------------------------------
void TSimData::calculateDistances(double* obsValueArray, double* distances, bool verbose){
	if(verbose){
		logFile->listFlush("Calculating distances ...");
		checkIfstatsAreWithinRange(obsValueArray);
	}

	//do we have to standardize the statistics?
	if(standardizeStats){
		//standardize observed stats
		double* obsStats;
		obsStats=new double[pointerToObsData->numStats];
		for(int j=0; j<pointerToObsData->numStats; ++j)
			obsStats[j]=(obsValueArray[j]-statMeans[j])/statSDs[j];

		//compute distance with standardized stats
		//for(std::vector<double*>::iterator it=simStats.begin(); it!=simStats.end(); ++it, ++i){
		double temp;
		#ifdef USE_OMP
		#pragma omp parallel for private(temp)
		#endif
		for(unsigned long i=0; i<simStats.size(); ++i){
			distances[i]=0;
			for(std::vector<int>::iterator its=pointerToObsData->usedStats.begin(); its!=pointerToObsData->usedStats.end(); ++its){
				//make sure to only include polymorphic stats when calculating distances from standardized stats
				if(!statIsMonomorphic[*its]){
					temp=(simStats[i][*its] - obsValueArray[*its])/statSDs[*its];
					distances[i]+=temp*temp;
				}
			}
		}
		delete[] obsStats;
	} else {
		//for(std::vector<double*>::iterator it=simStats.begin(); it!=simStats.end(); ++it, ++i){
		#ifdef USE_OMP
		#pragma omp parallel for
		#endif
		for(unsigned long i=0; i<simStats.size(); ++i){
			distances[i]=0;
			for(std::vector<int>::iterator its=pointerToObsData->usedStats.begin(); its!=pointerToObsData->usedStats.end(); ++its){
				distances[i]+=(simStats[i][*its]-obsValueArray[*its])*(simStats[i][*its]-obsValueArray[*its]);
			}
		}
	}
    if(verbose)  logFile->write(" done!");
}
//---------------------------------------------------------------------------
void TSimData::calculateDistancesLeaveOneOut(long simNum, double* distances, bool verbose){
	  if(verbose) logFile->listFlush("Calculating distances to simulation number ", simNum, " ...");
	  calculateDistances(simStats[simNum], distances, verbose);
	  //set distance of chosen sim to be very large, such that this sim will be excluded
	  distances[simNum]=99999999999;
	  if(verbose) logFile->write(" done!");
}
//---------------------------------------------------------------------------
void TSimData::calculateLargestDistances(int startObsDataSetNum, int numObsDataSets, double* distances, bool verbose=false){
	if(verbose) logFile->listFlush("Calculating largest distance among ",  numObsDataSets, " data sets ...");
	if(numObsDataSets<1 || startObsDataSetNum+numObsDataSets-1 > pointerToObsData->numObsDataSets) throw "TSimData::calculateLargestDistances()";

	//calculate distance to first
	calculateDistances(pointerToObsData->getObservedValuesAllStats(startObsDataSetNum), distances, false);

	//calculate other distances
	if(numObsDataSets>1){
		double* tempDistances=new double[numReadSims];
		for(int i=1; i<numObsDataSets; ++i){
			calculateDistances(pointerToObsData->getObservedValuesAllStats(startObsDataSetNum+i), tempDistances, false);
			//get if larger
			for(long j=0; j<numReadSims; ++j){
				if(distances[j]<tempDistances[j]) distances[j]=tempDistances[j];
			}
		}
		delete[] tempDistances;
	}
	if(verbose)  logFile->write(" done!");
}
//---------------------------------------------------------------------------
void TSimData::calculateMeanDistances(int startObsDataSetNum, int numObsDataSets, double* distances, bool verbose=false){
	if(verbose) logFile->listFlush("Calculating mean distance among ",  numObsDataSets, " data sets ...");
	if(numObsDataSets<1 || startObsDataSetNum+numObsDataSets-1 > pointerToObsData->numObsDataSets) throw "TSimData::calculateLargestDistances()";

	//calculate distance to first
	calculateDistances(pointerToObsData->getObservedValuesAllStats(startObsDataSetNum), distances, false);

	//calculate other distances
	if(numObsDataSets>1){
		double* tempDistances=new double[numReadSims];
		for(int i=1; i<numObsDataSets; ++i){
			calculateDistances(pointerToObsData->getObservedValuesAllStats(startObsDataSetNum+i), tempDistances, false);
			//add to distance
			for(long j=0; j<numReadSims; ++j)
				distances[j] += tempDistances[j];
		}
		delete[] tempDistances;
	}

	//get mean
	for(long j=0; j<numReadSims; ++j)
		distances[j] /= numObsDataSets;

	if(verbose)  logFile->write(" done!");
}
//---------------------------------------------------------------------------
void TSimData::fillStatAndParamMatrices(double* distances, bool verbose){
	//keep only retained -> check how they are defined
	retainedSims.clear();
	if(thresholdDefined){
		if(retainedDefined) fillRetainedFromThresholdAndNumRetained(distances, verbose);
		else fillRetainedFromThreshold(distances, verbose);
	} else {
		if(retainedDefined) fillRetainedVectorFromNumRetained(distances, verbose);
		else fillRetainedVectorKeepAll(verbose);
	}

	//create matrices
	numUsedSims=retainedSims.size();
	paramMatrix=Matrix(numUsedSims, numParams+1);
	statMatrix=Matrix(pointerToObsData->numUsedStats, numUsedSims);

	long i=1;
	int numParamsPlusOne=numParams+1;
	for(std::vector<long>::iterator it=retainedSims.begin(); it!=retainedSims.end(); ++it, ++i) {
		paramMatrix.submatrix(i,i,2,numParamsPlusOne) << simParams[*it];
		for(int j=0; j<pointerToObsData->numUsedStats; ++j)
			statMatrix(j+1,i)=simStats[*it][pointerToObsData->usedStats[j]];
	}
	paramMatrix.column(1) = 1;
	if(verbose){
		//logFile->write(" done!");
		if(threshold > 0)
			logFile->conclude(numUsedSims, " simulations retained with tolerance ", threshold, ".");
		else
			logFile->conclude(numUsedSims, " simulations retained");
	}
	//check if at least 10 simulations have been retained
	if(numUsedSims<10) throw "Given the current specified tolerance / numRetained, less than 10 (only " + toString(numUsedSims) + ") simulation have been retained!";
}

void TSimData::fillRetainedVectorKeepAll(bool verbose){
	if(verbose) logFile->list("Using all ", numReadSims, " simulations for the estimation (no rejection).");
	threshold = 999999999; //to make sure simulations are printed to bestSimsParamStats file.
	for(long i=0; i<numReadSims;++i) retainedSims.push_back(i);
}
//---------------------------------------------------------------------------
void TSimData::fillRetainedVectorFromNumRetained(double* distances, bool verbose){
   if(numToRetain < numReadSims){
	  if(verbose) logFile->listFlush("Retaining the best ", numToRetain, " simulations ...");

	  //sort the distances
	  //TODO: implement partial soting algorithm
	  double** distances_ordered = new double*[numReadSims];
	  double* curDist = distances;
	  double** curCaliDist=distances_ordered;
	  for(long i=0; i< numReadSims ;++curDist, ++curCaliDist,++i){
		*curCaliDist=curDist;
	  }
	  quicksortP(distances_ordered, 0, numReadSims-1);

	  //get threshold
	  threshold = *distances_ordered[numToRetain];
	  delete[] distances_ordered;

	  int j=1;
	  for(long i=0; i<numReadSims;++i){
		  if(distances[i]<=threshold){
			  retainedSims.push_back(i);
			 ++j;
			 if(j>numToRetain) break;
		 }
	  }

	  if(verbose) logFile->write(" done!");
   } else {
	   fillRetainedVectorKeepAll(verbose);
   }
}
//---------------------------------------------------------------------------
void TSimData::fillRetainedFromThresholdAndNumRetained(double* distances, bool verbose){
	if(verbose) logFile->listFlush("Retaining the best simulations with tolerance ", thresholdToUse, " and numRetained ", numToRetain, " ...");

   //get number of sims to retain
   for(long i=0; i<numReadSims;++i){
	   if(distances[i]<=thresholdToUse) retainedSims.push_back(i);
   }

   if(retainedSims.size() > (std::vector<long>::size_type) numToRetain){
	   //Do not use fillRetainedVectorFromNumRetained in order to speed up as we do not need to sort all distances
	   if(numToRetain<numReadSims){
		  //sort the distances
		  double** distances_ordered=new double*[retainedSims.size()];
		  double** curCaliDist=distances_ordered;

		  for(std::vector<long>::iterator it=retainedSims.begin(); it!=retainedSims.end(); ++it, ++curCaliDist){
			  *curCaliDist=&distances[*it];
		  }

		  quicksortP(distances_ordered, 0, retainedSims.size()-1);
		  //get threshold
		  threshold=*distances_ordered[numToRetain];

		  delete[] distances_ordered;

		  std::vector<long>::iterator it=retainedSims.begin();
		  while(retainedSims.size() > (std::vector<long>::size_type) numToRetain && it!=retainedSims.end()){
			  if(distances[*it]>=threshold){
				  retainedSims.erase(it);
			  } else {
				  ++it;
			  }
		  }
		  if(verbose) logFile->write(" done!");
	   } else {
		   if(verbose){
			   logFile->write(" done!");
		   }
		   fillRetainedVectorKeepAll(false);
	   }
   }
   else threshold=thresholdToUse;
}
//---------------------------------------------------------------------------
void TSimData::fillRetainedFromThreshold(double* distances, bool verbose){
	if(verbose) logFile->listFlush("Retaining the best simulations with tolerance ", thresholdToUse, " ...");

	retainedSims.clear();
   for(long i=0; i<numReadSims;++i){
	   if(distances[i]<=thresholdToUse) retainedSims.push_back(i);
   }
   threshold=thresholdToUse;
}
//---------------------------------------------------------------------------
void TSimData::fillVectorWithNumbersOfRetainedSims(std::vector<long> & sims, long numSims){
	int i=0;
	for(std::vector<long>::iterator it=retainedSims.begin(); i<numSims && it!=retainedSims.end(); ++it, ++i) {
		sims.push_back(*it);
	}
}
//---------------------------------------------------------------------------
void TSimData::fillVectorWithNumbersOfRandomSims(std::vector<long> & sims, long numSims, TRandomGenerator* randomGenerator){
	for(long i=numReadSims; i>0;--i){
		if(randomGenerator->getRand() < ((double) numSims/ (double) i)){
			sims.push_back(i-1);
			--numSims;
			if(numSims==0) break;
		}
	}
}
//---------------------------------------------------------------------------
double* TSimData::getPointerToStatValues(long & simNum){
	return simStats[simNum];
}
//---------------------------------------------------------------------------
void TSimData::getObsStatValuesIntoColumnVector(int obsDataSetNum, ColumnVector & colVector){
	pointerToObsData->getObsValuesIntoColumnVector(obsDataSetNum, colVector);
}
//---------------------------------------------------------------------------
void TSimData::getStatValuesIntoColumnVector(long simNum,  ColumnVector &colVector){
   colVector.resize(pointerToObsData->numUsedStats);
   for(int i=0; i<pointerToObsData->numUsedStats; ++i){
	   colVector(i+1)=simStats[simNum][pointerToObsData->usedStats[i]];
   }
}
void TSimData::getStatValuesIntoColumnVector(double* obsValueArray,  ColumnVector & colVector){
	colVector.resize(pointerToObsData->numUsedStats);
   for(int i=0; i<pointerToObsData->numUsedStats; ++i){
	   colVector(i+1)=obsValueArray[pointerToObsData->usedStats[i]];
   }
}
//---------------------------------------------------------------------------
double TSimData::getParamValueParamScale(const long & simNum, const int & param){
	return paramRanges->toParamScale(param, simParams[simNum][param]);
}
//---------------------------------------------------------------------------
void TSimData::writeRetained(std::string outputPrefix, double* distances, int dataSet){
	logFile->listFlush("Writing retained ...");
	std::string filename=outputPrefix+"BestSimsParamStats";
   if(dataSet>=0) filename+="_Obs"+toString(dataSet);
   filename+=".txt";
   std::ofstream output;
   output.open(filename.c_str());

   //write Header
   output << "Sim_num\tDist";
   for(int i=0;i<numParams;++i) output << "\t" << paramNames[i];
   for(int i=0;i<pointerToObsData->numStats;++i) output << "\t" << pointerToObsData->obsNameVector[i];
   output << std::endl;

   //write stats -> destandardize parameters!!
   int j=1;
   for(long i=0; i<numReadSims;++i){
	  if(distances[i] <= threshold && j <= numUsedSims){
		 output << (i+1) << "\t" << distances[i];
		 for(int k=0;k<numParams;++k) output << "\t" << paramRanges->toParamScale(k+1, simParams[i][k]);
		 for(int k=0;k<pointerToObsData->numStats;++k) output << "\t" << simStats[i][k];
		 output << std::endl;
		 ++j;
	  }
   }
   output.close();
   logFile->write(" done!");
}
void TSimData::writeRetainedScaled(std::string outputPrefix, double* distances, int dataSet){
	logFile->listFlush("Writing retained ...");
	std::string filename=outputPrefix+"BestSimsParamStatsScaled";
   if(dataSet>=0) filename+="_Obs"+toString(dataSet);
   filename+=".txt";
   std::ofstream output;
   output.open(filename.c_str());
   //write Header
   output << "Sim_num\tDist";
   for(int i=0;i<numParams;++i) output << "\t" << paramNames[i];
   for(int i=0;i<pointerToObsData->numStats;++i) output << "\t" << pointerToObsData->obsNameVector[i];
   output << std::endl;
   //write stats -> destandardize parameters!!
   int j=1;
   for(long i=0; i<numReadSims;++i){
	  if(distances[i]<=threshold && j<=numUsedSims){
		 output << (i+1) << "\t" << distances[i];
		 for(int k=0;k<numParams;++k) output << "\t" << simParams[i][k];
		 for(int k=0;k<pointerToObsData->numStats;++k) output << "\t" << simStats[i][k];
		 output << std::endl;
		 ++j;
	  }
   }
   output.close();
   logFile->write(" done!");
}
//---------------------------------------------------------------------------
//functions to sort an array of pointers
double TSimData::partitionP(double** a, long left, long right, long pivotIndex) {
	int storeIndex=left;
	//swap
	double* temp=a[right];
	a[right]=a[pivotIndex];
	a[pivotIndex]=temp;
	for (int i=left; i < right; i++){
		if(*a[i] <= *a[right]){
			//swap
			temp=a[storeIndex];
			a[storeIndex]=a[i];
			a[i]=temp;
			++storeIndex;
		}
	}
	//Pivot back to final place
	temp=a[right];
	a[right]=a[storeIndex];
	a[storeIndex]=temp;
	return storeIndex;
}

void TSimData::quicksortP(double** a, long left, long right){
	if(right > left){
		long pivotIndex=(left+right)/2;
		pivotIndex=partitionP(a, left, right, pivotIndex);
		quicksortP(a, left, pivotIndex-1);
		quicksortP(a, pivotIndex+1, right);
	}
}
//---------------------------------------------------------------------------
/*compute Tukey depth as the random Tukey depth similar to J.A. Cuesta-Albertos & A. Nieto-Reyes (2008)
	specifically, we will compute the Tukey depth of a point as the smallest depth obtained when the points
	are projected to a series of "random" directions and computing the depth on each of them. We will do this
	by first moving all points such that x=0 and then use the other points as "random" directions.
	*/

double TSimData::getMinTukeyDepth(double* x, unsigned long excludeDirection){
	//Each dot product has to be calculated twice: one when projecting vector y onto direction x,
	//and once when projecting vector x onto direction y -> compute once and store for both!

	//prepare storage
	int* tukey_count=new int[retainedSims.size()];
	for(unsigned long i=0; i<retainedSims.size();++i) tukey_count[i]=0;
	double p;
	unsigned long counter1=0;
	unsigned long counter2=0;
	for(std::vector<long>::iterator it1=retainedSims.begin(); it1!=retainedSims.end(); ++it1, ++counter1){
		if(excludeDirection!=counter1){
			counter2=counter1+1;
			for(std::vector<long>::iterator it2=(it1+1); it2!=retainedSims.end(); ++it2, ++counter2){
				p=0.0;
				for(int k=0; k<pointerToObsData->numUsedStats; ++k){
					p+=(simStats[*it1][pointerToObsData->usedStats[k]]-x[k]) * (simStats[*it2][pointerToObsData->usedStats[k]]-x[k]);
				}
				if(p>0.0){
					tukey_count[counter1]+=1;
					tukey_count[counter2]+=1;
				}
			}
		}
	}
	//find minimum depth
	//first get half the possible counts to mirror the depth
	long numRetainedUsed;
	long half_dir;
	if(excludeDirection>0) numRetainedUsed=retainedSims.size()-1;
	else numRetainedUsed=retainedSims.size();
	half_dir=numRetainedUsed/2;
	double min_tukey_count=retainedSims.size();
	for(unsigned long i=0; i<retainedSims.size(); ++i){
		if(tukey_count[i] > half_dir) tukey_count[i]=numRetainedUsed-tukey_count[i];
		if(tukey_count[i]<min_tukey_count && i!=excludeDirection) min_tukey_count=tukey_count[i];
	}
	//clean up
	delete[] tukey_count;
	//return
	return (double) min_tukey_count / (double) numRetainedUsed;
}
//---------------------------------------------------------------------------
double TSimData::computeTukeyDepthOfObservedAmongRetained(int obsDataSetNum){
	return getMinTukeyDepth(pointerToObsData->getObservedValues(obsDataSetNum));
}
double TSimData::computeTukeyDepthOfRetainedAmongRetained(long & thisRetainedSim){
	double* x=new double[pointerToObsData->numUsedStats];
	for(int k=0; k<pointerToObsData->numUsedStats; ++k){
		x[k]=simStats[retainedSims[thisRetainedSim]][pointerToObsData->usedStats[k]];
	}
	double tmp = getMinTukeyDepth(x, thisRetainedSim);
	delete[] x;
	return tmp;
}
//---------------------------------------------------------------------------
double TSimData::computePValueOfTukeyDepthAmongRetained(double tukeyDepth, unsigned int numReplicates){
	//idea is to calculate the Tukey depth of retained and to compare it to the Tukey depth given
	//choose the retained in the order they are in simStats
	if(numReplicates > retainedSims.size()) numReplicates=retainedSims.size();
	logFile->listFlush("Calculating the Tukey depth of ", numReplicates, " retained simulations ... (0%)");
	int prog;
	int oldProg=0;
	double pval=0.0;

	long counter=0;

#ifdef USE_OMP
	#pragma omp parallel for reduction(+:pval)
#endif
	for(long s=0; s<numReplicates; ++s){
		if(computeTukeyDepthOfRetainedAmongRetained(s)<tukeyDepth) ++pval;

#ifdef USE_OMP
		if(omp_get_thread_num() == 0){
#endif
			//report progress
			++counter;
#ifdef USE_OMP
			prog = 100 * omp_get_num_threads() * (double)counter / (double)numReplicates;
#else
			prog = 100 * (double)counter / (double)numReplicates;
#endif
			if(prog>oldProg){
				oldProg=prog;
				logFile->listOverFlush("Calculating the Tukey depth of ", numReplicates, " retained simulations ... (", prog, "%)");
			}
#ifdef USE_OMP
		}
#endif
	}
	logFile->overList("Calculating the Tukey depth of ", numReplicates, " retained simulations ... done!   ");

	//return p-value
	pval=pval/numReplicates;
	logFile->conclude("p-value = ", pval);
	return pval;
}

//---------------------------------------------------------------------------
//TSimDataLinearComb
//---------------------------------------------------------------------------
//TODO!!!!
/*
TSimDataLinearComb::TSimDataLinearComb(my_string simfilename, my_string sparams, long maxSimsToRead, double & maxCor, TObsData* obsData, my_string linearCombFileName, bool DoBoxCox, TLog* logFile){
	init(simfilename, params, MaxSimsToRead, maxCor, obsData, LogFile);
	doBoxCox=DoBoxCox;
	std::vector<my_string> statNames;
	pointerToObsData->fillNamesVector(statNames);
	if(doBoxCox) myLinearComb=new TLinearCombBoxCox(linearCombFileName, statNames);
	else myLinearComb=new TLinearComb(linearCombFileName, statNames);

	std::vector<double> pointerToObsData=myData->getObsDataVector();
	myLinearComb->calcObsLinearComb(obsData);


}
*/

//---------------------------------------------------------------------------
//TSimDataDistFromDifferentStats
//---------------------------------------------------------------------------
TSimDataDistFromDifferentStats::TSimDataDistFromDifferentStats(std::string simfilename, std::string params, long maxSimsToRead, TObsData* obsData, TLog* logFile){
	init(simfilename, params, maxSimsToRead, obsData, logFile);
	readSimFile(simStats);
	readSimDataForDist=false;
	readObsDataForDist=false;
	simDataForDist=NULL;
	obsDataForDist=NULL;
}
//---------------------------------------------------------------------------
void TSimDataDistFromDifferentStats::addfilesForDistance(std::string distObsFileName, std::string distSimFileName){
	logFile->startIndent("Reading files used to compute distances:");
	//read observed file
	readObsDataForDist=true;
	obsDataForDist=new TObsData(distObsFileName, logFile);
	//check
	if(obsDataForDist->numObsDataSets!=pointerToObsData->numObsDataSets)
		throw "The number of observed data sets differs between the file used for estimation and the file '" + distObsFileName + "' used to calculate distances!";
	//read sim file
	readSimDataForDist=true;
	//TODO: read ONLY stats, no need to read parameter 1!
	simDataForDist=new TSimData(distSimFileName, "1", maxSimsToRead, obsDataForDist, logFile);
	logFile->endIndent();
}

//---------------------------------------------------------------------------
void TSimDataDistFromDifferentStats::calculateDistances(int obsDataSetNum, double* distances, bool verbose){
	simDataForDist->calculateDistances(obsDataSetNum, distances, verbose);
}
//---------------------------------------------------------------------------
void TSimDataDistFromDifferentStats::calculateDistances(double* obsValueArray, double* distances, bool verbose){
	throw "Not implemented for TSimDataDistFromDifferentStats!";
}
//---------------------------------------------------------------------------
void TSimDataDistFromDifferentStats::calculateLargestDistances(int startObsDataSetNum, int numObsDataSets, double* distances, bool verbose=false){
	simDataForDist->calculateLargestDistances(startObsDataSetNum, numObsDataSets, distances, verbose);
}
//---------------------------------------------------------------------------
void TSimDataDistFromDifferentStats::calculateMeanDistances(int startObsDataSetNum, int numObsDataSets, double* distances, bool verbose=false){
	simDataForDist->calculateMeanDistances(startObsDataSetNum, numObsDataSets, distances, verbose);
}
//---------------------------------------------------------------------------
void TSimDataDistFromDifferentStats::calculateDistancesLeaveOneOut(long simNum, double* distances, bool verbose=false){
	simDataForDist->calculateDistancesLeaveOneOut(simNum, distances, verbose);
}

//---------------------------------------------------------------------------
//TSimDataVector: a class containing multiple simulation data sets
//---------------------------------------------------------------------------
TSimDataCommonParams::TSimDataCommonParams(std::vector<TSimData*>* simData){
	simDataVec=simData;
	TBaseSimData::init((*simDataVec)[0]->pointerToObsData, (*simDataVec)[0]->logFile);
	//check for common names
	std::vector<TSimData*>::iterator first=(*simDataVec).begin();
	std::vector<TSimData*>::iterator it;
	bool common;
	std::string nameToCheck;
	for(int i=1; i<=(*first)->numParams; ++i){
		//check if param exists in other sets
		common=true;
		nameToCheck=(*first)->getParamNameFromNumber(i);
		it=(*simDataVec).begin(); ++it;
		for(;it!=(*simDataVec).end(); ++it){
			if(!(*it)->isParam(nameToCheck)){
				common=false;
				break;
			}
		}
		//store if common
		if(common){
			paramNames.push_back(nameToCheck);
		}
	}

	//store min and max over all simData objects
	numParams=paramNames.size();
	paramRanges=new TParamRanges(&paramNames);
	paramMaximaMinimaSet=true;
	std::pair<double,double> minMaxTmp;
	double tmp;
	int i=0;
	for(std::vector<std::string>::iterator nameIt=paramNames.begin(); nameIt!=paramNames.end(); ++nameIt, ++i){
		it=(*simDataVec).begin();
		minMaxTmp.first=(*it)->getParamMinimumFromName(*nameIt);
		minMaxTmp.second=(*it)->getParamMaximumFromName(*nameIt);
		++it;
		for(;it!=(*simDataVec).end(); ++it){
			tmp=(*it)->getParamMinimumFromName(*nameIt);
			if(minMaxTmp.first > tmp) minMaxTmp.first=tmp;
			tmp=(*it)->getParamMaximumFromName(*nameIt);
			if(minMaxTmp.second<tmp) minMaxTmp.second=tmp;
		}
		paramRanges->setMinMax(*nameIt, minMaxTmp);
	}
}
void TSimDataCommonParams::findParamMaximaMinima(){
	int i=1; //start at 1!!
	for(std::vector<std::string>::iterator nameIt=paramNames.begin(); nameIt!=paramNames.end(); ++nameIt, ++i){
		for(std::vector<TSimData*>::iterator it=(*simDataVec).begin();it!=(*simDataVec).end(); ++it){
			(*it)->setParamMinMax(*nameIt, paramRanges->getMinMax(i));
		}
	}
}

//---------------------------------------------------------------------------
//TSimDataVector: a class containing multiple simulation data sets
//---------------------------------------------------------------------------
TSimDataVector::TSimDataVector(TParameters* Parameters, TObsData* obsData, TLog* LogFile){
	//init
	commonParamsMatched=false;
	parameters=Parameters;
	commonParams=NULL;
	logFile=LogFile;

	//check correlations
	maxCor = parameters->getParameterDoubleWithDefault("maxCor", 0.95);
	pruneCorrelatedStats = parameters->parameterExists("pruneCorrelatedStats");
	//initialize simulation data sets -> start with checking to produce error prior to the time consuming file parsing.
	std::vector<std::string> simNames;
	parameters->fillParameterIntoVector("simName", simNames, ';');
	if(simNames.size()==0) throw "No files with simulations given!";
	std::vector<std::string> paramString;
	parameters->fillParameterIntoVector("params", paramString, ';');
	if(simNames.size()!=paramString.size()) throw "The number of files given by 'simName' does not match the number of parameter definitions given!";

	std::vector<long> maxReadSims;
	parameters->fillParameterIntoVector("maxReadSims", maxReadSims, ';');
	if(simNames.size()!=maxReadSims.size()){
		if(maxReadSims.size()==1){
			for(std::vector<std::string>::size_type i=1; i<simNames.size(); ++i) maxReadSims.push_back(maxReadSims[0]);
		} else throw "The number of files given by 'simName' does not match the number of parameter definitions given!";
	}

	//Use linear Combination?
	//TODO!
/*
	std::vector<my_string> linearCombNames;
	useLinearComb=new bool[simNames.size()];
	for(int i=0; i<simNames.size(); ++i) useLinearComb[i]=false;
	if(parameters->parameterExists("linearCombName")){
		parameters->fillParameterIntoVector("linearCombName", linearCombNames, ';');
		if(simNames.size()!=linearCombNames.size()) throw "The number of files given by 'simName' data does not match the number of entries given by 'linearCombName'!";
		for(int i=0; i<simNames.size(); ++i){
			if(linearCombNames[i]!="") useLinearComb[i]=true;
		}
		if(parameters->parameterExists("boxcox"))
			parameters->fillParameterIntoVectorBool("boxcox", useBoxCox, ';');
		if(simNames.size()!=useBoxCox.size()) throw "The number of files given by 'simName' data does not match the number of entries given by 'useBoxCox'!";
	}
*/

	//calculate distance from different stats?
	distFromDifferentStats=new bool[simNames.size()];
	std::vector<std::string> distSimNames;
	std::vector<std::string> distObsNames;
    for(std::vector<std::string>::size_type i=0; i<simNames.size(); ++i) distFromDifferentStats[i]=false;
	if(parameters->getParameterString("distSimName", false)!=""){
		if(!parameters->getParameterString("distObsName", false).empty()){
			//check number of entries
			parameters->fillParameterIntoVector("distSimName", distSimNames, ';');
			if(simNames.size()!=distSimNames.size()) throw "The number of files given by 'simName' data does not match the number of entries given by 'distSimName'!";

			parameters->fillParameterIntoVector("distObsName", distObsNames, ';');
			if(simNames.size()!=distSimNames.size()) throw "The number of files given by 'simName' data does not match the number of entries given by 'distObsName'!";
			//check for which sets extra files are given
			for(std::vector<std::string>::size_type i=0; i<simNames.size(); ++i){
				if(distSimNames[i]=="" && distObsNames[i]=="") distFromDifferentStats[i]=false;
				else {
					if(distSimNames[i]!="" && distObsNames[i]!=""){
						//TODO: make that is is possible ???
						//if(useLinearComb[i]) throw "Impossible to use a linear combination transformation together with a set of simulations only for distance calculation!";
						distFromDifferentStats[i]=true;
					}
					else if(distSimNames[i]=="" && distObsNames[i]!="") throw "Name of file with simulations for distance computation missing!";
					else throw "Name of file with observed statistics for distance computation missing!";
				}
			}
		} else {
			logFile->warning("'distObsName' is not given: unable to use different files to compute distances!");
		}
	} else {
	   if(!parameters->getParameterString("distObsFile", false).empty()){
		   logFile->warning("'distSimName' is not given: unable to use different files to compute distances!");
	   }
	}

	//reading parameters regarding rejection (simulation set specific)
	//if present, a single value or a value for all simulation sets has to be given
	std::vector<int> tempNumRetained;
	std::vector<double> tempThreshold;
	if(parameters->parameterExists("numRetained")){
		parameters->fillParameterIntoVector("numRetained", tempNumRetained, ';');
		if(tempNumRetained.size()<simNames.size()){
			if(tempNumRetained.size()==1){
				for(std::vector<std::string>::size_type i=1; i<simNames.size(); ++i) tempNumRetained.push_back(tempNumRetained[0]);
			} else throw "The number of 'numRetained' entries does not match the number of files given by 'simNames'! Note that a single value may be used for all sets.";
		}
		for(std::vector<int>::iterator it=tempNumRetained.begin(); it!=tempNumRetained.end(); ++it){
			if((*it)<10) throw "The number of simulations to retain has to be at least 10!";
		}
	}
	if(parameters->parameterExists("tolerance")){
		parameters->fillParameterIntoVector("tolerance", tempThreshold, ';');
		if(tempNumRetained.size()<simNames.size()){
			if(tempThreshold.size()==1){
				for(std::vector<std::string>::size_type i=1; i<simNames.size(); ++i) tempThreshold.push_back(tempThreshold[0]);
			} else throw "The number of 'tolerance' entries does not match the number of files given by 'simNames'! Note that a single value may be used for all sets.";
		}
		for(std::vector<double>::iterator it=tempThreshold.begin(); it!=tempThreshold.end(); ++it){
			if((*it)<=0 && (*it)>1) throw "The threshold has to be >0 and <=1!";
		}
	}

	//reading files
	std::vector<std::string>::iterator psIt=paramString.begin();
	std::vector<long>::iterator mrsIt=maxReadSims.begin();
	std::vector<std::string>::iterator dsnIt=distSimNames.begin();
	std::vector<std::string>::iterator donIt=distObsNames.begin();
	std::map<int,TObsData>::iterator obsMapIt;
	int i=0;
	logFile->startIndent("Reading files with simulations:");
	for(std::vector<std::string>::iterator snIt=simNames.begin(); snIt!=simNames.end(); ++snIt, ++psIt, ++mrsIt, ++dsnIt, ++donIt, ++i){
		if(simNames.size()>1) logFile->startIndent("Reading simulations of set ", (i+1), ":");
		//if(useLinearComb[i]) simDataVec.push_back(new TSimDataLinearCombForDist(*snIt, *psIt, *mrsIt, maxCor, obsData, linearComNames[i], useBoxCox[i], logFile);
		if(distFromDifferentStats[i]) simDataVec.push_back(new TSimDataDistFromDifferentStats(*snIt, *psIt, *mrsIt, obsData, logFile));
		else simDataVec.push_back(new TSimData(*snIt, *psIt, *mrsIt, obsData, logFile));

		//check for unhealthy correlations
		if(pruneCorrelatedStats) (*simDataVec.rbegin())->pruneCorrelatedStats(maxCor);
		else (*simDataVec.rbegin())->checkCorrelations(maxCor);
		if((*simDataVec.rbegin())->numReadSims < 10) throw "Less than 10 simulations retained!";
		//distance from different stats
		if(distFromDifferentStats[i]) (*simDataVec.rbegin())->addfilesForDistance(*donIt, *dsnIt);
		if(simNames.size()>1) logFile->endIndent();
	}
	logFile->endIndent();
	//set rejection parameters
	if(tempNumRetained.size()==simNames.size()){
		std::vector<int>::iterator tIt=tempNumRetained.begin();
		for(std::vector<TSimData*>::iterator it=simDataVec.begin(); it!=simDataVec.end(); ++it, ++tIt){
			(*it)->setNumToRetain(*tIt);
		}
	}
	if(tempThreshold.size()==simNames.size()){
		std::vector<double>::iterator tIt=tempThreshold.begin();
		for(std::vector<TSimData*>::iterator it=simDataVec.begin(); it!=simDataVec.end(); ++it, ++tIt){
			(*it)->setThreshold(*tIt);
		}
	}

	//search min and max of all parameters for each simData
	for(std::vector<TSimData*>::iterator it=simDataVec.begin();it!=simDataVec.end(); ++it)
		(*it)->findParamMaximaMinima();

	//read true parameters
	if(parameters->parameterExists("trueParamName")) readTrueParameters();

}
//---------------------------------------------------------------------------
//check for common parameters across models
void TSimDataVector::matchCommonParams(){
	if(simDataVec.size()>1){
		commonParams=new TSimDataCommonParams(&simDataVec);
		commonParams->findParamMaximaMinima();
		commonParamsMatched=true;
	}
}
//---------------------------------------------------------------------------
void TSimDataVector::useParamRangesFromFile(std::string filename){
	 //open file
	 logFile->listFlush("Reading parameter ranges from file '" + filename + "' ...");
	 std::ifstream is (filename.c_str());
	 if(!is) throw "File '" + filename + "' could not be opened!";

	 //Each line is expected to have three columns: name of param, min, max
	 //empty lines and those escaped with // are ignored
	 //read header line with names
	 std::vector<std::string> vec;
	 std::string line;
	 std::vector<std::string> paramsNotFound;
	 std::pair<double,double> minmax;
	 int l=0;
	 bool paramFound=false;
	 while(is.good() && !is.eof()){
		 //read lineEntry
		 getline(is, line);
		 ++l;
		 line=extractBeforeDoubleSlash(line);
		 trimString(line);
		 if(!line.empty()){
			 fillVectorFromStringWhiteSpaceSkipEmpty(line, vec);
			 if(vec.size()!=3) throw "Number of entries on line '"+toString(l)+"' is different form 3! (Expect name, min and max)";
			 minmax.first=stringToDouble(vec[1]);
			 minmax.second=stringToDouble(vec[2]);
			 if(minmax.first>=minmax.second) throw "Min <= Max on line '"+toString(l)+"'!";
			 //search through sim files to match param
			 paramFound=false;
			 for(std::vector<TSimData*>::iterator it=simDataVec.begin(); it!=simDataVec.end(); ++it){
				 if((*it)->isParam(vec[0])){
					 (*it)->setParamMinMax(vec[0], minmax);
					 paramFound=true;
				 }
			 }
			 if(!paramFound) paramsNotFound.push_back(vec[0]);
		 }
	 }
	 logFile->write(" done!");
	 //Output params not found
	 if(paramsNotFound.size()>0){
		 std::string out;
		 std::string delim=", ";
		 concatenateString(paramsNotFound, out, delim);
		 logFile->warning("These names do not match any parameter: " + out);
	 }
}
//---------------------------------------------------------------------------

void TSimDataVector::initializeJointPosteriorCalculation(){
	std::vector<std::string> paramString;
	parameters->fillParameterIntoVector("jointPosteriors", paramString, ';');
	if(simDataVec.size()!=paramString.size()) throw "The number of files given by 'simNames' does not match the number of joint parameter definitions given by 'jointPosteriors'!";

	std::vector<std::string>::iterator psIt=paramString.begin();
	for(std::vector<TSimData*>::iterator it=simDataVec.begin(); it!=simDataVec.end(); ++it, ++psIt){
		if(!psIt->empty()){
			(*it)->addJointPosteriors(*psIt);
			computeJointPosteriors=true;
		}
	}
}
//---------------------------------------------------------------------------
void TSimDataVector::readTrueParameters(){
	std::vector<std::string> trueParamName;
	parameters->fillParameterIntoVector("trueParamName", trueParamName,';');
	if(simDataVec.size()!=trueParamName.size()) throw "The number of entries in 'trueParamName' does not match the number of files given by 'simNames'!";

	std::vector<std::string>::iterator psIt=trueParamName.begin();
	for(std::vector<TSimData*>::iterator it=simDataVec.begin(); it!=simDataVec.end(); ++it, ++psIt){
		if(!psIt->empty()) (*it)->addTueParams(*psIt);
	}
}
//---------------------------------------------------------------------------
void TSimDataVector::standardizeParameters(){
	for(std::vector<TSimData*>::iterator it=simDataVec.begin(); it!=simDataVec.end(); ++it){
		(*it)->standardizeParameters();
	}
}
void TSimDataVector::standardizeStatistics(){
	//only standardize statistics for distance calculations
	for(std::vector<TSimData*>::iterator it=simDataVec.begin(); it!=simDataVec.end(); ++it){
		(*it)->standardizeStatistics();
	}
}
//---------------------------------------------------------------------------
double TSimDataVector::getLargestCorrelation(const int & stat1, const int & stat2){
	std::vector<TSimData*>::iterator it=simDataVec.begin();
	double largestCor=(*it)->getCorrelationBetweenStats(stat1, stat2);
	for(++it; it!=simDataVec.end(); ++it){
		if((*it)->getCorrelationBetweenStats(stat1, stat2) > largestCor) largestCor=(*it)->getCorrelationBetweenStats(stat1, stat2);
	}
	return largestCor;
}















