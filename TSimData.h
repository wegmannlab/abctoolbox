//---------------------------------------------------------------------------

#ifndef TSimDataH
#define TSimDataH
#include <sstream>
#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include "newmat.h"
#include "newmatap.h"
#include "TParameters.h"
#include <vector>
#include <map>
#include "TObsData.h"
#include "newmatio.h"
#include <algorithm>
#include <math.h>
#include <limits>
#include "TLog.h"
#include "TRandomGenerator.h"
#include "TLinearComb.h"
#include "TLinearCombBoxCox.h"
#include "stringFunctions.h"
#ifdef USE_OMP
#include <omp.h>
#endif

//---------------------------------------------------------------------------
//Forward declaration
class TBaseSimData;
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
//TParamRanges: a class to manage parameter standardization
//NOTE: while internal param numbers tart at 0,
//	    the interface to other classes pretends they start at 1!
//---------------------------------------------------------------------------
class TParamRanges{
public:
	int numParams;
	pair<double,double>* paramMinMax;
	double* paramMinMaxDiff; //absolute values!
	bool minMaxFound;
	vector<std::string>* paramNames;

	TParamRanges(vector<std::string> * ParamNames);
	~TParamRanges(){
		delete[] paramMinMaxDiff;
		delete[] paramMinMax;
	};

	int getNumParams(){return numParams;};
	std::string getParamName(const int & param);
	int getParamNumFromName(const std::string & param);
	void setMinimum(const int & param, double min);
	void setMaximum(const int & param, double max);
	void setMinMax(const int & param, pair<double,double> & minmax);
	void setMinMax(const std::string & param, pair<double,double> & minmax);
	void calcMinMaxDiff(const int & paramMinusOne);
	void findMinMax(vector<double*> & simParams);
	double getMin(const int & param);
	double getMin(const std::string & param);
	double getMax(const int & param);
	double getMax(const std::string & param);
	double getMinMaxDiff(int param){return paramMinMaxDiff[param-1];};
	pair<double,double> getMinMax(int param){ return paramMinMax[param-1];}
	void standardizeParams(vector<double*> & simParams);
	double toParamScale(const int & param, const double & val);
	double toInternalScale(const int & param, const double & val);
};

//---------------------------------------------------------------------------
//Joint Posterior Storage
//---------------------------------------------------------------------------
class TJointPosteriorUnit{
private:
	double density;
	double HDI;
	double weight;
	ColumnVector position;
	int num;

public:
	TJointPosteriorUnit(int numParams, int Num){
		position.ReSize(numParams);
		num = Num;
		clear();
	};
	~TJointPosteriorUnit(){};
	void clear(){
		density = 0.0;
		weight = 1.0;
		HDI = 0.0;
	};
	void scaleWeight(double scale){ weight*=scale;};
	void scaleHDI(double scale){ HDI*=scale;};
	void setPosition(int p, double pos){ position.element(p)=pos;};
	double getWeight(){return weight;};
	double getDensity(){return density;};
	double getPosition(int p){return position.element(p);};
	int getNum(){return num;};
	double getHDI(){return HDI;};
	void addToDensity(double d){density+=d;};
	void setHDI(double d){HDI=d;};
	ColumnVector* getPointerToPosition(){return &position;};
	double getArea(){return (double) weight*density;};
};

class TJointPosteriorStorage{
private:
	vector<TJointPosteriorUnit*> samplingVecs;
	ColumnVector* t_j_sub;
	SymmetricMatrix* T_j_sub;
	long numSamplingVecs;
	bool vectorsAndMatricesAllocated;
	long at;
	int numPosteriorDensityPoints;
	bool areaComputed;
	bool hdiComputed;
	double area;
	double* cumulDist;
	bool cumulDistAllocated;
	bool cumulDistComputed;
	TLog* logfile;

public:
	vector<int> params;
	vector<std::string> paramNames;
	TJointPosteriorStorage(TLog* logFile);
	~TJointPosteriorStorage(){clear();};
	void addParam(int param, std::string paramName);
	void restart();
	void reset();

	void printPos();

	//int getNumSamplingVecs(){return numSamplingVecs;};
	int getNumSamplingVecs(){return samplingVecs.size();};
	ColumnVector* get_t_j_sub(ColumnVector t_j);
	SymmetricMatrix* get_T_j_sub(SymmetricMatrix & T_j);
	ColumnVector* next(); //will give the first element after reset;
	int numParams(){return params.size();};
	long getIndexAt(ColumnVector* position);
	void print();
	void check();
	bool contains(int param);
	bool allExpectedParamsInArray(int* array, int arrayLength);
	bool allExpectedParamsMinusOneInArray(int* array, int arrayLength);
	void prepareVectorsAndMatrices(int jointPosteriorDensityPoints);
	void prepareSamplingVectors(int posteriorDensityPoints);
	void prepareSamplingVectors(int jointPosteriorDensityPoints, Matrix posteriorMatrix);
	void standardizeWeights();
	void addDensity(double density){ samplingVecs[at]->addToDensity(density);};
	void addSampleToDensity(double value, ColumnVector* position);
	void clear();
	double getArea();
	int getAt(){return at;};
	std::string getParameterString(char delim);
	std::string getParameterStringOfNames(std::string delim, std::string lastDelim="");
	void calculateCumulativeDistribution();
	ColumnVector getJointapproxRandomSample(TRandomGenerator* randomGenerator);
	void writeApproximatelyRandomSamples(long numSamples, ofstream* out, TParamRanges* paramRanges, TRandomGenerator* randomGenerator);
	ColumnVector getJointMode();
	void computeHDI();
	double getHDIAt(ColumnVector* position);
	void writeToFile(std::string & filename, TParamRanges* paramRanges);
};

//---------------------------------------------------------------------------
//TBaseSimData: a class that contains the basic functionalities but no actual data. It is used for model averaging etc.
//---------------------------------------------------------------------------
class TBaseSimData{
public:
	TLog* logFile;
	int numParams;
	vector<std::string> paramNames;
	TObsData* pointerToObsData;

	//standardization
	TParamRanges* paramRanges;
	bool paramMaximaMinimaSet;

	//rejection
	Matrix statMatrix, paramMatrix;
	long numUsedSims;
	long numReadSims;

	//joint posteriors
	bool doJointPosteriors;
	vector<TJointPosteriorStorage*> jointPosteriorStorage;

	TBaseSimData(){
		  logFile=NULL;
		  numParams=0;
		  pointerToObsData=NULL;
		  paramRanges=NULL;
		  paramMaximaMinimaSet=false;
		  numUsedSims=0;
		  numReadSims=0;
		  doJointPosteriors=false;
	};
	TBaseSimData(TObsData* obsData, TLog* logFile);
	virtual ~TBaseSimData(){
		if(paramMaximaMinimaSet){
			delete paramRanges;
		}
	};
	void init(TObsData* obsData, TLog* LogFile);
	virtual void findParamMaximaMinima();
	double getParamMinimumFromName(std::string name);
	double getParamMaximumFromName(std::string name);
	void setParamMinMax(std::string name, pair<double,double> minmax);
	int getParamNumberFromName(std::string name);
	bool isParam(std::string name);
	std::string getParamNameFromNumber(int num);
	void addJointPosteriors(std::string & parameterString);
};
//---------------------------------------------------------------------------
//TSimData: the derived object actually containing simulated data
//---------------------------------------------------------------------------
class TSimData:public TBaseSimData {
   public:
		std::string simFileName;
		std::string paramString;
		bool* simColIsParam;
		vector<double*> simParams;
		vector<double*> simStats;

		long maxSimsToRead;
		int numStats;

		//true params
		vector<std::string> trueNameVector;
		vector<double*> trueParams;
		int numTrueParams;
		std::string trueParamName;
		bool trueParamsAvailable;
		int* trueParamNumInSimData;
		int internalTrueParamCounter;

		//standardization
		double* statMeans;
		double* statSDs;
		double* statMaxima;
		double* statMinima;
		bool* statIsMonomorphic;
		bool hasMonomorphicStats;
		bool statMinMaxCalculated;
		bool statMeansCalculated, statSDsCalculated, standardizeStats, paramsStandardized;

		//correlations
		double** correlationsStats;
		double** correlationsParams;
		bool correlationsParamsComputed, correlationsStatsComputed;

		//rejection
		int numToRetain;
		double thresholdToUse;
		bool retainedDefined, thresholdDefined;
		double threshold;
		vector<long> retainedSims;

	  TSimData(){
			logFile=NULL;
			simFileName="";
			paramString="";
			simColIsParam=NULL;
			maxSimsToRead=-1;
			numStats=-1;
			//true params
			numTrueParams=-1;
			trueParamName="";
			trueParamsAvailable=false;
			trueParamNumInSimData=NULL;
			internalTrueParamCounter=-1;
			//standardization
			statMeans=NULL;
			statSDs=NULL;
			statMaxima=NULL;
			statMinima=NULL;
			statIsMonomorphic=NULL;
			hasMonomorphicStats=false;
			statMinMaxCalculated=false;
			statMeansCalculated=false;
			statSDsCalculated=false;
			standardizeStats=false;
			paramsStandardized=false;
			//correlations
			correlationsStats=NULL;
			correlationsParams=NULL;
			correlationsParamsComputed=false;
			correlationsStatsComputed=false;
			//rejection
			numToRetain=-1;
			thresholdToUse=-1;
			retainedDefined=false;
			thresholdDefined=false;
			threshold=-1;
	  };
	  TSimData(std::string & simfilename, std::string params, long & maxSimsToRead, TObsData* obsData, TLog* logFile);

	  virtual ~TSimData(){
		  for(vector<double*>::iterator it=simParams.begin(); it!=simParams.end(); ++it) delete[] *it;
		  for(vector<double*>::iterator it=simStats.begin(); it!=simStats.end(); ++it) delete[] *it;
		  simParams.clear();
		  simStats.clear();

		  if(statMeansCalculated) delete[] statMeans;
		  if(statSDsCalculated) delete[] statSDs;
		  if(statMinMaxCalculated){
			  delete[] statMaxima;
			  delete[] statMinima;
			  delete[] statIsMonomorphic;
		  }
		  delete[] simColIsParam;
		  for(vector<TJointPosteriorStorage*>::iterator it=jointPosteriorStorage.begin(); it!=jointPosteriorStorage.end(); ++it){
			  delete (*it);
		  }
		  jointPosteriorStorage.clear();
		  if(correlationsStatsComputed){
			  for(int i=0; i<numStats; ++i){
				  delete[] correlationsStats[i];
			  }
			  delete[] correlationsStats;
		  }
		  if(correlationsParamsComputed){
			  for(int i=0; i<numParams; ++i){
				  delete[] correlationsParams[i];
			  }
			  delete[] correlationsParams;
		  }
		  if(trueParamsAvailable){
			  delete[] trueParamNumInSimData;
			  for(vector<double*>::iterator it=trueParams.begin(); it!=trueParams.end(); ++it) delete[] *it;
		  }
	  }
	  void init(std::string & simfilename, std::string & params, long & maxSimsToRead, TObsData* obsData, TLog* logFile);
	  void readParameterString(int numColsInFile);
	  virtual void readSimFile(vector<double*> & statVector);
	  void addTueParams(std::string filename);
	  virtual void addfilesForDistance(std::string distObsFileName, std::string distSimFileName){
		throw "Not possible in base class!";
	  };
	  int getParamNumFromColNum(int colNum);
	  virtual void findParamMaximaMinima();
	  void calculateStatisticsMeans();
	  void calculateStatisticsSDs();
	  void standardizeStatistics();
	  void standardizeParameters();
	  bool someStatsAreMonomorphic();
	  bool someParamsAreMonomorphic();
	  void calculateMinMaxofStats();
	  void checkIfstatsAreWithinRange(double* obsValueArray);

	  //check correlations
	  void checkCorrelations(double & threshold);
	  void checkCorrelationsAndRemoveCorrelatedStats(double & threshold);
	  void checkCorrelationsStatistics(double & threshold);
	  void pruneCorrelatedStats(double & threshold);
	  void checkCorrelationsParameters(double & threshold);
	  double computePearsonCorrelation(vector<double*> &vec1, int col1, double & vecSum1, double & sumOfSquares1, vector<double*> & vec2, int col2,double & vecSum2, double & sumOfSquares2);
	  void computePairwiseCorrelations(vector<double*> & vec, int & length, double*** storage, bool verbose=true);
	  void computePairwiseCorrelationsStatistics(bool verbose=true);
	  void computePairwiseCorrelationsParams();
	  double getCorrelationBetweenStats(const int & stat1, const int & stat2);
	  void setNumToRetain(const int & NumToRetain);
	  void setThreshold(const double & ThresholdToUse);

	  //calculate distances
	  virtual void calculateDistances(int obsDataSetNum, double* distances, bool verbose);
	  virtual void calculateDistances(double* obsValueArray, double* distances, bool verbose);
	  virtual void calculateLargestDistances(int startObsDataSetNum, int numObsDataSets, double* distances, bool verbose);
	  virtual void calculateMeanDistances(int startObsDataSetNum, int numObsDataSets, double* distances, bool verbose);
	  virtual void calculateDistancesLeaveOneOut(long simNum, double* distances, bool verbose);
	  //rejection and associated functions
	  void fillStatAndParamMatrices(double* distances, bool verbose=false);
	  void fillRetainedVectorKeepAll(bool verbose=false);
	  void fillRetainedVectorFromNumRetained(double* distances, bool verbose=false);
	  void fillRetainedFromThresholdAndNumRetained(double* distances, bool verbose);
	  void fillRetainedFromThreshold(double* distances, bool verbose);
	  void fillVectorWithNumbersOfRetainedSims(vector<long> & sims, long numSims);
	  void fillVectorWithNumbersOfRandomSims(vector<long> & sims, long numSims, TRandomGenerator* randomGenerator);
	  virtual double* getPointerToStatValues(long & simNum);
	  void getObsStatValuesIntoColumnVector(int obsDataSetNum, ColumnVector & colVector);
	  void getStatValuesIntoColumnVector(long simNum,  ColumnVector & colVector);
	  void getStatValuesIntoColumnVector(double* obsValueArray,  ColumnVector & colVector);
	  double getParamValue(long simNum, int param){ return simParams[simNum][param]; };
	  double getParamValueParamScale(const long & simNum, const int & param);
	  void writeRetained(std::string outputPrefix, double* distances, int dataSet=-1);
	  void writeRetainedScaled(std::string outputPrefix, double* distances, int dataSet=-1);
	  double partitionP(double** a, long left, long right, long pivotIndex);
	  void quicksortP(double** a, long left, long right);
	  double getMinTukeyDepth(double* x, unsigned long excludeDirection=-1);
	  double computeTukeyDepthOfObservedAmongRetained(int obsDataSetNum);
	  double computeTukeyDepthOfRetainedAmongRetained(long & thisRetainedSim);
	  double computePValueOfTukeyDepthAmongRetained(double tukeyDepth, unsigned int numReplicates);
};

//TODO: Transformation on the fly. Store transformed!!!
/*
class TSimDataLinearComb:public TSimData{
public:
	bool doBoxCox;
	TLinearComb* myLinearComb;

	TSimDataLinearComb(my_string simfilename, my_string sparams, long maxSimsToRead, double & maxCor, TObsData* obsData, my_string linearCombFileName, bool DoBoxCox, TLog* logFile);
	void readSimFile(vector<double*> & statVector);

};
*/

class TSimDataDistFromDifferentStats:public TSimData{
public:
	bool readSimDataForDist, readObsDataForDist;
	TSimData* simDataForDist;
	TObsData* obsDataForDist;

	TSimDataDistFromDifferentStats(std::string simfilename, std::string sparams, long maxSimsToRead, TObsData* obsData, TLog* logFile);
	~TSimDataDistFromDifferentStats(){
		if(readSimDataForDist) delete simDataForDist;
		if(readObsDataForDist) delete obsDataForDist;
	};
	void addfilesForDistance(std::string distObsFileName, std::string distSimFileName);
	virtual void calculateDistances(int obsDataSetNum, double* distances, bool verbose);
	virtual void calculateDistances(double* obsValueArray, double* distances, bool verbose);
	virtual void calculateLargestDistances(int startObsDataSetNum, int numObsDataSets, double* distances, bool verbose);
	virtual void calculateMeanDistances(int startObsDataSetNum, int numObsDataSets, double* distances, bool verbose);
	virtual void calculateDistancesLeaveOneOut(long simNum, double* distances, bool verbose);
};
//---------------------------------------------------------------------------
//TSimDataCommonParams
//---------------------------------------------------------------------------
class TSimDataCommonParams:public TBaseSimData{
public:
	vector<TSimData*>* simDataVec;

	TSimDataCommonParams(vector<TSimData*>* simData);
	~TSimDataCommonParams(){
	};
	bool isCommonParam(std::string & name);
	std::map<std::string, int>::iterator getIndexOfCommonParam(std::string & name);
	double getMinimum(std::string & name);
	double getMaximum(std::string & name);
	virtual void findParamMaximaMinima();
};
//---------------------------------------------------------------------------
//TSimDataVector
//---------------------------------------------------------------------------
class TSimDataVector{
public:
	vector<TSimData*> simDataVec;
	bool* useLinearComb;
	vector<bool*> useBoxCox;
	bool* distFromDifferentStats;
	bool computeJointPosteriors;
	bool pruneCorrelatedStats;
	bool commonParamsMatched;
	TParameters* parameters;
	double maxCor; //largest correlation tolerated
	TSimDataCommonParams* commonParams;
	TLog* logFile;

	TSimDataVector(TParameters* Parameters, TObsData* obsData, TLog* logFile);
	~TSimDataVector(){
		for(vector<TSimData*>::iterator it=simDataVec.begin(); it!=simDataVec.end(); ++it)
			delete (*it);
		delete[] distFromDifferentStats;
		if(commonParamsMatched) delete commonParams;
	};
	void matchCommonParams();
	void useParamRangesFromFile(std::string filename);
	int numSimDataSets(){return simDataVec.size();};
	void initializeJointPosteriorCalculation();
	void readTrueParameters();
	TSimData* simDataSet(int num){return simDataVec.at(num);};
	void standardizeParameters();
	void standardizeStatistics();
	double getLargestCorrelation(const int & stat1, const int & stat2);
};

#endif
