//------------------------------------------------------------------------------


#include "TSimDatabase.h"
//------------------------------------------------------------------------------
TSimDatabase::TSimDatabase(int gotNum_cali_sims, TDataVector* gotDataPointer, TInputFileVector* gotinputFiles, TLog* gotLogFile){
	myDataObject=gotDataPointer;
	inputFiles=gotinputFiles;
	logFile=gotLogFile;
	//prepare arrays to store data, priors and distances
	num_sims=0;
	//initialize other things
	meanVarianceCalculated = false;
	priorMeansBelowThresholdCalculated = false;
	priorVariancesBelowThresholdCalculated = false;
	thresholdSet=false;
	//empty
	empty(gotNum_cali_sims);
}
//------------------------------------------------------------------------------
void TSimDatabase::empty(int newsize){
	if(newsize!=num_sims){
		if(num_sims>0){
			//clean up
			for(int i=0; i<num_sims; ++i){
				delete[] data[i];
				delete[] priors[i];
			}
			delete[] data;
			delete[] priors;
			delete[] distances;
			if(matrixWithSimsBelowThresholdFilled) delete[] simsBelowThreshold;
		}
		if(priorMeansBelowThresholdCalculated) delete[] priorMeansBelowThreshold;
		if(priorVariancesBelowThresholdCalculated) delete[] priorVariancesBelowThreshold;

		//create new arrays
		num_sims=newsize;
		data=new double*[num_sims];
		priors=new double*[num_sims];
		for(int i=0; i<num_sims; ++i){
			data[i]=new double[myDataObject->numObsData];
			priors[i]=new double[inputFiles->priors->numSimplePrior];
		}
		distances=new double[num_sims];

	}
	num_sims_in_DB=0;
	num_sims_below_threshold=-1;
	matrixWithSimsBelowThresholdFilled=false;
	distancesCalculated=false;
}
//------------------------------------------------------------------------------
void TSimDatabase::reset(){
	//allows to use the same sims again for a new calibration
	if(priorMeansBelowThresholdCalculated){
		delete[] priorMeansBelowThreshold;
		priorMeansBelowThresholdCalculated=false;
	}
	if(priorVariancesBelowThresholdCalculated){
		delete[] priorVariancesBelowThreshold;
		priorVariancesBelowThresholdCalculated=false;
	}
	if(matrixWithSimsBelowThresholdFilled){
		delete[] simsBelowThreshold;
		matrixWithSimsBelowThresholdFilled=false;
	}
	thresholdSet=false;
	distancesCalculated=false;
}
//------------------------------------------------------------------------------
void TSimDatabase::addSimsFromFile(std::string fileName, int Num_cali_sims){
	logFile->listFlush("Reading calibration file '" + fileName +"' ...");
	std::ifstream is (fileName.c_str()); // opening the file for reading
	if(!is)	throw "Calibration file '" + fileName + "' could not be opened!";

	//prepare arrays
	empty(Num_cali_sims);

	//read header line with names
	std::vector<std::string> lineVec;
	fillVectorFromLineWhiteSpaceSkipEmpty(is, lineVec);
	std::string s=getFirstNonUniqueString(lineVec);
	if(!s.empty()) throw "Entry '"+s+"' is listed multiple times in header of file '"+ fileName +"'!";

	//create a vector of pointers pointing to the cali arrays storing the values for each cali File column
	int numCaliColumns=(int)lineVec.size();
	int* caliColumnIndexPrior = new int[inputFiles->priors->numSimplePrior];
	int* caliColumnIndexData= new int[myDataObject->numObsData];
	//for the priors
	for(unsigned int k=0; k<inputFiles->priors->numSimplePrior;++k){
		for(int i=0; i<(numCaliColumns+1); ++i){
			if(i==numCaliColumns)
				throw "The parameter column '" + inputFiles->getSimplePriorName(k) + "' is missing in calibration file '"+fileName+"'!";
			if(inputFiles->priors->simplePriors[k]->name==lineVec[i]){
				caliColumnIndexPrior[k]=i;
				break;
			}
		}
	}
	//for the data
	myDataObject->matchColumns(lineVec,numCaliColumns,&caliColumnIndexData);

	//read lines with data
	std::vector<double> lineDouble;
	for(int i=0; i< num_sims ; ++i){
		if(is.good() && !is.eof()){
			fillVectorFromLineWhiteSpaceSkipEmpty(is, lineDouble);
			if(lineDouble.size() == 0) --i;
			else {
				if(lineDouble.size() != lineVec.size()) throw "Number of entries on line " + toString(i+1) + " does not match the number of names in header line of calibration file '"+fileName+"'!";

				//now write the values into the specific arrays
				for(unsigned int k=0; k<inputFiles->priors->numSimplePrior;++k){
					priors[i][k]=lineDouble[caliColumnIndexPrior[k]];
				}
				for(int k=0; k<myDataObject->numObsData;++k){
					data[i][k] = lineDouble[caliColumnIndexData[k]];
				}
			}
		} else
			throw "Calibration file '" + fileName + "' is too short!";
	}
	num_sims_in_DB=num_sims;
	//clean up and write to log
	delete[] caliColumnIndexPrior;
	delete[] caliColumnIndexData;
	logFile->write(" done!");
	logFile->conclude("A total of ", num_sims_in_DB, " simulations read");
}

//------------------------------------------------------------------------------
void TSimDatabase::addSimToDB(TDataVector* gotDataPointer, TInputFileVector* gotinputFiles, double distance){
	if(num_sims_in_DB==num_sims) throw "Database of simulations already full!"; //NEW ERROR
	for(int k=0; k<gotDataPointer->numObsData;++k){
		data[num_sims_in_DB][k]=*gotDataPointer->pointersToSimData[k];
	}
	for(unsigned int k=0; k<gotinputFiles->priors->numSimplePrior;++k){
		priors[num_sims_in_DB][k]=gotinputFiles->priors->simplePriors[k]->curValue;
	}
	if(distance>=0) distances[num_sims_in_DB]=distance;
	++num_sims_in_DB;
}
//------------------------------------------------------------------------------
void TSimDatabase::calculateMeanVariances(){
	data_mean=new double[myDataObject->numObsData];
	data_var=new double[myDataObject->numObsData];
	meanVarianceCalculated=true;
	for(int k=0; k<myDataObject->numObsData;++k){
		data_mean[k]=0;
		data_var[k]=0;
		for(int i=0; i< num_sims ; ++i){
			data_mean[k]+=data[i][k];
		}
	}
	//calculate mean of simulated data
	for(int k=0; k<myDataObject->numObsData;++k){
		  data_mean[k]=data_mean[k]/num_sims;
	}
	//calculate variance of simulated data
	for(int k=0; k<myDataObject->numObsData;++k){
		data_var[k]=0;
		for(int i=0; i< num_sims ; ++i){
			data_var[k]=data_var[k]+(data_mean[k]-data[i][k])*(data_mean[k]-data[i][k]);
		}
		data_var[k]=data_var[k]/(num_sims-1);
		*myDataObject->pointersToVariances[k]=data_var[k];
	}
	//print
	for(int k=0; k<myDataObject->numObsData;++k){
		logFile->list(myDataObject->obsDataNamesWithPrefix[k] + ": mean = " + toString(data_mean[k]) + ", var = " + toString(data_var[k]));
		if(data_var[k] == 0) throw "Summary statistics " + myDataObject->obsDataNamesWithPrefix[k] + " is monomorphic!";
	}
}
//---------------------------------------------------------------------------
void TSimDatabase::calculateMinMaxofParameters(double* & min, double* & max){
	min=new double[inputFiles->priors->numSimplePrior];
	max=new double[inputFiles->priors->numSimplePrior];
	for(unsigned int m=0; m<inputFiles->priors->numSimplePrior; ++m){
		min[m]=priors[0][m];
		max[m]=priors[0][m];
	}
	for(int i=1; i< num_sims ; ++i){
		for(unsigned int m=0; m<inputFiles->priors->numSimplePrior; ++m){
			if(min[m]>priors[i][m]) min[m]=priors[i][m];
			if(max[m]<priors[i][m]) max[m]=priors[i][m];
		}
	}
}
//---------------------------------------------------------------------------
void TSimDatabase::standardizeRetainedParameters(double* min, double* max){
	//standardizes the parameters between 0 and 1
	standardizedParameters=new double*[num_sims_below_threshold];
	for(int i=0; i<num_sims_below_threshold; ++i){
		standardizedParameters[i]=new double[inputFiles->priors->numSimplePrior];
	}
	for(int i=0; i< num_sims_below_threshold ; ++i){
		for(unsigned int m=0; m<inputFiles->priors->numSimplePrior; ++m){
			standardizedParameters[i][m]=(priors[simsBelowThreshold[i]][m]-min[m])/(max[m]-min[m]);
		}
	}
}
//---------------------------------------------------------------------------
void TSimDatabase::calculateWeightedSigmaOfRetainedParameters(SymmetricMatrix & Sigma, double* weights){
	if(!matrixWithSimsBelowThresholdFilled) throw "SimDatabase: Attempt to use the array of simulations below threshold before filling it!";
	//get weighted mean of the different parameters...
	double means[inputFiles->priors->numSimplePrior];
	for(unsigned int m=0; m<inputFiles->priors->numSimplePrior; ++m) means[m]=0;
	for(int i=0; i<num_sims_below_threshold ; ++i){
		for(unsigned int m=0; m<inputFiles->priors->numSimplePrior; ++m){
			means[m]+=weights[i]*standardizedParameters[i][m];
		}
	}
	Sigma.ReSize(inputFiles->priors->numSimplePrior);
	Sigma=0;
	for(int i=0; i<num_sims_below_threshold ; ++i){
		for(unsigned int m=0; m<inputFiles->priors->numSimplePrior; ++m){
			for(unsigned int n=m; n<inputFiles->priors->numSimplePrior; ++n){
				Sigma.element(m, n)=Sigma.element(m, n)+weights[i]*(standardizedParameters[i][m]-means[m])*(standardizedParameters[i][n]-means[n]);
			}
		}
	}
	//Sigma=Sigma/(num_sims_below_threshold-1);
}
//---------------------------------------------------------------------------
double TSimDatabase::getPriorDensityOneRetainedSim(int sim){
	return inputFiles->getLogPriorDensity(priors[simsBelowThreshold[sim]]);
}
//---------------------------------------------------------------------------
void TSimDatabase::calculateDistances(){
	//calculate distances
	smallestDistCaliSim=0;
	distances[0]=myDataObject->calculateDistance(data[0]);
	for(int i=1; i< num_sims ; ++i){
		distances[i] = myDataObject->calculateDistance(data[i]);
		if(distances[i]<distances[smallestDistCaliSim]) smallestDistCaliSim=i;
	}
	distancesCalculated=true;
}
void TSimDatabase::calculateDistances(TLinearComb* linearComb){
	//do if linear combinations will be standardized for distance calculation...
	if(myDataObject->stdLinearCombForDist){
		//Save values to standardize
		double** cali_linearComb;
		double* cali_data_mean_linearCombination;
		double* cali_data_var_linearCombination;
		cali_linearComb = new double*[linearComb->numLinearComb];
		cali_data_mean_linearCombination = new double[linearComb->numLinearComb];
		cali_data_var_linearCombination = new double[linearComb->numLinearComb];

		for(int i=0; i<linearComb->numLinearComb;++i){
			cali_linearComb[i]=new double[num_sims];
			cali_data_var_linearCombination[i] = 0.0;
			cali_data_mean_linearCombination[i] = 0.0;
		}
		//calculate pca's
		for(int j=0; j<num_sims; ++j){
			linearComb->calcSimDataPCA(data[j]);
			for(int i=0; i<linearComb->numLinearComb;++i){
				cali_linearComb[i][j] = linearComb->simPCA[i];
				cali_data_mean_linearCombination[i] += linearComb->simPCA[i];
			}
		}
		//calculate mean and variance
		for(int i=0; i<linearComb->numLinearComb;++i){
			cali_data_mean_linearCombination[i] = cali_data_mean_linearCombination[i]/num_sims;
		}
		for(int i=0; i<linearComb->numLinearComb;++i){
			for(int j=0; j<num_sims; ++j){
				cali_data_var_linearCombination[i] += (cali_linearComb[i][j]-cali_data_mean_linearCombination[i])*(cali_linearComb[i][j]-cali_data_mean_linearCombination[i]);
			}
			linearComb->linearCombVariances[i] = cali_data_var_linearCombination[i]/(num_sims-1);
		}

		//delete
		for(int i=0; i<linearComb->numLinearComb;++i)
			delete[] cali_linearComb[i];
		delete[] cali_linearComb;
		delete[] cali_data_mean_linearCombination;
		delete[] cali_data_var_linearCombination;
	}

	//calculate distances
	smallestDistCaliSim = 0;
	distances[0] = myDataObject->calculateDistance(data[0], linearComb);
	for(int j=1; j< num_sims ; ++j){
		distances[j] = myDataObject->calculateDistance(data[j], linearComb);
	   if(distances[j] < distances[smallestDistCaliSim]) smallestDistCaliSim = j;
    }
	distancesCalculated=true;
}
//------------------------------------------------------------------------------
double TSimDatabase::getThreshold(double thresholdProportion){
	setThreshold((int)num_sims * thresholdProportion);
	return threshold;
}
void TSimDatabase::setThreshold(int numToRetain){
	num_sims_below_threshold=numToRetain;
	distances_ordered=new double*[num_sims];
	double* curDist=distances;
	double** curCaliDist=distances_ordered;
	for(int i=0; i< num_sims ;++curDist, ++curCaliDist, ++i){
		*curCaliDist=curDist;
	}
	quicksortP(distances_ordered, 0, num_sims-1);
	threshold = *distances_ordered[numToRetain-1];

	delete[] distances_ordered;
	thresholdSet=true;
}
void TSimDatabase::setFixedThreshold(double Threshold){
	threshold=Threshold;
	thresholdSet=true;
	countSimsBelowThreshold();
}
void TSimDatabase::countSimsBelowThreshold(){
	num_sims_below_threshold=0;
	for(int i=0; i< num_sims ; ++i){
		if(distances[i] <= threshold) ++num_sims_below_threshold;
	}
}
//------------------------------------------------------------------------------
void TSimDatabase::writeToFile(std::string fileName){
	logFile->listFlush("Writing simulations to file '" + fileName + "' ...");
	std::ofstream cf;
	cf.open(fileName.c_str());
	cf << inputFiles->priors->simplePriors[0]->name;
	for(unsigned int k=1; k<inputFiles->priors->numSimplePrior;++k){
		cf << "\t" << inputFiles->priors->simplePriors[k]->name;
	}
	myDataObject->writeHeader(cf);
	cf << std::endl;
	for(int i=0; i< num_sims ; ++i){
		cf << priors[i][0];
		for(unsigned int k=1; k<inputFiles->priors->numSimplePrior;++k){
			cf << "\t" << priors[i][k];
		}
		for(int k=0; k<myDataObject->numObsData;++k){
			cf << "\t" << data[i][k];
		}
		cf << std::endl;
	}
	cf.close();
	logFile->write(" done!");
}
//------------------------------------------------------------------------------
void TSimDatabase::writeSimsBelowThresholdToFile(std::string fileName){
	if(!thresholdSet) throw "SimDatabase: Attempt to write simulations below threshold before the threshold was computed!";

	logFile->listFlush("Writing simulations below threshold to file '" + fileName + "' ...");
	std::ofstream cf;
	cf.open(fileName.c_str());
	cf << "distance";
	for(unsigned int k=0; k<inputFiles->priors->numSimplePrior;++k){
		cf << "\t" << inputFiles->priors->simplePriors[k]->name;
	}
	myDataObject->writeHeader(cf);
	cf << std::endl;
	for(int i=0; i< num_sims ; ++i){
		if(distances[i] <= threshold){
			cf << distances[i];
			for(unsigned int k=0; k<inputFiles->priors->numSimplePrior;++k){
				cf << "\t" << priors[i][k];
			}
			for(int k=0; k<myDataObject->numObsData;++k){
				cf << "\t" << data[i][k];
			}
			cf << std::endl;
		}
	}
	cf.close();
	logFile->write(" done!");
}
//------------------------------------------------------------------------------
void TSimDatabase::writeDistanceFile(std::string fileName){
   logFile->listFlush("Writing distance File '" + fileName + "...");
   std::ofstream cf;
	cf.open(fileName.c_str());
	cf << num_sims << std::endl;
	for(int i=0; i< num_sims ; ++i){
		cf << distances[i] << std::endl;
	}
	cf.close();
	logFile->write(" done!");
}
//------------------------------------------------------------------------------
void TSimDatabase::fillArrayOfSimsBelowThreshold(){
   if(!thresholdSet) throw "SimDatabase: Attempt to fill array of simulations below threshold before the threshold was computed!";
   if(!matrixWithSimsBelowThresholdFilled){
	   logFile->listFlush("Retaining calibration simulations ...");
	   if(num_sims_below_threshold<0) countSimsBelowThreshold();
		simsBelowThreshold=new int[num_sims_below_threshold];
		int p=0;
		for(int i=0; i<num_sims ; ++i){
			if(distances[i]<=threshold && p<num_sims_below_threshold){
			 simsBelowThreshold[p]=i;
			 ++p;
			}
		}
		//fix num_sims_below_threshold as in the case of an iterative mcmc some sims may be replicate several times
		matrixWithSimsBelowThresholdFilled=true;
		num_sims_below_threshold=p;
		logFile->write(" done!");
		logFile->conclude(num_sims_below_threshold, " simulations retained");
		if(num_sims_below_threshold<1) throw "No calibration simulations below threshold!";
   }
}
//------------------------------------------------------------------------------
void TSimDatabase::calculatePriorMeansBelowThreshold(){
	if(!priorMeansBelowThresholdCalculated){
		fillArrayOfSimsBelowThreshold();
		priorMeansBelowThreshold=new double[inputFiles->priors->numSimplePrior];
		priorMeansBelowThresholdCalculated=true;
		for(unsigned int k=0; k<inputFiles->priors->numSimplePrior;++k){
			priorMeansBelowThreshold[k]=0;
			for(int i=0; i<num_sims_below_threshold ; ++i)
				priorMeansBelowThreshold[k]+=priors[simsBelowThreshold[i]][k];
			priorMeansBelowThreshold[k]=priorMeansBelowThreshold[k]/(double)num_sims_below_threshold;
		}
	}
}
void TSimDatabase::calculatePriorVariancesBelowThreshold(){
	calculatePriorMeansBelowThreshold();
	if(!priorVariancesBelowThresholdCalculated){
		fillArrayOfSimsBelowThreshold();
		priorVariancesBelowThreshold=new double[inputFiles->priors->numSimplePrior];
		priorVariancesBelowThresholdCalculated=true;
		for(unsigned int k=0; k<inputFiles->priors->numSimplePrior;++k){
			priorVariancesBelowThreshold[k]=0;
			for(int i=0; i<num_sims_below_threshold ; ++i){
				priorVariancesBelowThreshold[k]+=(priors[simsBelowThreshold[i]][k]-priorMeansBelowThreshold[k])*(priors[simsBelowThreshold[i]][k]-priorMeansBelowThreshold[k]);
			}
			priorVariancesBelowThreshold[k]=priorVariancesBelowThreshold[k]/(double)(num_sims_below_threshold-1.0);
		}
	}
}
//------------------------------------------------------------------------------
double TSimDatabase::getMeanBelowThreshold(int prior){
	calculatePriorMeansBelowThreshold();
	return priorMeansBelowThreshold[prior];
}
double TSimDatabase::getVarianceBelowThreshold(int prior){
	calculatePriorVariancesBelowThreshold();
	return priorVariancesBelowThreshold[prior];
}
//------------------------------------------------------------------------------
void TSimDatabase::setMcmcRanges(const double & mcmc_range_proportion){
	fillArrayOfSimsBelowThreshold();
	for(unsigned int k=0; k<inputFiles->priors->numSimplePrior;++k)
		setMcmcRangesOnePrior(k, mcmc_range_proportion);
}
void TSimDatabase::setFixedMcmcRanges(const double & fixedRange){
	for(unsigned int k=0; k<inputFiles->priors->numSimplePrior;++k)
		setFixedMcmcRangesOnePrior(k, fixedRange);
}
double TSimDatabase::setMcmcRangesOnePrior(const int & prior, const double & mcmc_range_proportion){
	calculatePriorVariancesBelowThreshold();
	inputFiles->priors->simplePriors[prior]->setMCMCStep(sqrt(priorVariancesBelowThreshold[prior])*mcmc_range_proportion);
	return inputFiles->priors->simplePriors[prior]->mcmcStep;
}
void TSimDatabase::setFixedMcmcRangesOnePrior(const int & prior, const double & fixedRange){
	inputFiles->priors->simplePriors[prior]->setMCMCStep(fixedRange);
}
void TSimDatabase::setMcmcRangesRelativeToRangeOnePrior(const int prior, const double fixedRange){
	inputFiles->priors->simplePriors[prior]->setMCMCStepRelativeToRange(fixedRange);
}
//------------------------------------------------------------------------------
//Functions to set the priors to a certain point in the parameter space.
void TSimDatabase::setPriorStartingConditionsFromBestSimulation(){
	logFile->list("Setting starting point at best calibration simulation");
	fillArrayOfSimsBelowThreshold();
	for(unsigned int k=0; k < inputFiles->priors->numSimplePrior; ++k){
	  inputFiles->priors->simplePriors[k]->curValue=priors[smallestDistCaliSim][k];
	  inputFiles->priors->simplePriors[k]->oldValue=priors[smallestDistCaliSim][k];
   }
   // calc all combined parameters
   inputFiles->priors->updateCombinedParameters();
}
void TSimDatabase::setPriorStartingConditionsAtRandom(double rand){
	fillArrayOfSimsBelowThreshold();
	int i=floor(rand*(num_sims_below_threshold-0.000001));
	logFile->list("Setting starting point randomly at simulation ",i);
    for(unsigned int k=0; k < inputFiles->priors->numSimplePrior; ++k){
	  inputFiles->priors->simplePriors[k]->curValue=priors[simsBelowThreshold[i]][k];
	  inputFiles->priors->simplePriors[k]->oldValue=priors[simsBelowThreshold[i]][k];
    }
   // calc all combined parameters
   inputFiles->priors->updateCombinedParameters();
}
void TSimDatabase::setPriorStartingConditions(std::string & startingPoint){
	logFile->list("Setting starting point from string '", startingPoint, "'");
	std::vector<double> startingValues;
	fillVectorFromString(startingPoint, startingValues, '#');
	if(startingValues.size()!=inputFiles->priors->numSimplePrior) throw "Number of values provided does not match number of model parameters!";
    for(unsigned int k=0; k < inputFiles->priors->numSimplePrior; ++k){
	  inputFiles->priors->simplePriors[k]->curValue=startingValues[k];
	  inputFiles->priors->simplePriors[k]->oldValue=startingValues[k];
    }
   // calc all combined parameters
   inputFiles->priors->updateCombinedParameters();
}

//---------------------------------------------------------------------------
//functions to sort an array of pointers
float TSimDatabase::partitionP(double** a, int left, int right, int pivotIndex) {
	int storeIndex=left;
	//swap
	double* temp=a[right];
	a[right]=a[pivotIndex];
	a[pivotIndex]=temp;
	for (int i=left; i < right; i++){
		if(*a[i] <= *a[right]){
			//swap
			temp=a[storeIndex];
			a[storeIndex]=a[i];
			a[i]=temp;
			++storeIndex;
		}
	}
	//Pivot back to final place
	temp=a[right];
	a[right]=a[storeIndex];
	a[storeIndex]=temp;
	return storeIndex;
}

void TSimDatabase::quicksortP(double** a, int left, int right){
	if(right > left){
		int pivotIndex=(left+right)/2;
		pivotIndex=partitionP(a, left, right, pivotIndex);
		quicksortP(a, left, pivotIndex-1);
		quicksortP(a, pivotIndex+1, right);
	}
}
