//---------------------------------------------------------------------------

#ifndef TCaliDatabaseH
#define TCaliDatabaseH
#include "TDataVector.h"
#include "TInputFileVector.h"
#include "newmat.h"
#include "newmatap.h"
#include "newmatio.h"
#include "TLog.h"
#include "stringFunctions.h"
//---------------------------------------------------------------------------

class TSimDatabase{
	public:
		TDataVector* myDataObject;
		TInputFileVector* inputFiles;
		TLog* logFile;
		int num_sims;
		int num_sims_below_threshold;
		int num_sims_in_DB;

		double* data_mean;
		double* data_var;
		double** data;
		double* distances;
		double* distances_normalized;
		double distance_mean, distance_std;
		double** priors;
		double** standardizedParameters;
		double* paramMin;
		double* paramMax;
		double** distances_ordered;
		int* simsBelowThreshold;
		double* priorMeansBelowThreshold;
		double* priorVariancesBelowThreshold;
		bool priorMeansBelowThresholdCalculated, priorVariancesBelowThresholdCalculated;
		int smallestDistCaliSim;
		double threshold;
		bool thresholdSet;
		bool matrixWithSimsBelowThresholdFilled;
		bool distancesCalculated;
		bool meanVarianceCalculated;

		TSimDatabase (int gotNum_cali_sims, TDataVector* gotDataPointer, TInputFileVector* gotinputFiles, TLog* gotLogFile);
		~TSimDatabase(){
			if(matrixWithSimsBelowThresholdFilled){
				delete[] simsBelowThreshold;
			}
			if(meanVarianceCalculated){
				delete[] data_mean;
				delete[] data_var;
			}
			if(priorMeansBelowThresholdCalculated) delete[] priorMeansBelowThreshold;
			if(priorVariancesBelowThresholdCalculated) delete[] priorVariancesBelowThreshold;
			if(num_sims>0){
				for(int i=0; i<num_sims; ++i){
					delete[] data[i];
					delete[] priors[i];
				}
				delete[] priors;
				delete[] data;
				delete[] distances;
			}
		}
		void empty(int newsize);
		void reset();
		void addSimsFromFile(std::string fileName, int Num_cali_sims);
		void addSimToDB(TDataVector* gotDataPointer, TInputFileVector* gotinputFiles, double distance=-1);
		void calculateMeanVariances();
		double getMeanBelowThreshold(int prior);
		double getVarianceBelowThreshold(int prior);
		void calculateMinMaxofParameters(double* & min, double* & max);
		void standardizeRetainedParameters(double* min, double* max);
		void calculateWeightedSigmaOfRetainedParameters(SymmetricMatrix & Sigma, double* weights);
		double getPriorDensityOneRetainedSim(int sim);
		void calculateDistances();
		void calculateDistances(TLinearComb* linearComb);
		double getThreshold(double thresholdProportion);
		void setThreshold(int numToRetain);
		void setFixedThreshold(double Threshold);
		void countSimsBelowThreshold();
		void writeToFile(std::string fileName);
		void writeSimsBelowThresholdToFile(std::string fileName);
		void writeDistanceFile(std::string fileName);
		void setPriorStartingConditionsFromBestSimulation();
		void setPriorStartingConditionsAtRandom(double rand);
		void setPriorStartingConditions(std::string & startingPoint);
		void calculatePriorMeansBelowThreshold();
		void calculatePriorVariancesBelowThreshold();
		void setMcmcRanges(const double & mcmc_range_proportion);
		void setFixedMcmcRanges(const double & fixedRange);
		double setMcmcRangesOnePrior(const int & prior, const double & mcmc_range_proportion);
		void setFixedMcmcRangesOnePrior(const int & prior, const double & fixedRange);
		void setMcmcRangesRelativeToRangeOnePrior(const int prior, const double fixedRange);
		void fillArrayOfSimsBelowThreshold();
		float partitionP(double** a, int left, int right, int pivotIndex);
		void quicksortP(double** a, int left, int right);
};
#endif
