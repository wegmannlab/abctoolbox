//---------------------------------------------------------------------------
#include "TStandardEstimation.h"
//---------------------------------------------------------------------------

TStandardEstimation::TStandardEstimation(TSimData* simData, ColumnVector* Theta, double DiracPeakWidth, bool ComputeTruncatedPrior, std::string OutputPrefix, TJointPosteriorSettings* JointPosteriorSettings, TRandomGenerator* RandomGenerator, TLog* logFile)
:TBaseEstimation(simData, Theta, DiracPeakWidth, ComputeTruncatedPrior, OutputPrefix, JointPosteriorSettings, RandomGenerator, logFile){
	retainedSimNumbersStored=false;
	tempDistancesInitialized=false;
	prepareTrueParamValidation();
}
//---------------------------------------------------------------------------
bool TStandardEstimation::performPosteriorEstimation(int ObsDataSetNum, bool writeRetainedSimulations, int numSimsForObsPValue, int numReplicatesPValTukeyDepth){
	//make rejection
	makeRejection(ObsDataSetNum, writeRetainedSimulations);
	if(computeSmoothedTruncatedPrior){
		calculateSmoothedRetainedMatrix();
		writeTruncatedPrior();
	}

	//TODO: check if retained params or stats are monomoprhic!
	//check if a statistic is monomorphic


	//Calculate Tukey depth
	logfile->startIndent("Calculating Tukey depth of observed data:");
	tukeyDepth = mySimData->computeTukeyDepthOfObservedAmongRetained(ObsDataSetNum);
	logfile->list("Tukey depth = ", tukeyDepth);
	if(numReplicatesPValTukeyDepth>0){
		tukeyDepthPValue = mySimData->computePValueOfTukeyDepthAmongRetained(tukeyDepth, numReplicatesPValTukeyDepth);
	}
	logfile->endIndent();

	//perform ABC-GLM regression
	mySimData->getObsStatValuesIntoColumnVector(ObsDataSetNum, obsValues);
	if(!performLinearRegression(logfile->verbose())){
	  logfile->warning("Regression failed -> Skipping this dataset!");
	  return false;
	}
	calculatePosterior(logfile->verbose());

	//compute model fit
	if(numSimsForObsPValue>0) marDens=calculateMarginalDensity(numSimsForObsPValue, marDensPValue, logfile->verbose());
	else marDens=calculateMarginalDensity(logfile->verbose());

	//write estimation results
	logfile->listFlush("Writing marginal posterior densities ...");
	std::string filename=outputPrefix+"MarginalPosteriorDensities_Obs" + toString(ObsDataSetNum) + ".txt";
	std::ofstream marDensFile;
	marDensFile.open(filename.c_str());
	posteriorMatrix->writePosteriorFiles(marDensFile);
	logfile->write(" done!");
	writePosteriorCharacteristics(ObsDataSetNum);
	//write true param characteristics
	writeTueParamCharacteristics(ObsDataSetNum);
	return true;
}
//---------------------------------------------------------------------------
void TStandardEstimation::makeRejection(int thisObsDataSet, bool writeRetainedSimulations){
	//prepare distance array
	if(!tempDistancesInitialized){
		tempDistances = new double[mySimData->numReadSims];
		tempDistancesInitialized = true;
	}
	//calculate distance and make rejection
	mySimData->calculateDistances(thisObsDataSet, tempDistances, logfile->verbose());
	mySimData->fillStatAndParamMatrices(tempDistances, logfile->verbose());

	//write retained, if requested
	if(writeRetainedSimulations) mySimData->writeRetained(outputPrefix, tempDistances, thisObsDataSet);
	//mySimData->writeRetainedScaled(outputPrefix, tempDistances, thisObsDataSet);
}
//---------------------------------------------------------------------------
double TStandardEstimation::calculateMarginalDensity(int numSimsForObsPValue, double & mardensPV, bool verbose){
   double f_M;
   logfile->listFlush("Calculating the marginal density ...");
   Matrix D_temp = SigmaS+CHat*SigmaTheta*CHat.t();
   SymmetricMatrix D;
   fillInvertable(D_temp, D);
   double det=0;
   try {
	   det = D.Determinant();
   } catch(...){
	   logfile->warning("Problems computing determinant of D!");
	   return 0.0;
   }
   Matrix D_inv;
   if(det<=0.0){
	   logfile->warning("Problems calculating the marginal density: determinant is zero!");
	   return 0.0;
   }
   try{
	   D_inv=D.i();
	   f_M = marginalDensity(D, D_inv, obsValues);
   } catch(...){
	   logfile->warning("Problems calculating the marginal density: inverting D failed!");
	   return 0.0;
   }
   logfile->write(" done!");
   logfile->conclude("Marginal density = ", f_M);
   if(numSimsForObsPValue>0){
		marginalDensitiesOfRetained(numSimsForObsPValue, D, D_inv);
		mardensPV=0;
		for(std::vector<double>::iterator it=fmFromRetained.begin(); it!=fmFromRetained.end(); ++it){ if((*it)<=f_M) ++mardensPV; }
		mardensPV=mardensPV/(double)numSimsForObsPValue;
		if(verbose) logfile->conclude("p-value = ", mardensPV);
   }
   return f_M;
}
double TStandardEstimation::calculateMarginalDensity(bool verbose){
	   Matrix D_temp = SigmaS+CHat*SigmaTheta*CHat.t();
	   SymmetricMatrix D;
	   fillInvertable(D_temp, D);
	   double det=0;
	   try {
		   det=D.Determinant();
	   } catch(...){
		   logfile->warning("Problems computing determinant of D!");
		   return 0.0;
	   }

	   if(det<=0.0){
		   logfile->warning("Problems calculating the marginal density: determinant is zero!");
		   return 0.0;
	   }
	   try{
		   Matrix D_inv=D.i();
		   double f_M=marginalDensity(D, D_inv, obsValues);
		   return f_M;
	   } catch(...){
		   logfile->warning("Problems calculating the marginal density: inverting D failed!");
		   return 0.0;
	   }
	   return 0.0;
}
//---------------------------------------------------------------------------
void TStandardEstimation::prepareTrueParamValidation(){
	if(mySimData->trueParamsAvailable){
		//check length of true param file
		if(mySimData->trueParams.size()!= (std::vector<double*>::size_type) mySimData->pointerToObsData->numObsDataSets) throw "Number of true parameters in '" + mySimData->trueParamName + "' do not match the number of observed data set!";
		std::string filename=outputPrefix + "TrueParamValidation.txt";
		trueParamValidationFile.open(filename.c_str());
		trueParamValidationFileOpen=true;

		//write header
		trueParamValidationFile << "observedSet\tmarginalDensity";
		for(int i=0; i<mySimData->numTrueParams; ++i)
			trueParamValidationFile << "\t" << mySimData->trueNameVector[i] << "\t" << mySimData->trueNameVector[i] << "_Quantile\t" << mySimData->trueNameVector[i] << "_HDI";

		//joint posterior
		for(std::vector<TJointPosteriorStorage*>::iterator a=mySimData->jointPosteriorStorage.begin(); a!=mySimData->jointPosteriorStorage.end(); ++a){
		   //check if true params are given for this joint posterior
		   if((*a)->allExpectedParamsMinusOneInArray(mySimData->trueParamNumInSimData, mySimData->numTrueParams)){
			   trueParamValidationFile << "\tjoint_" << (*a)->getParameterString('_') << "_HDI";
			   jointPosteriorsForWhichTrueParamsAreAvailable.push_back(*a);
		   }
	   }
	   trueParamValidationFile << std::endl;
	}
}
//---------------------------------------------------------------------------
void TStandardEstimation::writeTueParamCharacteristics(int thisObsDataSet){
	if(mySimData->trueParamsAvailable){
		double val;
		int param;
		trueParamValidationFile << thisObsDataSet << "\t" << marDens;
		for(int tp=0; tp<mySimData->numTrueParams; ++tp){
			val=mySimData->trueParams.at(thisObsDataSet)[tp];
			param=mySimData->trueParamNumInSimData[tp];
			posteriorMatrix->writeValidationStats(trueParamValidationFile, param, val);
		}
		for(std::vector<TJointPosteriorStorage*>::iterator a=jointPosteriorsForWhichTrueParamsAreAvailable.begin(); a!=jointPosteriorsForWhichTrueParamsAreAvailable.end(); ++a){
		  //prepare true parameter position TODO: think of a better way to handle those, move handling to TObservedData?
		  ColumnVector positions((*a)->numParams());
		  for(std::vector<int>::size_type p=0;p<(*a)->params.size(); ++p){
				for(int w=0;w<mySimData->numTrueParams;++w){
					if(mySimData->trueParamNumInSimData[w]==(*a)->params[p]-1){
						positions.element(p)=posteriorMatrix->paramRanges->toInternalScale(mySimData->trueParamNumInSimData[w], mySimData->trueParams.at(thisObsDataSet)[w]);
					}
				}
		  }
		  //print HDI at true position
		  trueParamValidationFile << "\t" << (*a)->getHDIAt(&positions);
		}
		trueParamValidationFile << std::endl;
	}
};

//---------------------------------------------------------------------------
//TODO: add joint posterior to validation
//TODO: add some analysis of the results
//---------------------------------------------------------------------------
double TStandardEstimation::runValidationEstimation(long & simNum){
	//make rejection -> use same number of sims!
	if(!tempDistancesInitialized){
		tempDistances=new double[mySimData->numReadSims];
		tempDistancesInitialized=true;
	}

	mySimData->calculateDistancesLeaveOneOut(simNum, tempDistances, false);
	mySimData->fillStatAndParamMatrices(tempDistances, false);

	//compute posterior -> obs values must be a ColumnVector!
	mySimData->getStatValuesIntoColumnVector(simNum, obsValues);
	performLinearRegression();
	calculatePosterior();

	//compute marginal density
	return calculateMarginalDensity(false);
}
double TStandardEstimation::runValidationEstimation(double* obsValueArray){
	//make rejection -> use same number of sims!
	if(!tempDistancesInitialized){
		tempDistances=new double[mySimData->numReadSims];
		tempDistancesInitialized=true;
	}
	mySimData->calculateDistances(obsValueArray, tempDistances, false);
	mySimData->fillStatAndParamMatrices(tempDistances, false);
	//compute posterior -> obs values must be a ColumnVector!
	mySimData->getStatValuesIntoColumnVector(obsValueArray, obsValues);
	if(!performLinearRegression()) return 0; //TODO: Think if this is clever!
	calculatePosterior();
	//compute marginal density
	return calculateMarginalDensity(false);
}


void TStandardEstimation::performValidation(std::vector<long> & simNumbers, long numToRetain, std::string filenameTag, std::string progressTag){
	//perfom validation (generate quantile  etc.) using the retained values
	logfile->flush(progressTag + " (0%)");

	//open output file
	std::ofstream output;
	std::string filename=outputPrefix+filenameTag+".txt";
	output.open(filename.c_str());

	//write Header
	output << "number\tmarginalDensity";
	for(int p=0; p<mySimData->numParams;++p){
		posteriorMatrix->writeValidationHeader(output, mySimData->paramNames[p]);
	}
	output << std::endl;

	//now go through all those simulations
	int prog, oldProg=0;
	long i=0;
	double md;
	double val;
	for(std::vector<long>::iterator it=simNumbers.begin(); it!=simNumbers.end(); ++it,++i){
		md = runValidationEstimation(*it);
		output << (long) ((*it)+1) << "\t" << md;

		//compute quantiles and HDI
		for(int p=0; p<mySimData->numParams;++p){
			val = mySimData->getParamValue(*it, p);
			posteriorMatrix->writeValidationStats(output, p+1, val);
		}
		output << std::endl;

		//report progress
		prog=(100*(double)(i+1)/(double)simNumbers.size());
		if(prog>oldProg){
			oldProg=prog;
			logfile->overFlush(progressTag + " (", prog, "%)");
		}
	}

	output.close();
	logfile->overWrite(progressTag + " done!     ");
}
//---------------------------------------------------------------------------
void TStandardEstimation::performRetainedValidation(long numSims, std::string filenameTag){
	//array of retained simulations stored during rejection
	if(mySimData->numUsedSims<numSims) numSims=mySimData->numUsedSims;
	std::vector<long> retainedSims;
	mySimData->fillVectorWithNumbersOfRetainedSims(retainedSims, numSims);
	performValidation(retainedSims, numRetainedSims, "RetainedValidation"+filenameTag, "   - Performing validation using " + toString(numSims) + " retained simulations ...");
}
//---------------------------------------------------------------------------
void TStandardEstimation::fillVectorWithNumbersOfRandomSims(std::vector<long> & sims, long & numSims){
	mySimData->fillVectorWithNumbersOfRandomSims(sims, numSims, randomGenerator);
}
void TStandardEstimation::performRandomValidation(long numSims){
	//get random sims
	std::vector<long> randomSims;
	fillVectorWithNumbersOfRandomSims(randomSims, numSims);
	performValidation(randomSims, numRetainedSims, "RandomValidation", "   - Performing validation using " + toString(numSims) + " random simulations ...");
}

