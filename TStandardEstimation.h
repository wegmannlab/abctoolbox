//---------------------------------------------------------------------------

#ifndef TStandardEstimationH
#define TStandardEstimationH
#include "TBaseEstimation.h"
//---------------------------------------------------------------------------
class TStandardEstimation:public TBaseEstimation {
   public:
	  long* retainedSimNumbers;
	  long numRetainedSims;
	  bool retainedSimNumbersStored;
	  double* tempDistances;
	  bool tempDistancesInitialized;

	  vector<TJointPosteriorStorage*> jointPosteriorsForWhichTrueParamsAreAvailable;

	  TStandardEstimation(TSimData* simData, ColumnVector* Theta, double DiracPeakWidth, bool ComputeTruncatedPrior, std::string OutputPrefix, TJointPosteriorSettings* JointPosteriorSettings, TRandomGenerator* RandomGenerator, TLog* logFile);
	  ~TStandardEstimation(){
		  if(retainedSimNumbersStored) delete[] retainedSimNumbers;
		  if(tempDistancesInitialized) delete[] tempDistances;
	  };
	  void prepareTrueParamValidation();
	  void standardize();
	  bool performPosteriorEstimation(int obsDataSetNum, bool writeRetainedSimulations, int calcObsPValue, int numReplicatesPValTukeyDepth);
	  void makeRejection(int thisObsDataSet, bool writeRetainedSimulations);
	  double calculateMarginalDensity(int numSimsForObsPValue, double & obsPV, bool verbose=false);
	  double calculateMarginalDensity(bool verbose=false);
	  void writeTueParamCharacteristics(int thisObsDataSet);
	  double getRMISEOfTrueParam(int param, double trueValue);
	  double getREMODEOfTrueParam(int param, double trueValue);
	  double getREMEANOfTrueParam(int param, double trueValue);
	  double getREMEDIANOfTrueParam(int param, double trueValue);
	  virtual double runValidationEstimation(long & simNum);
	  virtual double runValidationEstimation(double* obsValueArray);
	  void performValidation(vector<long> & simNumbers, long numToRetain, std::string filenameTag, std::string progressTag);
	  void performRetainedValidation(long numSims, std::string filenameTag="");
	  void fillVectorWithNumbersOfRandomSims(vector<long> & sims, long & numSims);
	  void performRandomValidation(long numSims);




};


#endif
