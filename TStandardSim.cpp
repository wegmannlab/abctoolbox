//---------------------------------------------------------------------------


#include "TStandardSim.h"
//#include <sys/wait.h>
//---------------------------------------------------------------------------

TStandardSim::TStandardSim(TParameters* GotParameters, std::string gotexedir, TLog* gotLogFile, TRandomGenerator* RandomGenerator):TSim(GotParameters, gotexedir, gotLogFile, RandomGenerator){
	nbSims=gotParameters->getParameterDouble("numSims");
	logFile->startIndent("Performing a standard simulation run:");
	logFile->list("Performing a total of " + toString(nbSims) + " simulations");
	//do it in parallel?
	if(gotParameters->parameterExists("numCores")){
		numCores=gotParameters->getParameterInt("numCores");
		if(numCores<1) throw "The number of requested cores is " + toString(numCores) + ", which is < 1!";
	} else numCores=1;
	if(numCores>1){
		logFile->list("Performing simulations on "+toString(numCores)+" cores in parallel");
		//create necessary objects
		logFile->startIndent("Initializing necessary objects:");
		for(int i=1; i<numCores; ++i){
			std::string newTempSuffix=tempSuffix+"_"+toString(i);
			inputFiles = initializeSimulations(newTempSuffix);
			myData = performFirstSimAndCreateDataObjects(inputFiles, newTempSuffix);
		}
		logFile->endIndent();
	}
	//initialize a few things
	distance=0;
	progress=0;
	oldProgress=0;
	overallCounter=0;
	totNumSims=nbSims*repeatsPerParameterVector;
};

//---------------------------------------------------------------------------
void TStandardSim::runSimulations(){
	clock_t starttime=clock();
	//prepare output file
	if(addDistanceToOutputfile){
		logFile->warning("Distances will be computed, but statistics will not be standardized.");
		myOutputFiles=new TOutputVector("1", outName, myData, inputFiles, writeStatsToOutputFile, true, writeSeparateOutputFiles, forceOverWrite);
	}
	else myOutputFiles=new TOutputVector("1", outName, myData, inputFiles, writeStatsToOutputFile, false, writeSeparateOutputFiles, forceOverWrite);

	if(!gotParameters->parameterExists("noHeader")){
		if(doLinearComb) myOutputFiles->writeHeader(myLinearComb);
		else myOutputFiles->writeHeader();
	}

	//for testing purposes, also make output files for each core
	/*
	testOutFiles=new TOutputVector*[numCores];
	for(int id=0; id<numCores; ++id){
		testOutFiles[id]=new TOutputVector("1", outName+"_core"+toString(id), myData, inputFiles, false, writeSeparateOutputFiles, forceOverWrite);
		testOutFiles[id]->writeHeader();
	}
	*/

	//perform simulations
	logFile->listFlush("Performing simulations ... (0%)");

	if(numCores==1) runLoop(0, nbSims, myOutputFiles);
	else {
		//split among several cores
		int step = ceil((float)nbSims / (float)numCores);
		#ifdef USE_OMP
		#pragma omp parallel for num_threads(numCores)
		#endif
		for(int id=0; id<numCores; ++id){
			runLoop(id*step, (id+1)*step, myOutputFiles, id);
		}
	}

	float runtime=(float) (clock()-starttime)/CLOCKS_PER_SEC/60;
	logFile->overList("Performing simulations ... done in ", runtime, " min!");
	// close the files
	delete myOutputFiles;
	/*
	for(int id=0; id<numCores; ++id){
		delete testOutFiles[id];
	}
	*/
};
//---------------------------------------------------------------------------
//--------------------------------------------------------------------------
void TStandardSim::runLoop(int start, int end, TOutputVector* outFiles, int paralellID){
	double distance=0;
	for (int s=start+1; s<(end+1); ++s){
		// create new input filed
		inputFilesVec[paralellID]->createNewInputFiles(s);
		for(int i=0; i<repeatsPerParameterVector; ++i){
			// perform simulations
			inputFilesVec[paralellID]->performSimulations(s);
			//calculate SumStats
			myDataVec[paralellID]->calculateSumStats(s);
			//write Simulation
			if(addDistanceToOutputfile) distance=calcDistance(myDataVec[paralellID]);
			if(doLinearComb) outFiles->writeSimulations(s, inputFilesVec[paralellID], myDataVec[paralellID], myLinearComb, distance);
			else outFiles->writeSimulations(s, inputFilesVec[paralellID], myDataVec[paralellID], distance);
			//testOutFiles[paralellID]->writeSimulations(s, inputFilesVec[paralellID], myDataVec[paralellID], distance);
			//report progress
			++overallCounter;
			progress=floor((100*(float)(overallCounter)/(float)totNumSims));
			if(progress>oldProgress){
				oldProgress=progress;
				logFile->listOverFlush("Performing simulations ... (", progress, "%)");
			}
		}
	}
}


