#ifndef TStandardSimH
#define TStandardSimH
//---------------------------------------------------------------------------

#include "TLog.h"
#include "TSim.h"
#include "TParameters.h"
#ifdef USE_OMP
#include "omp.h"
#endif

//---------------------------------------------------------------------------
class TStandardSim:public TSim{
   public:
	   int nbSims;
	   int totNumSims;
		double distance;
		int overallCounter;
		int progress;
		int oldProgress;
		int numCores;
		//TOutputVector** testOutFiles;
        virtual void runSimulations();
        void runLoop(int start, int end, TOutputVector* outFiles, int paralellID=0);

      TStandardSim(TParameters* gotParameters, std::string gotexedir, TLog* gotLogFile, TRandomGenerator* RandomGenerator);
      ~TStandardSim(){}

};
#endif
