//---------------------------------------------------------------------------

#ifndef TSuffStatBlockH
#define TSuffStatBlockH

#include <vector>
#include "TLinearComb.h"
#include "TLinearCombBoxCox.h"
#include "TDataVector.h"
#include "TSimDatabase.h"
#include "TInputFileVector.h"
#include "TLog.h"
#include "stringFunctions.h"
//---------------------------------------------------------------------------

class TSuffStatBlock{
	private:

	   std::vector<std::string>::iterator curPriorName;
	   double** startingValues;
	   TSimDatabase* caliData;
	   bool priorsInitialized;
	   bool startingValuesInitialized;
	   bool caliDistCalculated;
	   TPriorVector* priors;

	public:
	   std::vector<std::string> priorNames;
       TLinearComb* myLinearComb;
       std::string myLinearCombFile;
       bool linearCombCreated;
	   double myThreshold;
	   int* priorNumbers;
	   int numPrior;
	   int numStartingValues;
	   int numAccepted;
	   double weight, cumulWeight;

	   TSuffStatBlock(const std::string& filename, std::string & Weight, TPriorVector* Priors);
	   ~TSuffStatBlock(){
		   if(priorsInitialized)  delete[] priorNumbers;
		   if(startingValuesInitialized){
			   for(int i=0; i<numPrior; ++i){
				   delete[] startingValues[i];
			   }
			   delete[] startingValues;
		   }
		   if(linearCombCreated) delete myLinearComb;
	   }
	   void setCaliData(TSimDatabase* CaliData){caliData=CaliData;};
	   bool add(std::string & priorName);
	   void createLinearCombObject(const std::vector<std::string>& statNames, const std::vector<double>& obsData, bool boxcox);
	   std::vector<std::string> getPriorNames();
	   std::string getPriorNames(std::string delim);
	   bool checkPriorNameExistence(const std::string& name);
	   void initializePriorNumbers();
	   void calculateCaliDistances();
	   void setThreshold(float thresholdProportion);
	   void setThresholdFixed(float threshold);
	   void setMcmcRanges(float mcmcRangeProportion, TLog* logFile);
	   void setMcmcRangesFixed(float mcmcRange);
	   void setMcmcRangesRelativeToRange(float mcmcRange);
	   void fillStartingValues();
	   void fillStartingValuesRandom(int numValues);
	   void fillStartingValuesFixed();
	   void setStartingValues(double rand);
	   void updateParametersMcmc();
};
class TSuffStatBlockVector{
	private:
	  void begin();
	  void next();

	public:
	  std::vector<TSuffStatBlock*> suffStatBlocks;
	  std::vector<TSuffStatBlock*>::iterator curSuffStatBlock;
	  bool iterationStarted;
      TRandomGenerator* randomGenerator;
      TLog* logFile;
      bool doBoxCox;
      double oneOverNumBlocks;
      bool blocksHaveEqualWeight;

	  TSuffStatBlockVector(std::string filename, bool doBoxCox, TDataVector* myData, TPriorVector* Priors, TRandomGenerator* RandomGenerator, TLog* gotLogFile);
	  ~TSuffStatBlockVector(){
		  for(curSuffStatBlock=suffStatBlocks.begin(); curSuffStatBlock!=suffStatBlocks.end();++curSuffStatBlock)
		  	 delete *curSuffStatBlock;
	  };
	  void createLinearCombObjects(TDataVector* myData);
	  void setThresholdAndMcmcRanges(TSimDatabase* caliData, std::vector<double> & thresholds, bool thresholdFixed, std::vector<double> & mcmcRanges, bool rangesFixed, bool startingPointGiven);
	  void setStartingValues();
	  void setBestStartingValues();
	  void chooseBlock();
	  void createNewInputFiles(TInputFileVector* inputFiles, int SimNum);
	  double calculateDistance(TDataVector* dataVector);
	  double getThreshold();
	  void updateWasAccepted();
	  bool allMoved();
	  unsigned int getNumBlocks(){ return suffStatBlocks.size(); };
};


#endif
