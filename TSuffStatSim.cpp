//------------------------------------------------------------------------------

#include "TSuffStatSim.h"
//------------------------------------------------------------------------------
TSuffStatSim::TSuffStatSim(TParameters* GotParameters, std::string gotexedir, TLog* gotLogFile, TRandomGenerator* RandomGenerator):TMcmcSim(GotParameters, gotexedir, gotLogFile, RandomGenerator){
	//read settings
	if(writeSeparateOutputFiles) throw "Cannot perform a sufficient-statistics-run if several output files are written!!";
    calibrationFile=gotParameters->getParameterString("calName", 0);

	// TODO: multiple parameter vectors
	if(repeatsPerParameterVector>1) throw "Multiple runs per parameter vector not implemented!!!";

	//linear transformation to statistics: read from specific file
	logFile->startIndent("Creating sufficient statistics blocks:");
	if(doLinearComb) throw "Tag 'linearCombName' can not be used in combination with a sufficient statistics run!";
	suffStatFileName=gotParameters->getParameterString("suffStatBlocksFile");
	doBoxCox = gotParameters->parameterExists("doBoxCox");
	myBlocks=new TSuffStatBlockVector(suffStatFileName, doBoxCox, myData, inputFiles->priors, randomGenerator, logFile);
	logFile->endIndent();
}
//---------------------------------------------------------------------------
void TSuffStatSim::initialCalibration(){
	logFile->startIndent("Calibration:");
	myCaliData=new TSimDatabase(0, myData, inputFiles, logFile);
	std::vector<double> thresholds;
	std::vector<double> ranges;
	bool thresholdFixed=false;
	bool rangesFixed=false;
	bool needCalibrationSims=false;
	//set threshold
	if(gotParameters->parameterExists("threshold")){
		//allow either one or one per parameter
		fillVectorFromStringAny(gotParameters->getParameterString("threshold"), thresholds, "#");
		if(thresholds.size()==1){
			logFile->list("A fixed threshold (distance of acceptance) is set to ", thresholds[0], " for all blocks");
			for(unsigned int i=1; i<myBlocks->getNumBlocks(); ++i){
				thresholds.push_back(thresholds[0]);
			}
		} else {
			if(thresholds.size()!=myBlocks->getNumBlocks()) throw "Number of provided thresholds does not match number of blocks!";
			logFile->list("An individual but fixed threshold (distance of acceptance) is set for each block");
		}
		thresholdFixed=true;
	} else {
		needCalibrationSims=true;
		threshold=gotParameters->getParameterDouble("thresholdProp");
		for(unsigned int i=0; i<myBlocks->getNumBlocks(); ++i){
			thresholds.push_back(threshold);
		}
		logFile->list("Threshold (distance of acceptance) is set such that ", threshold, " of all calibration simulations are below.");
	}
	//set range
	if(gotParameters->parameterExists("range")){
		//allow either one or one per parameter
		fillVectorFromStringAny(gotParameters->getParameterString("range"), ranges, "#");
		if(ranges.size()==1){
			logFile->list("A fixed range (width of proposal) is set to ", ranges[0], " for all blocks");
			for(unsigned int i=1; i<myBlocks->getNumBlocks(); ++i){
				ranges.push_back(ranges[0]);
			}
		} else {
			if(ranges.size()!=myBlocks->getNumBlocks()) throw "Number of provided thresholds does not match number of blocks!";
			logFile->list("An individual but fixed range (width of proposal) is set for each block");
		}
		rangesFixed=true;
	} else {
		needCalibrationSims=true;
		float mcmc_range=gotParameters->getParameterDouble("rangeProp");
		for(unsigned int i=0; i<myBlocks->getNumBlocks(); ++i){
			ranges.push_back(mcmc_range);
		}
		logFile->list("Proposal range set to ", mcmc_range, " of the standard deviation among simulations below threshold.");
	}
	//starting point
	std::string startingPoint=gotParameters->getParameterStringWithDefault("startingPoint", "random");
	if(startingPoint=="random" || startingPoint=="best") needCalibrationSims=true;
	else {
		if(startingPoint.find_first_of('#')!=std::string::npos)
			startingPointFixed=true;
		else throw "Starting point tag '"+startingPoint+"' unknown!";
	}

	//now set!
	if(needCalibrationSims) fillCalibrationSims();
	logFile->startNumbering("Setting threshold, proposal ranges and starting point for each block:");
	if(startingPointFixed) myCaliData->setPriorStartingConditions(startingPoint);
	myBlocks->setThresholdAndMcmcRanges(myCaliData, thresholds, thresholdFixed, ranges, rangesFixed, startingPointFixed);
	if(!startingPointFixed){
		if(startingPoint=="best") myBlocks->setBestStartingValues();
		else myBlocks->setStartingValues();
	}
	logFile->endNumbering();
	logOldPriorDensity = inputFiles->getLogPriorDensity();
	logFile->endIndent();
}

//---------------------------------------------------------------------------
void TSuffStatSim::performMcmcSimulation(int simNum){
	myBlocks->chooseBlock();
	myBlocks->createNewInputFiles(inputFiles,simNum);

	if((*myBlocks->curSuffStatBlock)->linearCombCreated){
		performSimulation(simNum);
		distance = myBlocks->calculateDistance(myData);
	} else distance = 0;

	//check acceptance
	if(distance <= myBlocks->getThreshold()){
		if(calculcateH()){
			++nbSimulationsAccepted;
			inputFiles->priors->saveOldValues();
			myData->saveOldValues();
			myBlocks->updateWasAccepted();
			logOldPriorDensity = logPriorDensity;
		} else {
			inputFiles->priors->resetOldValues();
			myData->resetOldValues();
		}
	} else {
		inputFiles->priors->resetOldValues();
		myData->resetOldValues();
		++nbSimulationsLargerThreshold;
	}
}
//---------------------------------------------------------------------------
void TSuffStatSim::writeSimulations(int & s){
	if(addDistanceToOutputfile){
		myOutputFiles->writeSimulations(s, distance);
	} else {
		myOutputFiles->writeSimulations(s);
	}
}
//---------------------------------------------------------------------------
void TSuffStatSim::runSimulations(){
	logFile->startIndent("Performing an 'sufficient statistics' chain:");
	 runMcmcChain();
	 logFile->endIndent();
	 writeDensityBins("end");
};
//---------------------------------------------------------------------------
bool TSuffStatSim::startupPassed(){
	//check if all blocks moved
	return myBlocks->allMoved();
}

//---------------------------------------------------------------------------
void TSuffStatSim::resetStartingPoints(){
	myBlocks->setStartingValues();
	logOldPriorDensity = inputFiles->getLogPriorDensity();
}

//---------------------------------------------------------------------------
