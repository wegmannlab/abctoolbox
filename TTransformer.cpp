
#include "TTransformer.h"

TTransformer::TTransformer(TParameters* gotParameters, TLog* gotLogFile){
	//read paraneters
	logFile=gotLogFile;
	LinearComb_fileName=gotParameters->getParameterString("linearComb");
	inputFileName=gotParameters->getParameterString("input");
	outputFileName=gotParameters->getParameterString("output");
	if(gotParameters->parameterExists("numLinearComb")){
		numLinearComb=gotParameters->getParameterInt("numLinearComb");
		logFile->list("Limiting transformation to the first "+toString(numLinearComb)+" linear combinations.");
	}
	else numLinearComb=-1;
	boxcox=false;
	if(gotParameters->parameterExists("boxcox") && gotParameters->getParameterString("boxcox", false)!="0") boxcox=true;
}

void TTransformer::transform(){
	int numParams, numValuesPerLine;

	//open file to perfom the linear combinations on
	logFile->listFlush("Reading header of input file '"+inputFileName+"'...");
	std::ifstream inputfile(inputFileName.c_str());
	if(!inputfile) throw "The input-File '" + inputFileName + "' could not be read!";

	//read header line with the names
	std::vector<std::string> InputFileNamesVector;
	fillVectorFromLineWhiteSpaceSkipEmpty(inputfile, InputFileNamesVector);
	numValuesPerLine=InputFileNamesVector.size();
	logFile->write(" done!");
	logFile->conclude("Read a total of "+toString(numValuesPerLine)+" columns.");

	//read Linear-Combination File
	logFile->listFlush("Reading linear combination file '"+LinearComb_fileName+"'...");
	TLinearComb* myLC;
	if(boxcox) myLC = new TLinearCombBoxCox(LinearComb_fileName, InputFileNamesVector, numLinearComb);
	else myLC = new TLinearComb(LinearComb_fileName, InputFileNamesVector, numLinearComb);

	//get the number of columns of parameters
	numParams=myLC->getNumberOfFirstUsedStat();
	logFile->write(" done!");
	logFile->conclude("Read a total of "+toString(myLC->numLinearComb)+" columns.");
	logFile->conclude("The first "+toString(numParams)+" columns of the input file are parameters and will not be transformed.");


	//check stats
	std::string missingStat=myLC->getFirstMissingStat();
	if(!missingStat.empty()) throw "The statistics '" + missingStat +"' is missing in the input file but requested by a linear combination!";

	//create Output-File
	logFile->listFlush("Opening output file '"+outputFileName+"'...");
	std::ofstream outputfile(outputFileName.c_str());
	if(!outputfile) throw "The output-File '" + outputFileName + "' can not be created!";

	//first write the header line: parameters and then the stats
	for(int i=0; i<numParams; ++i){
		if(i>0) outputfile << "\t";
		outputfile << InputFileNamesVector[i];
	}
	myLC->writeHeader(outputfile);
	outputfile << std::endl;
	logFile->write(" done!");

	//now go through the input file an copy the lines
	logFile->listFlush("Parsing input file and transforming statistics ...");
	std::vector<double> data;
	std::vector<double>::iterator it;
	long lineIterator=1;
	long copiedLines=0;
	while(inputfile.good() && !inputfile.eof()){
		++lineIterator;
		fillVectorFromLineWhiteSpaceSkipEmpty(inputfile, data);
		//skip empty lines
		if(data.size()>0){
			if(data.size()!=InputFileNamesVector.size()){
				logFile->write("Found " + toString(data.size()) + " instead of " + toString(InputFileNamesVector.size()) + " columns on line " + toString(lineIterator) + ". This has been read on that line:");
				std::string x;
				bool first=true;
				for(it=data.begin(); it!=data.end(); ++it){
					if(first) first=false;
					else x+="\t";
					x+=toString(*it);
				}
				logFile->write(x);
				throw "Unequal number of columns in header line and Line " + toString(lineIterator) + "!";
			}

			//write parameters to outputfile -> allow for no parameters!
			for(int i=0; i<numParams; ++i){
				outputfile << data[i];
				if(i<numParams-1) outputfile << "\t";
			}

			//calculate and write Linear Combinations
			myLC->calcSimDataPCA(data);
			myLC->writeSimPCA(outputfile);
			outputfile << std::endl;
			++copiedLines;
		}
	}
	logFile->write(" done!");
	logFile->conclude("Sucessfully parsed "+toString(copiedLines)+" lines.");
}
