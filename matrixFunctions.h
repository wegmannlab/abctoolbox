/*
 * matrixfunctions.h
 *
 *  Created on: May 10, 2012
 *      Author: wegmannd
 */

#ifndef MATRIXFUNCTIONS_H_
#define MATRIXFUNCTIONS_H_

#include <stdlib.h>
#include <iostream>
#include <iostream>
#include <iomanip>
#include "newmatio.h"
#include "newmat.h"
#include "newmatap.h"

void makeSymmetric(Matrix & m);
void makeInvertable(SymmetricMatrix & m);
void fillInvertable(Matrix & m, SymmetricMatrix & m_invertable);
void invertMatrix(Matrix & m, SymmetricMatrix & inverted);
void invertMatrix(SymmetricMatrix m, SymmetricMatrix & inverted);

#endif /* MATRIXFUNCTIONS_H_ */
